<?php

    return [
        'country'=>[
            'MAL'=>'Malaysia'
        ],
        'state'=>[
            'KUL'=>[
                'slug'=>'KUL',
                'name'=>'Kuala Lumpur',
                'code'=>''
            ],
            'LBN'=>[
                'slug'=>'LBN',
                'name'=>'Labuan',
            ],
            'PJY'=>[
                'slug'=>'PJY',
                'name'=>'Putrajaya',
            ],
            'JHR'=>[
                'slug'=>'JHR',
                'name'=>'Johor',
            ],
            'KDH'=>[
                'slug'=>'KDH',
                'name'=>'Kedah',
            ],
            'KTN'=>[
                'slug'=>'KTN',
                'name'=>'Kelantan'
            ],
            'MLK'=>[
                'slug'=>'MLK',
                'name'=>'Malacca',
            ],
            'NSN'=>[
                'slug'=>'NSN',
                'name'=>'Negeri Sembilan'
            ],
            'PHG'=>[
                'slug'=>'PHG',
                'name'=>'Pahang'
            ],
            'PRK'=>[
                'slug'=>'PRK',
                'name'=>'Perak'
            ],
            'PLS'=>[
                'slug'=>'PLS',
                'name'=>'Perlis'
            ],
            'PNG'=>[
                'slug'=>'PNG',
                'name'=>'Penang'
            ],
            'SBH'=>[
                'slug'=>'SBH',
                'name'=>'Sabah'
            ],
            'SWK'=>[
                'slug'=>'SWK',
                'name'=>'Sarawak'
            ],
            'SGR'=>[
                'slug'=>'SGR',
                'name'=>'Selangor'
            ],
            'TRG'=>[
                'slug'=>'TRG',
                'name'=>'Terengganu'
            ],
        ],

    ];



?>