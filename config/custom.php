<?php

return[
    'wb' =>  [
        'officer'=>[
            'fallback_email_detail'=>[
                'receiver_name'=>'Siti',
                'receiver_email'=>'siti@hrdcorp.gov.my'
            ],
            'setting'=>[
                'page'=>[
                    'count'=> 10,
                ],
            ],
            'type'=>[
                1=>[
                    'slug'=>1,
                    'name'=>'Chairman of HRD Corp'
                ],
                2=>[
                    'slug'=>2,
                    'name'=>'Board of Director'
                ],  
                3=>[
                    'slug'=>3,
                    'name'=>'Chief Executive'
                ],
                4=>[
                    'slug'=>4,
                    'name'=>'All employees Below CEO'
                ],
                5=>[
                    'slug'=>5,
                    'name'=>'External Parties'
                ]
            ],
            'disclosure'=>[
                1 => [
                    'slug'=>1,
                    'name'=>'disclosureagainstchairman',
                    'reminder'=>'dischairmanreminder',
                    'user_type'=>[1],
                    'disclosure_email'=>[],
                    'email_group_to_send'=>["sitifatimah@hrdcorp.gov.my","shahul@hrdcorp.gov.my"],
                ],
                2 => [
                    'slug'=>2,
                    'name'=>'disclosureagainstboardofdirect',
                    'reminder'=>'disboardofdirectreminder',
                    'user_type'=>[2],
                    'disclosure_email'=>[],
                    'email_group_to_send'=>["sitifatimah@hrdcorp.gov.my"],
                ],
                3 => [
                    'slug'=>3,
                    'name' =>'disclosureagainstchiefexecutive',
                    'reminder'=>'dischiefexecutivereminder',
                    'user_type'=>[3],
                    'disclosure_email'=>[],
                    'email_group_to_send'=>["sitifatimah@hrdcorp.gov.my"],
                ],
                4 => [
                    'slug'=>4,
                    'name'=>'disclosureagainstallemployeebelowceo',
                    'reminder'=>'disemployeebelowceoreminder',
                    'user_type'=>[2,4],
                    'disclosure_email'=>[],
                    'email_group_to_send'=>["sitifatimah@hrdcorp.gov.my"],
                ],
                5 => [
                    'slug'=> 5,
                    'name' =>'disclosureagainstexternalparties',
                    'reminder'=>'disexternalpartiesreminder',
                    'user_type'=>[5],
                    'disclosure_email'=>[],
                    'email_group_to_send'=>["sitifatimah@hrdcorp.gov.my"],
                ]
            ],
            'status'=>[
                0 => [
                    'slug'=>'0',
                    'name'=>'pending',
                    'name_email_bi'=>'Case Receive',
                    'name_email_bm'=>'Penerimaan Kes',
                ],
                1 => [
                    'slug'=>'1',
                    'name'=>'approved',
                    'name_email_bi'=>'Whistleblower Committee',
                    'name_email_bm'=>'Jawatankuasa Whistleblower',
                ],
                2 => [
                    'slug'=>'2',
                    'name'=>'reject',
                    'name_email_bi'=>'Investigation',
                    'name_email_bm'=>'Siasatan',
                ],
                3 => [
                    'slug'=>'3',
                    'name'=>'delete',
                    'name_email_bi'=>'Case Close',
                    'name_email_bm'=>'Siasatan Selesai',

                ],
            ],
            'statusinterest'=>[
                1 => [
                    'slug'=>'0',
                    'name'=>'pending',
                    'name_email_bi'=>'Case Receive',
                    'name_email_bm'=>'Penerimaan Kes',
                ],
                1 => [
                    'slug'=>'1',
                    'name'=>'approved',
                    'name_email_bi'=>'Case Receive',
                    'name_email_bm'=>'Penerimaan Kes',
                ],
                2 => [
                    'slug'=>'2',
                    'name'=>'reject',
                    'name_email_bi'=>'Case Receive',
                    'name_email_bm'=>'Penerimaan Kes',
                ],
                
            ],
            'statustrainingscheme'=>[
                1=>[
                    'slug'=>'1',
                    'name'=>'HRD Corp Claimable Course',
                    'value'=>'HRD Corp Claimable Course'
                ],
                2=>[
                    'slug'=>'2',
                    'name'=>'e-LATiH',
                    'value'=>'e-LATiH'
                ]

            ],
            'typeoftrainingcertificate'=>[
                1=>[
                    'slug'=>'1',
                    'name'=>'Attendance/Completion',
                    'value'=>'Attendance/Completion'
                ],
                2=>[
                    'slug'=>'2',
                    'name'=>'Professional Certification',
                    'value'=>'Professional Certification'
                ],

            ]
       
        ],
        'user'=>[
            'fallback_email_detail'=>[
                'receiver_name'=>'James Lee',
            ],
        ],
    
    ],
    
];






?>