<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title Page-->
    <!-- <title>Whistle Blower</title> -->

    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
    @include('layouts.partials._locale')
    @extends('layouts.master-public')
    @section('content')
    <body class="bg">
        <div class="container-wrapper"> 
            <!-- <div class="container text-center py-5 bg">
                <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="" height="150px" style="padding-top:30px; padding-bottom:30px">
            </div> -->
            @include('layouts.partials._title',['type'=>'public'])
            <div class="container login-container">

                @if (Session::has('resetpasswordsuccess')) 
                <div class="alert alert-success">
                    <ul>
                        <li>{{ Session::get('resetpasswordsuccess') }}</li>
                        <li>{{__('messages.resetpasswordsuccessmsg')}} <a href="{{route('login')}}" target="_blank">{{__('form.login')}}</a></li>
                    </ul>
                </div>
                @endif 
                <div class="card">
                    <div class="card-header text-center font-weight-bold">
                    {{__('form.resetpassword')}}
                    </div>
                    <div class="card-body">
                        <!-- <div class="alert alert-success" role="alert">
                        A simple success alert—check it out!
                        </div> -->
                        @error('msg')
                        <div class="alert alert-danger" role="alert">
                        {{$message}}
                        </div>
                        @enderror
                        <form id="form_login" action="{{ route('processResetPassword')}}" method="post">
                        {{ csrf_field() }}
                            <div class="mb-3 row">
                                <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.currentemail')}}</label>
                                <div class="col-sm-9">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.newpassword')}}</label>
                                <div class="col-sm-9">
                                <input type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" value="{{ old('new_password') }}" required autocomplete="new_password" autofocus>
                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Confirm Password</label>
                                <div class="col-sm-9">
                                <input type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" value="{{ old('confirm_password') }}" required autocomplete="confirm_password" autofocus>
                                @error('confirm_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{$id}}" />
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.reset')}}</button>
                            </div>
                          
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    @endsection
    {{--@include('layouts.partials._footer')--}}
</html>
