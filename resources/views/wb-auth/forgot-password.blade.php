<!DOCTYPE html>
<html lang="en">

<head>
 
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>

    @include('layouts.partials._locale')
    @extends('layouts.master-public',['type'=>'public'])
    @section('content')
    
    <body class="bg">
        <div class="container-wrapper"> 
            <!-- <div class="container text-center py-5 bg">
                <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="" height="150px" style="padding-top:30px; padding-bottom:30px">
            </div> -->
            @include('layouts.partials._title',['type'=>'public'])
            <div class="container login-container">
               @if (Session::has('resetsuccess')) 
                <div class="alert alert-success">
                    <ul>
                        <li>{{ Session::get('resetsuccess') }}</li>
                    </ul>
                </div>
                @endif 
                <div class="card">
                    <div class="card-header text-center font-weight-bold">
                    {{__('form.forgotpassword')}}
                    </div>
                    <div class="card-body">
                        <form id="form_login" action="{{ route('processForgotPassword')}}" method="post">
                        {{ csrf_field() }}
                            <div class="mb-3 row">
                                <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.email')}}</label>
                                <div class="col-sm-9">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <!-- <span class="invalid-feedback" role="alert"> -->
                                    <span style="color:red">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.submit')}}</button>
                            </div>
                          </form>
                    </div>
                </div>
            </div>
        </div> 
    </body>
    @endsection
    {{--@include('layouts.partials._footer')--}}
</html>
