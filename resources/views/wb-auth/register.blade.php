<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
</head>

<style>
    .login-container{
        /* padding: 3rem 0; */
        display: flex;
        flex-direction: column;
        margin: auto;
        /* margin-top : 8rem; */
    }    
</style>

@php
$user_types = config('custom.wb.officer.type');
@endphp
    @include('layouts.partials._locale')
    @extends('layouts.master-public',['type'=>'public'])
    @section('content')

    <body class="bg-officer">
        <!-- <div class="bg"> -->
           {{-- <div class="container text-center py-5">
                <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="" height="150px" style="padding-top:30px; padding-bottom:30px">
            </div> --}}
            <div class="container-wrapper">
                @include('layouts.partials._title',['type'=>'public'])
                <div class="container login-container"> 
                    @if (Session::has('registersuccess')) 
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ Session::get('registersuccess') }}</li>
                        </ul>
                    </div>
                    @endif

                    <div class="card">
                        <div class="card-header text-center font-weight-bold">
                        {{__('form.register')}}
                        </div>
                        <div class="card-body">
                            <form id="form_login" action="{{ route('processRegister')}}" method="post">
                            {{ csrf_field() }}
                                <div class="mb-3 row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.registerusername')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="username" name="username" value="" required>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.registerpassword')}}</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="password" name="password" value="" required>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="email" class="col-sm-3 col-form-label">{{__('form.registeremail')}}</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email" name="email" value="" required>
                                    </div>
                                </div>
    
                                <div class="mb-3 row">
                                    <label for="fullname" class="col-sm-3 col-form-label">{{__('form.registerfullname')}}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="fullname" name="fullname" required>
                                    </div>
                                </div>

                                <div class="mb-3 row">
                                    <label for="fullname" class="col-sm-3 col-form-label">Reference Id </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="refid" name="refid" required>
                                    </div>
                                </div>

                                <div class="mb-3 row">
                                    <label for="type" class="col-sm-3 col-form-label">Type</label>
                                    <div class="col-sm-9">
                                        <select id="type" class="form-select" name="type" aria-label="Default select example">
                                            <option value="tp">Training Provider</option>
                                            <option value="admin">Admin</option>
                                            <option value="superadmin">Super Admin</option>
                                            <option value="employer">Employer</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="mb-3 row">
                                    <label for="role" class="col-sm-3 col-form-label">Role</label>
                                    <div class="col-sm-9">
                                        <select id="role" name="role" class="form-select" aria-label="Default select example">
                                            <option value="tp">Training Provider</option>
                                            <option value="admin">Admin</option>
                                            <option value="superadmin">Super Admin</option>
                                            <option value="employer">Employer</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.register')}}</button>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                   
                </div>
            </div>
        <!-- </div> -->
    </body>
    @endsection
   {{-- @include('layouts.partials._footer') --}}
</html>
