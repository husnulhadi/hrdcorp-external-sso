<!DOCTYPE html>
<html lang="en">
<style>


.btn-tq{
    font-size: 20px;
    margin: 1rem 0;
    padding: 0.5rem 0.75rem;
    border-radius: .25rem;
    color: #ffffff;
}
.tq-text{
    font-size :20px;
}

.tq-text-bold{
    font-size:20px;
}

@media screen and (max-width: 750px) {
    .tq-text,.btn-tq, .tq-text-bold{
        font-size : 16px;
    }
}


</style>
<head>
    <!-- Title Page-->
    <!-- <title>HRD Corp Whistleblower</title> -->

    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
    @extends('layouts.master-public')
    @section('content')
    @include('layouts.partials._locale')
    <body class="bg">
        <div class="container-wrapper">
            <div class="">
                <!-- <div class="container">
                    <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
                </div> -->
                <div class="container text-center" style="padding:10rem 0;">
                    <h2 class="tq-text">{{ __('messages.thankyou') }}</h2>
                    <!-- <h2 class="tq-text">Here is the Case Id <b class="tq-text-bold" >{{ urldecode(base64_decode(Request::get('caseId')))}}</b> for your reference.</h2> -->
                    <h2 class="tq-text">{{__('messages.thankyoureferencecaseidtext1')}} <b class="tq-text-bold" >{{ $caseId }}</b> {{__('messages.thankyoureferencecaseidtext2')}}</h2>
                    <h2 class="tq-text">{{__('messages.thankyouchecksubmission')}} </h2>
                    <h2 class="tq-text"><a href="{{$searchUrlWithParam}}" target="_blank"><button type="button"  class="btn-hrd-theme btn-tq">{{__('messages.checkmysubssionbtn')}}</button></a></h2>
                </div>
            </div>
        </div>
    </body>
    <!-- <script>
             var url = "{{ route('thankYou', ['caseId'=>':id']) }}";
                url = url.replace(':id', 'V0IxNjkyNzk4NTcyMTk%3D');
                console.log(url);

    </script> -->
    @endsection
</html>
