
@php 


$statusList = config('custom.wb.officer.status');
$caseStatus = 1;
$officerInfo = !empty($user) ? $user : [];
$isAssignedOfficer = !empty($officerInfo) ?  true : false; 
$isDisabled = true;

@endphp

<style>
.suspect-individual-label{
    font-style : italic;
}

.file-wrapper{
    display:flex;
    column-gap : 0.5rem;
    padding : 0.5rem 0;
}

.file-upload-instruction-wrapper{
        /* display : flex; */
}

.file-upload-instruction-group{
    display : flex;
}

.file-upload-instruction{
    /* font-style : italic; */
    font-size : 12px; 
    font-weight : bold;
}

.file-upload-ins-sub{
    font-size : 12px; 
}
.suspect-container{
    height: 300px;
    overflow: auto;
}

.collapse{
    visibility : visible;
}

.btn-wrapper{
    display : flex;
    column-gap : 0.2rem;
}

.acc-container{
    display :flex;
    column-gap: 1rem;
    padding: 1rem 0;
}

.custom-width-setting{
    width : 50%;
}

.modal-dialog.modal-dialog-scrollable.modal-dialog-centered{
    min-width : 90%;
}


.complaint-country-container, .complaint-state-container{
    display : flex;
    column-gap : 0.5rem;
}

.half-width {
    width : 50%;
}

.full-width{
    width : 100%;
}

.col-3-width{
    width : 33%;
}

.complaint-info{
    
}

.old{
    height:200px;
    overflow:auto;"
}

.input-error{
    border : 1px solid #FF0000 !important;
}

.error-message{
    color : #FF0000;
}

.input-error:focus{
    box-shadow : 0 0 0 0.25rem rgba(255, 0, 0,.25) !important;
}

.loader,.error{
    display: flex;
    justify-content: center;
    padding: 1rem 0;
}

.disclosure-email-container{
    height: 60px;
    overflow: auto;
}

 /* .activity-thead{
    display : table;
    width : 100%;
}

.activity-tbody{
    width : 100%;
    display : block;
    height : 300px;
    overflow-x : none;
    overflow-y : auto;
} 

thead, tbody tr {
    display:table;
    width:100%;
    table-layout:fixed;
} */

@media only screen and (max-width: 750px) {
    .acc-container{
        display : block;
        column-gap: 0;
        padding: 1rem 0;
    }
    .custom-width-setting{
        width : 100%;
    }

    .file-upload-instruction-wrapper, .file-upload-instruction-group{
            display : block;
    }

    .complaint-country-container, .complaint-state-container{
        display : block;
    }

    .half-width {
        width : 100%;
    }

    .col-3-width{
        width : 100%;
    }

}


.modal{
    color : #000;
}
</style>


<div class="modal fade" id="modalInterest" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="modalInterestLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                {{--<h5 class="modal-title" id="modalInterestLabel">{{__('form.case')}} : <span id="caseIdText"></span> </h5>--}}
                <!-- <h5>Employer Detail</h5> -->
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"> 
                <form class="form" name="interest-form" id="interest-form" method="post"  novalidate="">
                    {{ csrf_field() }}
                    
                    <input type="hidden" id="course_id" name="course_id" value=""> 
                    <input type="hidden" id="course_tp_mycoid" name="course_tp_mycoid" value=""> 
                    <input type="hidden" id="course_name" name="course_name" value=""> 
                    <div class="accordion" id="accordionPanelsStayOpenExample">

                        <div class="accordion-item " id="secretarial_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingSix">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="true" aria-controls="panelsStayOpen-collapseSix">
                                <b>{{__('form.employer_detail_title')}}</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseSix" aria-labelledby="panelsStayOpen-headingSix">
                                <div class="accordion-body">       
                                    <div>
                                        <div class="mb-3">
                                            <label for="employer_mycoid">MYCOID<span class="text-danger">*</span></label>
                                            <input type="text" id="employer_mycoid" name="employer_mycoid" class="form-control" required>
                                            <div class="error-message hide" id="err-remark">
                                                Please select a valid state.
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <label for="employer_name">{{__('form.employer_name')}}<span class="text-danger">*</span></label>
                                            <input type="text" id="employer_name" name="employer_name" class="form-control" required>
                                            <div class="error-message hide" id="err-remark">
                                                Please select a valid state.
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <label for="employer_contact">{{__('form.employer_contact')}}<span class="text-danger">*</span></label>
                                            <input type="text" id="employer_contact" name="employer_contact" class="form-control" required>
                                            <div class="error-message hide" id="err-remark">
                                                Please select a valid state.
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <label for="employer_email">{{__('form.employer_email')}}<span class="text-danger">*</span></label>
                                            <input type="text" id="employer_email" name="employer_email" class="form-control" required>
                                            <div class="error-message hide" id="err-remark">
                                                Please select a valid state.
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <label for="employer_business_forte">{{__('form.employer_business_forte')}}<span class="text-danger">*</span></label>
                                            <input type="text" id="employer_business_forte" name="employer_business_forte" class="form-control" required>
                                            <div class="error-message hide" id="err-remark">
                                                Please select a valid state.
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <label for="employer_no_of_enrollment">{{__('form.employer_enrollment')}}<span class="text-danger">*</span></label>
                                            <input type="number" min="1" id="employer_no_of_enrollment" name="employer_no_of_enrollment" class="form-control" value="1" required>
                                            <div class="error-message hide" id="err-remark">
                                                Please select a valid state.
                                            </div>
                                        </div>



                                        {{--<div class="mb-3">
                                            <div id="field-container-doc">
                                                <label for="remark">{{__('form.supportingdocumentinput')}}</label>
                                                <div class="form-field">
                                                    <div class="file-upload-instruction-wrapper">
                                                        <!-- <span class="text-danger">*</span> -->
                                                        <div class="file-upload-instruction-group">
                                                            <span class="file-upload-instruction">{{__('form.particularofimproperconductdocumentinstrcution')}}</span>
                                                            <span class="file-upload-ins-sub ">/ {{__('form.particularofimproperconductdocumentinstrcution2')}}</span>
                                                            <span class="text-danger">*</span>
                                                        </div>
                                                        <div class="file-upload-instruction-group">
                                                            <span class="file-upload-instruction">{{__('form.particularofimproperconductdocumentinstrcution3')}}</span>
                                                            <span class="file-upload-ins-sub">/ {{__('form.particularofimproperconductdocumentinstrcution4')}}</span>
                                                            <span class="text-danger">*</span>
                                                        </div>
                                                       
                                                    </div>
                                                    <div class="file-container" id="file-container" style="padding-bottom:10px">
                                                        <div class="file-wrapper">
                                                            <input type="file" class="form-control supporting_doc" id="supporting_doc_0" onchange="checkUpload('supporting_doc_0')" name="supporting_docs[]" >
                                                            <button type="button" class="btn btn-primary btn-hrd-theme" onClick="appendFileInput()">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--}}

                                    </div>
                                </div>
                            </div>
                        </div> 

                    </div>
                </form>

                {{--<div id="loader" class="loader">
                    <div class="spinner-border" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>--}}

                {{--<div id="error" class="error hide">
                    {{__('form.somethingwentwrong')}}
                </div>--}}
                <!-- <div id="spinner-interest" style="display:flex;justify-content:center;">
                <div class="spinner-border text-primary" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
                </div> -->
            </div>

            <div class="modal-footer">
               
                <div class="btn-wrapper btn-align-center">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('form.close')}}</button>
                    {{--<form name="whistle-blower-personal-print-form" id="whistle-blower-personal-print-form" method="get" action="{{route('createPDFPersonal')}}">
                        <input type="hidden" name="printProfileId" id="printProfileId" value=""/>
                        <button id="print" type="submit" onclick="" class="btn btn-primary btn-hrd-theme">Print Pdf</button>
                    </form>--}}
                   {{-- <button id="assign" type="submit" onclick="assign()" class="btn btn-primary btn-hrd-theme">{{__('form.assign')}}</button>--}}
                    <button id="interest-update" type="button" onclick="interest()" class="btn btn-primary btn-hrd-theme">{{__('form.interest')}}</button>
                    <button id="interest-loading" type="button" onclick="interest()" class="btn btn-primary btn-hrd-theme hide"> 
                        <div class="spinner-border text-primary" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </button>

                   
                </div>
            </div>
        </div>
    </div>
</div> 

<script>
    
    function openInterestModal(id, data){
        // console.log('open interest')
        // console.log(data)
        const {courseId = '', courseName = "", tpcoid = "" } = data;
        //console.log(courseId);
        $('#course_id').val(id);
        $('#course_name').val(courseName);
        $('#course_tp_mycoid').val(tpcoid);
       
    }

    function loadingInterest(){
        $('#interest-update').addClass('hide');
        $('#interest-loading').removeClass('hide');
    }

    function afterLoadInterest(){
        $('#interest-update').removeClass('hide');
        $('#interest-loading').addClass('hide');
    }
       
    function interest(){
        const form = document.getElementById('interest-form');
        var formData = new FormData(form);
        let id = $('#course_id').val();
        let courseName = $('#course_name').val();
        let courseTpMycoid = $('#course_tp_mycoid').val();
        formData.append("_token" ,'{{ csrf_token() }}');
        formData.append("id", id);
        formData.append("course_name", courseName);
        formData.append("course_tp_mycoid", courseTpMycoid);
        formData.append("applicant_type", 1);   
        formData.append("current_status", "1"); 


        let totalRequired = $('form#interest-form input[required]').length;
        //let totalRequired = $('[required]').length;
        //document.getElementById(form).querySelectorAll("[required]")
        let validCount = 0; let invalidCount = 0;
        $('[required]').each(function() {
            console.log($(this).attr('id'))
            if ($(this).is(':invalid') || !$(this).val()){
                valid = false;
                invalidCount++;
            }else{
                validCount++;      
            } 
        })

        // console.log('interest')
        // console.log(validCount)
        // console.log(totalRequired)

        if(validCount == totalRequired){
            Swal.fire({
                title: '{{__("form.confirmupdate")}}?',
                showCancelButton: true,
                confirmButtonText: '{{__("form.confirm")}}',
                denyButtonText: '{{__("form.cancel")}}',
                confirmButtonColor: '#002169',
                cancelButtonColor: '#6c757d',
            }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    loadingInterest();

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processInterest')}}",
                        //contentType: 'multipart/form-data',
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                        data : formData,
                        success: function(response){
                            afterLoadInterest();
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: '{{__("form.statusupdated")}}!',
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#002169',
                                    cancelButtonColor: '#002169',
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {        
                                        location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            window.setTimeout(function () {
                                afterLoadInterest();
                                Swal.fire({
                                    title :'{{__("form.unsuccessful")}}!', 
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    confirmButtonColor: '#002169',
                                    icon: 'error',
                                });

                            }, 2000);
                        }
                    });

                } else if (result.isCanceled) {
                // Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                   
                    Swal.fire({
                        title :'{{__("form.changenotsave")}}!', 
                        confirmButtonText: '{{__("form.confirm")}}',
                        confirmButtonColor: '#002169',
                        icon: 'info',
                    });
                }
            })
        }else{
            Swal.fire({
                title :'{{__("form.invalid_fill_in_title")}}', 
                text : '{{__("form.invalid_fill_in_text")}}',
                confirmButtonText: '{{__("form.confirm")}}',
                confirmButtonColor: '#002169',
                icon: 'info',
            });
        }
      
    }


</script>