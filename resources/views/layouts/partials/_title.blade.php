<style>
	.title-container{
		color : #F04E23;
		font-size : 12px;
		/* position: absolute;
   	 	width: 100%;
		bottom : 0; */
	}

	h2{
		font-family: "Montserrat",sans-serif; 
    	font-weight : 700;
		font-size : 40px;
		line-height : 1;
	}

	.title-wrapper{
		padding : 0;
		margin-bottom : 20px;
	}

	.welcome-user{
		font-size : 20px;
	}

	.welcome-user-name{
		font-size : 20px;
		font-weight : bold;
		color : #002169;
	}

	@media screen and (max-width: 750px) {
		h2{
			font-size : 30px;
		}
	}


	
</style>

<div class="container">
	<div class="title-container">
		<div class="container title-wrapper">
			<h2>{{__('header.systemname')}}</h2>
			@if($type == 'officer')
			@php
				$userName = Session::has('username') ? Session::get('username') : 'Admin';
			@endphp
			<h4 class="welcome-user">{{__('messages.welcome')}}, <span class="welcome-user-name">{{$userName}}</span> !</h4>
			@endif
		</div>
	</div>
</div>