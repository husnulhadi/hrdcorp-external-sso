<style>
.custom-nav{
  position: fixed;
  top: 0;
  width: 100%;
  background: #ffffff;
  /* background: #f2f2f2; */
  z-index: 1;
  /* font-family : "Montserrat"; */
}


.navbar-item-wrapper{
  display: flex;
  align-items: center;
  color: #002169!important;
  font-size : 16px;
  justify-content : space-between;
  font-family: "montserrat",Sans-serif;
  column-gap: 1rem;
}
   
.navbar-light .navbar-nav .nav-link {
  color : #002169;
  font-size : 16px;
  font-weight : 600;
}

.link{
  color : #002169;
  font-size : 16px;
  font-weight : 600;
  text-decoration : none;
}

.link:hover{
  color :  #002169;
}

.navbar-section {
    display : flex;
    padding : 0.5rem;
    column-gap : 1rem;
    width : 100%;
    align-items : center;
    font-weight : 500;
    justify-content:space-between;
   
}

.navbar-item{
    width : 33%;
    text-align : center;
    font-size : 16px;
    /* font-weight : bold; */
    /* font-family: 'montserrat'; */
    color: #002169;
}

.dropdown-item {
  color : #002169;
  font-size : 16px;
  font-weight : 600;
}

.dropdown-item:active{
  background-color : #002169;
  color : #FFFFFF;
}

.navbar-light .navbar-nav .nav-link:hover {
    color: #002169;
}

.left{
    text-align : left;
}

.right {
    text-align : right;
    display : flex;
    justify-content : flex-end;
}

.logo-img-wrapper{
    max-width : 250px;
    max-height : 70px;
}


.logo{
    width : 100%;
    height : 100%;
}

.navbar-section-responsive{
  display : none;
}


.float {
    /* position: fixed;
    top: 20px;
    left: 20px;
    bottom: 20px;
    right: 20px; */
    /* display : none; */
    margin-top:4px;
    border: none;
    border-radius: 20%;
    width: 35px;
    height: 35px;
    /* background-color: #223164;
    color: #fff; */
    background-color: transparent;
    color: #223164;
    cursor: pointer;
    /* display: flex; */
    justify-content: center;
    align-items: center;
}

.float-icon{
  color:#002169;
  font-size : 18px;
}


/* The side navigation menu */
.sidenav {
  height: 100%; /* 100% Full-height */
  width: 0; /* 0 width - change this with JavaScript */
  position: fixed; /* Stay in place */
  z-index: 6; /* Stay on top */
  top: 0; /* Stay at the top */
  /* left: 0; */
  right : 0;
  background-color: #223164; /* Black*/
  overflow-x: hidden; /* Disable horizontal scroll */
  padding-top: 60px; /* Place content 60px from the top */
  transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
  /* padding-left : 1rem;
  padding-right : 1rem;   */
  font-weight : 500;
}

.sidenav-inner{
  padding : 1rem;
}
.sidenav-item.link{
    padding : 1rem;
    border-bottom : 1px solid #1595af;
    font-weight : 500;
  }

/* The navigation menu links */
.sidenav a {
/* padding: 8px 8px 8px 32px; */
text-decoration: none;
font-size: 20px;
color: #FFFFFF;
display: block;
transition: 0.3s;
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover {
color: #EF5023;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
position: absolute;
top: 0;
right: 25px;
font-size: 36px;
margin-left: 50px;
}


      
@media only screen and (max-width: 750px) {

  /* .logo-img-wrapper{
    width : 200px;
    height : 60px;
  } */

  .dropdown-item {
    color : #002169;
    font-size : 14px;
  }
  .navbar-item{
    font-size : 14px;
  }
  .navbar-light .navbar-nav .nav-link {
    color : #002169;
    font-size : 14px;
  }

  .link{
    color : #002169;
    font-size : 14px;
    text-decoration : none;
  }

  .navbar-section-responsive{
    display : flex;
    width : 100%;
    padding : 0 1rem;
  }

  .navbar-item.responsive{
    display : flex;
    column-gap : 1rem;
    width : 100%;
  }

  .navbar-item-wrapper{
    width : 100%;
    justify-content : space-between;
  }

  .float {
      display : block;
      width: 30px;
      height: 30px;
      position : unset;
  }
  .navbar-nav .dropdown-menu {
    position: absolute;
  }


  .my-float {
      font-size: 15px;
  }

  .navbar-item.left,.navbar-item.middle{
    width : 50%;
  }

  .navbar-item.right{
    display : none;
  }
  
}

</style>

@php

$currentLocale = !empty(Session::get('locale')) ?  Session::get('locale') : config('app.fallback_locale');
$locales = config('locale');
$localeName = !empty($locales) ? $locales[$currentLocale]['name'] : $locales[config('app.fallback_locale')]['name'];
//$mainPageUrl = env('APP_URL').'/WhistleBlower/public';
$mainPageUrl = 'https://hrdcorp.gov.my/';

@endphp
<nav class="navbar navbar-expand-lg navbar-light bg-light custom-nav">
    <div class="container">
        <div class="navbar-section">
            <div class="navbar-item left">
                <div class="container">
                    <a href="{{$mainPageUrl}}">
                      <div class="logo-img-wrapper">
                       
                        @if($currentLocale == null || $currentLocale == config('app.fallback_locale'))
                        <img class="logo" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px">
                        @else
                        <img class="logo" src="{!! asset('assets/img/logo-hrd-bm.png') !!}" alt="logo" class="centerz" height="150px">
                        @endif
                      </div>
                    </a>
                </div>
            </div>

            <!-- <div class="navbar-item middle">HRDCorp Whistle Blower</div> -->
            
            <div class="navbar-item right"> 
              <div class="">
                  <ul class="navbar-nav mr-auto">
                      <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" onclick="toggleDropdown('dropdown-menu')" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          {{$localeName == 'BI' ? 'English' : 'Malay'}}
                          </a>
                          <div class="dropdown-menu" id="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{route('changeLanguage',['locale'=>'en'])}}">English</a>
                              <a class="dropdown-item" href="{{route('changeLanguage',['locale'=>'bm'])}}">Malay</a>
                              <!-- <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Something else here</a> -->
                          </div>
                      </li>
                      <button onclick="openNav()" class="float">
                          <!-- <i class="bi-list my-float"></i> -->
                          <i class="fa fa-bars float-icon"  aria-hidden="true"></i>
                      </button>
                      {{--<li class="nav-item active">
                          <a class="nav-link" href="{{route('create')}}">Form</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="{{route('trackList')}}">Tracker</a>
                      </li> --}}
                      
                  </ul>
              </div>
            </div>
            
        </div>

        <div class="navbar-section-responsive">
          <div class="navbar-item responsive"> 
             
            <div class="navbar-item-wrapper">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" onclick="toggleDropdown('dropdown-menu2')" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{$localeName == 'BI' ? 'English' : 'Malay'}}
                        </a>
                        <div class="dropdown-menu" id="dropdown-menu2" aria-labelledby="navbarDropdown2">
                          <a class="dropdown-item" href="{{route('changeLanguage',['locale'=>'en'])}}">English</a>
                            <a class="dropdown-item" href="{{route('changeLanguage',['locale'=>'bm'])}}">Malay</a>
                            <!-- <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a> -->
                        </div>
                    </li>
                </ul>
                <button onclick="openNav()" class="float">
                    <!-- <i class="bi-list my-float"></i> -->
                    <i class="fa fa-bars float-icon" aria-hidden="true"></i>
                </button>
            </div>
          </div>
        </div>

    </div>
</nav>
  
  <div id="mySidenav" class="sidenav">
    <div class="sidenav-inner">
      <a class="sidenav-item closebtn" href="javascript:void(0)" onclick="closeNav()">&times;</a>
      <a class="sidenav-item link" href="{{route('courses')}}">{{__('header.courselist')}}</a>
      {{--<a class="sidenav-item link" href="{{route('trackList')}}">{{__('header.checkmysubmission')}}</a>--}}
      {{--<a class="sidenav-item link" href="{{route('termsAndConditionPage')}}">{{__('header.termsandcondition')}}</a>--}}
      <a class="sidenav-item link" href="https://hrdcorp.gov.my/">{{__('header.hrdportal')}}</a>
      {{--<hr>--}}
      {{--<a href="{{route('logout')}}">Logout</a>--}}
    </div>
  </div>

  <script>
  let toggle = false;
  function toggleDropdown(targetSelectorId){
      toggle = !toggle;
      console.log('test toggle')
      console.log(toggle)
      var element = document.getElementById(targetSelectorId);
      element.classList.remove("mystyle");
      if(toggle){
          //$('#dropdown-menu').addClass('show');
          element.classList.add("show");
      }else{
          //$('#dropdown-menu').removeClass('show');
          element.classList.remove("show");
      }
  }

  
  /* Set the width of the side navigation to 250px */
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }

</script>