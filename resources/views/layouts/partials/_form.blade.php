<!DOCTYPE html>
<html lang="en">

<head>
    
    <!-- Title Page-->
    <title>Whistle Blower Report Form</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

</head>
    
<style>
    .wb-title-custom{
        text-align : center;
        padding : 1rem;
    }

    .custom-verification{
        display : flex;
    }
    .wb-checkbox{
        margin-right : 0.5rem;
    }

    .hide{
        display : none!important;
    }

    .btn-container{
        display: flex;
        justify-content: flex-end;
        padding : 0.5rem 0;
    }
    .input-error{
        border : 1px solid #FF0000 !important;
    }

    .error-message{
        color : #FF0000;
    }

    .input-error:focus{
        box-shadow : 0 0 0 0.25rem rgba(255, 0, 0,.25) !important;
    }

    .file-wrapper{
        display:flex;
        column-gap : 0.5rem;
        padding : 0.5rem 0;
    }

    .supporting-documents-label{
        margin : 0.2rem 0;
    }

    .btn-align-center{
        display : flex;
        justify-content : center;
        padding : 0.5rem 0;
    }


    /**Custom dropdown */

    .custom-multiple-select{

    }

    .custom-input{

    }

    .custom-dropdown{
        border :1px solid #ced4da;
        border-radius : .25rem;
        height : 200px;
        overflow : auto;
    }

    .custom-dropdown-ul{
        list-style-type : none;
        margin : 0;
        padding : 0;
        /* padding : 0.5rem 0; */
    }

    .single:hover{
        background-color : grey; 
    }

    .single {
        padding : 0.2rem 0;
    }

    .custom-dropdown-ul-span{
        padding : 0 1rem;
    }

    .bold {
        font-weight : bold;
    }

    .custom-dropdown-li:hover{
        background-color : grey;
    }

    .custom-dropdown-li-wrapper{
        display : flex;
        align-items : center;
        padding : 0.2rem 1rem;
        column-gap : 0.5rem;        
    }

    .scroll {
        height : 150px;
        overflow : auto;
    }

    .declaration-wrapper{
        display:flex;
    }

    .btn-wrapper{
        display: flex;
        justify-content: center;
        column-gap: 0.5rem;
    }
    

</style>
    @section('content')
    
    @php 

    $isDisabled = $user == 1 ? false : true ; 
    $isAssignedOfficer = $status == 2 ?  true : false;

    $fInfo = !empty($formInfo) ? $formInfo['info'] : [];
    $applicantInfo = !empty($fInfo) ? $fInfo['applicant_info'] : [];
    $suspectInfo = !empty($fInfo) ? $fInfo['suspect_info'] : [];
    $witnessInfo = !empty($fInfo) ? $fInfo['witness_info'] : [];
    $complaintInfo = !empty($fInfo) ? $fInfo['complaint_info'] : [];
    $officerList = config('custom.wb.officer.users');
    $formInfoPIC = !empty($formInfo) ? $formInfo['pic'] : ''; 
    
    @endphp
    
<!-- 
    {{--@dd($data);--}}
    <div class="container">
        <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
    </div>
    <div class="container" style="padding-bottom:10px; text-align:right;">
        <a href="https://hrdcorp-my.sharepoint.com/:b:/g/personal/wanmohdfatihin_hrdcorp_gov_my/EZyhkC2BkQNDp4QSPn9EJPcBDmzSXPDUvZDjNjNmP9HKoA?e=RCo0p2" target="_blank"><button type="button" class="btn btn-primary" data-bs-dismiss="modal">User Guide</button></a>
    </div> -->
    <div class="container">
        <div class="card">
            <div class="card-header text-center font-weight-bold">
                Whistle Blower Report Form
            </div>
            
            <form name="whistle-blower-form" id="whistle-blower-form" action="{{route('processCreate')}}" method="post" enctype="multipart/form-data" novalidate>
                {{ csrf_field() }}
                <div class="accordion" id="accordionPanelsStayOpenExample">

                    <div class="accordion-item {{ $isAssignedOfficer ? 'hide' : '' }}" id="disclosure_info">
                        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                            <b>Disclosure</b>
                        </button>
                        </h2>
                        <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
                            <div class="accordion-body">
                                <div>
                                    @foreach($officerList as $officer)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="{{$officer['email']}}" name="disclosure_email[]" id="disclosure_jaksa_{{$officer['id']}}">
                                        <label class="form-check-label" for="flexCheckDefault">
                                        {{!empty($officer) ? $officer['name'] : ''}}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="accordion-item " id="applicant_info">
                        <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                            <b>Applicant Information</b>
                        </button>
                        </h2>
                        <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                            <div class="accordion-body">
                                <div>
                                    <div class="mb-3">
                                        <label for="applicant_name">Name </label>
                                        @if(!$isDisabled)
                                        <input type="text" id="applicant_name" name="applicant_name" class="form-control"/>
                                        @else
                                        <input type="text" id="applicant_name" name="applicant_name" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['name'] : ''}}" disabled> 
                                        @endif
                                        <div class="error-message hide" id="err-applicant_name">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                
                                    <div class="mb-3">
                                      
                                        @if(!$isDisabled)
                                        <label for="applicant_mobile_phone_no">Mobile Phone No. <span class="text-danger">*</span></label>
                                        <input type="number" id="applicant_mobile_phone_no" name="applicant_mobile_phone_no" class="form-control" required/>
                                        @else
                                        <label for="applicant_mobile_phone_no">Mobile Phone No.</label>
                                        <input type="number" id="applicant_mobile_phone_no" name="applicant_mobile_phone_no" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['mobile'] : ''}}" disabled/>
                                        @endif
                                        <div class="error-message hide" id="err-applicant_mobile_phone_no">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="applicant_email_address">E-Mail <span class="text-danger">*</span></label>
                                        <input type="email" id="applicant_email_address" name="applicant_email_address" class="form-control" required>
                                        @else
                                        <label for="applicant_email_address">E-Mail</label>
                                        <input type="email" id="applicant_email_address" name="applicant_email_address" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['email'] : ''}}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-applicant_email_address">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="applicant_designation">Designation<span class="text-danger">*</span></label>
                                        <input type="text" id="applicant_designation" name="applicant_designation" class="form-control" required>
                                        @else
                                        <label for="applicant_designation">Designation</label>
                                        <input type="text" id="applicant_designation" name="applicant_designation" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['designation'] : ''}}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-applicant_designation">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="applicant_department">Department/Agency<span class="text-danger">*</span></label>
                                        <input type="text" id="applicant_department" name="applicant_department" class="form-control" required>
                                        @else
                                        <label for="applicant_department">Department/Agency</label>
                                        <input type="text" id="applicant_department" name="applicant_department" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['department'] : ''}}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-applicant_department">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    @if(!$isDisabled)
                                    <div class="mb-3">
                                        <button id="btn_applicant_info" type="button" onclick="next('applicant_info','suspect_info')" class="btn btn-primary">Next</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> 


               
                    <div class="accordion-item {{ $isDisabled ?  $isAssignedOfficer ? '' : 'hide' : 'hide'}}" id="suspect_info">
                        <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true" aria-controls="panelsStayOpen-collapseThree">
                            <b>Suspect Information</b>
                        </button>
                        </h2>
                        <div class="accordion-collapse collapse show"  id="panelsStayOpen-collapseThree" aria-labelledby="panelsStayOpen-headingThree">
                            <div class="accordion-body">
                                <div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="supect_name">Name <span class="text-danger">*</span></label>
                                        <input type="text" id="supect_name" name="suspect_name" class="form-control" required>
                                        @else
                                        <label for="supect_name">Name </label>
                                        <input type="text" id="supect_name" name="suspect_name" class="form-control" value="{{ !empty($suspectInfo) ? $suspectInfo['name'] : '' }}" disabled> 
                                        @endif
                                        <div class="error-message hide" id="err-suspect_name">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="suspect_mobile_phone_no">Mobile Phone No. <span class="text-danger">*</span></label>
                                        <input type="number" id="suspect_mobile_phone_no" name="suspect_mobile_phone_no" class="form-control" required>
                                        @else
                                        <label for="suspect_mobile_phone_no">Mobile Phone No.</label>
                                        <input type="number" id="suspect_mobile_phone_no" name="suspect_mobile_phone_no" class="form-control" value="{{ !empty($suspectInfo) ? $suspectInfo['mobile'] : '' }}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-suspect_mobile_phone_no">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="suspect_email_address">E-Mail <span class="text-danger">*</span></label>
                                        <input type="email" id="suspect_email_address" name="suspect_email_address" class="form-control" required>
                                        @else
                                        <label for="suspect_email_address">E-Mail</label>
                                        <input type="email" id="suspect_email_address" name="suspect_email_address" class="form-control" value="{{ !empty($suspectInfo) ? $suspectInfo['email'] : '' }}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-suspect_email_address">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="suspect_designation">Designation<span class="text-danger">*</span></label>
                                        <input type="text" id="suspect_designation" name="suspect_designation" class="form-control" required>
                                        @else
                                        <label for="suspect_designation">Designation</label>
                                        <input type="text" id="suspect_designation" name="suspect_designation" class="form-control" value="{{!empty($suspectInfo) ? $suspectInfo['designation'] : ''}}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-suspect_designation">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="suspect_department">Department/Agency<span class="text-danger">*</span></label>
                                        <input type="text" id="suspect_department" name="suspect_department" class="form-control" required>
                                        @else
                                        <label for="suspect_department">Department/Agency</label>
                                        <input type="text" id="suspect_department" name="suspect_department" class="form-control" value="{{ !empty($suspectInfo) ? $suspectInfo['department'] : ''}}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-suspect_department">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    @if(!$isDisabled)
                                    <div class="mb-3">
                                        <button id="btn_suspect_info" type="button" onclick="next('suspect_info','witness_info')" class="btn btn-primary">Next</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> 

                  
                    <div class="accordion-item {{$isDisabled ? $isAssignedOfficer ? '' : 'hide' : 'hide'}}" id="witness_info">
                        <h2 class="accordion-header" id="panelsStayOpen-headingFour">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFour" aria-expanded="true" aria-controls="panelsStayOpen-collapseFour">
                            <b>Witness Information</b>
                        </button>
                        </h2>
                        <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseFour" aria-labelledby="panelsStayOpen-headingFour">
                            <div class="accordion-body">
                                <div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="witness_name">Name <span class="text-danger">*</span></label>
                                        <input type="text" id="witness_name" name="witness_name" class="form-control" required>
                                        @else
                                        <label for="witness_name">Name </label>
                                        <input type="text" id="witness_name" name="witness_name" class="form-control" value="{{ !empty($witnessInfo) ? $witnessInfo['name'] : '' }}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-witness_name">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="witness_mobile_phone_no">Mobile Phone No. <span class="text-danger">*</span></label>
                                        <input type="number" id="witness_mobile_phone_no" name="witness_mobile_phone_no" class="form-control" required>
                                        @else
                                        <label for="witness_mobile_phone_no">Mobile Phone No.</label>
                                        <input type="number" id="witness_mobile_phone_no" name="witness_mobile_phone_no" class="form-control" value="{{ !empty($witnessInfo) ? $witnessInfo['mobile'] : '' }}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-witness_mobile_phone_no">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="witness_email_address">E-Mail <span class="text-danger">*</span></label>
                                        <input type="email" id="witness_email_address" name="witness_email_address" class="form-control" required>
                                        @else
                                        <label for="witness_email_address">E-Mail</span></label>
                                        <input type="email" id="witness_email_address" name="witness_email_address" class="form-control" value="{{  !empty($witnessInfo) ? $witnessInfo['email'] : ''  }}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-witness_email_address">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="witness_designation">Designation<span class="text-danger">*</span></label>
                                        <input type="text" id="witness_designation" name="witness_designation" class="form-control" required>
                                        @else
                                        <label for="witness_designation">Designation</label>
                                        <input type="text" id="witness_designation" name="witness_designation" class="form-control" value="{{  !empty($witnessInfo) ? $witnessInfo['designation'] : ''  }}"  disabled>
                                        @endif
                                        <div class="error-message hide" id="err-witness_designation">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="witness_department">Department/Agency<span class="text-danger">*</span></label>
                                        <input type="text" id="witness_department" name="witness_department" class="form-control" required>
                                        @else
                                        <label for="witness_department">Department/Agency</label>
                                        <input type="text" id="witness_department" name="witness_department" class="form-control" value="{{ !empty($witnessInfo) ? $witnessInfo['department'] : ''  }}" disabled>
                                        @endif
                                        <div class="error-message hide" id="err-witness_department">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    @if(!$isDisabled)
                                    <div class="mb-3">
                                        <button id="btn_witness_info" type="button" onclick="next('witness_info','complaint_info')" class="btn btn-primary">Next</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>  

                   
                    <div class="accordion-item {{$isDisabled ? '' : 'hide'}}" id="complaint_info">
                        <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                            <b>Complaint Information</b>
                        </button>
                        </h2>
                        <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseFour" aria-labelledby="panelsStayOpen-headingFive">
                            <div class="accordion-body">
                                <div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="complaint_misconduct_activity">What misconduct/improper activity occured?<span class="text-danger">*</span></label>
                                        <textarea id="complaint_misconduct_activity" name="complaint_misconduct_activity" class="form-control" required></textarea>
                                        @else
                                        <label for="complaint_misconduct_activity">What misconduct/improper activity occured?</label>
                                        <textarea id="complaint_misconduct_activity" name="complaint_misconduct_activity" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['misconduct_activity']  : ''}}</textarea>
                                        @endif

                                        <div class="error-message hide" id="err-complaint_misconduct_activity">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="complaint_person_conduc">Who committed the misconduct/improper activity? <span class="text-danger">*</span></label>
                                        <textarea id="complaint_person_conduct" name="complaint_person_conduct" class="form-control" required></textarea>
                                        @else
                                        <label for="complaint_person_conduc">Who committed the misconduct/improper activity? </label>
                                        <textarea id="complaint_person_conduct" name="complaint_person_conduct" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['person_conduct']  : '' }}</textarea>

                                        @endif
                                        <div class="error-message hide" id="err-complaint_person_conduct">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="complaint_when_notice_happen">When did it happen and when did you notice it?<span class="text-danger">*</span></label>
                                        <textarea id="complaint_when_notice_happen" name="complaint_when_notice_happen" class="form-control" required></textarea>
                                        @else
                                        <label for="complaint_when_notice_happen">When did it happen and when did you notice it?</label>
                                        <textarea id="complaint_when_notice_happen" name="complaint_when_notice_happen" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['when_notice_happen']  : '' }}</textarea>
                                        @endif
                                        <div class="error-message hide" id="err-complaint_when_notice_happen">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        @if(!$isDisabled)
                                        <label for="complaint_when_happen">Where did ot happen?<span class="text-danger">*</span></label>
                                        <textarea id="complaint_when_happen" name="complaint_when_happen" class="form-control" required></textarea>
                                        @else
                                        <label for="complaint_when_happen">Where did ot happen?</label>
                                        <textarea id="complaint_when_happen" name="complaint_when_happen" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['when_notice_happen']  : '' }}</textarea>
                                        @endif
                                        <div class="error-message hide" id="err-complaint_when_happen">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="complaint_evidence">Is there any evidence that you could provide us?</label>
                                        @if(!$isDisabled)
                                        <textarea id="complaint_evidence" name="complaint_evidence" class="form-control" required></textarea>
                                        @else
                                        <textarea id="complaint_evidence" name="complaint_evidence" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['complaint_evidence']  : '' }}</textarea>
                                        @endif
                                        <div class="error-message hide" id="err-complaint_evidence">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="complaint_parties_involve">Are there any other parties involved other than the suspect stated above?</span></label>
                                        @if(!$isDisabled)
                                        <textarea id="complaint_parties_involve" name="complaint_parties_involve" class="form-control" required></textarea>
                                        @else
                                        <textarea id="complaint_parties_involve" name="complaint_parties_involve" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['parties_involve']  : '' }}</textarea>
                                        @endif
                                        <div class="error-message hide" id="err-complaint_parties_involve">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="complaint_addon_info">Do you have any other details or information which would assist us in the investigation?</label>
                                        @if(!$isDisabled)
                                        <textarea id="complaint_addon_info" name="complaint_addon_info" class="form-control" required></textarea>
                                        @else
                                        <textarea id="complaint_addon_info" name="complaint_addon_info" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['addon_info']  : '' }}</textarea>
                                        @endif
                                        <div class="error-message hide" id="err-complaint_addon_info">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="complaint_other_comment">Any other comments?</label>
                                        @if(!$isDisabled)
                                        <textarea id="complaint_other_comment" name="complaint_other_comment" class="form-control" required></textarea>
                                        @else
                                        <textarea id="complaint_other_comment" name="complaint_other_comment" class="form-control" disabled>{{ !empty($complaintInfo) ? $complaintInfo['other_comment']  : '' }}</textarea>
                                        @endif
                                        <div class="error-message hide" id="err-witness_department">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label class="supporting-documents-label" for="complaint_supporting_docs">If you have any supporting documents, please upload here. </label>
                                        @if(!$isDisabled)
                                        <div id="field-container-doc">
                                            <div class="form-field">
                                                <div class="file-container" id="file-container" style="padding-bottom:10px">
                                                    <div class="file-wrapper">
                                                        <input type="file" class="form-control" id="supporting_doc_0" name="complaint_supporting_docs[]" >
                                                        <button type="button" class="btn btn-primary" onClick="appendFileInput()">+</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <div id="field-container-doc">
                                            <div><a href="#">test.pdf</a></div>
                                            <div><a href="#">test2.pdf</a></div>
                                        </div>

                                        @endif
                                        <div class="error-message hide" id="err-witness_department">
                                            Please select a valid state.
                                        </div>
                                    </div>

                                    @if(!$isDisabled)
                                    <div class="declaration-container">
                                        <div>
                                            <h4>Declaration</h4>
                                            <span>*Please read the following statements carefully and tick all the boxes before submitting</sapn>
                                        <div>
                                        <div class="declaration-wrapper">
                                            <input class="wb-checkbox" type="checkbox" id="declare_1" name="declare_1" required>
                                            <label for="declare_1">I declare that all information provided in this form is correct and complete to the best of my knowledge, information and belief.</label>
                                        </div>
                                        <div class="declaration-wrapper"> 
                                            <input class="wb-checkbox" type="checkbox" id="declare_2" name="declare_2" required>
                                            <label for="declare_2">I hereby agree that the information provided herein to be used and processed for investigate purpose and further agree that the information provided herein may be forwarded to another department/authority/enforcement agency for of investigate.</label>
                                        </div>
                                        <div class="error-message hide" id="err-witness_department">
                                            Please select a valid state.
                                        </div>
                                    </div>
                                    @endif

                                    @if(!$isDisabled)
                                    <div class="mb-3 btn-align-center">
                                        <button id="submit" type="submit" onclick="" class="btn btn-primary">Submit</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> 

                    @if($isDisabled)
                        @if($isAssignedOfficer)
                            @if($officerInfo['id'] == $formInfoPIC )
                                @if($formInfo['status'] == 'Approve' || $formInfo['status'] == 'Reject' || $formInfo['status'] == 'Case Close')
                                <div class="mb-3 btn-align-center">
                                    <a href="{{route('listById',['officerId'=>$officerInfo['id']])}}"><button id="assign" type="button" class="btn btn-primary">Back</button></a>
                                </div>
                                @else
                                <!--check role to show-->
                                <div class="accordion-item " id="secretary_info">
                                    <h2 class="accordion-header" id="panelsStayOpen-headingSix">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="true" aria-controls="panelsStayOpen-collapseSix">
                                        <b>For Secretary Use</b>
                                    </button>
                                    </h2>
                                    <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseSix" aria-labelledby="panelsStayOpen-headingSix">
                                        <div class="accordion-body">
                                            <div>
                                            <div class="mb-3">
                                                    <label for="complaint_misconduct_activity">Process History</label>
                                                    @if($formInfo['status'] == 'KIV')
                                                    <div class="table-responsive scroll">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Status</th>
                                                                    <th>Remark</th>
                                                                    <th>Date</th>
                                                                </tr>
                                                                <tr>
                                                                    <td>Pending</td>
                                                                    <td></td>
                                                                    <td>2023-01-01 15:00:00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Reject</td>
                                                                    <td>Please reupdate</td>
                                                                    <td>2023-01-02 15:00:00</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Resubmit</td>
                                                                    <td></td>
                                                                    <td>2023-01-03 16:00:00</td>
                                                                </tr>

                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                @endif

                                                <div class="mb-3">
                                                    <label for="complaint_misconduct_activity">Case Status<span class="text-danger">*</span></label>
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option value="1">Approve</option>
                                                        <option value="2">Reject</option>
                                                        <option value="3" {{$formInfo['status'] == 'KIV' ? 'selected' : ''}}>KIV</option>
                                                        <option value="4">Full Investigate</option>
                                                        <option value="5">Investigation</option>
                                                        <option value="6">Delegate to WB Comittee</option>
                                                        <option value="7">Case Close</option>
                                                    </select>
                                                </div>

                                              
                                                
                                                <div class="mb-3">
                                                    <label for="complaint_person_conduc">Remark <span class="text-danger">*</span></label>
                                                    @if($isDisabled)
                                                    <textarea id="complaint_person_conduct" name="complaint_person_conduct" class="form-control" required>{{$formInfo['remark']}}</textarea>
                                                    @else
                                                    <textarea id="complaint_person_conduct" name="complaint_person_conduct" class="form-control" required  disabled></textarea>
                                                    @endif
                                                    <div class="error-message hide" id="err-complaint_person_conduct">
                                                        Please select a valid state.
                                                    </div>
                                                </div>
                                            
                                                <div class="mb-3 btn-wrapper">
                                                    <div class="mb-3 btn-align-center">
                                                        <a href="{{route('listById',['officerId'=>$officerInfo['id']])}}"><button id="assign" type="button" class="btn btn-primary">Back</button></a>
                                                    </div>
                                                    <div class="mb-3 btn-align-center">
                                                        <button id="assign" type="submit" onclick="" class="btn btn-primary">Update</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endif
                            @endif
                        @else
                        <div class="mb-3 btn-align-center">
                            <div class="mb-3 btn-wrapper">
                                <div class="mb-3 btn-align-center">
                                    <a href="{{route('listById',['officerId'=>$officerInfo['id']])}}"><button id="assign" type="button" class="btn btn-primary">Back</button></a>
                                </div>
                                <div class="mb-3 btn-align-center">
                                    <button id="assign" type="submit" onclick="" class="btn btn-primary">Assign/Pick</button>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif

                    
                </div>

                <button class="btn btn-primary d-none" type="button" id="loader-btn" disabled>
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
                
            </form>
        </div>
    </div>
    
    
    <script>
        let totalAccumulatedAppendCount = 0;
        let open = false;
        let exclude_required = ['applicant_name'];
        let sections_settings = {
            applicant_info : {
                validate : false,
                inputs : [
                    {
                        slug : 'applicant_name',
                        name : 'applicant name'
                    },{
                        slug : 'applicant_mobile_phone_no',
                        name : 'applicant mobile phone no'
                    },
                    {
                        slug : 'applicant_email_address',
                        name : 'applicant email address'
                    },
                    {
                        slug : 'applicant_designation',
                        name : 'applicant designation'
                    },
                    {
                        slug : 'applicant_department',
                        name : 'applicant department'
                    },
                ],
            },
            suspect_info : {
                validate : false,
                inputs : [
                    {
                        slug : 'suspect_name',
                        name : 'suspect name'
                    },{
                        slug : 'suspect_mobile_phone_no',
                        name : 'suspect mobile phone no'
                    },
                    {
                        slug : 'suspect_email_address',
                        name : 'suspect email address'
                    },
                    {
                        slug : 'suspect_designation',
                        name : 'suspect designation'
                    },
                    {
                        slug : 'suspect_department',
                        name : 'suspect department'
                    },
                ],
            },
            witness_info : {
                validate : false,
                inputs : [
                    {
                        slug : 'witness_name',
                        name : 'witness name'
                    },{
                        slug : 'witness_mobile_phone_no',
                        name : 'witness mobile phone no'
                    },
                    {
                        slug : 'witness_email_address',
                        name : 'witness email address'
                    },
                    {
                        slug : 'witness_designation',
                        name : 'witness designation'
                    },
                    {
                        slug : 'witness_department',
                        name : 'witness department'
                    },
                ],
            },
            complaint_info : {}
        }


        // $(document).ready(function() {
        //     // Select2 Multiple
        //     $('.select2-multiple').select2({
        //         theme: "bootstrap-5",
        //         width: $( this ).data( 'width' ) ? $( this ).data( 'width' ) : $( this ).hasClass( 'w-100' ) ? '100%' : 'style',
        //         placeholder: "Select",
        //         allowClear: true,
        //     });
        // });


        // add form validation using Bootstrap
        $('#whistle-blower-form').on('submit', function(event) {
            if ($('#whistle-blower-form')[0].checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } else {
                // show the loader button
                //$("#book-flight-btn").addClass("d-none"); // hide the book flight button
                //$("#loader-btn").removeClass("d-none"); // show the loader button
            }
            $('#whistle-blower-form').addClass('was-validated');
        });


        $('#assign').on('focus',function(){
            $('#custom-dropdown').removeClass('hide');
        });


        function next(current_section,next_section){
            validateSectionInput(current_section);
            let currentSectionInfo = sections_settings[current_section];
            let checkCurrentSectionValidationStatus = currentSectionInfo.validate;
            if(checkCurrentSectionValidationStatus){
                // console.log('next success')
                // console.log('#'+next_section)
                $('#'+next_section).removeClass('hide');
                $('#btn_'+current_section).addClass('hide');
            }
           // console.log(current_section, next_section)
        }

        
        function validateSectionInput(section_key){
            let selected_section = sections_settings[section_key].inputs;
            let requiredValidCount = 0;
            let numberCheck = false;
            let emailCheck = false;
            let checkboxCheck = false;
            for(let i=0; i< selected_section.length; i++){
                let info = selected_section[i];
                let value = $('#'+info.slug).val(); 
                let type = $('#'+info.slug).attr('type');
                if(value == ''){
                    if(exclude_required.includes(info.slug)){
                        requiredValidCount++;
                    }else{
                        $('#err-'+info.slug).text('Please fill in '+info.name);
                        $('#err-'+info.slug).removeClass('hide');
                        $('#'+info.slug).addClass('input-error');
                    }
                }else{
                    $('#err-'+info.slug).text('');
                    $('#'+info.slug).removeClass('input-error')
                    requiredValidCount++;
                }  
            
                if(type == 'number'){
                    if(!$.isNumeric(value)){
                        $('#err'+info.slug).val('Please fill only number for '+info.name)   
                    }else{
                        numberCheck = true;
                    }
                    
                }

                if(type == 'email'){
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if(!emailReg.test(value)){
                        console.log('Please fill in correct email format')
                        $('#err-'+info.slug).text('Please fill in correct email format for'+info.name);
                        $('#err-'+info.slug).removeClass('hide');
                        $('#'+info.slug).addClass('input-error');
                    }else{
                        emailCheck = true;
                    }
                }

                if(type == 'checkbox'){

                }

                if(requiredValidCount == 5 && numberCheck == true && emailCheck == true ){
                    sections_settings[section_key].validate = true;
                }       
            }   
        }


        function appendFileInput () {
            totalAccumulatedAppendCount++;
            $('#file-container').append(`
            <div class="file-wrapper" data-count=${totalAccumulatedAppendCount} id="supporting_doc_${totalAccumulatedAppendCount}">
                <input type="file" class="form-control supporting_doc" name="complaint_supporting_docs[]" required>
                <button type="button" class="btn btn-primary" onClick="removeSelectedFileInput('supporting_doc_${totalAccumulatedAppendCount}')">-</button>
            </div>
            `);
        }

        function removeSelectedFileInput(selectorIdToRemove){
            $('#'+selectorIdToRemove).remove();
        }

        
        

    </script>
    @endsection

</html>


