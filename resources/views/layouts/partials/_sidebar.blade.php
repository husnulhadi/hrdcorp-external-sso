<?php
  $domainip =  config('server.domain.ip');
  $domainpath =  config('server.domain.path');

  use Illuminate\Support\Facades\DB;
  use App\Platform\Bpms;
  use App\Field\Lookup;

  $roleOfUser = DB::table('role_of_user')->where('user_id','=',auth()->user()->id)->get();           

  $list_role = '';

  $checklastlogin = DB::table('audit_trail')
	->where('adt_action_name', '=', 1)
	->where('adt_created_by','=',auth()->user()->id)
	->orderBy('adt_created_date', 'desc')
	->skip(1)
	->take(1)
	->exists();

	if ($checklastlogin) {

	 $lastlogin = DB::table('audit_trail')
		->where('adt_action_name', '=', 1)
		->where('adt_created_by','=',auth()->user()->id)
		->orderBy('adt_created_date', 'desc')
		->skip(1)
		->take(1)
		->first();

	} else {

	$lastlogin = DB::table('audit_trail')
		->where('adt_action_name', '=', 1)
		->where('adt_created_by','=',auth()->user()->id)
		->first();

	}
?>
<style>
.sidebar-dark .nav-sidebar>.nav-item-open>.nav-link:not(.disabled),.sidebar-dark .nav-sidebar>.nav-item>.nav-link.active,.sidebar-light .card[class*=bg-]:not(.bg-light):not(.bg-white):not(.bg-transparent) .nav-sidebar>.nav-item-open>.nav-link:not(.disabled),.sidebar-light .card[class*=bg-]:not(.bg-light):not(.bg-white):not(.bg-transparent) .nav-sidebar>.nav-item>.nav-link.active{background-color:#2196f3;color:#fff}
</style>
<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
          <i class="icon-arrow-left8"></i>
        </a>
        {{trans('roles.list.navigation')}}
        <a href="#" class="sidebar-mobile-expand">
          <i class="icon-screen-full"></i>
          <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
          	<div class="card-body">
            	<div class="media">
		            <div class="mr-3">
		            </div>

		            <div class="media-body">
		                <div class="media-title font-weight-semibold">{{ auth()->user()->name }}</div>
		                <div class="font-size-xs opacity-50 font-weight-semibold text-teal-custom">
		                  {{trans('common.menu.last_login')}}: <br>
		                  {{ date("d-m-Y", strtotime($lastlogin->adt_created_date)) }} {{ date("h:i A", strtotime($lastlogin->adt_created_date)) }}
		                </div>
		            </div>
            	</div>
          	</div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
		    <ul class="nav nav-sidebar" data-nav-type="accordion">

		        <!-- Main -->
		        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">{{trans('roles.list.main')}}</div> <i class="icon-menu" title="Main"></i></li>
		        <li class="nav-item">
		            <a href="{{ route('dashboard') }}" class="nav-link">
		                <i class="icon-home4"></i>
		                <span>
		                  {{trans('roles.dashboard')}}
		                  <span class="d-block font-weight-normal opacity-50">total task</span>
		                </span>
		            </a>
		        </li>

		        <!-- Components -->
		        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">{{trans('common.menu.navigation')}}</div> <i class="icon-menu" title="Components"></i></li>
		        <?php
		          	$listroles = DB::table('role_of_user')->where('user_id','=', auth()->user()->id)->get();

		          	$roleexist = DB::table('role_of_user')->where('user_id','=', auth()->user()->id)->exists();

		          	$role_access 	= array();
		          	$arrRole 		= array();
		           
		            foreach ($listroles as $listrole) {
		                $arrRole[] = $listrole->role_id;
		            }

		           	$role_access = DB::table('role_access_right')
		                          ->select( DB::raw('DISTINCT rar_menu_id, sidebar_id, sidebar_name, sidebar_path, sidebar_icon') )
		                          ->leftJoin('sidebar', 'sidebar.sidebar_id','=','role_access_right.rar_menu_id')
		                          ->whereIn('rar_role_id',$arrRole)
		                          ->where('sidebar_parent_id','=',0) 
		                          ->get();
		        ?>
		        @if ($roleexist)
		        @foreach($role_access as $ra)
		            <?php
		                $sub_menu = DB::table('role_access_right')
		                            ->select( DB::raw('DISTINCT rar_menu_id, sidebar_id, sidebar_name, sidebar_path, sidebar_icon') )
		                            ->leftJoin('sidebar', 'sidebar.sidebar_id','=','role_access_right.rar_menu_id')
		                            ->whereIn('rar_role_id',$arrRole)
		                            ->where('sidebar_parent_id','=',$ra->sidebar_id)
		                            ->where('sidebar_isactive','=',1)
		                            // ->orderBy('sidebar.sidebar_order', 'asc')
		                            ->get();
		            ?>
		            @if(count($sub_menu)>0)
		            <li <?=(Request::is($ra->sidebar_path) ? 'class="nav-item nav-item-submenu nav-item-expanded nav-item-open"' : 'class="nav-item nav-item-submenu"')?>>
		              	<a href="#" class="nav-link"><i class="{{ $ra->sidebar_icon }}"></i> <span><?=Lookup::displaySidebar('sidebar','sidebar_name','sidebar_name',$ra->sidebar_name); ?></span></a>
		              
		                <ul class="nav nav-group-sub" data-submenu-title="{{ $ra->sidebar_name }}">
		                    @foreach($sub_menu as $sb)
		                        <?php
		                            $sub_menu2 = DB::table('role_access_right')
		                                        ->select( DB::raw('DISTINCT rar_menu_id, sidebar_id, sidebar_name, sidebar_path, sidebar_icon') )
		                                        ->leftJoin('sidebar', 'sidebar.sidebar_id','=','role_access_right.rar_menu_id')
		                                        ->whereIn('rar_role_id',$arrRole)
		                                        ->where('sidebar_parent_id','=',$sb->sidebar_id)
		                                        // ->orderBy('sidebar.sidebar_order', 'asc')
		                                        ->get();
		                        ?>
		                        @if(count($sub_menu2)>0)
		                            <li <?=(Request::is($sb->sidebar_path) ? 'class="nav-item nav-item-submenu nav-item-expanded nav-item-open"' : 'class="nav-item nav-item-submenu"')?>>
		                              	<a href="#" class="nav-link"><i class="{{ $sb->sidebar_icon }}"></i> <span><?=Lookup::displaySidebar('sidebar','sidebar_name','sidebar_name',$sb->sidebar_name); ?></span></a>
			                            <ul class="nav nav-group-sub" data-submenu-title="{{ $sb->sidebar_name}}">
			                                @foreach($sub_menu2 as $smc)                  
			                                    <?php
			                                        $sub_menu3 = DB::table('sidebar')->where('sidebar_parent_id', '=', $smc->sidebar_id)->get();
			                                    ?>   
			                                    @if(count($sub_menu3)>0)
			                                    <li <?=(Request::is($smc->sidebar_path) ? 'class="nav-item nav-item-submenu nav-item-expanded nav-item-open"' : 'class="nav-item nav-item-submenu"')?>>
			                                        <a href="#" class="nav-link"><i class="{{ $smc->sidebar_icon }}"></i> <span><?=Lookup::displaySidebar('sidebar','sidebar_name','sidebar_name',$smc->sidebar_name); ?></span></a>
			                                        <ul class="nav nav-group-sub" data-submenu-title="{{ $smc->sidebar_name}}">
			                                        @foreach($sub_menu3 as $sm3) 
			                                        <li class="nav-item"><a href="{{ url($sm3->sidebar_path) }}" @if(Request::path() === $sm3->sidebar_path) class="nav-link active" @else class="nav-link" @endif ><i class="i{{ $sm3->sidebar_icon }}"></i> {{ trans($sm3->sidebar_name) }}</a></li>
			                                        @endforeach 
			                                        </ul>
			                                    </li>     
			                                    @else
			                                    <li class="nav-item"><a href="{{ url($smc->sidebar_path) }}" @if(Request::path() === $smc->sidebar_path) class="nav-link active" @else class="nav-link" @endif ><i class="{{ $smc->sidebar_icon }}"></i> <?=Lookup::displaySidebar('sidebar','sidebar_name','sidebar_name',$smc->sidebar_name); ?></a></li>
			                                    @endif
			                                @endforeach
			                            </ul>
		                            </li>
		                        @else
		                            <li class="nav-item"><a href="{{ url($sb->sidebar_path) }}" @if(Request::path() === $sb->sidebar_path) class="nav-link active" @else class="nav-link" @endif ><i class="{{ $sb->sidebar_icon }}"></i> <?=Lookup::displaySidebar('sidebar','sidebar_name','sidebar_name',$sb->sidebar_name); ?></a></li>
		                        @endif
		                    @endforeach
		                </ul>
		            </li>
		            @else
		            <li class="nav-item"><a href="{{ url($ra->sidebar_path) }}" @if(Request::path() === $ra->sidebar_path) class="nav-link active" @else class="nav-link" @endif ><i class="{{ $ra->sidebar_icon }}"></i> <?=Lookup::displaySidebar('sidebar','sidebar_name','sidebar_name',$ra->sidebar_name); ?></a></li>
		            @endif
		        @endforeach
		        @endif
		        <!-- /components -->
			</ul>
		</div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->  
</div>
<!-- /main sidebar -->
