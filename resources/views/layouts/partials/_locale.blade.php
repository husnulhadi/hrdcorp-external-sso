@php
//$currentLocale = Config::get('app.locale');
//$locale = App::currentLocale();
$currentLocale = Session::get('locale');
App::setLocale($currentLocale);
$wbTextStyle = Session::get('locale') == config('app.fallback_locale') ? 'wb-text-italic' : '';
@endphp