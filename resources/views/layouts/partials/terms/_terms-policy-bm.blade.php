<style>
.list-terms{
    list-style-type : none;
}
</style>
<div>
    <!-- <h4>Terma dan syarat</h4>
    <span>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
    </span> -->
    <h4>1.DEKLARASI</h4>
    <p> a) Saya dengan ini mengisytiharkan bahawa semua maklumat yang diberikan dalam aplikasi Pemberi Maklumat ini adalah benar dan tepat.</p>
    <p> b) Saya memahami sepenuhnya bahawa dengan laporan ini, saya berhak mendapat perlindungan pemberi maklumat daripada Syarikat seperti yang dinyatakan dalam Dasar Pemberi Maklumat Syarikat.</p>
    <p> c) Saya memahami sepenuhnya bahawa sekiranya saya membuat laporan ini dengan berniat jahat, perlindungan pemberi maklumat yang terkandung dalam Dasar Pemberi Maklumat Syarikat tidak lagi terpakai kepada saya dan saya mungkin tertakluk kepada prosiding tatatertib atau tindakan undang-undang oleh Syarikat.</p>
    <p> d) Saya memahami dan mengakui bahawa Syarikat tidak akan bertanggungjawab atas sebarang kesilapan atau kelewatan yang mungkin timbul akibat daripada anda tidak dapat mengakses aplikasi Pemberi Maklumat atau perkhidmatan yang berkaitan kerana ralat pada peranti mudah alih, perisian atau pembekal perkhidmatan pihak ketiga anda.</p>
    <p> e) Saya memahami dan mengakui bahawa Syarikat mempunyai hak untuk mengubah semua aduan yang dikemukakan yang tidak berkaitan dengan aduan Pemberi Maklumat ke saluran aduan yang lain dalam Syarikat untuk tindakan selanjutnya.</p>
    <p> f) Saya memahami dan mengakui bahawa walaupun Syarikat pada setiap masa akan menggunakan usaha terbaik mereka dan mengambil langkah-langkah yang munasabah untuk memastikan bahawa semua maklumat tersebut dirahsiakan dan untuk memastikan kerahsiaan mana-mana orang yang membuat pendedahan itu, Syarikat tidak akan bertanggungjawab atas sebarang pendedahan yang bukan hasil daripada kesalahan atau kesilapan Syarikat.</p>
    <p> g) Saya memahami dan mengakui bahawa walaupun Syarikat pada setiap masa akan menggunakan usaha terbaik mereka dan mengambil langkah yang munasabah untuk memastikan aplikasi Pemberi Maklumat beroperasi dengan lancar, Syarikat tidak boleh menjamin bahawa aplikasi Pemberi Maklumat serasi atau akan beroperasi dengan peranti mudah alih anda atau sebarang perisian/perkakasan yang saya gunakan.</p>
    <br>
    <h4>2.KLAUSA PERSETUJUAN AKTA PERLINDUNGAN DATA PERIBADI 2010 ("PDPA")</h4>
    <p> a) Dengan penyerahan laporan ini, saya dengan ini bersetuju bahawa PSMB akan mengumpul, memperoleh, menyimpan, dan memproses data peribadi saya seperti yang diperuntukkan dalam aplikasi Pemberi Maklumat ini untuk tujuan memproses sebarang aduan, menerima maklumat terkini, berita atau apa-apa bahan daripada PSMB.</p>
    <p> b) Saya memahami sepenuhnya dan dengan ini memberi persetujuan kepada PSMB untuk:-</p>
    <div> 
        <ul class="list-terms">
            <li>i. Simpan dan proses data peribadi saya.</li>
            <li>ii. Mendedahkan data peribadi saya kepada pihak berkuasa kerajaan yang berkaitan atau pihak ketiga jika dikehendaki oleh undang-undang atau untuk tujuan undang-undang.</li>
        </ul>
    </div>
    <p>
        c) Untuk mengelakkan keraguan, data peribadi termasuk semua data yang ditakrifkan dalam Akta Perlindungan Data Peribadi 2010 termasuk semua data yang telah didedahkan kepada PSMB dalam Borang ini.
    </p>
    
        
</div>
<br>