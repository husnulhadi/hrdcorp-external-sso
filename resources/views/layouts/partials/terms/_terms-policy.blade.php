<style>
.list-terms{
    list-style-type : none;
}
</style>
<div>
    <!-- <h4>Terms & Condition</h4>
    <span>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
    </span> -->
    <h4>1.DECLARATION</h4>
    <p> a) I hereby declare that all the information provided in this WB apps is true and accurate.</p>
    <p> b) I fully understand that with this report, I shall be entitled to whistleblower protection from the Company as set out in the Company’s Whistleblower Policy.</p>
    <p> c) I fully understand that in the event I have made this Report maliciously or in bad faith, the whistleblower protection contained in the Company’s Whistleblower Policy shall no longer be applicable to me and I may be subject to disciplinary or legal proceedings by the Company.</p>
    <p> d) I understand and acknowledge that the Company shall not be responsible for any error or delay that may arise as a result of you being unable to access the WB apps or related services due to the error on your mobile device, software or third-party service provider.</p>
    <p> e) I understand and acknowledge that the Company have a right to reroute all complaints submitted which are not related to the WB complaints to the respective channels within the Company for further action.</p>
    <p> f) I understand and acknowledge that whilst the Company will at all times use their best efforts and take reasonable steps to ensure that all such information is kept confidential and to ensure the anonymity of any person making the disclosure, the Company shall not be held responsible for any disclosure that are not a result of the Company's fault or error.</p>
    <p> g) I understand and acknowledge that whilst the Company will at all times use their best efforts and take reasonable steps to ensure the WB app operates smoothly, the Company cannot warrant that the WB app are compatible or will operate with your mobile devices or any software/hardware that I am using.</p>

    <br><br>
    <h4>2.PERSONAL DATA PROTECTION ACT 2010 (“PDPA”) CONSENT CLAUSE</h4>
    <p> a) By summiting this report, I hereby agree that PSMB shall collect, obtain, store, and process my personal data as provided in this WB apps for the purpose of processing any complaints, receiving updates, news, or any materials from PSMB.</p>
    <p> b) I fully understand and hereby give my consent to PSMB to: -</p>
    <div> 
        <ul class="list-terms">
            <li>i. Store and process my personal data.</li>
            <li>ii. Disclose my personal data to the relevant government authorities or third parties where required by law or for legal purposes.</li>
        </ul>
    </div>
    <p>
        c) For the avoidance of doubt, personal data includes all data defined within the Personal Data Protection Act 2010 including all data that had been disclosed to PSMB in this Form
    </p>
</div>
<br>
