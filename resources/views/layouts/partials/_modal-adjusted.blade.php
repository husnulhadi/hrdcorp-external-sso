
@php 


$statusList = config('custom.wb.officer.status');
$caseStatus = 1;
$officerInfo = !empty($user) ? $user : [];
$isAssignedOfficer = !empty($officerInfo) ?  true : false; 
$isDisabled = true;

@endphp

<style>
.suspect-individual-label{
    font-style : italic;
}

.file-wrapper{
    display:flex;
    column-gap : 0.5rem;
    padding : 0.5rem 0;
}

.file-upload-instruction-wrapper{
        /* display : flex; */
}

.file-upload-instruction-group{
    display : flex;
}

.file-upload-instruction{
    /* font-style : italic; */
    font-size : 12px; 
    font-weight : bold;
}

.file-upload-ins-sub{
    font-size : 12px; 
}
.suspect-container{
    height: 300px;
    overflow: auto;
}

.collapse{
    visibility : visible;
}

.btn-wrapper{
    display : flex;
    column-gap : 0.2rem;
}

.acc-container{
    display :flex;
    column-gap: 1rem;
    padding: 1rem 0;
}

.custom-width-setting{
    width : 50%;
}

.modal-dialog.modal-dialog-scrollable.modal-dialog-centered{
    min-width : 90%;
}


.complaint-country-container, .complaint-state-container{
    display : flex;
    column-gap : 0.5rem;
}

.half-width {
    width : 50%;
}

.full-width{
    width : 100%;
}

.col-3-width{
    width : 33%;
}

.complaint-info{
    
}

.old{
    height:200px;
    overflow:auto;"
}

.input-error{
    border : 1px solid #FF0000 !important;
}

.error-message{
    color : #FF0000;
}

.input-error:focus{
    box-shadow : 0 0 0 0.25rem rgba(255, 0, 0,.25) !important;
}

.loader,.error{
    display: flex;
    justify-content: center;
    padding: 1rem 0;
}

.disclosure-email-container{
    height: 60px;
    overflow: auto;
}

 /* .activity-thead{
    display : table;
    width : 100%;
}

.activity-tbody{
    width : 100%;
    display : block;
    height : 300px;
    overflow-x : none;
    overflow-y : auto;
} 

thead, tbody tr {
    display:table;
    width:100%;
    table-layout:fixed;
} */

@media only screen and (max-width: 750px) {
    .acc-container{
        display : block;
        column-gap: 0;
        padding: 1rem 0;
    }
    .custom-width-setting{
        width : 100%;
    }

    .file-upload-instruction-wrapper, .file-upload-instruction-group{
            display : block;
    }

    .complaint-country-container, .complaint-state-container{
        display : block;
    }

    .half-width {
        width : 100%;
    }

    .col-3-width{
        width : 100%;
    }

}


.modal{
    color : #000;
}
</style>


<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">{{__('form.case')}} : <span id="caseIdText"></span> </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"> 
                <form class="form hide" name="whistle-blower-form" id="whistle-blower-form" method="post"  novalidate="">
                    {{ csrf_field() }}
                    <input type="hidden" id="currentStatus" name="currentStatus" value=""/>
                    <input type="hidden" id="assignStatus" name="assignStatus" value="2"/>
                    <input type="hidden" id="caseId" name="caseId" value=""/>
                    <input type="hidden" id="profileId" name="profileId" value=""/>
                    <input type="hidden" id="applicantEmail" name="applicantEmail" value=""/>
                    <input type="hidden" id="applicantName" name="applicantName" value=""/>
                    <input type="hidden" id="complaintId" name="complaintId" value=""/>
                    <input type="hidden" id="suspectId" name="suspectId" value=""/>  
                    <input type="hidden" id="officerId" name="officerId" value="{{!empty($officerInfo) ? $officerInfo['id'] : ''}}"/> 
                    <input type="hidden" id="officerEmail" name="officerEmail" value="{{!empty($officerInfo) ? $officerInfo['Email'] : ''}}"/> 
                    <input type="hidden" id="officerName" name="officerName" value="{{!empty($officerInfo) ? $officerInfo['fullname'] : ''}}"/>
                    <input type="hidden" id="currentOfficer" name="currentOfficer" value="{{$currentOfficer}}"/>
                    <div class="accordion" id="accordionPanelsStayOpenExample">
                        
                        <div class="accordion-item " id="disclosure_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                <b>{{ __('form.disclosureemail')}}</b>
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
                                <div class="accordion-body">
                                    <div id="disclosure-email-container" class="disclosure-email-container">
                                        <div><span> </span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        <div class="acc-container">
                            <div class="accordion-item custom-width-setting" id="applicant_info">
                                <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                                <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                                    @if(empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale') )
                                    <b>A.{{__('form.particularofwb')}} Whistleblower / {{__('form.particularofwb2')}} (<span class="wb-text wb-text-italic">Whistleblower</span>)</b> 
                                    @else
                                    <b>A.{{__('form.particularofwb')}} (<span class="wb-text wb-text-italic">Whistleblower</span>)/ {{__('form.particularofwb2')}} Whistleblower</b>
                                    @endif
                                </button>
                                </h2>
                                <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                                    <div class="accordion-body">
                                        <div>
                                            <div class="mb-3">
                                                <label for="applicant_name">{{__('form.particularofwblabelname')}} </label>
                                                <input type="text" id="applicant_name" name="applicant_name" class="form-control" value="" disabled> 
                                            </div>
                                        
                                            <div class="mb-3">
                                                <label for="applicant_mobile_phone_no">{{__('form.particularofwblabelphone')}}.</label>
                                                <input type="number" id="applicant_mobile_phone_no" name="applicant_mobile_phone_no" class="form-control" value="" disabled/>
                                            </div>
                                            
                                            <div class="mb-3">
                                                <label for="applicant_email_address">{{__('form.particularofwblabelemail')}}</label>
                                                <input type="email" id="applicant_email_address" name="applicant_email_address" class="form-control" value="" disabled>  
                                            </div>
                                            <div class="mb-3">
                                                <label for="applicant_designation">{{__('form.particularofwblabeldesignation')}}</label>
                                                <input type="text" id="applicant_designation" name="applicant_designation" class="form-control" value="" disabled>
                                            </div>
                                            <div class="mb-3">
                                                <label for="applicant_department">{{__('form.particularofwblabeldepartment')}}</label>
                                                <input type="text" id="applicant_department" name="applicant_department" class="form-control" value="" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                            <div class="accordion-item custom-width-setting {{ $isDisabled ?  $isAssignedOfficer ? '' : 'hide' : 'hide'}}" id="suspect_info">
                                <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                                <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true" aria-controls="panelsStayOpen-collapseThree">
                                    <b>B. {{__('form.particularofsuspect')}}</b>
                                </button>
                                </h2>
                                <div class="accordion-collapse collapse show"  id="panelsStayOpen-collapseThree" aria-labelledby="panelsStayOpen-headingThree">
                                    <div class="accordion-body">
                                        <div class="suspect-container" id="suspect-container">
                                            <div class="suspect-wrapper hide" id="previous-suspect">
                                                <span class="suspect-individual-label">{{__('form.particularofsuspectindividual')}}</span>
                                                <div class="mb-3">
                                                    <label for="supect_name">{{__('form.particularofsuspectlabelname')}} </label>
                                                    <input type="text" id="suspect_name" name="suspect_name" class="form-control" value="" disabled> 
                                                </div>
                                                <div class="mb-3">
                                                    <label for="suspect_email_address">{{__('form.particularofsuspectlabelemail')}}</label>
                                                    <input type="email" id="suspect_email_address" name="suspect_email_address" class="form-control" value="" disabled>
                                                </div>
                                                <div class="mb-3">      
                                                    <label for="suspect_designation">{{__('form.particularofsuspectlabeldepartment')}}</label>
                                                    <input type="text" id="suspect_designation" name="suspect_designation" class="form-control" value="" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div> 
                        </div>
                     

                        <div class="acc-container" id="complaint-info-acc-container">
                            <div class="accordion-item custom-width-setting {{$isDisabled ? '' : 'hide'}}" id="complaint_info">
                                <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                                <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                    <b>C. {{__('form.particularofimproperconduct')}}</b>
                                </button>
                                </h2>
                                <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseFive" aria-labelledby="panelsStayOpen-headingFive">
                                    <div class="accordion-body">

                                        <div id="new-complaint-info" class="complaint-info">
                                            <div class="mb-3">
                                                <label for="complaint_improper_activity_date">{{__('form.particularofimproperconductdate')}} </label>
                                                <input type="text" id="complaint_improper_activity_date" name="complaint_improper_activity_date" class="form-control" value="" disabled>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_improper_activity_time">{{__('form.particularofimproperconducttime')}}  </label>
                                                <input type="text" id="complaint_improper_activity_time" name="complaint_improper_activity_time" class="form-control" value="" disabled>
                                            </div>

                                            <div class="complaint-country-container">
                                                <div class="mb-3 half-width">
                                                    <label for="complaint_improper_activity_country">{{__('form.country')}}</label>
                                                    <input type="text" id="complaint_improper_activity_country" name="complaint_improper_activity_country" class="form-control" value="" disabled>
                                                </div>
                                                <div class="mb-3 half-width">
                                                    <label for="complaint_improper_activity_other_country">{{__('form.othercountry')}}</label>
                                                    <input type="text" id="complaint_improper_activity_other_country" name="complaint_improper_activity_other_country" class="form-control" value="" disabled>
                                                </div>
                                            </div>

                                            <div class="complaint-state-container complaint-info" id="complaint-state-container">
                                                <div class="mb-3 col-3-width">
                                                    <label for="complaint_improper_activity_state">{{__('form.state')}}</label>
                                                    <input type="text" id="complaint_improper_activity_state" name="complaint_improper_activity_state" class="form-control" value="" disabled>
                                                </div>
                                                <div class="mb-3 col-3-width">
                                                    <label for="complaint_improper_activity_district">{{__('form.district')}}</label>
                                                    <input type="text" id="complaint_improper_activity_district" name="complaint_improper_activity_district" class="form-control" value="" disabled>
                                                </div>
                                                <div class="mb-3 col-3-width">
                                                    <label for="complaint_improper_activity_postcode">{{__('form.postcode')}}</label>
                                                    <input type="text" id="complaint_improper_activity_postcode" name="complaint_improper_activity_postcode" class="form-control" value="" disabled>
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="complaint_improper_activity_place">{{__('form.particularofimproperconductplace')}}</label>
                                                <input type="text" id="complaint_improper_activity_place" name="complaint_improper_activity_place" class="form-control" value="" disabled>
                                            </div>
                                            
                                            <div class="mb-3">
                                                <label for="complaint_improper_activity_detail">{{__('form.particularofimproperconductdetail')}}</label>
                                                <textarea id="complaint_improper_activity_detail" name="complaint_improper_activity_detail" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                        
                                            <div class="mb-3">
                                                <label class="supporting-documents-label" for="complaint_supporting_docs">{{__('form.supportingdocumentinput')}}</label> 
                                                <div id="field-container-doc">
                                                {{--  @foreach($fileInfo as $file)
                                                    <div><a href="{{base_url().$file['FileName']}}" target="_blank">{{!empty($file) ? $file['FileName'] : ''}}</a></div>
                                                    @endforeach--}}
                                                </div>
                                            </div>
                                        </div>


                                        <div id="old-complaint-info" class="complaint-info old hide" >
                                            <div class="mb-3">
                                                <label for="complaint_Q1">{{__('form.q1')}}</label>
                                                <textarea id="complaint_Q1" name="complaint_Q1" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_Q2">{{__('form.q2')}}</label>
                                                <textarea id="complaint_Q2" name="complaint_Q2" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_Q3">{{__('form.q3')}}</label>
                                                <textarea id="complaint_Q3" name="complaint_Q3" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_Q4">{{__('form.q4')}}</label>
                                                <textarea id="complaint_Q4" name="complaint_Q4" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_Q5">{{__('form.q5')}}</label>
                                                <textarea id="complaint_Q5" name="complaint_Q5" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_Q6">{{__('form.q6')}}</label>
                                                <textarea id="complaint_Q6" name="complaint_Q6" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_Q7">{{__('form.q7')}}</label>
                                                <textarea id="complaint_Q7" name="complaint_Q7" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="complaint_Q8">{{__('form.q8')}}</label>
                                                <textarea id="complaint_Q8" name="complaint_Q8" class="form-control textarea-setting" disabled></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <!--check role to show-->
                            <div class="accordion-item custom-width-setting" id="process_info">
                                <h2 class="accordion-header" id="panelsStayOpen-headingSeven">
                                <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSeven" aria-expanded="true" aria-controls="panelsStayOpen-collapseSeven">
                                    <b>{{__('form.activitylog')}}</b>
                                </button>
                                </h2>
                                <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseSeven" aria-labelledby="panelsStayOpen-headingSeven">
                                    <div class="accordion-body">
                                        <div class="mb-3">     
                                            <div class="table-responsive scroll">
                                                <table class="table" id="activity-table">
                                                    <thead class="activity-thead">
                                                        <tr>
                                                            <th>{{__('form.status')}}</th>
                                                            <th>{{__('form.remark')}}</th>
                                                            <th>{{__('form.updatedby')}}</th>
                                                            <th>{{__('form.date')}}</th>
                                                            <th>{{__('form.attachment')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="activity-tbody">
                                                
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item " id="secretarial_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingSix">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="true" aria-controls="panelsStayOpen-collapseSix">
                                <b>{{__('form.forsecretaryuse')}}</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseSix" aria-labelledby="panelsStayOpen-headingSix">
                                <div class="accordion-body">       
                                    <div>
                                        <div class="mb-3">
                                            <label for="status">{{__('form.casestatus')}}<span class="text-danger">*</span></label>
                                            <select class="form-select" id="status" name="status" aria-label="Default select example">
                                            @foreach($statusList as $status)   
                                            @if($status['slug'] > 2)
                                            <option value="{{!empty($status) ? $status['slug'] : ''}}">{{!empty($status) ? __('form.'.$status['name']) : ''}}</option>
                                            @endif
                                            @endforeach 
                                            </select>
                                        </div>

                                        
                                        <div class="mb-3">
                                            <label for="remark">{{__('form.remarkinput')}} <span class="text-danger">*</span></label>
                                            <textarea id="remark" name="remark" class="form-control" required></textarea>
                                            <div class="error-message hide" id="err-remark">
                                                Please select a valid state.
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <div id="field-container-doc">
                                                <label for="remark">{{__('form.supportingdocumentinput')}}</label>
                                                <div class="form-field">
                                                    <div class="file-upload-instruction-wrapper">
                                                        <!-- <span class="text-danger">*</span> -->
                                                        <div class="file-upload-instruction-group">
                                                            <span class="file-upload-instruction">{{__('form.particularofimproperconductdocumentinstrcution')}}</span>
                                                            <span class="file-upload-ins-sub ">/ {{__('form.particularofimproperconductdocumentinstrcution2')}}</span>
                                                            <span class="text-danger">*</span>
                                                        </div>
                                                        <div class="file-upload-instruction-group">
                                                            <span class="file-upload-instruction">{{__('form.particularofimproperconductdocumentinstrcution3')}}</span>
                                                            <span class="file-upload-ins-sub">/ {{__('form.particularofimproperconductdocumentinstrcution4')}}</span>
                                                            <span class="text-danger">*</span>
                                                        </div>
                                                       
                                                    </div>
                                                    <div class="file-container" id="file-container" style="padding-bottom:10px">
                                                        <div class="file-wrapper">
                                                            <input type="file" class="form-control supporting_doc" id="supporting_doc_0" onchange="checkUpload('supporting_doc_0')" name="supporting_docs[]" >
                                                            <button type="button" class="btn btn-primary btn-hrd-theme" onClick="appendFileInput()">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                    </div>

                    <button class="btn btn-primary d-none" type="button" id="loader-btn" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                    
                </form>

                <div id="loader" class="loader">
                    <div class="spinner-border" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>

                <div id="error" class="error hide">
                    {{__('form.somethingwentwrong')}}
                </div>
            </div>
            <div class="modal-footer">
               

                <div class="btn-wrapper btn-align-center">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('form.close')}}</button>
                    <form name="whistle-blower-personal-print-form" id="whistle-blower-personal-print-form" method="get" action="{{route('createPDFPersonal')}}">
                        <input type="hidden" name="printProfileId" id="printProfileId" value=""/>
                        {{--<button id="print" type="submit" onclick="" class="btn btn-primary btn-hrd-theme">Print Pdf</button>--}}
                    </form>
                    <button id="assign" type="submit" onclick="assign()" class="btn btn-primary btn-hrd-theme">{{__('form.assign')}}</button>
                    <button id="update" type="submit" onclick="update()" class="btn btn-primary btn-hrd-theme">{{__('form.update')}}</button>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>

        // $( document ).ready(function() {
        //     console.log( "ready!" );
        // });

        // $('#staticBackdrop').on('shown.bs.modal', function (e) {
        //     // do something...
        //      console.log( "ready!" );
        // })

        let isLoading = false;
        let isError = false;

        let totalAccumulatedAppendCount = 0;


        function checkUpload(targetElementId){
            // size: 5867185
            // type : "application/pdf"
            let availableFileTypes = ["pdf","jpg","jpeg","png","docx"];
            var file = $('#'+targetElementId).prop('files')
            // console.log('test file upload')
            // console.log(file)
            if(Object.keys(file).length != 0){
                let fileName = file[0].name;
                let fileNameSplit = fileName.split('.');
                let fileExt = fileNameSplit[1];
                // console.log('file info')
                // console.log(fileName)
                // console.log(fileExt)
                if(!availableFileTypes.includes(fileExt)){
                    Swal.fire({
                        icon: 'error',
                        title: '{{__("form.fileuploadtypeerrtitle")}}',
                        text: '{{__("form.fileuploadtypeerrtext")}}',
                        confirmButtonColor: '#002169',
                        cancelButtonColor: '#6c757d',
                        //footer: '<a href="">Why do I have this issue?</a>'
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            $('#'+targetElementId).val('')
                        } 
                    })
                }
                if(file[0].size > 10485760 ){  // 10mb
                    Swal.fire({
                        icon: 'error',
                        title: '{{__("form.fileuploadsizeerrtitle")}}',
                        text: '{{__("form.fileuploadsizeerrtext")}}',
                        confirmButtonColor: '#002169',
                        cancelButtonColor: '#6c757d',
                        //footer: '<a href="">Why do I have this issue?</a>'
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            $('#'+targetElementId).val('')
                        } 
                    })
                }
           }
        }

        function checkSectionByStatus(status,casePICEmail){
            let currentOfficer = $('#currentOfficer').val();
            if(status == 1 ){
                $('#applicant_info').addClass('hide');
                $('#suspect_info').addClass('hide');
                $('#complaint_info').removeClass('custom-width-setting');
                $('#secretarial_info').addClass('hide');
                $('#process_info').addClass('hide');
                $('#complaint-info-acc-container').removeClass('acc-container');
                $('#assign').removeClass('hide');
                $('#update').addClass('hide');
                $('#print').addClass('hide');
            }else if(status > 1 && status < 4){
                // console.log('test')
                // console.log(currentOfficer, casePICEmail)
                $('#applicant_info').removeClass('hide');
                $('#suspect_info').removeClass('hide');
                $('#process_info').removeClass('hide');
                $('#complaint_info').addClass('custom-width-setting');
                $('#complaint-info-acc-container').addClass('acc-container');
                // if(currentOfficer == casePICEmail){
                //     $('#secretarial_info').removeClass('hide');
                //     $('#assign').addClass('hide');
                //     $('#update').removeClass('hide');
                //     $('#print').removeClass('hide');
                // }else{
                //     $('#secretarial_info').addClass('hide');
                //     $('#assign').addClass('hide');
                //     $('#update').addClass('hide');
                //     $('#print').addClass('hide');
                // } 
                $('#secretarial_info').removeClass('hide');
                $('#assign').addClass('hide');
                $('#update').removeClass('hide');
                $('#print').removeClass('hide');
            }else if(status == 4 || status == 99){
                $('#applicant_info').removeClass('hide');
                $('#suspect_info').removeClass('hide');
                $('#secretarial_info').addClass('hide');
                $('#assign').addClass('hide');
                $('#update').addClass('hide');
                $('#complaint-info-acc-container').addClass('acc-container');
                $('#process_info').removeClass('hide');
                $('#complaint_info').addClass('custom-width-setting');
                if(currentOfficer == casePICEmail){
                    $('#print').removeClass('hide')
                }else{
                    $('#print').addClass('hide')
                }
                
            }

        }

        function setDisclosureEmails(disclosureEmails, disclosureEmailGroup){    
            if(disclosureEmailGroup == null){

            }else{
               if(disclosureEmailGroup.length > 0){
                    $('#disclosure-email-container').empty();
                    for(let a=0; a < disclosureEmailGroup.length; a++){
                        $('#disclosure-email-container').append(`<div><span>${disclosureEmailGroup[a].email}</span></div>`);
                    }
               }   
            }
            // if(disclosureEmails.length > 0){
            //     $('#disclosure-email-container').empty();
            //     for(let a=0; a < disclosureEmails.length; a++){
            //         $('#disclosure-email-container').append(`<div><span>${disclosureEmails[a]}</span></div>`);
            //     }
            // }
            
        }

        function setApplicantInfo(applicantInfo){
            const {name = '', email = '', mobile = '', designation = '', department = ''} = applicantInfo;  
            let key = '#applicant_';
            $('#applicantName').val(name);
            $('#applicantEmail').val(email);
            $(key+'name').val(name);
            $(key+'email_address').val(email);
            $(key+'mobile_phone_no').val(mobile);
            $(key+'designation').val(designation);
            $(key+'department').val(department);
            
        }


        function setSuspectInfo(suspectInfo,applicantInfo){
            let count = 0;
            let {ids = '', list = []} = suspectInfo;
            let {suspect_name : app_suspect_name = '', suspect_email : app_suspect_email = '', suspect_designation : app_suspect_designation = ''} = applicantInfo;
            
            if(list.length > 0){
                $('#suspectId').val(ids);
                $('#suspect-container').empty();
                $('#previous-suspect').addClass('hide');
                for(let i =0 ; i< list.length; i++){
                    const {suspect_name = '', suspect_email = '', suspect_agency = '', suspect_relationship = ''} = list[i];
                    count ++;
                    $('#suspect-container').append(`<div class="suspect-wrapper">
                        <span class="suspect-individual-label">{{__('form.particularofsuspectindividual')}} ${count}</span>
                        <div class="mb-3">
                            <label for="supect_name">{{__('form.particularofsuspectlabelname')}} </label>
                            <input type="text" id="suspect_name_${count}" name="suspect_name" class="form-control" value="${suspect_name}" disabled> 
                        </div>
                        <div class="mb-3">
                            <label for="suspect_email_address">{{__('form.particularofsuspectlabelemail')}}</label>
                            <input type="email" id="suspect_email_address_${count}" name="suspect_email_address" class="form-control" value="${suspect_email}" disabled>
                        </div>
                        <div class="mb-3">
                            <label for="suspect_department">{{__('form.particularofsuspectlabeldepartment')}}</label>
                            <input type="text" id="suspect_department_${count}" name="suspect_department" class="form-control" value="${suspect_agency}" disabled>
                        </div>
                        <div class="mb-3">      
                            <label for="suspect_relationship">{{__('form.particularofsuspectlabelrelationship')}}</label>
                            <input type="text" id="suspect_relationship_${count}" name="suspect_relationship" class="form-control" value="${suspect_relationship}" disabled>
                        </div>
                    </div>`);
                }
            }else{

                $('#previous-suspect').removeClass('hide');
                $('#suspect_name').val(app_suspect_name);
                $('#suspect_email_address').val(app_suspect_email);
                $('#suspect_designation').val(app_suspect_designation);
                               
            }
        }


        function setOldComplaintInfo(complaintInfo){
            let {q1 = '', q2 = '', q3 = '', q4 = '', q5 = '', q6 = '', q7 = '', q8 = ''} = complaintInfo;
            $('#old-complaint-info').removeClass('hide');
            $('#new-complaint-info').addClass('hide');
            $('#complaint_Q1').text(q1);
            $('#complaint_Q2').text(q2);
            $('#complaint_Q3').text(q3);
            $('#complaint_Q4').text(q4);
            $('#complaint_Q5').text(q5);
            $('#complaint_Q6').text(q6);
            $('#complaint_Q7').text(q7);
            $('#complaint_Q8').text(q8);
        }

        function setNewComplaintInfo(complaintInfo, countryList, stateList){

            let { ComplaintID = '', ImproperActivityDate = '', ImproperActivityTime = '', ImproperActivityPlace = '', ImproperActivityDetail = '',
                ImproperActivityCountry = '' , ImproperActivityOtherCountry = null, ImproperActivityState = null, ImproperActivityDistrict = '', 
                ImproperActivityPostcode = ''
            } = complaintInfo;
        
            $('#new-complaint-info').removeClass('hide');
            $('#old-complaint-info').addClass('hide');
            let key = '#complaint_improper_activity_';
            $('#complaintId').val(ComplaintID);
            $(key+'date').val(ImproperActivityDate);
            $(key+'time').val(ImproperActivityTime);
            $(key+'place').val(ImproperActivityPlace);
            $(key+'detail').val(ImproperActivityDetail);
            if(ImproperActivityCountry == 'MAL' && ImproperActivityCountry !== null ){
                $('#complaint-state-container').removeClass('hide');
                $(key+'country').val(countryList[ImproperActivityCountry]);    
            }else{
                $('#complaint-state-container').addClass('hide');
                $(key+'country').val(ImproperActivityCountry);
            }

            $(key+'other_country').val(ImproperActivityOtherCountry);
            
            if( ImproperActivityState !== null ){
                let {name : status_name = ''} = stateList[ImproperActivityState]; 
                $(key+'state').val(status_name);
            }else{
                $(key+'state').val(ImproperActivityState);
            }
            $(key+'district').val(ImproperActivityDistrict);
            $(key+'postcode').val(ImproperActivityPostcode);
        }

        function setComplaintInfo(complaintInfo, countryList, stateList){
            let {q1 = null, } = complaintInfo;
            if( q1 == null){
                setNewComplaintInfo(complaintInfo, countryList, stateList);
            }else{
                
                setOldComplaintInfo(complaintInfo);
            }
        }

    
        function setFileInfo(fileInfo,complaintInfo){
            if(fileInfo.length > 0){
                $('#field-container-doc').empty();
                for(let i=0; i<fileInfo.length; i++){
                    let {Type = '', FilePath = '', FileName = ''} = fileInfo[i];
                    if(Type == 1){
                        $('#field-container-doc').append(`
                            <div><a href="<?=url('').'/wbattachments/'?>${FilePath}" target="_blank">${FileName}</a></div>
                        `);
                    }
                }
            }
        }

        // function openTab(url){
        //     window.open(url,'_blank');
        // }    
        
        
        function selectCase(status){
            status = status != null ? parseInt(status) : '';
            let item = '';
            switch(status){
                case 1 : 
                    item = `{{__('form.casereceive')}}`;
                    break;
                case 2 : 
                    item = `{{__('form.wbcommittee')}}`;
                    break;
                case 3 : 
                    item = `{{__('form.investigation')}}`;
                    break;
                case 4 : 
                    item = `{{__('form.caseclose')}}`;
                    break;
                case 99 : 
                    item = `{{__('form.nfa')}}`;
                    break;
                default:
                    item = '';
            }
            return item;
        }

        function getMonthName(month){
            let monthNameList = {
                1 : 'Jan',
                2 : 'Feb',
                3 : 'Mac',
                4 : 'Apr',
                5 : 'May',
                6 : 'June',
                7 : 'July',
                8 : 'Aug',
                9 : 'Sep',
                10 : 'Oct',
                11 : 'Nov',
                12 : 'Dec',
            };

            let selectedMonthName = '';
            if(month != ''){
                selectedMonthName = monthNameList[month+1];
            }
            return selectedMonthName;
        }


        function processDate(date){
            let datedate = new Date(date);
            let checkDateIsValid = datedate != null || datedate != undefined || datedate != '' ? true : false;
            let formatedDate = '';
            if(checkDateIsValid){
                let getYear = datedate.getFullYear(); 
                let getMonth = datedate.getMonth();
                let monthName = getMonthName(getMonth);
                let getDate = datedate.getDate();
                formatedDate = getDate +' '+monthName+' '+getYear;
            }
            return formatedDate;
        }

        function setActivityInfo(activityInfo, statusList){
            $('#activity-table > tbody:last-child').empty();
            if(activityInfo.length > 0){
                activityInfo.map((acInfo)=>{
                    let data = [];
                    let { activity_id:act_activity_id = '', upload_activity = [], activity_status = '', user = {}, remark = '', activity_datetime = ''} = acInfo;
                    
                    upload_activity.map((uploadAvt)=>{
                        let { activity_id = '', FileName = ''} = uploadAvt;
                        let url = `<?=url('').'/wbattachments/'?>${activity_id+`/`+FileName}`;
                        data.push(
                            `<div><a href="${url}" target="_blank">${FileName}</a></div>`
                        );
                        // $('#attachment-'+uploadAvt.activity_id).append(`
                        //     <div><a href="${url}" target="_blank">${uploadAvt.FileName}</a></div>
                        // `);
                    })
                    let statusItem = selectCase(activity_status);
                    let {fullname = ''} = user;
                    let formatedDate = ''; 
                    //formatedDate =  processDate(acInfo.activity_datetime);
            
                    $('#activity-table > tbody:last-child').append(`<tr>
                        <td>${statusItem}</td>
                        <td>${remark}</td>
                        <td>${fullname}</td>
                        <td>${activity_datetime}</td>
                        <td><div id="attachment-${act_activity_id}">${data}</div></td>
                    </tr>`);
                })
                
            }
            
        }

        function setInputValue(data, statusList, countryList, stateList) {
            let { profile_id = '', case_id = '', applicant_info = {}, file_info = [], disclosure_emails = [], disclosure_email_group = [],
            complaint_info = {}, suspect_info = [], activity_info = [], status = '',
            } = data;
            $('#caseIdText').text(case_id);
            $('#caseId').val(case_id);
            $('#profileId').val(profile_id);
            $('#printProfileId').val(profile_id);
            $('#currentStatus').val(status);
            setApplicantInfo(applicant_info);
            setComplaintInfo(complaint_info, countryList, stateList);
            setSuspectInfo(suspect_info, applicant_info);
            setDisclosureEmails(disclosure_emails, disclosure_email_group);
            setFileInfo(file_info,complaint_info);
            setActivityInfo(activity_info, statusList);

        }

        function loadViewData(pId){
            var url = "{{ route('complaintUpdateView', ":pId") }}";
            url = url.replace(':pId', pId);
            isLoading = true;
            $.ajax({
                type:'get',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                processData: false, // Prevent jQuery from converting the data to a string
                contentType: false, 
                    //data : formData,
                    success: function(response){
                        if(Object.keys(response).length !== 0){
                            let {data = {}, state_list = [], country_list = [], status_list = []} = response;
                            // console.log('data')
                            // console.log(data)
                            setInputValue(data, status_list, country_list, state_list);
                            isLoading = false;
                            isError = false;
                            $('#loader').addClass('hide');
                            $('#whistle-blower-form').removeClass('hide');
                            $('#error').addClass('hide');
                        }
                       
                },
                error: function(_response){
                    window.setTimeout(function () {
                        Swal.fire('Unsuccessful!', '', 'error');
                        //console.log('error')
                        isLoading = false;
                        isError = true;
                        $('#loader').addClass('hide');
                        $('#whistle-blower-form').addClass('hide');
                        $('#error').removeClass('hide');
                    }, 2000);
                }
            });
        }


        async function testOpen(pId, status, casePICEmail){
            let profileId = pId;
            $('#err-remark').addClass('hide');
            $('#remark').removeClass('input-error');
            checkSectionByStatus(status,casePICEmail);
            await loadViewData(pId);
        }

        function appendFileInput () {
            let caseId = $('#caseId').val();
            totalAccumulatedAppendCount++;
            $('#file-container').append(`
            <div class="file-wrapper" data-count=${totalAccumulatedAppendCount} id="file_wrapper_${totalAccumulatedAppendCount}">
                <input type="file" class="form-control supporting_doc" id="supporting_doc_${totalAccumulatedAppendCount}" onchange="checkUpload('supporting_doc_${totalAccumulatedAppendCount}')" name="supporting_docs[]" required>
                <button type="button" class="btn btn-primary btn-hrd-theme" onClick="removeSelectedFileInput('file_wrapper_${totalAccumulatedAppendCount}')">-</button>
            </div>
            `);
        }

        function removeSelectedFileInput(selectorIdToRemove){
            $('#'+selectorIdToRemove).remove();
        }


        function validateUpdateInput(){
            let remark = $('#remark').val();
            let validate = false;
            if(remark == ''){
                $('#remark').addClass('input-error');
                $('#err-remark').removeClass('hide');
                $('#err-remark').text('{{__("form.provideremark")}}');
            }else{
                $('#err-remark').addClass('hide');
                $('#remark').removeClass('input-error');
                validate = true;
            }
            return validate;
        }

        function assign(){
            const form = document.getElementById('whistle-blower-form');
            var formData = new FormData(form);
            Swal.fire({
                title: '{{__("form.confirmassign")}}?',
                showCancelButton: true,
                confirmButtonText: '{{__("form.confirm")}}',
                denyButtonText: '{{__("form.cancel")}}',
                confirmButtonColor: '#002169',
                cancelButtonColor: '#6c757d',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processAssign')}}",
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                        //contentType: 'multipart/form-data',
                            data : formData,
                            success: function(response){

                                if(response.data == 1)
                                {
                                    Swal.fire({
                                        title: '{{__("form.statusupdated")}}!',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#002169',
                                        cancelButtonColor: '#002169',
                                        }).then((result) => {
                                        /* Read more about isConfirmed, isDenied below */
                                        if (result.isConfirmed) {
                                            location.reload();
                                        }
                                    })
                                }
                            },
                            error: function(_response){
                                window.setTimeout(function () {
                                    Swal.fire('{{__("form.unsuccessful")}}!', '', 'error');

                                }, 2000);
                            }
                        });

                } else if (result.isCanceled) {
                    Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                }
            })
        }


        function update(){
            const form = document.getElementById('whistle-blower-form');
            var formData = new FormData(form);
            let isValidateSuccess = validateUpdateInput();
            
            if(isValidateSuccess){
                Swal.fire({
                        title: '{{__("form.confirmupdate")}}?',
                        showCancelButton: true,
                        confirmButtonText: '{{__("form.confirm")}}',
                        denyButtonText: '{{__("form.cancel")}}',
                        confirmButtonColor: '#002169',
                        cancelButtonColor: '#6c757d',
                    }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {

                        $.ajax({
                            type:'post',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{route('processUpdate')}}",
                            //contentType: 'multipart/form-data',
                            processData: false, // Prevent jQuery from converting the data to a string
                            contentType: false, 
                                data : formData,
                                success: function(response){
                                if(response.data == 1)
                                {
                                    Swal.fire({
                                        title: '{{__("form.statusupdated")}}!',
                                        confirmButtonText: '{{__("form.confirm")}}',
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#002169',
                                        cancelButtonColor: '#002169',
                                        }).then((result) => {
                                        /* Read more about isConfirmed, isDenied below */
                                        if (result.isConfirmed) {
                                            location.reload();
                                        }
                                    })
                                }
                            },
                            error: function(_response){
                                window.setTimeout(function () {
                                    Swal.fire('{{__("form.unsuccessful")}}!', '', 'error');

                                }, 2000);
                            }
                        });

                    } else if (result.isCanceled) {
                        Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                    }
                })
            }
            

           
        }

</script>