<style>
	footer{
		padding : 0;
		text-align : center;
		/* position : absolute;
		width : 100%;
		bottom : 0; */
	}

	.footer-container{
		background-color : #11132b;
		color : #FFFFFF;
		/* position: absolute;
   	 	width: 100%;
		bottom : 0; */
	}

	.footer-wrapper{
		padding : 1rem;
	}

	.footer-item{
		font-size : 12px;	
	}

	.tp-offer-container{
		/* display : flex;
		align-items : center; */
	}

	.tp-offer-text{
		padding : 1rem 0;
		color : #FFFFFF;
	}

	.logo-wrapper{
		width : 100px;
		height : 100px;
		display: flex;
		align-items : center;
		/* height : 100px; */
	}

	.logo{
		width : 100%;
		height : auto;
	}

</style>
<div class="footer-container">
	<div class="" style="background: white;">

		<!-- @if($isPublic) -->
		<div class="tp-offer-container">
			<div class="tp-offer-text" style="color:#F04E23;font-weight: 600; font-size: 18px;">{{__('footer.offer_text')}}</div>
			<div class="tp-carousel-container">
				<div class="owl-carousel">
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/1Training-Worklife-Management-Sdn.-Bhd.-resized.png') !!}" alt="logo" class="centerz" >
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Kumpulan-Aplikasi-Sempurna-Sdn-Bhd-resized.png') !!}" alt="logo" class="centerz" >
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/UNIVERSITI-TEKNOLOGI-PETRONAS-resized.png') !!}" alt="logo" class="centerz" >
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/PPL-PERFORMANCE-SDN-BHD-resized.jpg') !!}" alt="logo" class="centerz" >
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/HUN-YUAN-TRAINING-AND-WELLNESS-CENTRE-SDN-BHD-resized.png') !!}" alt="logo" class="centerz">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/ATCEN-SDN-BHD-resized.png') !!}" alt="logo" class="centerz">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/GRADIENTX-SDN-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Clarus-Consulting-Sdn-Bhd-resixed.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Quandatics-Academy-Sdn-Bhd-resized.jpg') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Junzo-Sdn-Bhd-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Quest-Learning-Sdn-Bhd-resized.jpg') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Vision-2-Business-(Malaysia)-Sdn-Bhd-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/MAA-TRAINING-ACADEMY-PLT-resized.jpg') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/MYWAVE-SDN-BHD-resized.jpg') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/COMPAS-MTC-(MALAYSIA)-SDN-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/MMT-UNIVERSAL-ACADEMY-SDN.BHD-resized.jpg') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Iconic-Training-Solutions-Sdn.-Bhd.-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Laisha-Active-Life-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/MIS-ACADEMY-(M)-SDN.-BHD-resized.jpg') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/COMPAS-MTC-(MALAYSIA)-SDN-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/IKLIM-PRIMA-SDN-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/FOCUS-LEARNING-CONSULTING-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/UPUM-RESOURCES-SDN-BHD-resied.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/GLOBAL-TRAINING-NETWORK-ALLIANCES-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Engage-Corporate-Training-&-Consultancy-Sdn.-Bhd-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/TNG-Digital-Sdn-Bhd-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/GEMRAIN-CONSULTING-SDN-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/TVET-AUSTRALASIA-SDN.-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Sunago-Education-resied.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/DRI-MALAYSIA-SDN-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/Instep-Leaning-Asia-Sdn-Bhd-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/TALENT-&-HUMAN-SOLUTIONS-SDN-BHD-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>

					<div class="logo-wrapper">
						<img class="logo" src="{!! asset('assets/img/tp-logo/THAMES-OXFORD-ACADEMY-PLt-resized.png') !!}" alt="logo" class="centerz" height="50px">
					</div>
					
				</div>
			</div>
		</div>
		<!-- @endif -->
	</div>
	<div class="container footer-wrapper">

		<div class="footer-item">{{__('footer.footerdescription1')}}</div>
		<div class="footer-item">{{__('footer.footerdescription2',['year'=>date('Y')])}}</div>
		<!-- <div>Copyright © {{date('Y')}} HRD Corp All rights reserved | Safety and Privacy Policy</div> -->
	</div>
</div>

<script>
// $(document).ready(function(){
//   $(".owl-carousel").owlCarousel();
// });

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	//nav : false,
	navText: ['', ''],
	autoplay:true,
	autoplayTimeout:2000,
	autoplayHoverPause:true,
    responsive:{
        400:{
            items:3,
            nav:true
        },
        850:{
            items:5,
            nav:false
        },
        1000:{
            items:8,
            nav:true,
            loop:false
        }
    }
})

</script>