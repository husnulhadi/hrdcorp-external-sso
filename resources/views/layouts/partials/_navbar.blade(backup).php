<?php

    $domainpath = config('server.domain.path');
    if (Request::is('login') || Request::is('register') || Request::is('reset-password') || Request::is('reset-password') || Request::is('set-password/*') || Request::is('new-password/*') || Request::is('email_verification/*')){

    }else{
      //$user = DB::table('users')->where('id','=',auth()->user()->id)->first();
    }
?>


  <!-- Main navbar -->

  @if (Request::is('login') || Request::is('register') || Request::is('reset-password') || Request::is('set-password/*') || Request::is('new-password/*') || Request::is('email_verification/*'))
  <div class="navbar navbar-expand-md">
    @else
    <div class="navbar navbar-expand-md navbar-dark">
    @endif
    
    <div class="navbar-brand">
      <a href="{{$domainpath}}/dashboard" class="d-inline-block">
        <img src="{{$domainpath}}/assets/img/icons/spms_logo.png" alt="">
      </a>
    </div>

    <div class="d-md-none">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
        <i class="icon-tree5"></i>
      </button>
      <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
        <i class="icon-paragraph-justify3"></i>
      </button>
    </div>
 
    <div class="collapse navbar-collapse" id="navbar-mobile">
      @if (Request::is('login') || Request::is('register') || Request::is('reset-password') || Request::is('set-password/*') || Request::is('new-password/*') || Request::is('email_verification/*'))
      @else
      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
            <i class="icon-paragraph-justify3"></i>
          </a>
        </li>
      </ul>
      @endif

      <span class="navbar-text ml-md-3 mr-md-auto">
        <!-- <span class="badge bg-success">Online</span> -->
      </span>

      <ul class="navbar-nav">

        <!-- <li class="nav-item dropdown">
          <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
            @if (App::isLocale('en'))
              <img src="{{$domainpath}}/assets/img/flags/en.png" class="position-left" alt="">
            @else
              <img src="{{$domainpath}}/assets/img/flags/my.png" class="position-left" alt="">
            @endif
          </a>
          
          <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
            <div class="dropdown-content-header">
              <span class="font-weight-semibold">{{trans('roles.list.language')}}</span>
            </div>

            <div class="dropdown-content-body dropdown-scrollable">
              <ul class="media-list">
                

                <li class="media">
                  <div class="mr-3">
                    <img src="{{$domainpath}}/assets/img/flags/my.png" alt="">
                  </div>
                  <div class="media-body">
                    <div class="media-title">
                      <a href="{{ URL::to('lang') }}/my">
                        <span class="font-weight-semibold">Bahasa Malaysia</span>
                      </a>
                    </div>
                  </div>
                </li>
                <li class="media">
                  <div class="mr-3">
                    <img src="{{$domainpath}}/assets/img/flags/en.png" alt="">
                  </div>
                  <div class="media-body">
                    <div class="media-title">
                      <a href="{{ URL::to('lang') }}/en">
                        <span class="font-weight-semibold">English</span>
                        
                      </a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </li> -->

      
        @if (Request::is('login') || Request::is('register') || Request::is('reset-password') || Request::is('set-password/*') || Request::is('new-password/*') || Request::is('email_verification/*'))
        @else
        <li class="nav-item dropdown dropdown-user">
          <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
            {{--<span class="font-weight-semibold">{{ auth()->user()->name }}</span>--}}
            <span>asasas</span>
          </a>

          <div class="dropdown-menu dropdown-menu-right">
           {{-- <a class="dropdown-item font-weight-semibold" href="{{ route('showprofile', auth()->user()->id) }}"><i class="icon-user"></i>{{trans('roles.list.view_profile')}}</a>--}}
           <a href="#">Show Profile</a> 
           <div class="dropdown-divider"></div>
            <a href="{{ route('logout') }}" class="dropdown-item font-weight-semibold"><i class="icon-switch2"></i> {{trans('roles.list.log_out')}}</a>
          </div>
        </li>
        @endif
      </ul>
    </div>

  </div>
   <div class="progress-line " id="mainProgressBar" style="display: none" ></div>
   @if (Request::is('login') || Request::is('register') || Request::is('reset-password') || Request::is('set-password/*') || Request::is('new-password/*') || Request::is('email_verification/*'))
    @else
    @endif
  <!-- /main navbar -->