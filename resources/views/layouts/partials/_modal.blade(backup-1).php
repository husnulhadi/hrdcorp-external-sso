
@php 

$isDisabled = !empty($user) ? true : false ; 
$fInfo = !empty($formInfo) ? $formInfo : [];
$caseId = !empty($formInfo) ? $fInfo['CaseID'] : '';
$caseStatus = !empty($formInfo) ? $fInfo['Status'] : '';

$applicantInfo = !empty($fInfo) ? $fInfo['applicant_info'] : [];

$suspectInfoInfo = !empty($fInfo) ? $fInfo['suspect_info'] : [];
$suspectInfo = !empty($suspectInfoInfo) ? $suspectInfoInfo['list'] : [];
$suspectIds = !empty($suspectInfoInfo) ? $suspectInfoInfo['ids']: '';

//$witnessInfo = !empty($fInfo) ? $fInfo['witness_info'] : [];
$complaintInfo = !empty($fInfo) ? $fInfo['complaint_info'] : [];

$fileInfo = !empty($fInfo) ? $fInfo['file_info'] : [];
$officerList = config('custom.wb.officer.users');
$individualCount = 1;

$formInfoPIC = !empty($formInfo) ? $formInfo['pic'] : ''; 

$disclosureEmails = !empty($formInfo) ? $formInfo['disclosure_emails'] : '';
$statusList = config('custom.wb.officer.status');

$officerInfo = !empty($user) ? $user : [];
$isAssignedOfficer = !empty($officerInfo) ?  true : false; 


@endphp

<style>
.suspect-individual-label{
    font-style : italic;
}

</style>


<div class="modal fade" id="staticBackdrop-{{$modalId}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Case : {{$caseId}} {{!empty($fInfo) ? $fInfo['ProfileID'] : ''}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">            
                <form name="whistle-blower-form" id="whistle-blower-form-{{$caseId}}" action="{{route('processUpdate')}}" method="post" enctype="multipart/form-data" novalidate>
                    {{ csrf_field() }}
                    <input type="text" id="testId-{{$caseId}}" value="{{$testId}}"/>
                    <input type="text" value="{{$caseId}}"/>
                    <input type="text" id="profileId-{{$caseId}}" name="profileId" value="{{!empty($fInfo) ? $fInfo['ProfileID'] : ''}}"/>
                    <input type="hidden" name="complaintId" value="{{!empty($complaintInfo) ? $complaintInfo['ComplaintID'] : ''}}"/>
                    <input type="hidden" name="suspectId" value="{{$suspectIds}}"/>  
                    <input type="hidden" name="officerId" value="{{!empty($officerInfo) ? $officerInfo['id'] : ''}}"/> 
                   
                    <div class="accordion" id="accordionPanelsStayOpenExample">
                        <div class="accordion-item " id="disclosure_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                <b>Disclosure</b>
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
                                <div class="accordion-body">
                                    <div>
                                        @foreach($disclosureEmails as $disEmail)
                                            <div><span> {{!empty($disEmail) ? $disEmail : ''}}</span></div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>


                        @if($caseStatus != 1)
                       <div class="accordion-item " id="applicant_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                                <b>Applicant Information</b>
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                                <div class="accordion-body">
                                    <div>
                                        <div class="mb-3">
                                            <label for="applicant_name">Name </label>
                                            <input type="text" id="applicant_name" name="applicant_name" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['name'] : ''}}" disabled> 
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label for="applicant_mobile_phone_no">Mobile Phone No.</label>
                                            <input type="number" id="applicant_mobile_phone_no" name="applicant_mobile_phone_no" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['mobile'] : ''}}" disabled/>
                                        </div>
                                        
                                        <div class="mb-3">
                                            <label for="applicant_email_address">E-Mail</label>
                                            <input type="email" id="applicant_email_address" name="applicant_email_address" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['email'] : ''}}" disabled>  
                                        </div>
                                        <div class="mb-3">
                                            <label for="applicant_designation">Designation</label>
                                            <input type="text" id="applicant_designation" name="applicant_designation" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['designation'] : ''}}" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="applicant_department">Department/Agency</label>
                                            <input type="text" id="applicant_department" name="applicant_department" class="form-control" value="{{ !empty($applicantInfo) ? $applicantInfo['department'] : ''}}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif 
                        
                        @if($caseStatus != 1)
                       <div class="accordion-item {{ $isDisabled ?  $isAssignedOfficer ? '' : 'hide' : 'hide'}}" id="suspect_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true" aria-controls="panelsStayOpen-collapseThree">
                                <b>Suspect Information</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show"  id="panelsStayOpen-collapseThree" aria-labelledby="panelsStayOpen-headingThree">
                                <div class="accordion-body">
                                            
                                    @foreach($suspectInfo as $suspect)
                                    <div>
                                        <span class="suspect-individual-label">Individual {{$individualCount++}}</span>
                                        <div class="mb-3">
                                            <label for="supect_name">Name </label>
                                            <input type="text" id="supect_name" name="suspect_name" class="form-control" value="{{ !empty($suspect) ? $suspect['suspect_name'] : '' }}" disabled> 
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label for="suspect_email_address">E-Mail</label>
                                            <input type="email" id="suspect_email_address" name="suspect_email_address" class="form-control" value="{{ !empty($suspect) ? $suspect['suspect_email'] : '' }}" disabled>
                                        </div>
                                        <div class="mb-3">      
                                            <label for="suspect_designation">Relationship</label>
                                            <input type="text" id="suspect_designation" name="suspect_designation" class="form-control" value="{{!empty($suspect) ? $suspect['suspect_relationship'] : ''}}" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="suspect_department">Department/Agency</label>
                                            <input type="text" id="suspect_department" name="suspect_department" class="form-control" value="{{ !empty($suspect) ? $suspect['suspect_agency'] : ''}}" disabled>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div> 
                        @endif

                
                        <div class="accordion-item {{$isDisabled ? '' : 'hide'}}" id="complaint_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                <b>B.Particulars of the Suspect</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseFive" aria-labelledby="panelsStayOpen-headingFive">
                                <div class="accordion-body">
                                    <div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_date">Date </label>
                                            <input type="text" id="complaint_improper_activity_date" name="complaint_improper_activity_date" class="form-control" value="{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityDate'] : ''}}" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_time">Time </label>
                                            <input type="text" id="complaint_improper_activity_time" name="complaint_improper_activity_time" class="form-control" value="{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityTime'] : ''}}" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_place">Place</label>
                                            <input type="text" id="complaint_improper_activity_place" name="complaint_improper_activity_place" class="form-control" value="{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityPlace'] : ''}}" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_detail">Details</label>
                                            <textarea id="complaint_improper_activity_detail" name="complaint_improper_activity_detail" class="form-control" disabled>{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityDetail'] : ''}}</textarea>
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label class="supporting-documents-label" for="complaint_supporting_docs">If you have any supporting documents, please upload here. </label> 
                                            <div id="field-container-doc">
                                                @foreach($fileInfo as $file)
                                                <div><a href="#">{{!empty($file) ? $file['FileName'] : ''}}</a></div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  

                        

                        @if($caseStatus > 1 && $caseStatus < 4)
                        <!--check role to show-->
                        <div class="accordion-item " id="secretary_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingSix">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="true" aria-controls="panelsStayOpen-collapseSix">
                                <b>For Secretary Use</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseSix" aria-labelledby="panelsStayOpen-headingSix">
                                <div class="accordion-body">
                                    <div>
                                        <div class="mb-3">
                                            <label for="complaint_misconduct_activity">Case Status<span class="text-danger">*</span></label>
                                            <select class="form-select" name="status" aria-label="Default select example">
                                            @foreach($statusList as $status)   
                                            <option value="{{!empty($status) ? $status['slug'] : ''}}">{{!empty($status) ? $status['name'] : ''}}</option>
                                            @endforeach 
                                            </select>
                                        </div>

                                        
                                        <div class="mb-3">
                                            <label for="remark">Remark <span class="text-danger">*</span></label>
                                            <textarea id="complaint_person_conduct" name="remark" class="form-control" required></textarea>
                                            <div class="error-message hide" id="err-complaint_person_conduct">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div id="field-container-doc">
                                                <div class="form-field">
                                                    <div class="file-container" id="file-container-{{$caseId}}" style="padding-bottom:10px">
                                                        <div class="file-wrapper">
                                                            <input type="file" class="form-control supporting_doc" id="supporting_doc_{{$caseId}}_0" name="supporting_docs[]" >
                                                            <button type="button" class="btn btn-primary" onClick="appendFileInput()">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        @endif

                    
                    </div>

                    <button class="btn btn-primary d-none" type="button" id="loader-btn" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                
                @if($caseStatus == 1)
                <div class="mb-3 btn-align-center">
                    <button id="assign" type="submit" onclick="assign()" class="btn btn-primary">Assign/Pick</button>
                </div>
                @else if($caseStatus > 1 && $caseStatus < 4)
                <div class="mb-3 btn-align-center">
                    <button id="assign" type="submit" onclick="submit()" class="btn btn-primary">Update</button>
                </div>
                @endif

            </div>
        </div>
    </div>
</div> 

<script>

        var files_list = [];

        $( document ).ready(function() {
            console.log( "ready!" );
            console.log($('#testId-{{$caseId}}').val());
        });

        $('#staticBackdrop-{{$modalId}}').on('shown.bs.modal', function (e) {
            // do something...
            console.log( "ready!" );
            console.log($('#testId-{{$caseId}}').val());
        })
    
        // add form validation using Bootstrap
        // $('#whistle-blower-form').on('submit', function(event) {
        //     if ($('#whistle-blower-form')[0].checkValidity() === false) {
        //         event.preventDefault();
        //         event.stopPropagation();
        //     } else {
        //         // show the loader button
        //         //$("#book-flight-btn").addClass("d-none"); // hide the book flight button
        //         //$("#loader-btn").removeClass("d-none"); // show the loader button
        //     }
        //     $('#whistle-blower-form').addClass('was-validated');
        // });

        function appendFileInput () {
            let caseId = $('#caseId').val();
            let totalAccumulatedAppendCount = 0;
            totalAccumulatedAppendCount++;
         
            $('#file-container-'+caseId).append(`
            <div class="file-wrapper" data-count=${totalAccumulatedAppendCount} id="supporting_doc_${caseId}_${totalAccumulatedAppendCount}">
                <input type="file" class="form-control supporting_doc" name="supporting_docs[]" required>
                <button type="button" class="btn btn-primary" onClick="removeSelectedFileInput('supporting_doc_${totalAccumulatedAppendCount}')">-</button>
            </div>
            `);
        }

        function removeSelectedFileInput(selectorIdToRemove){
            $('#'+selectorIdToRemove).remove();
        }


        $('.supporting_doc').on('change', function() {
          
            // Get the selected files for each input
            var files = this.files;
            var files_list = [];
            // Do something with the selected files (e.g., display file names)
            var fileNames = [];
            for (var i = 0; i < files.length; i++) {
                fileNames.push(files[i].name);
                files_list.push(files);
            }
            

            console.log('test files')
            console.log(files)
            // Display the file names for the current input
            //console.log('Selected files for ' + this.name + ': ' + fileNames.join(', '));
            });

        function assign(){
            // let caseId = $('#caseId').val();
            // console.log(caseId)
            const form = document.getElementById('whistle-blower-form-{{$caseId}}');
            var formData = new FormData(form);
            formData.append('activity_status',2)
            formData.append('testId',$('#profileId-{{$caseId}}').val())
            Swal.fire({
                    title: 'Confirm Assign?',
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    denyButtonText: 'Cancel',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processAssign')}}",
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: 'Status Updated!',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false,
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                       // location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            window.setTimeout(function () {
                                Swal.fire('Unsuccessful!', '', 'error');

                            }, 2000);
                        }
                    });

                } else if (result.isCanceled) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
            

           
        }



        function update(){
            let caseId = $('#caseId').val();
            const form = document.getElementById('whistle-blower-form-'+caseId);
            var formData = new FormData(form);
            Swal.fire({
                    title: 'Confirm Assign?',
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    denyButtonText: 'Cancel',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processUpdate')}}",
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: 'Status Updated!',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false,
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                       // location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            window.setTimeout(function () {
                                Swal.fire('Unsuccessful!', '', 'error');

                            }, 2000);
                        }
                    });

                } else if (result.isCanceled) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
            

           
        }

</script>