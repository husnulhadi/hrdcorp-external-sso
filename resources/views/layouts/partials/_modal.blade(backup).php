
@php 


$statusList = config('custom.wb.officer.status');
$caseStatus = 1;
$officerInfo = !empty($user) ? $user : [];
$isAssignedOfficer = !empty($officerInfo) ?  true : false; 
$isDisabled = true;

@endphp

<style>
.suspect-individual-label{
    font-style : italic;
}

.file-wrapper{
    display:flex;
    column-gap : 0.5rem;
    padding : 0.5rem 0;
}

.suspect-container{
    height: 300px;
    overflow: auto;
}

.collapse{
    visibility : visible;
}

.btn-wrapper{
    display : flex;
    column-gap : 0.2rem;
}
</style>


<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Case : <span id="caseId"></span> </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"> 
                <form name="whistle-blower-form" id="whistle-blower-form" method="post"  novalidate="">
                    {{ csrf_field() }}
                    <input type="hidden" id="assign_status" name="assign_status" value="2"/>
                    <input type="hidden" id="profileId" name="profileId" value=""/>
                    <input type="hidden" id="applicantName" name="applicantName" value=""/>
                    <input type="hidden" id="complaintId" name="complaintId" value=""/>
                    <input type="hidden" id="suspectId" name="suspectId" value=""/>  
                    <input type="hidden" id="officerId" name="officerId" value="{{!empty($officerInfo) ? $officerInfo['id'] : ''}}"/> 
                    <input type="hidden" id="currentOfficer" name="currentOfficer" value="{{$currentOfficer}}"/>
                    <div class="accordion" id="accordionPanelsStayOpenExample">
                        
                        <div class="accordion-item " id="disclosure_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                <b>Type of Disclosure</b>
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
                                <div class="accordion-body">
                                    <div id="disclosure-email-container">
                                        <div><span> </span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        <div class="accordion-item " id="applicant_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                                <b>A. Particulars of the Whistleblower</b>
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                                <div class="accordion-body">
                                    <div>
                                        <div class="mb-3">
                                            <label for="applicant_name">Name </label>
                                            <input type="text" id="applicant_name" name="applicant_name" class="form-control" value="" disabled> 
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label for="applicant_mobile_phone_no">Mobile Phone No.</label>
                                            <input type="number" id="applicant_mobile_phone_no" name="applicant_mobile_phone_no" class="form-control" value="" disabled/>
                                        </div>
                                        
                                        <div class="mb-3">
                                            <label for="applicant_email_address">E-Mail</label>
                                            <input type="email" id="applicant_email_address" name="applicant_email_address" class="form-control" value="" disabled>  
                                        </div>
                                        <div class="mb-3">
                                            <label for="applicant_designation">Designation</label>
                                            <input type="text" id="applicant_designation" name="applicant_designation" class="form-control" value="" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="applicant_department">Department/Agency</label>
                                            <input type="text" id="applicant_department" name="applicant_department" class="form-control" value="" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  
                        <div class="accordion-item {{ $isDisabled ?  $isAssignedOfficer ? '' : 'hide' : 'hide'}}" id="suspect_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true" aria-controls="panelsStayOpen-collapseThree">
                                <b>B. Particulars of the Suspect</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show"  id="panelsStayOpen-collapseThree" aria-labelledby="panelsStayOpen-headingThree">
                                <div class="accordion-body">
                                    <div class="suspect-container" id="suspect-container">
                                        {{-- <div class="suspect-wrapper">
                                            <span class="suspect-individual-label">Individual</span>
                                            <div class="mb-3">
                                                <label for="supect_name">Name </label>
                                                <input type="text" id="supect_name" name="suspect_name" class="form-control" value="" disabled> 
                                            </div>
                                            <div class="mb-3">
                                                <label for="suspect_email_address">E-Mail</label>
                                                <input type="email" id="suspect_email_address" name="suspect_email_address" class="form-control" value="" disabled>
                                            </div>
                                            <div class="mb-3">      
                                                <label for="suspect_designation">Relationship</label>
                                                <input type="text" id="suspect_designation" name="suspect_designation" class="form-control" value="" disabled>
                                            </div>
                                            <div class="mb-3">
                                                <label for="suspect_department">Department/Agency</label>
                                                <input type="text" id="suspect_department" name="suspect_department" class="form-control" value="" disabled>
                                            </div>
                                        </div>--}}
                                    </div>
                                   
                                </div>
                            </div>
                        </div> 
                     

                
                        <div class="accordion-item {{$isDisabled ? '' : 'hide'}}" id="complaint_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                <b>C. Details of Improper Conduct</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseFive" aria-labelledby="panelsStayOpen-headingFive">
                                <div class="accordion-body">
                                    <div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_date">Date </label>
                                            <input type="text" id="complaint_improper_activity_date" name="complaint_improper_activity_date" class="form-control" value="" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_time">Time </label>
                                            <input type="text" id="complaint_improper_activity_time" name="complaint_improper_activity_time" class="form-control" value="" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_place">Place</label>
                                            <input type="text" id="complaint_improper_activity_place" name="complaint_improper_activity_place" class="form-control" value="" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_detail">Details</label>
                                            <textarea id="complaint_improper_activity_detail" name="complaint_improper_activity_detail" class="form-control" disabled></textarea>
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label class="supporting-documents-label" for="complaint_supporting_docs">If you have any supporting documents, please upload here </label> 
                                            <div id="field-container-doc">
                                              {{--  @foreach($fileInfo as $file)
                                                <div><a href="{{base_url().$file['FileName']}}" target="_blank">{{!empty($file) ? $file['FileName'] : ''}}</a></div>
                                                @endforeach--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  

                        <!--check role to show-->
                        <div class="accordion-item " id="secretarial_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingSix">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseSix" aria-expanded="true" aria-controls="panelsStayOpen-collapseSix">
                                <b>For Secretary Use</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseSix" aria-labelledby="panelsStayOpen-headingSix">
                                <div class="accordion-body">
                                    <div class="mb-3">
                                        <label for="complaint_misconduct_activity">Process History</label>
                                        
                                        <div class="table-responsive scroll">
                                            <table class="table" id="activity-table">
                                                <thead>
                                                    <tr>
                                                        <th>Status</th>
                                                        <th>Remark</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                
                                                    <!-- <tr>
                                                        <td>Pending</td>
                                                        <td></td>
                                                        <td>2023-01-01 15:00:00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Reject</td>
                                                        <td>Please reupdate</td>
                                                        <td>2023-01-02 15:00:00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Resubmit</td>
                                                        <td></td>
                                                        <td>2023-01-03 16:00:00</td>
                                                    </tr> -->
                                                  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                                   
                                    <div>
                                        <div class="mb-3">
                                            <label for="complaint_misconduct_activity">Case Status<span class="text-danger">*</span></label>
                                            <select class="form-select" name="status" aria-label="Default select example">
                                            @foreach($statusList as $status)   
                                            <option value="{{!empty($status) ? $status['slug'] : ''}}">{{!empty($status) ? $status['name'] : ''}}</option>
                                            @endforeach 
                                            </select>
                                        </div>

                                        
                                        <div class="mb-3">
                                            <label for="remark">Remark <span class="text-danger">*</span></label>
                                            <textarea id="complaint_person_conduct" name="remark" class="form-control" required></textarea>
                                            <div class="error-message hide" id="err-complaint_person_conduct">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <div id="field-container-doc">
                                                <div class="form-field">
                                                    <div class="file-container" id="file-container" style="padding-bottom:10px">
                                                        <div class="file-wrapper">
                                                            <input type="file" class="form-control supporting_doc" id="supporting_doc_0" name="supporting_docs[]" >
                                                            <button type="button" class="btn btn-primary" onClick="appendFileInput()">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                    </div>

                    <button class="btn btn-primary d-none" type="button" id="loader-btn" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                    
                </form>
            </div>
            <div class="modal-footer">
               

                <div class="btn-wrapper btn-align-center">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <form name="whistle-blower-personal-print-form" id="whistle-blower-personal-print-form" method="get" action="{{route('createPDFPersonal')}}">
                        <input type="hidden" name="printProfileId" id="printProfileId" value=""/>
                        <button id="print" type="submit" onclick="" class="btn btn-primary">Print Pdf</button>
                    </form>
                    <button id="assign" type="submit" onclick="assign()" class="btn btn-primary">Assign/Pick</button>
                    <button id="update" type="submit" onclick="update()" class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </div>
</div> 

<script>

        // $( document ).ready(function() {
        //     console.log( "ready!" );
        // });

        // $('#staticBackdrop').on('shown.bs.modal', function (e) {
        //     // do something...
        //      console.log( "ready!" );
        // })


        let totalAccumulatedAppendCount = 0;
        function checkSectionByStatus(status,casePICEmail){
            let currentOfficer = $('#currentOfficer').val();
            if(status == 1 ){
                $('#applicant_info').addClass('hide');
                $('#suspect_info').addClass('hide');
                //$('#complaint_info').addClass('hide');
                $('#secretarial_info').addClass('hide');
                $('#update').addClass('hide');
                $('#print').addClass('hide');
            }else if(status > 1 && status < 4){
                $('#applicant_info').removeClass('hide');
                $('#suspect_info').removeClass('hide');
                if(currentOfficer == casePICEmail){
                    $('#secretarial_info').removeClass('hide');
                    $('#assign').addClass('hide');
                    $('#update').removeClass('hide');
                    $('print').removeClass('hide');
                }else{
                    $('#secretarial_info').addClass('hide');
                    $('#assign').addClass('hide');
                    $('#update').addClass('hide');
                    $('#print').addClass('hide');
                } 
            }else if(status == 4){
                $('#applicant_info').removeClass('hide');
                $('#suspect_info').removeClass('hide');
                $('#secretarial_info').addClass('hide');
                $('#assign').addClass('hide');
                $('#update').addClass('hide');
                if(currentOfficer == casePICEmail){
                    $('#print').removeClass('hide')
                }else{
                    $('#print').addClass('hide')
                }
                
            }

        }

        function setDisclosureEmails(disclosureEmails){
            if(disclosureEmails.length > 0){
                $('#disclosure-email-container').empty();
                for(let a=0; a < disclosureEmails.length; a++){
                    $('#disclosure-email-container').append(`<div><span>${disclosureEmails[a]}</span></div>`);
                }
            }
            
        }

        function setApplicantInfo(applicantInfo){
            if(Object.keys(applicantInfo) !== 0){
                let key = '#applicant_';
                $('#applicantName').val(applicantInfo.name);
                $(key+'name').val(applicantInfo.name);
                $(key+'email_address').val(applicantInfo.email);
                $(key+'mobile_phone_no').val(applicantInfo.mobile);
                $(key+'designation').val(applicantInfo.designation);
                $(key+'department').val(applicantInfo.department);
            }
        }


        function setSuspectInfo(suspectInfo){
            let ids = '';
            let list = [];
            let count = 0;
            if(Object.keys(suspectInfo) !== 0){
                ids = suspectInfo.ids;
                list = suspectInfo.list;
                $('#suspectId').val(ids);
                $('#suspect-container').empty();
                for(let i =0 ; i< list.length; i++){
                    count ++;
                    $('#suspect-container').append(`<div class="suspect-wrapper">
                        <span class="suspect-individual-label">Individual ${count}</span>
                        <div class="mb-3">
                            <label for="supect_name">Name </label>
                            <input type="text" id="supect_name" name="suspect_name" class="form-control" value="${list[i].suspect_name}" disabled> 
                        </div>
                        <div class="mb-3">
                            <label for="suspect_email_address">E-Mail</label>
                            <input type="email" id="suspect_email_address" name="suspect_email_address" class="form-control" value="${list[i].suspect_email}" disabled>
                        </div>
                        <div class="mb-3">      
                            <label for="suspect_relationship">Relationship</label>
                            <input type="text" id="suspect_relationship" name="suspect_relationship" class="form-control" value="${list[i].suspect_relationship}" disabled>
                        </div>
                        <div class="mb-3">
                            <label for="suspect_department">Department/Agency</label>
                            <input type="text" id="suspect_department" name="suspect_department" class="form-control" value="${list[i].suspect_agency}" disabled>
                        </div>
                    </div>`);
                }
            }
        }

        function setComplaintInfo(complaintInfo){
            if(Object.keys(complaintInfo) !== 0){
                let key = '#complaint_improper_activity_';
                $('#complaintId').val(complaintInfo.ComplaintID);
                $(key+'date').val(complaintInfo.ImproperActivityDate);
                $(key+'time').val(complaintInfo.ImproperActivityTime);
                $(key+'place').val(complaintInfo.ImproperActivityPlace);
                $(key+'detail').val(complaintInfo.ImproperActivityDetail);
            }
        }


        function setFileInfo(fileInfo,complaintInfo){
            if(fileInfo.length > 0){
                $('#field-container-doc').empty();
                for(let i=0; i<fileInfo.length; i++){
                    if(fileInfo[i].Type == 1){
                        $('#field-container-doc').append(`
                            <div><a href="<?=url('').'/wbattachments/'?>${fileInfo[i].FilePath}" target="_blank">${fileInfo[i].FileName}</a></div>
                        `);
                    }
                }
            }
        }

        function setActivityInfo(activityInfo, statusList){
            $('#activity-table > tbody:last-child').empty();
            if(Object.keys(activityInfo) !== 0){
                activityInfo.map((acInfo)=>{
                    $('#activity-table > tbody:last-child').append(`<tr>
                        <td>${statusList[acInfo.activity_status].name}</td>
                        <td>${acInfo.remark}</td>
                        <td>${acInfo.activity_datetime}</td>
                    </tr>`);
                })
                
            }
            
        }

        function setInputValue(data, statusList) {
            let allData = data;
            if(Object.keys(allData).length == 0){

            }else{
                let profileId = allData.profile_id;
                let caseId = allData.case_id;
                let applicantInfo = allData.applicant_info;
                let fileInfo = allData.file_info;
                let disclosureEmails = allData.disclosure_emails;
                let complaintInfo = allData.complaint_info;
                let suspectInfo = allData.suspect_info;
                let activityInfo = allData.activity_info;
                $('#caseId').text(caseId);
                $('#profileId').val(profileId);
                $('#printProfileId').val(profileId);
                setApplicantInfo(applicantInfo);
                setComplaintInfo(complaintInfo);
                setSuspectInfo(suspectInfo);
                setDisclosureEmails(disclosureEmails);
                setFileInfo(fileInfo,complaintInfo);
                setActivityInfo(activityInfo, statusList);
            }

        }

        function loadViewData(pId){
            var url = "{{ route('complaintUpdateView', ":pId") }}";
            url = url.replace(':pId', pId);
            $.ajax({
                type:'get',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: url,
                processData: false, // Prevent jQuery from converting the data to a string
                contentType: false, 
                    //data : formData,
                    success: function(response){
                        if(Object.keys(response).length !== 0){
                            let data = response.data;
                            let statusList = response.status_list;
                            console.log(response)
                            setInputValue(data, statusList);
                        }
                       
                },
                error: function(_response){
                    window.setTimeout(function () {
                        Swal.fire('Unsuccessful!', '', 'error');

                    }, 2000);
                }
            });
        }


        function testOpen(pId, status, casePICEmail){
            let profileId = pId;
            //todo
            //show and hide section based on status   
            console.log(casePICEmail) 
            checkSectionByStatus(status,casePICEmail);
            loadViewData(pId);
        }

        function appendFileInput () {
            let caseId = $('#caseId').val();
          
            totalAccumulatedAppendCount++;
            $('#file-container').append(`
            <div class="file-wrapper" data-count=${totalAccumulatedAppendCount} id="supporting_doc_${totalAccumulatedAppendCount}">
                <input type="file" class="form-control supporting_doc" id="supporting_doc_${totalAccumulatedAppendCount}" name="supporting_docs[]" required>
                <button type="button" class="btn btn-primary" onClick="removeSelectedFileInput('supporting_doc_${totalAccumulatedAppendCount}')">-</button>
            </div>
            `);
        }

        function removeSelectedFileInput(selectorIdToRemove){
            $('#'+selectorIdToRemove).remove();
        }

        function assign(){
            const form = document.getElementById('whistle-blower-form');
            var formData = new FormData(form);
            Swal.fire({
                    title: 'Confirm Assign?',
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    denyButtonText: 'Cancel',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processAssign')}}",
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                        //contentType: 'multipart/form-data',
                            data : formData,
                            success: function(response){

                                if(response.data == 1)
                                {
                                    Swal.fire({
                                        title: 'Status Updated!',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false,
                                        }).then((result) => {
                                        /* Read more about isConfirmed, isDenied below */
                                        if (result.isConfirmed) {
                                        location.reload();
                                        }
                                    })
                                }
                            },
                            error: function(_response){
                                window.setTimeout(function () {
                                    Swal.fire('Unsuccessful!', '', 'error');

                                }, 2000);
                            }
                        });

                } else if (result.isCanceled) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        }


        function update(){
            const form = document.getElementById('whistle-blower-form');
            var formData = new FormData(form);
            Swal.fire({
                    title: 'Confirm Update?',
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    denyButtonText: 'Cancel',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processUpdate')}}",
                        //contentType: 'multipart/form-data',
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: 'Status Updated!',
                                    confirmButtonText: 'OK',
                                    allowOutsideClick: false,
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            window.setTimeout(function () {
                                Swal.fire('Unsuccessful!', '', 'error');

                            }, 2000);
                        }
                    });

                } else if (result.isCanceled) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
            

           
        }

</script>