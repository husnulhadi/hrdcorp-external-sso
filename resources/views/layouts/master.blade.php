<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    
        <title>HRD Corp BizMatch</title>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
         <!-- Styles -->
        <!-- <link href="{{ asset('assets/css/main.css')}}" rel="stylesheet"> -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,300;0,500;0,600;0,700;1,400;1,500&display=swap" rel="stylesheet">
        <meta property="og:title" content="HRD Corp Bizmatch" />
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    
    </head>

    @php 
    
    $isOfficer = false;
 
    if(isset($user)){
        $isOfficer = !empty($user) ? true : false ;
    }else{
        $isOfficer = false;
    }
    //$officerInfo = !empty($officerInfo) ? $officerInfo : [];
    $officerInfo = !empty($user) ? $user : [];

    @endphp

    
    @include((isset($source) && $source == 'app') ? '' : 'layouts.partials._navbar')
    <body class="bg-officer">
        <!-- Page content -->
        <div class="page-content">
            <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
            <div >
            @yield('content')
            </div>
        </div>
    </body>
    <footer>
        @include('layouts.partials._footer',['isPublic'=>false])
    </footer>
</html>
<script type="text/javascript">
 
    
   
</script>

