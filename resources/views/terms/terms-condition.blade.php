<head>
    
    <!-- Title Page-->
    <!-- <title>Whistle Blower Report Form</title> -->
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

</head>

<style>
.list-terms{
    list-style-type : none;
}
</style>
@include('layouts.partials._locale')
@extends('layouts.master-public')
@section('content')
<div class="container-wrapper">
    <div class="container">
        <div style="height:100%;">
            <!-- <h4>Terms and condition 123</h4>
            <span>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </span>
            <br><br> -->
            <h4>1.DECLARATION</h4>
            <p> a) I hereby declare that all the information provided in this WB apps is true and accurate.</p>
            <p> b) I fully understand that with this report, I shall be entitled to whistleblower protection from the Company as set out in the Company’s Whistleblower Policy.</p>
            <p> c) I fully understand that in the event I have made this Report maliciously or in bad faith, the whistleblower protection contained in the Company’s Whistleblower Policy shall no longer be applicable to me and I may be subject to disciplinary or legal proceedings by the Company.</p>
            <p> d) I understand and acknowledge that the Company shall not be responsible for any error or delay that may arise as a result of you being unable to access the WB apps or related services due to the error on your mobile device, software or third-party service provider.</p>
            <p> e) I understand and acknowledge that the Company have a right to reroute all complaints submitted which are not related to the WB complaints to the respective channels within the Company for further action.</p>
            <p> f) I understand and acknowledge that whilst the Company will at all times use their best efforts and take reasonable steps to ensure that all such information is kept confidential and to ensure the anonymity of any person making the disclosure, the Company shall not be held responsible for any disclosure that are not a result of the Company's fault or error.</p>
            <p> g) I understand and acknowledge that whilst the Company will at all times use their best efforts and take reasonable steps to ensure the WB app operates smoothly, the Company cannot warrant that the WB app are compatible or will operate with your mobile devices or any software/hardware that I am using.</p>
        
            <br><br>
            <h4>2.PERSONAL DATA PROTECTION ACT 2010 (“PDPA”) CONSENT CLAUSE</h4>
            <p> a) By summiting this report, I hereby agree that PSMB shall collect, obtain, store, and process my personal data as provided in this WB apps for the purpose of processing any complaints, receiving updates, news, or any materials from PSMB.</p>
            <p> b) I fully understand and hereby give my consent to PSMB to: -</p>
            <div> 
                <ul class="list-terms">
                    <li>i. Store and process my personal data.</li>
                    <li>ii. Disclose my personal data to the relevant government authorities or third parties where required by law or for legal purposes.</li>
                </ul>
            </div>
            <p>
                c) For the avoidance of doubt, personal data includes all data defined within the Personal Data Protection Act 2010 including all data that had been disclosed to PSMB in this Form
            </p>
        </div>
        
        

        @if($isVerify)
        <div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" onchange="setVerifyChecked()" onChange="enableButtonByCheckField()" id="declarePageTerm" name="declare_page_term" required>
                <label for="declarePageTerm">I agree with the terms and condition</label>
            </div>                           
        </div>
        @endif
    </div>
</div>


<script>
    function setVerifyChecked(){
        localStorage.clear();
        // let totalCount = $('[required]').length;
        // let validCount = 0;
        // let invalidCount = 0;
        // let isAllRequiredFieldValid = false;
        // $('[required]').each(function() {
        //     if ($(this).is(':invalid') || !$(this).val()){
        //         valid = false;
        //         invalidCount++;
        //     }else{
        //         validCount++;      
        //     } 
        // })
        if ($('#declarePageTerm').is(":checked")){
            console.log('test terms checked')
            localStorage.setItem("termsischeck", true);
        }else{
            localStorage.clear();
        }

    
    }
</script>