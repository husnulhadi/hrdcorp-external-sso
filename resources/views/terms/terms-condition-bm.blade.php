<head>
    
    <!-- Title Page-->
    <!-- <title>Whistle Blower Report Form</title> -->
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

</head>

<style>
.list-terms{
    list-style-type : none;
}

</style>

@include('layouts.partials._locale')
@extends('layouts.master-public')
@section('content')
<div class="container-wrapper">
    <div class="container">
        <div>
            <!-- <h4>Terma dan syarat</h4>
            <span>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </span> -->
            <h4>1.DEKLARASI</h4>
            <p> a) Saya dengan ini mengisytiharkan bahawa semua maklumat yang diberikan dalam aplikasi Pemberi Maklumat ini adalah benar dan tepat.</p>
            <p> b) Saya memahami sepenuhnya bahawa dengan laporan ini, saya berhak mendapat perlindungan pemberi maklumat daripada Syarikat seperti yang dinyatakan dalam Dasar Pemberi Maklumat Syarikat.</p>
            <p> c) Saya memahami sepenuhnya bahawa sekiranya saya membuat laporan ini dengan berniat jahat, perlindungan pemberi maklumat yang terkandung dalam Dasar Pemberi Maklumat Syarikat tidak lagi terpakai kepada saya dan saya mungkin tertakluk kepada prosiding tatatertib atau tindakan undang-undang oleh Syarikat.</p>
            <p> d) Saya memahami dan mengakui bahawa Syarikat tidak akan bertanggungjawab atas sebarang kesilapan atau kelewatan yang mungkin timbul akibat daripada anda tidak dapat mengakses aplikasi Pemberi Maklumat atau perkhidmatan yang berkaitan kerana ralat pada peranti mudah alih, perisian atau pembekal perkhidmatan pihak ketiga anda.</p>
            <p> e) Saya memahami dan mengakui bahawa Syarikat mempunyai hak untuk mengubah semua aduan yang dikemukakan yang tidak berkaitan dengan aduan Pemberi Maklumat ke saluran aduan yang lain dalam Syarikat untuk tindakan selanjutnya.</p>
            <p> f) Saya memahami dan mengakui bahawa walaupun Syarikat pada setiap masa akan menggunakan usaha terbaik mereka dan mengambil langkah-langkah yang munasabah untuk memastikan bahawa semua maklumat tersebut dirahsiakan dan untuk memastikan kerahsiaan mana-mana orang yang membuat pendedahan itu, Syarikat tidak akan bertanggungjawab atas sebarang pendedahan yang bukan hasil daripada kesalahan atau kesilapan Syarikat.</p>
            <p> g) Saya memahami dan mengakui bahawa walaupun Syarikat pada setiap masa akan menggunakan usaha terbaik mereka dan mengambil langkah yang munasabah untuk memastikan aplikasi Pemberi Maklumat beroperasi dengan lancar, Syarikat tidak boleh menjamin bahawa aplikasi Pemberi Maklumat serasi atau akan beroperasi dengan peranti mudah alih anda atau sebarang perisian/perkakasan yang saya gunakan.</p>
            <br>
            <h4>2.KLAUSA PERSETUJUAN AKTA PERLINDUNGAN DATA PERIBADI 2010 ("PDPA")</h4>
            <p> a) Dengan penyerahan laporan ini, saya dengan ini bersetuju bahawa PSMB akan mengumpul, memperoleh, menyimpan, dan memproses data peribadi saya seperti yang diperuntukkan dalam aplikasi Pemberi Maklumat ini untuk tujuan memproses sebarang aduan, menerima maklumat terkini, berita atau apa-apa bahan daripada PSMB.</p>
            <p> b) Saya memahami sepenuhnya dan dengan ini memberi persetujuan kepada PSMB untuk:-</p>
            <div> 
                <ul class="list-terms">
                    <li>i. Simpan dan proses data peribadi saya.</li>
                    <li>ii. Mendedahkan data peribadi saya kepada pihak berkuasa kerajaan yang berkaitan atau pihak ketiga jika dikehendaki oleh undang-undang atau untuk tujuan undang-undang.</li>
                </ul>
            </div>
            <p>
                c) Untuk mengelakkan keraguan, data peribadi termasuk semua data yang ditakrifkan dalam Akta Perlindungan Data Peribadi 2010 termasuk semua data yang telah didedahkan kepada PSMB dalam Borang ini.
            </p>
    
        </div>

        <br>

        @if($isVerify)
        <div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" onchange="setVerifyChecked()" onChange="enableButtonByCheckField()" id="declarePageTerm" name="declare_page_term" required>
                <label for="declarePageTerm">I agree with the terms and condition</label>
            </div>                           
        </div>
        @endif
    </div>
</div>


<script>
    function setVerifyChecked(){
        localStorage.clear();
        // let totalCount = $('[required]').length;
        // let validCount = 0;
        // let invalidCount = 0;
        // let isAllRequiredFieldValid = false;
        // $('[required]').each(function() {
        //     if ($(this).is(':invalid') || !$(this).val()){
        //         valid = false;
        //         invalidCount++;
        //     }else{
        //         validCount++;      
        //     } 
        // })
        if ($('#declarePageTerm').is(":checked")){
            console.log('test terms checked')
            localStorage.setItem("termsischeck", true);
        }else{
            localStorage.clear();
        }

    
    }
</script>