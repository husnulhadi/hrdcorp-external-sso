<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Title Page-->
    <!-- <title>Whistle Blower</title> -->

    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<style>
    .rmb-wrapper{
        display : flex;
        justify-content : center;
    }

    .login-logo-wrapper{
        display : flex;
        justify-content : center;
        margin : 0.5rem 0;
    }

    .login-logo-image-wrapper{
        /* height : 40px; */
    }

    .logo-login{
        width : 100%;
        height: 40px;
    }
</style>

@include('layouts.partials._locale')
@extends('layouts.master-public')
@section('content')
    

    <body class="bg">
        <!-- <div class="bg"> -->
            <!-- <div class="container text-center py-5 bg">
                <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="" height="150px" style="padding-top:30px; padding-bottom:30px">
            </div> -->
            <div class="container-wrapper">
                @include('layouts.partials._title',['type'=>'public'])
                <div class="container login-container ">
                    <div class="card">
                        <div class="card-header text-center font-weight-bold hrd-card-header-text">
                           <div>{{__('form.login')}}</div>
                        </div>
                        <div class="card-body">
                            <div class="login-logo-wrapper">
                                <div class="login-logo-image-wrapper">
                                    <img class="logo-login" src="{!! asset('assets/img/BizMatch---Logo.png') !!}" alt="logo" class="centerz">
                                </div>
                                <div class="login-logo-image-wrapper">
                                    <img class="logo-login" src="{!! asset('assets/img/hrdcorp-logo.png') !!}" alt="logo" class="centerz" >
                                </div>

                               {{-- @if(empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale'))
                                
                                <div class="login-logo-image-wrapper">
                                    <img class="logo" src="{!! asset('assets/img/hrd-jarta-eng.png') !!}" alt="logo" class="centerz">
                                </div>
                                @else
                                <div class="login-logo-image-wrapper">
                                    <img class="logo" src="{!! asset('assets/img/hrd-jarta-bm.png') !!}" alt="logo" class="centerz" height="150px">
                                </div>
                                @endif --}}

                            </div>
                            <form id="form_login" action="{{ route('processLogin')}}" method="post">
                            {{ csrf_field() }}
                                <div class="mb-3 row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.username')}}</label>
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">{{__('form.password')}}</label>
                                    <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span style="color:red">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                               {{-- <div class="mb-3 row">
                                    <div class="rmb-wrapper">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                {{ __('form.rememberme') }}
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}
                                <!-- <div class="row mb-3">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.login')}}</button>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-link link-hrd-theme" href="{{ route('forgotPassword') }}">
                                        {{ __('form.forgotpassword') }}
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!---->
    </body>
    

    @endsection
</html>
