<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<style>
    .rmb-wrapper{
        display : flex;
        justify-content : center;
    }

    .login-logo-wrapper{
        display : flex;
        justify-content : center;
        margin : 0.5rem 0;
    }

    .login-logo-image-wrapper{
        /* height : 40px; */
    }

    .logo-login{
        width : 100%;
        height: 40px;
    }
    /* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: navy;
    color: #F04E23;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #F04E23;
    color: navy;
    font-weight: 900;
}
/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border-top: none;
}
</style>

@include('layouts.partials._locale')
@extends('layouts.master-public')
@section('content')
    

    <body class="bg">
        <!-- <div class="bg"> -->
            <!-- <div class="container text-center py-5 bg">
                <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="" height="150px" style="padding-top:30px; padding-bottom:30px">
            </div> -->
            <div class="container-wrapper">
                @include('layouts.partials._title',['type'=>'public'])
                <div class="container login-container ">
                    @if (Session::has('registersuccess')) 
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ Session::get('registersuccess') }}</li>
                        </ul>
                    </div>
                    @endif
                    <div class="card">
                        <div class="tab card-header text-center font-weight-bold hrd-card-header-text" style="padding:0px 0px !important">
                            <button class="tablinks active" onclick="openLoginTab(event, 'registered')">HRDCorp Registered</button>
                            <button class="tablinks" onclick="openLoginTab(event, 'nonregistered')">HRDCorp Non Registered</button>
                            <button class="tablinks" onclick="openLoginTab(event, 'admin')"> Admin </button>
                        </div>
                        <div class="card-body">
                            <div class="login-logo-wrapper">
                                <div class="login-logo-image-wrapper">
                                    <img class="logo-login" src="{!! asset('assets/img/BizMatch---Logo.png') !!}" alt="logo" class="centerz">
                                </div>
                                <div class="login-logo-image-wrapper">
                                    <img class="logo-login" src="{!! asset('assets/img/hrdcorp-logo.png') !!}" alt="logo" class="centerz" >
                                </div>

                               {{-- @if(empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale'))
                                
                                <div class="login-logo-image-wrapper">
                                    <img class="logo" src="{!! asset('assets/img/hrd-jarta-eng.png') !!}" alt="logo" class="centerz">
                                </div>
                                @else
                                <div class="login-logo-image-wrapper">
                                    <img class="logo" src="{!! asset('assets/img/hrd-jarta-bm.png') !!}" alt="logo" class="centerz" height="150px">
                                </div>
                                @endif --}}

                            </div>
                            <!-- <form id="form_login" action="{{ route('processLogin')}}" method="post">
                            {{ csrf_field() }}
                                <div class="mb-3 row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.username')}}</label>
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">{{__('form.password')}}</label>
                                    <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span style="color:red">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.login')}}</button>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-link link-hrd-theme" href="{{ route('forgotPassword') }}">
                                        {{ __('form.forgotpassword') }}
                                    </a>
                                </div>
                            </form> -->

                                                        <!-- test -->
                        <!-- Tab content -->
                            <div id="registered" class="tabcontent active" style="display:block;">
                            <form id="form_login" action="{{ route('processLoginLdap')}}" method="post">
                                {{ csrf_field() }}
                                <div class="mb-3 row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.mycoid')}}</label>
                                    <div class="col-sm-9">
                                    <input type="text" class="form-control @error('mycoid') is-invalid @enderror" name="mycoid" value="{{ old('mycoid') }}" required autocomplete="mycoid" autofocus>
                                    @error('mycoid')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">{{__('form.password')}}</label>
                                    <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span style="color:red">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.login')}}</button>
                                </div>
                            </form>
                        </div>

                        <div id="nonregistered" class="tabcontent">
                        <form id="form_login" action="{{ route('processLogin')}}" method="post">
                            {{ csrf_field() }}
                                <div class="mb-3 row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.email')}}</label>
                                    <div class="col-sm-9">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">{{__('form.password')}}</label>
                                    <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span style="color:red">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.login')}}</button>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-link link-hrd-theme" href="{{ route('forgotPassword') }}">
                                        {{ __('form.forgotpassword') }}
                                    </a>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-link link-hrd-theme" href="{{ route('registerTP') }}">
                                        {{ __('form.registrationdirection') }}
                                    </a>
                                </div>
                            </form>
</div>

<div id="admin" class="tabcontent">
  <form id="form_login" action="{{ route('processLogin')}}" method="post">
                            {{ csrf_field() }}
                                <div class="mb-3 row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">{{__('form.email')}}</label>
                                    <div class="col-sm-9">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">{{__('form.password')}}</label>
                                    <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span style="color:red">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-hrd-theme">{{__('form.login')}}</button>
                                </div>
                                <div class="text-center">
                                    <a class="btn btn-link link-hrd-theme" href="{{ route('forgotPassword') }}">
                                        {{ __('form.forgotpassword') }}
                                    </a>
                                </div>
                            </form>
</div>
                    <!-- test -->


                        </div>
                    </div>
                </div>
            </div> 
    </body>
    

    @endsection
     <script>
function openLoginTab(evt, tablogin) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tablogin).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
</html>
