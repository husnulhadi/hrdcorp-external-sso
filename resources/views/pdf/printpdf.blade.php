<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title Page-->
    <title>Whistle Blower Report Form</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
</head>
<style>
    table{
        border-collapse: collapse;  
        font-family: "Poppins", "Arial", "Helvetica Neue", sans-serif;  
        width : 100%;
    }

    .print-table{
        width : 100%;
    }
    
    td,th{
      border : 1px solid #000000;  
      padding : 0.5rem;
      text-align : center;
    }

    .title{
        text-align:center;
        padding:1rem;
        
    }

    .padding{
        padding : 0.5rem;
    }

    .odd{
        background : #cccccc;
    }

    .even{
        background : #ffffff;
    }

    .container{
        display : flex;
        align-items:center;
    }

    .th-tr{
        background-color : #063058;
        color : #fff;
    }

</style>

<div class="container">
    {{--<img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;"> --}}
    <div ><img src="data:image/svg+xml;base64,<?php echo base64_encode(file_get_contents(base_path('public/assets/img/logo-removebg.png'))); ?>" width="200"></div>
    <div class="title">Whistle Blower List</div>
</div>

<div class="container">
    <div class="table-responsive print-table">
        <table class="table table-striped">
            <thead>
                <tr class="th-tr">      
                    <th>Case ID</th>
                    <th>Whistle Blower Name</th>
                    <th>Case Detail</th>
                    <th>Submit Date</th>
                    <th>PIC Officer</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($list as $li)
                <tr >
                    <td>{{$li['CaseID']}}</td>
                    <td>{{$li['ComName']}}</td>
                    <td>{{!empty($li['complaint']) ? $li['complaint']['ImproperActivityDetail'] : ''}}</td>
                    <td>{{$li['Created_date']}}</td>
                    <td>{{!empty($li['user']) ? $li['user']['fullname'] : '-'}}</td>
                    <td>{{$statusList[$li['Status']]['name']}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



</html>