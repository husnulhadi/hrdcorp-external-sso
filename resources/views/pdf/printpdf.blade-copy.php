<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title Page-->
    <title>Whistle Blower Report Form</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
</head>
<style>
    table{
        border-collapse: collapse;    
    }
    
    td,th{
      border : 1px solid #000000;  
      padding : 0.5rem;
      text-align : center;
    }

    .title{
        text-align:center;
        padding:1rem;
    }

    .padding{
        padding : 0.5rem;
    }

    .odd{
        background : grey
    }

    .even{
        background : #ffffff;
    }

</style>

<div class="container">
    <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;">
</div>

<div class="container">
    <div class="card">
        <div class="title">
            Whistle Blower List
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>      
                            <th>Case ID</th>
                            <th>Whistle Blower Name</th>
                            <th>Case Detail</th>
                            <th>Submit Date</th>
                            <th>PIC Officer</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd">
                            <td>sdsd</td>
                            <td>2023-12-34</td>
                            <td>assd</td>
                            <td>adadsds</td>
                            <td>sdsdsf</td>
                            <td>Adsfdf</td>
                            <td>sdsd</td>
                            <td>sdsfsf</td>
                            <td>sdsfsf</td>
                        </tr>
                        <tr class="even">
                            <td>sdsd</td>
                            <td>2023-12-34</td>
                            <td>assd</td>
                            <td>adadsds</td>
                            <td>sdsdsf</td>
                            <td>Adsfdf</td>
                            <td>sdsd</td>
                            <td>sdsfsf</td>
                            <td>sdsfsf</td>
                        </tr>
                        <tr class="odd">
                            <td>sdsd</td>
                            <td>2023-12-34</td>
                            <td>assd</td>
                            <td>adadsds</td>
                            <td>sdsdsf</td>
                            <td>Adsfdf</td>
                            <td>sdsd</td>
                            <td>sdsfsf</td>
                            <td>sdsfsf</td>
                        </tr>
                        <tr class="even">
                            <td>sdsd</td>
                            <td>2023-12-34</td>
                            <td>assd</td>
                            <td>adadsds</td>
                            <td>sdsdsf</td>
                            <td>Adsfdf</td>
                            <td>sdsd</td>
                            <td>sdsfsf</td>
                            <td>sdsfsf</td>
                        </tr>
                      
                    </tbody>
                </table>
               
            </div>
    
        </div>
    </div>
</div>



</html>