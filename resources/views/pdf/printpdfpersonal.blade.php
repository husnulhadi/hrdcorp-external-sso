<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title Page-->
    <title>Whistle Blower Report Form</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
</head>
<style>
    table{
        border-collapse: collapse;  
        font-family: "Poppins", "Arial", "Helvetica Neue", sans-serif;  
        width : 100%;
    }

    .print-table{
        width : 100%;
    }
    
    td,th{
      border : 1px solid #000000;  
      padding : 0.5rem;
      text-align : center;
    }

    .title{
        text-align:center;
        padding:1rem;
        font-weight : bold;
    }

    .left{
        text-align : left;
        padding : 0.5rem 0;
    }

    .padding{
        padding : 0.5rem;
    }

    .odd{
        background : #cccccc;
    }

    .even{
        background : #ffffff;
    }

    .container{
        display : flex;
        align-items:center;
        padding : 0 2rem;
    }

    .th-tr{
        background-color : #063058;
        color : #fff;
    }

</style>

@php

$suspectInfo = !empty($info) ? $info['suspect'] : [];
$complaintInfo = !empty($info) ? $info['complaint'] : [];
$activityInfo = !empty($info) ? $info['activity'] : [];

@endphp
<div class="container">
    {{--<img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;"> --}}
    <div><img src="data:image/svg+xml;base64,<?php echo base64_encode(file_get_contents(base_path('public/assets/img/logo-removebg.png'))); ?>" width="200"></div>
    <div class="title">Whistle Blower User Detail - Case Id : {{!empty($info) ? $info['CaseID'] : ''}}</div>
</div>
<br>
<div class="container">
    <div class="table-responsive print-table">
        <div class="title left">B. Particular of suspect </div>
        <table class="table table-striped" >
            <thead>
                <tr class="th-tr">
                    <th>Suspect Name</th>
                    <th>Suspect Email</th>
                    <th>Suspect Agency/Department</th>
                    <th>Suspect Relationship</th>
                </tr>
            </thead>
            <tbody>
                @foreach($suspectInfo as $suspect)
                <tr>
                    <td>{{$suspect['suspect_name']}}</td>
                    <td>{{$suspect['suspect_email']}}</td>
                    <td>{{$suspect['supect_agency']}}</td>
                    <td>{{$suspect['suspect_relationship']}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br>

        <div class="title left">C. Particular of Improper Activity </div>
        <table class="table table-striped">
            <thead>
                <tr class="th-tr">
                    <th>Improper Activity Date</th>
                    <th>Improper Activity Time</th>
                    <th>Improper Activity Detail</th>
                    <th>Improper Activity Place</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityDate'] : ''}}</td>
                    <td>{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityTime'] : ''}}</td>
                    <td>{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityDetail'] : ''}}</td>
                    <td>{{!empty($complaintInfo) ? $complaintInfo['ImproperActivityPlace'] : ''}}</td>
                </tr>
            </tbody>
        </table>

        <br>
        <div class="title left">Process History</div>
        <table class="table table-striped">
            <thead>
                <tr class="th-tr">
                    <th>Status</th>
                    <th>Remark</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activityInfo as $activity)
                <tr>
                    @php
                    $intStatus = !empty($activity) ? intval($activity['activity_status']) : '';
                    @endphp
                    <td>{{!empty($activity) ? $statusList[$intStatus]['name'] : ''}}</td>
                    <!-- <td>{{!empty($activity) ? $activity['activity_status'] : ''}}</td> -->
                    <td>{{!empty($activity) ? $activity['remark'] : ''}}</td>
                    <td>{{!empty($activity) ? $activity['activity_datetime'] : '' }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>



</html>