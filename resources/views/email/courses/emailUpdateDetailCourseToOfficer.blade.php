<style>
    /* table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        width : 100%;
    }
    th {
        width: 150px;
        text-align: left;
        background-color : #063058;
        color : #fff;
        min-width : 100px;

    }

    th,td{
        padding : 0.2rem 1rem;
    } */

    .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
        /* font-weight : bold; */
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    
    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    }
</style>

@php
$data = !empty($data) ? $data : [];
//$receiverName = !empty($data) ? $data['receiver_name'] : '';
$traceUrl = !empty($data) ? $data['trace_url'] : '';


$tpMycoid = !empty($data) ? $data['course_tp_mycoid'] : 'TEST TP MYCOID';
$course = !empty($data) ? $data['tp_course_name'] : 'TEST COURSE';
$updatedBy = !empty($data) ? $data['updated_by'] : 'TEST UPDATED BY';
$userType = !empty($data) ? $data['user_type'] : '';

@endphp

<div style="font-family : 'Montserrat';padding : 2rem;" class="email-container">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Notification on Course Detail Update</div>
            <br>
            <p>Dear Sir/Madam,</p>
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">NOTIFICATION ON COURSE DETAIL UPDATE</div>
            
            <div>
                <p>This email is to notify you on the Course Detail Update.</p> 
                <p>Details are as below: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Training Provider MYCOID<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMycoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Courses <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$course}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Updated By <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$updatedBy}}</div>
                    </div>
                </div>
                @if($userType == 'admin' || $userType == 'SuperAdmin')
                <p>You may track through  <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a>.Kindly access by your authentication.</p>
                @endif
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is an auto generated message. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Notifikasi Pengemaskinian Butiran Kursus</div>
            <br>
            <p>Kepada Tuan/Puan yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">NOTIFIKASI PENGEMASKINIAN BUTIRAN KURSUS</div>
            
            <div>
                <p>Emel ini adalah untuk memaklumkan pengemaskinian butiran kursus.</p>
               
                <p>Butiran adalah seperti berikut: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">MYCOID Penyedia Latihan <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMycoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Kursus <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$course}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Dikemaskini Oleh <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$updatedBy}}</div>
                    </div>
                </div>
                @if($userType == 'admin' || $userType == 'SuperAdmin')
                <p>Anda boleh melayari <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> untuk mengakses laman web ini.</p>
                @endif
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Maklumat ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>


   
</div>
