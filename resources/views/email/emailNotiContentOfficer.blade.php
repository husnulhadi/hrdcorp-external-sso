<style>
    /* table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        width : 100%;
    }
    th {
        width: 150px;
        text-align: left;
        background-color : #063058;
        color : #fff;
        min-width : 100px;

    }

    th,td{
        padding : 0.2rem 1rem;
    } */

    .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
        /* font-weight : bold; */
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    
    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    }
</style>

@php
$data = !empty($data) ? $data : [];
$caseId = !empty($data) ? $data['case_id'] : '';
$improperActivityDetail = !empty($data) ? $data['complaint_improper_activity_detail'] : '';
$receiverName = !empty($data) ? $data['receiver_name'] : '';
$traceUrl = !empty($data) ? $data['trace_url'] : '';

@endphp

<div style="font-family : 'Montserrat';padding : 2rem;" class="email-container">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Acknowledgement of Whistleblower Report</div>
            <br>
            <p>Dear Sir/Madam,</p>
        
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">ACKNOWLEDGEMENT OF WHISTLEBLOWER REPORT</div>
            
            <div>
                <p>This email is to acknowledging receive of your report dated {{date('d M Y')}} regarding whistleblower submission. While we investigate this, you can track the status case at</p> 
                <p> <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> access by your authentication.</p>
                <p>Your whistleblower case details as per below: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Case Id <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$caseId}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Description <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">Whistle Blower Submission</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Detail <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$improperActivityDetail}}</div>
                    </div>
                </div>
                <p>We want to thank you for taking the time to submit this report and for helping us to make our company a safer and more ethical workplace.</p>
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is computer generated. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Pengakuan Laporan Pemberi Maklumat (<span style="font-style:italic;">Whistleblower</span>)</div>
            <br>
            <p>Kepada Tuan/Puan yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">PENGAKUAN LAPORAN PEMBERI MAKLUMAT (<span style="font-style:italic;">WHISTLEBLOWER</span>)</div>
            
            <div>
                <p>E-mel ini hanya untuk mengesahkan penerimaan laporan anda bertarikh {{date('d M Y')}} mengenai penghantaran pemberi maklumat. Semasa kami menyiasat perkara ini, anda boleh menjejaki kes status di </p>
                <p><a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> akses melalui pengesahan anda.</p>
                <p>Butiran kes pemberi maklumat anda seperti di bawah: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Id Kes<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$caseId}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Penerangan <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">Penyerahan Pemberi Maklumat (<span style="font-style:italic;">Whistleblower</span>)</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Butiran <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$improperActivityDetail}}</div>
                    </div>
                </div>
                <p>Kami ingin mengucapkan terima kasih kerana meluangkan masa untuk menyerahkan laporan ini dan membantu kami menjadikan syarikat kami tempat kerja yang lebih selamat dan beretika.</p>
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>


    <!-- <h3>Particular of the Whistleblower</h3>
    <table style="width:100%">
        <tr>
            <th>Name:</th>  
            <td>Test</td>
        </tr>
        <tr>
            <th>IC No:</th>
            <td>12334</td>
        </tr>
        <tr>
            <th>Phone Number:</th>
            <td>123123123</td>
        </tr>
        <tr>
            <th>E-mail Address:</th>
            <td>test@gmail.com</td>
        </tr>
        <tr>
            <th>Department:</th>
            <td>ABC</td>
        </tr>
    </table>
    <br> -->

    <!-- <h3>Particular of Improper Activity</h3>
    <table style="width:100%">
        <tr>
            <th>Case ID:</th>
            <td>WB1012002100012</td>
        </tr>
        <tr>
            <th>Details:</th>
            <td>Exposed Confedential Document</td>
        </tr>
        <tr>
            <th>Place:</th>
            <td>Office</td>
        </tr>
        <tr>
            <th>Date:</th>
            <td>2023-06-06</td>
        </tr>
        <tr>
            <th>Time:</th>
            <td>18:00</td>
        </tr>
    </table> -->
</div>
