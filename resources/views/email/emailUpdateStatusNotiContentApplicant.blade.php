<style>
   

    /* .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    } */
</style>

@php

$data = !empty($data) ? $data : [];

$prev_statusBI = !empty($data) ? $data['previous_status_name_bi'] : '';
$prev_statusBM = !empty($data) ? $data['previous_status_name_bm'] : '';
$statusBI = !empty($data) ? $data['status_name_bi'] : '';
$statusBM = !empty($data) ? $data['status_name_bm'] : '';
$caseId = !empty($data) ? $data['caseId'] : '';
$picOfficerEmail = !empty($data) ? $data['officerEmail'] : '';
$picOfficerName = !empty($data) ? $data['officerName'] : '';
$applicantName = !empty($data) ? $data['applicantName'] : '';
$remark = !empty($data) ? $data['remark'] : '';
$traceUrl = !empty($data) ? $data['trace_url'] : '';
$receiverName = !empty($data) ? $data['receiver_name'] : ''; 
 
@endphp 

<div style="font-family:'Montserrat';padding : 2rem;">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">HRD Corp: Acknowledgement of Whistleblower Report</div>
            <br>
            <p>Dear Sir/Madam {{$receiverName}},</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">ACKNOWLEDGEMENT OF WHISTLEBLOWER REPORT</div>
            
            <div>
                <!-- <p>This email is just to acknowledging receipt of your report dated [date received] regarding [reportname]. While we investigate this, you could try track the status case at </p> -->
                <!-- <p>This email is just to acknowledging status of report have been change to WB Comitte. While we investigate this, you could try track the status case at </p> -->
                <p>This email is to inform you that the complaint <span>{{$caseId}}</span> has now changed its status from <span>{{$prev_statusBI}}</span> to <span>{{$statusBI}}</span>. While we investigate this, you can track the status case at
                <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> searching by case id. </p>
                <p>Your whistleblower case details as per below: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Case Id <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$caseId}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Remark<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$remark}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Status <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$statusBI}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Updated by <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$picOfficerName}} ({{$picOfficerEmail}})</div>
                    </div>
                </div>
                <p>We want to thank you for taking the time to submit this report and for helping us to make our company a safer and more ethical workplace.</p>
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is computer generated. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">HRD Corp: Pengakuan Laporan Pemberi Maklumat (<span style="font-style:italic;">Whistleblower</span>)</div>
            <br>
            <p>Kepada Tuan/Puan {{$receiverName}} yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">PENGAKUAN LAPORAN PEMBERI MAKLUMAT (<span style="font-style:italic;">WHISTLEBLOWER</span>)</div>
            
            <div>
                <!-- <p>E-mel ini hanya untuk mengakui penerimaan laporan anda bertarikh [tarikh terima] mengenai [reportname]. Semasa kami menyiasat perkara ini, anda boleh cuba menjejaki kes status di </p> -->
                <p>E-mel ini hanya untuk mengakui status laporan telah ditukar dari <span>{{$prev_statusBM}}</span> kepada <span>{{$statusBM}}. Semasa kami menyiasat perkara ini, anda boleh menjejaki kes status di <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> dengan id kes.</p>
                <p>Butiran kes pemberi maklumat anda seperti di bawah: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Id Kes <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$caseId}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Butiran<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$remark}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Status <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$statusBM}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Dikemaskini oleh<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$picOfficerName}} ({{$picOfficerEmail}})</div>
                    </div>
                </div>
                <p>Kami ingin mengucapkan terima kasih kerana meluangkan masa untuk menyerahkan laporan ini dan membantu kami menjadikan syarikat kami tempat kerja yang lebih selamat dan beretika.</p>
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>


    <!-- <h3>Particular of the Whistleblower</h3>
    <table style="width:100%">
        <tr>
            <th>Name:</th>  
            <td>Test</td>
        </tr>
        <tr>
            <th>IC No:</th>
            <td>12334</td>
        </tr>
        <tr>
            <th>Phone Number:</th>
            <td>123123123</td>
        </tr>
        <tr>
            <th>E-mail Address:</th>
            <td>test@gmail.com</td>
        </tr>
        <tr>
            <th>Department:</th>
            <td>ABC</td>
        </tr>
    </table>
    <br> -->

    <!-- <h3>Particular of Improper Activity</h3>
    <table style="width:100%">
        <tr>
            <th>Case ID:</th>
            <td>WB1012002100012</td>
        </tr>
        <tr>
            <th>Details:</th>
            <td>Exposed Confedential Document</td>
        </tr>
        <tr>
            <th>Place:</th>
            <td>Office</td>
        </tr>
        <tr>
            <th>Date:</th>
            <td>2023-06-06</td>
        </tr>
        <tr>
            <th>Time:</th>
            <td>18:00</td>
        </tr>
    </table> -->
</div>
