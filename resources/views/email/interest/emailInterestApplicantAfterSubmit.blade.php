<style>
   

    /* .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    } */
</style>

@php

$data = !empty($data) ? $data : [];
//$receiverName = !empty($data) ? $data['receiver_name'] : '';
$traceUrl = !empty($data) ? $data['trace_url'] : '';

$empMyCoid = !empty($data) ? $data['employer_mycoid'] : 'EMPTEST121';
$tpMyCoid = !empty($data) ? $data['course_tp_mycoid'] : 'TPTEST121';
$courseName = !empty($data) ? $data['course_name'] : 'Test Course';
$employerEmail = !empty($data) ? $data['employer_email'] : 'TEST EMPLOYER EMAIL';
$defaultPassword = !empty($data) ? $data['default_password'] : '123456';

@endphp 

<div style="font-family:'Montserrat';padding : 2rem;">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">Acknowledgement of Interest Declaration & Course Enrolment</div>
            <br>
            <p>Dear Sir/Madam,</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">ACKNOWLEDGEMENT OF COURSE INTEREST DECLARATION</div>
            
            <div>
                <p>Thank you for your declaration submission!</p>
                <p>Your details are as below:</p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Employer MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empMyCoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Training Provider MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMyCoid}}</div>
                    </div>  
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Course <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$courseName}}</div>
                    </div>  
                </div>
                {{--<p>You can track with <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> access by your authentication.</p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Username<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$employerEmail}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Default Password<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$defaultPassword}}</div>
                    </div>
                </div>--}}
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is auto generated message. No signature is required..</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">HRD Corp: Perakuan Pengisytiharan Minat</div>
            <br>
            <p>Kepada Tuan/Puan yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">Perakuan Pengisytiharan Minat</div>
            
            <div>
              
                <p>Terima kasih kerana berminat dengan kursus ini</p>
                <p>Butiran kursus seperti di bawah </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">MYCOID  Majikan <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empMyCoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">MYCOID Penyedia Latihan <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMyCoid}}</div>
                    </div>  
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Kursus <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$courseName}}</div>
                    </div>  
                </div>
               {{-- <p>Anda boleh melayari pada <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> akses melalui pengesahan anda.</p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Name Pengguna<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$employerEmail}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Kata Laluan Asal<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$defaultPassword}}</div>
                    </div>
                </div> --}}
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Maklumat ini dihasilkan secara automatik, tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>
</div>
