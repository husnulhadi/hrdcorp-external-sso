<style>
    /* table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        width : 100%;
    }
    th {
        width: 150px;
        text-align: left;
        background-color : #063058;
        color : #fff;
        min-width : 100px;

    }

    th,td{
        padding : 0.2rem 1rem;
    } */

    .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
        /* font-weight : bold; */
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    
    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    }
</style>

@php
$data = !empty($data) ? $data : [];
//$receiverName = !empty($data) ? $data['receiver_name'] : '';
$traceUrl = !empty($data) ? $data['trace_url'] : '';

$empMyyCoId = !empty($data) ? $data['emp_mycoid'] : 'TESTEMPMYCOID 123';
$tpMyyCoId = !empty($data) ? $data['tp_mycoid'] : 'TESTTPMYCOID 333';
$course = !empty($data) ? $data['course'] : 'TEST COURSE';
$status = !empty($data) ? $data['status'] : 'TEST STATUS';
$previousStatus = !empty($data) ? $data['status_prevoius'] : 'TEST STATUS PREVOIUS';
$updatedBy = !empty($data) ? $data['updated_by'] : 'TEST UPDATED BY';

@endphp

<div style="font-family : 'Montserrat';padding : 2rem;" class="email-container">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Notification of status update for applicant course interest</div>
            <br>
            <p>Dear Sir/Madam,</p>
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">NOTIFICATION OF STATUS UPDATE FOR APPLICANT COURSE INTEREST</div>
            
            <div>
                <p>This email is to inform you that the applicant has now changed its status from {{$previousStatus}}to {{$status}}.</p> 
                <p>Details as per below: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Employer MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{ $empMyyCoId }}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">TP MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMyyCoId}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Courses <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$course}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Status <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$status}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Updated By <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$updatedBy}}</div>
                    </div>
                </div>

                <p>You can track with <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> access by your authentication.</p>
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is computer generated. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Notifikasi penghantaran kursus berminat</div>
            <br>
            <p>Kepada Tuan/Puan yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">NOTIFIKASI PENGHANTARAN KURSUS BERMINAT</div>
            
            <div>
                <p>Emel ini adalah untuk memberitahu anda bahawa pemohon kini telah mengubah statusnya dari {{$previousStatus}} kepada {{$status}}.</p>
                <p>Butiran kes pemberi maklumat anda seperti di bawah: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Employer MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{ $empMyyCoId }}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">TP MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMyyCoId}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Kursus <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$course}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Status <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$status}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Dikemaskini oleh <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$updatedBy}}</div>
                    </div>
                </div>
                <p>Anda boleh melayari pada <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> akses melalui pengesahan anda.</p>
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>


   
</div>
