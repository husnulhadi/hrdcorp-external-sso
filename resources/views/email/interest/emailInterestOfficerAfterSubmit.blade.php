<style>
    /* table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        width : 100%;
    }
    th {
        width: 150px;
        text-align: left;
        background-color : #063058;
        color : #fff;
        min-width : 100px;

    }

    th,td{
        padding : 0.2rem 1rem;
    } */

    .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
        /* font-weight : bold; */
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    
    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    }
</style>

@php
$data = !empty($data) ? $data : [];
//$receiverName = !empty($data) ? $data['receiver_name'] : '';
$traceUrl = !empty($data) ? $data['trace_url'] : '';

$empMyCoid = !empty($data) ? $data['employer_mycoid'] : 'EMPTEST121';
$tpMyCoid = !empty($data) ? $data['course_tp_mycoid'] : 'TPTEST121';

$empName = !empty($data) ? $data['employer_name'] : 'TEST EMP NAME';
$empEmail = !empty($data) ? $data['employer_email'] : 'TEST EMP EMAIL';
$empContact = !empty($data) ? $data['employer_contact'] : 'TEST EMP CONTACT';

$courseName = !empty($data) ? $data['course_name'] : 'Test Course';

@endphp

<div style="font-family : 'Montserrat';padding : 2rem;" class="email-container">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Notification on Interest Declaration & Submission</div>
            <br>
            <p>Dear Sir/Madam,</p>
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">NOTIFICATION OF COURSE INTEREST DECLARATION SUBMISSION </div>
            
            <div>
                <p>PSMB has received notification on Interest submission.</p> 
                <p>Details are as below: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Employer MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empMyCoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Employer Name <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empName}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Employer Email <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empEmail}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Employer Contact <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empContact}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Training Provider MYCOID <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMyCoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Courses <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$courseName}}</div>
                    </div>
                </div>
               
               {{-- <p>You may track through <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a>. Kindly access by your authentication.</p> --}}
               
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is auto generated message. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size : 14px;" class="email-title">HRD Corp: Notifikasi Berkenaan Kursus yang Diminati</div>
            <br>
            <p>Kepada Tuan/Puan yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold;text-decoration : underline;" class="email-title-underline">NOTIFIKASI KURSUS YANG DIMINATI</div>
            
            <div>
                <p>Emel ini adalah untuk memaklumkan bahawa menerima kursus yang diminati oleh majikan. </p>
               
                <p>Butiran majikan seperti di bawah: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">MYCOID Majikan<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empMyCoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Nama Majikan<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empName}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">E-mel Majikan<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empEmail}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Maklumat Hubungi Majikan<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$empContact}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">MyCOID Penyedia Latihan  <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$tpMyCoid}}</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Kursus <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">{{$courseName}}</div>
                    </div>
                </div>
                {{--<p>Anda boleh melayari pada <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> untuk mengakses laman web ini. </p>--}}
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Maklumat ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>


   
</div>
