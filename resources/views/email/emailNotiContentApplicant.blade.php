<style>
   

    /* .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    } */
</style>

@php

$data = !empty($data) ? $data : [];
$receiverName = !empty($data) ? $data['receiver_name'] : '';
$traceUrl = !empty($data) ? $data['trace_url'] : '';

@endphp 

<div style="font-family:'Montserrat';padding : 2rem;">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">HRD Corp: Acknowledgement of Whistleblower Report</div>
            <br>
            <p>Dear Sir/Madam {{$receiverName}},</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">ACKNOWLEDGEMENT OF WHISTLEBLOWER REPORT</div>
            
            <div>
              
                <!--<p>This email is just to acknowledging receipt of your report dated [date received] regarding [reportname]. While we investigate this, you could try track the status case at </p>-->

                <p>Thank you for contacting HRD Corp Whistleblower!</p>
                <p>We acknowledge that we have received your report, and it has been escalated to the Whistleblower team. Your email ticket will be personally handled by the Whistleblower team.</p>
                <p>Rest assured, the Whistleblower team will take action on your complaint and will reach out to you if any additional information is needed from time to time.</p>
                <p>While we investigate this, you can track the status case at <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> searching by Case ID. </p>
                <p>Your whistleblower case details as per below: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Case Id <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data"></div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Description <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">Submission of Interest</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Detail <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data"></div>
                    </div>
                   
                   
                </div>
                <p>We want to thank you for taking the time to submit this report and for helping us to make our company a safer and more ethical workplace.</p>
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is computer generated. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">HRD Corp: Pengakuan Laporan Pemberi Maklumat (<span style="font-style:italic;">Whistleblower</span>)</div>
            <br>
            <p>Kepada Tuan/Puan {{$receiverName}} yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">PENGAKUAN LAPORAN PEMBERI MAKLUMAT (<span style="font-style:italic;">WHISTLEBLOWER</span>)</div>
            
            <div>
              
               <!-- <p>E-mel ini hanya untuk mengakui penerimaan laporan anda bertarikh [tarikh terima] mengenai [reportname]. Semasa kami menyiasat perkara ini, anda boleh cuba menjejaki kes status di </p>-->
                {{--<p><a target="_blank" href="http://localhost/WhistleBlower/public/searchMySubmission?status=all&searchfromemail=<?=$encodeUrl?>">{{$encodeUrl}} </a>dengan ID Kes.</p>--}} 
                
                <p>We want to thank you for taking the time to submit this report and for helping us to make our company a safer and more ethical workplace.</p>
                <p>Semasa kami menyiasat perkara ini, anda boleh menjejaki kes status di <a target="_blank" href="{{$traceUrl}}">{{$traceUrl}}</a> mencari mengikut Id Kes. </p>
                
                
                <p>Butiran kes pemberi maklumat anda seperti di bawah: </p>
                <div class="detail-container">
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Id Kes<span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data"></div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Penerangan <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data">Penghantaran berminat course</div>
                    </div>
                    <div style="display : flex;" class="detail-row">
                        <div style="min-width : 100px;display:flex;justify-content: space-between;" class="detail-label">Butiran <span style="padding : 0 0.2rem;" class="comma">:</span></div>
                        <div class="detail-data"></div>
                    </div>
                </div>
                <p>Kami ingin mengucapkan terima kasih kerana meluangkan masa untuk menyerahkan laporan ini dan membantu kami menjadikan syarikat kami tempat kerja yang lebih selamat dan beretika.</p>
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>
</div>
