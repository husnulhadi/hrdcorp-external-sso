<style>
   

    /* .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    } */
</style>

@php

$data = !empty($data) ? $data : [];
$receiverName = !empty($data) ? $data['receiver_name'] : '';
$pendingCases = !empty($data) ? $data['pending_case'] : [];

@endphp 

<div style="font-family:'Montserrat';padding : 2rem;">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">HRD Corp: Reminder Pending Whistleblower Cases</div>
            <br>
            <p>Dear Sir/Madam,</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">PENDING WHISTLEBLOWER CASES</div>
            
            <div>
                <!--<p>This email is just to acknowledging receipt of your report dated [date received] regarding [reportname]. While we investigate this, you could try track the status case at </p>-->
            
                <p>This email is to remind the pending Whistleblower cases not been process since a week ago.</p>
                <p>The pending cases shows below : </p>

                <div>
                    <div style="max-height: 200px;overflow: auto;">
                        @foreach($pendingCases as $pcase)
                        <div>{{ $pcase }}</div>
                        @endforeach
                    </div>
                </div>
                
                
                <p>Thank you.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*This is computer generated. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div style="font-size:14px;" class="email-title">HRD Corp: Peringatan Kes Pemberi Maklumat Tidak Selesai</div>
            <br>
            <p>Tuan/Puan yang dihormati,</p>
        
            <div style="font-size : 14px;font-weight : bold; text-decoration : underline;" class="email-title-underline">PERINGATAN UNTUK MENYELESAIKAN PENGAKUAN LAPORAN WHISTLEBLOWER</div>
            
            <div> 
                <p>E-mel ini bertujuan untuk mengingatkan mengenai kes pemberi maklumat (<span style="font-style:italic;">Whistleblower</span>) yang masih belum diselesaikan dan masih belum diproses sejak seminggu yang lalu.</p>
                <p>Kes yang belum selesai dipaparkan di bawah : </p>
                <div>
                    <div style="max-height: 200px;overflow: auto;">
                        @foreach($pendingCases as $pcase)
                        <div>{{ $pcase }}</div>
                        @endforeach
                    </div>
                </div>         
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div style="padding : 0.2rem 0;" class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div style="padding : 0.2rem 0;" class="sender-item">*Ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>


    <!-- <h3>Particular of the Whistleblower</h3>
    <table style="width:100%">
        <tr>
            <th>Name:</th>  
            <td>Test</td>
        </tr>
        <tr>
            <th>IC No:</th>
            <td>12334</td>
        </tr>
        <tr>
            <th>Phone Number:</th>
            <td>123123123</td>
        </tr>
        <tr>
            <th>E-mail Address:</th>
            <td>test@gmail.com</td>
        </tr>
        <tr>
            <th>Department:</th>
            <td>ABC</td>
        </tr>
    </table>
    <br> -->

    <!-- <h3>Particular of Improper Activity</h3>
    <table style="width:100%">
        <tr>
            <th>Case ID:</th>
            <td>WB1012002100012</td>
        </tr>
        <tr>
            <th>Details:</th>
            <td>Exposed Confedential Document</td>
        </tr>
        <tr>
            <th>Place:</th>
            <td>Office</td>
        </tr>
        <tr>
            <th>Date:</th>
            <td>2023-06-06</td>
        </tr>
        <tr>
            <th>Time:</th>
            <td>18:00</td>
        </tr>
    </table> -->
</div>
