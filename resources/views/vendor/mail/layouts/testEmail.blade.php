<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        width : 100%;
    }
    th {
        width: 150px;
        text-align: left;
        background-color : #063058;
        color : #fff;
        min-width : 100px;

    }

    th,td{
        padding : 0.2rem 1rem;
    }

    .email-container{
        font-family : "Montserrat";
        padding : 2rem;
    }

    .email-title{
        font-size : 14px;
        /* font-weight : bold; */
    }

    .email-title-underline{
        font-size : 14px;
        font-weight : bold;
        text-decoration : underline;
    }

    .detail-container{

    }

    .detail-row{
        display : flex;
    }

    .detail-label{
        min-width : 100px;
        display:flex;
        justify-content: space-between;
    }

    .detail-data{

    }

    .sender{

    }
    .sender-item{
        padding : 0.2rem 0;
    }
    .comma{
        padding : 0 0.2rem;
    }
</style>

@php
$username = 'Test'; 
$caseID = 'T0001';
$formInfo = [];

$caseId = isset($caseId) ? $caseId : 'WB1213232323';
$encodeUrl = isset($encodeUrl) ? $encodeUrl : 'http://localhost/WhistleBlower/public/searchTrackList?status=all&searchfromemail=V0IxNjkxNzE3NDg2OTU%3D';
@endphp 

<div class="email-container">
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div class="email-title">HRD Corp: Acknowledgement of Whistleblower Report</div>
            <br>
            <p>Dear Sir/Madam XXXXXXXXXXXX,</p>
        
            <div class="email-title-underline">ACKNOWLEDGEMENT OF WHISTLEBLOWER REPORT</div>
            
            <div>
                <!-- <p>Dear WhistleBlower {{$username}},</p>
                <p>A Whistle Blower cases submitted with Case Id : {{$caseID}}</p>
                <p>You may visit this link : http://xxx.xxxx.xxx/WhistleBlower/public/checkMySubmission to trace the progress of the case by using Case ID. -->

                <p>This email is just to acknowledging receipt of your report dated [date received] regarding [reportname]. While we investigate this, you could try track the status case at </p>
               
                <p><a target="_blank" href="http://localhost/WhistleBlower/public/searchMySubmission?status=all&searchfromemail=<?=$encodeUrl?>">{{$encodeUrl}}</a> searching by Case ID. </p>
                <p>Your whistleblower case details as per below: </p>
                <div class="detail-container">
                    <div class="detail-row">
                        <div class="detail-label">Report <span class="comma">:</span></div>
                        <div class="detail-data">xxxxxxxx</div>
                    </div>
                    <div class="detail-row">
                        <div class="detail-label">Name <span class="comma">:</span></div>
                        <div class="detail-data">Test Whistle Blower</div>
                    </div>
                    <div class="detail-row">
                        <div class="detail-label">Case Id <span class="comma">:</span></div>
                        <div class="detail-data">{{$caseId}}</div>
                    </div>
                </div>
                <p>We want to thank you for taking the time to submit this report and for helping us to make our company a safer and more ethical workplace.</p>
                <p>Thank you.</p>

                <div class="sender">
                    <div class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div class="sender-item">*This is computer generated. No signature is required.</div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="email-wrapper">
        <div class="logo">
            <img style="max-height:70px;" src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <br><br>
        <div class="container">
            <div class="email-title">HRD Corp: Pengakuan Laporan Pemberi Maklumat</div>
            <br>
            <p>Tn/Puan/En/Cik XXXXXXXX yang dihormati,</p>
        
            <div class="email-title-underline">PENGAKUAN LAPORAN PEMBICARA</div>
            
            <div>
                <!-- <p>Dear WhistleBlower {{$username}},</p>
                <p>A Whistle Blower cases submitted with Case Id : {{$caseID}}</p>
                <p>You may visit this link : http://xxx.xxxx.xxx/WhistleBlower/public/checkMySubmission to trace the progress of the case by using Case ID. -->

                <p>E-mel ini hanya untuk mengakui penerimaan laporan anda bertarikh [tarikh terima] mengenai [reportname]. Semasa kami menyiasat perkara ini, anda boleh cuba menjejaki kes status di </p>
                <p><a target="_blank" href="http://localhost/WhistleBlower/public/searchMySubmission?status=all&searchfromemail=<?=$encodeUrl?>">{{$encodeUrl}} </a>dengan ID Kes.</p>
                <p>Butiran kes pemberi maklumat anda seperti di bawah: </p>
                <div class="detail-container">
                    <div class="detail-row">
                        <div class="detail-label">Laporan <span class="comma">:</span></div>
                        <div class="detail-data">xxxxxxxx</div>
                    </div>
                    <div class="detail-row">
                        <div class="detail-label">Nama <span class="comma">:</span></div>
                        <div class="detail-data">Test Whistle Blower</div>
                    </div>
            
                    <div class="detail-row">
                        <div class="detail-label">Id Kes <span class="comma">:</span></div>
                        <div class="detail-data">{{$caseId}}</div>
                    </div>
                </div>
                <p>Kami ingin mengucapkan terima kasih kerana meluangkan masa untuk menyerahkan laporan ini dan membantu kami menjadikan syarikat kami tempat kerja yang lebih selamat dan beretika.</p>
                <p>Terima Kasih.</p>

                <div class="sender">
                    <div class="sender-item">Pembangunan Sumber Manusia Berhad</div>
                    <div class="sender-item">PSMB CONTACT CENTRE: 1800-88-4800</div>
                    <div class="sender-item">*Ini dihasilkan oleh komputer. Tiada tandatangan diperlukan.</div>
                </div>
            </div>
        </div>
    </div>

</div>
