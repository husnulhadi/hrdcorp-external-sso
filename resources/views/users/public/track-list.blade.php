<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

</head>
    @include('layouts.partials._locale')
    @extends('layouts.master-public')
    @section('content')
    @php
    
    $allInfo = $list;
    //$profileInfo = !empty($list) ? $list['info'] : [];
    //$complaintInfo = !empty($list) ? $list['complaint'] : [];
    //$activity = !empty($list) ? $list['activity'] : [];
    //$officerInfo = !empty($list) ? $list['officer'] : [];
    $statusList = config('custom.wb.officer.status');
    $filterSearch = !empty(Request::get('search')) ? Request::get('search') : $search; 
   
    @endphp
    <div class="container-wrapper">
        <!-- <div class="container">
            <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;">
        </div> -->
        @include('layouts.partials._title',['type'=>'public'])
        <div class="container">
            <div class="card">
                <div class="card-header text-center font-weight-bold">
                {{__('list.checkmysubmissiontitle')}} {{ app('request')->input('id') }}
                </div>

                <div class="card-body">
                    <div class="container text-center">
                        <div class="row">
                            <div class="container">
                                <form name="whistle-blower-search" id="flight-booking-search" method="get" action="{{route('searchTrackList')}}">
                                    {{ csrf_field() }}
                                    <div class="search-wrapper ">
                                        <div class="search-input-wrapper">
                                            <div class="mb-3">
                                                <label for="search" class="visually-hidden">{{__('list.search')}}</label>
                                                <input type="text" class="form-control" id="search" name="search" placeholder="{{__('list.search')}}" value="{{$search}}"/>
                                            </div>
                                            <div class="mb-3">
                                                <select class="form-select" name="status" aria-label="Default select example">
                                                    <option value="all" selected="true">All</option>
                                                    @foreach($statusList as $statusL)   
                                                    <option value="{{!empty($statusL) ? $statusL['slug'] : ''}}" <?=!empty($statusL) ? $statusL['slug'] == $status ? 'selected': '' : ''?>>{{!empty($statusL) ? __('form.'.$statusL['name']) : ''}}</option>
                                                    @endforeach 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="search-btn-wrapper">
                                            <div class="col-auto">
                                                <button class="btn btn-primary mb-3 btn-hrd-theme">{{__('list.search')}}</button>
                                            </div>
                                        
                                            <div class="col-auto">
                                                <a href="{{route('trackList')}}" style="color:#0d6efd; text-decoration:underline;"><button class="btn btn-primary mb-3 btn-hrd-theme">{{__('list.reset')}}</button></a>
                                            </div>
                                        </div>  
                                    </div>
                                </form>
                            </div>
                            <div class="col-4">
                            </div>
                            <!-- <div class="col-2">
                                <button class="btn btn-primary">Extract Data</button>
                            </div> -->
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                                {{__('list.caseid')}}
                                            </div>

                                            <div class="th-icon-wrapper">
                                               {{-- <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=asc&sortCol=CaseID'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'CaseID' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=desc&sortCol=CaseID'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'CaseID' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>--}}

                                            </div>
                                        </div>    
                                    </th>
                                    <th> 
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{-- @if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale'))
                                                {{__('list.wbname')}} <span class="wb-text ">Whistleblower</span>
                                                @else
                                                {{__('list.wbname')}} (<span class="wb-text wb-text-italic">Whistleblower</span>)
                                                @endif --}}

                                                {{__('list.wbname')}}

                                            </div>

                                            <div class="th-icon-wrapper">
                                             {{--   <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=asc&sortCol=ComName'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'ComName' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=desc&sortCol=ComName'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'ComName' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>--}}
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.casedetail')}}
                                            </div>

                                            <div class="th-icon-wrapper">
                                               {{-- <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=asc&sortCol=ImproperActivityDetail'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'ImproperActivityDetail' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=desc&sortCol=ImproperActivityDetail'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'ImproperActivityDetail' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>--}}
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.submitdate')}}
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=asc&sortCol=activity_datetime'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'activity_datetime' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=desc&sortCol=activity_datetime'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'activity_datetime' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.picofficer')}}
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=asc&sortCol=fullname'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'fullname' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=desc&sortCol=fullname'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'fullname' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.status')}}
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=asc&sortCol=activity_status'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'activity_status' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&search='.$filterSearch.'&page=1&sortOrder=desc&sortCol=activity_status'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'activity_status' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(empty($list))
                                <tr>
                                    <td colSpan="6" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                                </tr>
                                @else
                                @foreach($list as $ac)
                                @php
                                    $ac = json_decode(json_encode($ac), true);
                                @endphp
                                <tr>
                                    <td>{{$ac['CaseID']}}</td>
                                    <td>{{$ac['ComName']}}</td>
                                    <td>{{$ac['ImproperActivityDetail']}}</td>
                                    <td>{{$ac['activity_datetime']}}</td>
                                    <td>{{$ac['fullname']}}</td>
                                    @php
                                        $sta = !empty($ac) ? __('form.'.$statusList[$ac['activity_status']]['name']) : '';
                                    @endphp
                                    {{--<td>{{!empty($ac) ?$statusList[$ac['activity_status']]['name'] : ''}}</td>--}}
                                    <td>{{$sta}}</td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div class="d-flex pagination-container"> 
                            
                        @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        {{--<span class="">show {{ $list->perPage() }} list per page</span>--}}
                        
                     
                       {{-- <span>{{__('list.showpagecount',['pagecount'=>$list->perPage()])}}</span>--}}
                       <span>{{__('list.showpagecount2',['start'=>$list->firstItem(),'end'=>$list->lastItem(), 'total'=>$list->total()])}}</span>
                        @endif

                        @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                                <span> {{ $list->appends(['search' => $search, 'sortOrder'=>Request::get('sortOrder'), 'sortCol'=>Request::get('sortCol')])->links() }}</span>
                        @endif
                        

                    </div>
                   
                    <!-- <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-end">
                            <li class="page-item disabled">
                            <a class="page-link">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <script>
       
    </script>

    @endsection


</html>