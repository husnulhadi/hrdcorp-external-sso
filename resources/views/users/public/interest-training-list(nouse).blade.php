<!DOCTYPE html>
<html lang="en">
<head>
  
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <script src="https://cdn.tailwindcss.com"></script> -->
    
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->    
</head>
<style>
.th-bi-wb{
    min-width : 195px;
}

.th-bm-wb{
    min-width : 215px;
}

</style>
    @include('layouts.partials._locale')
    @extends('layouts.master')
    @section('content')

    @php
    //$officerInfo = !empty($officerInfo) ? $officerInfo : [];
    $officerInfo = [];
    $statusList = config('custom.wb.officer.status');
    $currentOfficer = !empty($user) ? $user['Email'] : '';
    $currentOfficerId = !empty($user) ? $user['id'] : '1';
    $statusList = config('custom.wb.officer.status');
    $rowCount = 0;
    $info = [];
    $suspect = '';
    $suspect_info = [];
    $wbTextStyle = Session::get('locale') != config('app.fallback_locale') ? 'wb-text-italic' : '';
    $thWbText = empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale') ? 'th-bi-wb' : 'th-bm-wb';
    //dd(url()->current());
    //dd(Request::get('status'));
    //dd(Request::get('page'));//page reset 1
    @endphp
    <div class="container-wrapper">
        <!-- <div class="container">
            <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;">
        </div> -->
        @include('layouts.partials._title',['type'=>'officer'])
        <div class="container">
            <div class="card">
                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                @if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale'))
                    {{__('list.listtitle')}}
                    @else
                    {{__('list.listtitle')}} (<span class="wb-text wb-title wb-text-italic">Business Matching NHCCE 2023</span>)
                    @endif
                </div>
                <div class="card-body">
                    <div class="container text-center">
                        <div class="row">
                            <div class="">
                                {{--<form name="whistle-blower-search" id="whistle-blower-search" method="get" action="{{route('listByIdSearch',['devOfficerId'=>$currentOfficerId])}}">--}}
                                    {{-- csrf_field() --}}
                                    
                                    <div class="search-container">
                                        <div class="search-wrapper left">
                                            <form name="whistle-blower-search" id="whistle-blower-search" method="get" action="{{route('listByIdSearch',['devOfficerId'=>$currentOfficerId])}}">
                                                <div class="search-wrapper">
                                                    <div class="search-input-wrapper">
                                                        <div class="mb-3">
                                                            <label for="search" class="visually-hidden">{{__('list.search')}}</label>
                                                            {{--<input type="text" class="form-control" id="search" name="search" placeholder="{{__('list.search')}}" value="{{$search}}">--}}
                                                            <input type="text" class="form-control" id="search" name="search"  value="{{$search}}" placeholder="{{__('list.search')}}"/>
                                                        </div>
                                                        <div class="mb-3">
                                                            <select class="form-select" name="status" aria-label="Default select example">
                                                                <option value="all" selected="true">{{__('form.all')}}</option>
                                                                @foreach($statusList as $statusL)   
                                                                <option value="{{!empty($statusL) ? $statusL['slug'] : ''}}" <?=!empty($statusL) ? $statusL['slug'] == $status ? 'selected': '' : ''?>>{{!empty($statusL) ? __('form.'.$statusL['name']) : ''}}</option>
                                                                @endforeach 
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="search-btn-wrapper">
                                                        <button type="submit" class="btn btn-primary btn-hrd-theme mb-3">{{__('list.search')}}</button>
                                                        <a href="{{route('listById2',['devOfficerId'=>$currentOfficerId])}}"><button type="button" class="btn btn-primary btn-hrd-theme mb-3">{{__('list.reset')}}</button></a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="search-wrapper right">
                                            <div class="print-btn-wrapper">
                                                {{-- <form name="whistle-blower-excel-print" id="whistle-blower-excel" method="get" action="{{route('exportExcel')}}">
                                                    <a href="#"><button type="submit" id="print-excel" class="btn btn-primary">Print Excel</button></a>
                                                </form> --}}
                                                <form name="whistle-blower-pdf-print" id="whistle-blower-pdf" method="get" action="{{route('createPDF')}}">
                                                    <input type="hidden" id="printIds" name="printIds" value=""/>
                                                    {{--<a href="#"><button type="submit" id="print-pdf" onclick="onPrint()" class="btn btn-primary btn-hrd-theme">Print Pdf</button></a>--}}
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                {{--</form>--}}
                            </div>
                            <!-- <div class="col-2">
                                <button class="btn btn-primary">Extract Data</button>
                            </div> -->
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    {{--<th><input type="checkbox" id="checkAll" class="form-check-input"></th>--}}
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                                {{__('list.caseid')}}
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=CaseID'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'CaseID' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=CaseID'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'CaseID' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text " >
                                               {{-- @if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale'))
                                                {{__('list.wbname')}} <span class="wb-text ">Whistleblower</span>
                                                @else
                                                {{__('list.wbname')}} (<span class="wb-text wb-text-italic">Whistleblower</span>)
                                                @endif --}}

                                                {{__('list.wbname')}}

                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=ComName'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'ComName' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=ComName'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'ComName' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.casedetail')}}
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=ImproperActivityDetail'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'ImproperActivityDetail' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=ImproperActivityDetail'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'ImproperActivityDetail' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.submitdate')}}
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=created_at'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'created_at' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=created_at'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'created_at' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.picofficer')}}
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=fullname'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'fullname' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=fullname'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'fullname' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.status')}}
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=Status'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'Status' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=Status'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'Status' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>{{__('list.action')}}</th>
                                </tr>
                            </thead>
                        <tbody>
                            
                            @if(empty($list))
                            <tr>
                                <td colSpan="7" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                            </tr>

                            @else
                                @foreach($list as $li)   
                                    @php
                                        $li = json_decode(json_encode($li), true);
                                    @endphp
                                    <tr>
                                        {{--@if($li['Status'] == 4)
                                        <td><input type="checkbox" id="check-{$rowCount++}" value="{{$li['ProfileID']}}" class="form-check-input"/></td> 
                                        @else
                                        <td></td>
                                        @endif--}}

                                        {{--<td><input type="checkbox" id="check-{$rowCount++}" onchange="updateCheckedId()" value="{{$li['ProfileID']}}" class="form-check-input"/></td>--}} 
                                        <td>{{$li['CaseID']}}</td>
                                        <td>{{$li['ComName']}}</td>
                                        <td>{{!empty($li['ImproperActivityDetail']) ? $li['ImproperActivityDetail'] : $li['Q1']}}</td>
                                        <td>{{date('d M Y h:i:s',strtotime($li['Created_date']))}}</td>
                                        <td>{{$li['fullname']}}</td>
                                        <td>{{__('form.'.$statusList[intval($li['Status'])]['name'])}}</td> 
                                        <td>
                                            {{--@if($li['status'] == 'Pending')
                                            <a href="{{route('inbox',['id'=>$li['id'],'officerId'=>$officerInfo['id']])}}"><buttton type="button" class="btn btn-primary">Process</button></a>
                                            @else
                                            <a href="{{route('process',['id'=>$li['id'],'officerId'=>$officerInfo['id']])}}"><buttton type="button" class="btn btn-primary">View</button></a>
                                            @endif --}}
                                            
                                            @php
                                                //$picUser = !empty($li) ? $li['user'] : [];
                                                //$picOfficer = !empty($picUser) ? $picUser['Email'] : '';

                                                $picOfficer = $li['Email'];
                                                $status_name = __('form.view');
                                                
                                                if(!empty($li['Status'])){
                                                    if($li['Status'] == 1 ){
                                                        $status_name = __('form.pickup');
                                                    }else if($li['Status'] == 4 || $li['Status'] == 99){
                                                        $status_name = __('form.view');
                                                    }else{
                                                        //pic is similar to current pic -> update else can view only
                                                        $status_name = __('form.update');
                                                        // if($currentOfficerId == intval($li['PICOfficer'])){
                                                        //     $status_name = __('form.update');
                                                        // }else{
                                                        //     $status_name = __('form.view');
                                                        // }
                                                        
                                                    }
                                                } 
                                            @endphp
                                            <a href="#">
                                                <button type="button" onclick="testOpen( {{!empty($li) ? $li['ProfileID'] : ''}}, {{!empty($li) ? $li['Status'] : ''}}, '{{$picOfficer}}'  )" data-id="{{!empty($li) ? $li['ProfileID'] : ''}}"  data-status="{{!empty($li) ? $li['Status'] : ''}}" class="btn btn-primary btn-hrd-theme action-btn-custom" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                                    {{$status_name}}
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach 
                            @endif
                            </tbody>
                        </table>
                        <!-- <div class="row">
                            <div class="col" id="buttonSection" style="display:none;">
                                <button class="btn btn-primary" id="rejectButton">Reject</button>
                                <button class="btn btn-primary" id="completeButton">Complete</button>
                            </div>
                        </div> -->
                    </div>
            
                    <div class="d-flex pagination-container"> 
                     
                        @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        {{--<span class="">show {{ $list->perPage() }} list per page</span>--}}
                        {{--<span>{{__('list.showpagecount',['pagecount'=>$list->perPage()])}}</span>--}}
                        <span>{{__('list.showpagecount2',['start'=>$list->firstItem(),'end'=>$list->lastItem(), 'total'=>$list->total()])}}</span>
                        @endif

                        @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        <span> {{ $list->appends(['search' => $search, 'status'=>$status, 'sortOrder'=>Request::get('sortOrder'), 'sortCol'=>Request::get('sortCol')])->links() }}</span>
                        @endif
                        

                    </div>
                    <!-- <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-end">
                            <li class="page-item disabled">
                            <a class="page-link">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav> -->
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    {{--@include('layouts.partials._modal',['testId'=>123,'modalId'=>123,'status'=>2, 'user'=>2, 'formInfo'=>[]])--}}
    @include('layouts.partials._modal-adjusted',['testId'=>123,'modalId'=>123,'status'=>2, 'user'=>2, 'formInfo'=>[]])
    <script>
       
        
       function onPrint(){
            // $('input[name=""]:checked').each(function() {
            //     console.log(this.value);
            // });
            let checkedIds = [];
            // console.log('test check input')
            // console.log($('.form-check-input:checked').length);
            let checkedLength = $('.form-check-input:checked').length;

            if(checkedLength == 0){
                Swal.fire({
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                    title : 'Invalid Print'.
                    text : 'Please tick to print',
                    icon : 'info'
                })
            }
            // $('.form-check-input:checked').each(function() {
            //     checkedIds.push(this.value);
            //     console.log(this.value);
            // });
            // var ids = checkedIds.join(',');
            // window.location.href = urlWithParam;
       }


       function updateCheckedId(){
            let checkedIds = [];
            $('.form-check-input:checked').each(function() {
                checkedIds.push(this.value);
                console.log(this.value);
            });
            var ids = checkedIds.join(',');
            $('#printIds').val(ids);
       }
       
    </script>

    @endsection


</html>


