<!DOCTYPE html>
<html lang="en">

<head>
    
    <!-- Title Page-->
    <title>HRD Corp BizMatch</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css">
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js"></script>
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->
    

</head>
    
<style>
    .wb-title-custom{
        text-align : center;
        padding : 1rem;
    }

    .custom-verification{
        display : flex;
    }
    .wb-checkbox{
        margin-right : 0.5rem;
    }

    .hide{
        display : none!important;
    }

    .btn-container{
        display: flex;
        justify-content: flex-end;
        padding : 0.5rem 0;
    }
    .input-error{
        border : 1px solid #FF0000 !important;
    }

    .error-message{
        color : #FF0000;
    }

    .input-error:focus{
        box-shadow : 0 0 0 0.25rem rgba(255, 0, 0,.25) !important;
    }

    .file-wrapper{
        display:flex;
        column-gap : 0.5rem;
        padding : 0.5rem 0;
    }

    .supporting-documents-label{
        margin : 0.2rem 0;
    }

    .btn-align-center{
        display : flex;
        justify-content : center;
        padding : 0.5rem 0;
    }


    /**Custom dropdown */

    .custom-multiple-select{

    }

    .custom-input{

    }

    .custom-dropdown{
        border :1px solid #ced4da;
        border-radius : .25rem;
        height : 200px;
        overflow : auto;
    }

    .custom-dropdown-ul{
        list-style-type : none;
        margin : 0;
        padding : 0;
        /* padding : 0.5rem 0; */
    }

    .single:hover{
        background-color : grey; 
    }

    .single {
        padding : 0.2rem 0;
    }

    .custom-dropdown-ul-span{
        padding : 0 1rem;
    }

    .bold {
        font-weight : bold;
    }

    .custom-dropdown-li:hover{
        background-color : grey;
    }

    .custom-dropdown-li-wrapper{
        display : flex;
        align-items : center;
        padding : 0.2rem 1rem;
        column-gap : 0.5rem;        
    }

    .scroll {
        height : 150px;
        overflow : auto;
    }

    .declaration-wrapper{
        display:flex;
    }

    .btn-wrapper{
        display: flex;
        justify-content: center;
        column-gap: 0.5rem;
    }

    .form-container{
        margin : 1rem auto;
    } 
    
    .form-instruction-box{
        padding: 0.5rem 0;
        /* font-weight : bold; */
        font-style : italic;
    }

    .form-ins-main{

    }

    .form-ins-sub{
        font-size : 14px;
    }

    .suspect-individual-label{
        font-style : italic;
    }

    .suspect-title-wrapper{
        display : flex;
        align-items: center;
        column-gap: 1rem;
    }

    .policy{
        height: 200px;
        overflow: auto;
        padding: 2rem;
        border: 1px solid #ced4da;
        border-radius: 4px;
        /* border: 1px solid #002169;
        border-radius: 0!important; */
    }


    .file-upload-instruction-wrapper{
        /* display : flex; */
    }

    .file-upload-instruction-group{
        display : flex;
    }

    .file-upload-instruction{
        /* font-style : italic; */
        font-size : 12px; 
        font-weight : bold;
    }

    .file-upload-ins-sub{
        font-size : 12px; 
    }

    .form-check-input:checked {
        background-color: #002169;
        border-color: #002169;
    }

    .country-info-wrapper, .state-info-wrapper{
        display:flex;
        column-gap:1rem;
    }

    .country-input-info-fullwidth{
        width : 100%;
    }

    .country-input-info{
        width : 50%;
    }

    .state-info-input-wrapper{
        width : 33%;
    }

    .custom-container-setting{
        margin-top : 8rem;
    }

    .form-title{
        display : flex;
        justify-content : space-between;
        align-items : center;
    }

    .disclosure-reminder{
        font-style : italic;
        font-size : 14px;
    }

    .declaration-input-wrapper{
        padding-bottom : 0.5rem;
    }

    .wb-text{
        font-weight : 500;
    }
    .wb-text-italic{
        font-style : italic;
    }

    
    .btn-submit:disabled{
        background-color: #6c757d!important;
        border : 1px solid #6c757d;
    }


    @media screen and (max-width: 750px) {
        .form-title{
            display : block;
        }

        .user-guide{
            width : 100%;
        }

        .state-info-wrapper{
            display : block;
        }

        .state-info-input-wrapper {
            width : 100%;
        }

        .file-upload-instruction-wrapper, .file-upload-instruction-group{
            display : block;
        }
    }


</style>
    @include('layouts.partials._locale')
    @extends('layouts.master-public')
    @section('content')
    
    
    @php 
    $isDisabled = false; 
    $isAssignedOfficer = false;
    $stateList = config('location.state');
    $fInfo = !empty($formInfo) ? $formInfo['info'] : [];
    $applicantInfo = !empty($fInfo) ? $fInfo['applicant_info'] : [];
    $suspectInfo = !empty($fInfo) ? $fInfo['suspect_info'] : [];
    $witnessInfo = !empty($fInfo) ? $fInfo['witness_info'] : [];
    $complaintInfo = !empty($fInfo) ? $fInfo['complaint_info'] : [];
    $disclosureList = config('custom.wb.officer.disclosure');
    $currentLocale = !empty(App::currentLocale()) ? App::currentLocale() : config('app.fallback_locale');
    $wbTextStyle = Session::get('locale') != config('app.fallback_locale') ? 'wb-text-italic' : '';

    $confirmSubmitText = Session::get('locale') != config('app.fallback_locale') ? __('form.confirmsubmittext',['wbtext'=>'Whistleblower','send'=>'Hantar']) : __('form.confirmsubmittext');
    @endphp
    
    <!-- <div class="container">
        <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
    </div> -->

    <div class="container-wrapper custom-container-setting">
        <div class="container form-title">
            @include('layouts.partials._title',['type'=>'public'])
            <div class="container" style="padding: 2rem 0;text-align:right;">
                <a href="https://hrdcorp-my.sharepoint.com/:b:/g/personal/wongleeping_hrdcorp_gov_my/EadM7EyrFXpFpZryBF1DEbkB39bAB_HQE6vvLDZBWaG9eA?e=FBlub8" target="_blank">
                    <button type="button" class="btn btn-primary btn-hrd-theme user-guide" data-bs-dismiss="modal">{{__('form.userguide')}}</button>
                </a>
            </div>
        </div>
        <div class="container">
            <div class="card">
                {{--<div class="card-header text-center font-weight-bold hrd-card-header-text">
                    {{ __('form.wbform') }}
                </div>--}}
               
                <!-- <form name="whistle-blower-form" id="whistle-blower-form" action="{{route('processCreate')}}" method="post" enctype="multipart/form-data" novalidate> -->
                <form name="whistle-blower-form" id="whistle-blower-form"  method="post" enctype="multipart/form-data" novalidate>
                    {{ csrf_field() }}

                    <div class="accordion" id="accordionPanelsStayOpenExample">

                        <div class="accordion-item" id="disclosure_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                <b>{{ __('form.typeofdisclosure')}}</b>
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseFive" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFive">
                                <div class="accordion-body">
                                    <div class="form-instruction-box">
                                        <span class="form-ins-main">{{__('form.disclosureinstruction')}}</span>
                                        <br>
                                        <span class="form-ins-sub">{{__('form.disclosureinstruction3')}}</span>
                                        <br>

                                        <span class="form-ins-main">{{__('form.disclosureinstruction2')}}</span> 
                                        <span class="text-danger">*</span>
                                        <br>
                                        <span class="form-ins-sub">{{__('form.disclosureinstruction4')}}</span> 
                                        <span class="text-danger">*</span>
                                    <div>
                                        @foreach($disclosure_email as $disclosure)
                                        <div>
                                            <div class="form-check">
                                                <input class="form-check-input"  class="disclosure_email" onchange="onDisclosureChange('{{$disclosure["id"]}}', '{{$disclosure["id"]}}')" id="{{!empty($disclosure) ? $disclosure['id'] : ''}}" type="checkbox" value="{{!empty($disclosure) ? $disclosure['id'] : ''}}" name="disclosure_email[]" id="disclosure_jaksa_{{$disclosure['id']}}">
                                                <label class="form-check-label" for="{{!empty($disclosure) ? $disclosure['id'] : ''}}">
                                                {{__('form.'.$disclosure['name'])}}
                                                </label>
                                            </div>
                                            <span id="disclosure-reminder-{{$disclosure['id']}}" class="disclosure-reminder hide">{{__('form.'.$disclosure['reminder'])}}</span>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item " id="applicant_info">
                            <h2 class="accordion-header hrd-acc-btn-header" id="panelsStayOpen-headingTwo">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                                @if(empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale') )
                                <b>A.{{__('form.particularofwb')}} Whistleblower / {{__('form.particularofwb2')}} (<span class="wb-text wb-text-italic">Whistleblower</span>)</b> 
                                @else
                                <b>A.{{__('form.particularofwb')}} (<span class="wb-text wb-text-italic">Whistleblower</span>)/ {{__('form.particularofwb2')}} Whistleblower</b>
                                @endif
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                                <div class="accordion-body">
                                    <div class="form-instruction-box">
                                        <span class="form-ins-main">{{__('form.particularofwbinstruction')}}</span><br>
                                        <span class="form-ins-sub">{{__('form.particularofwbinstruction2')}}</span>
                                    </div>
                                    <div>
                                        <div class="mb-3">
                                            <label for="applicant_name">{{__('form.particularofwblabelname')}}</label>
                                            <input type="text" id="applicant_name" onkeyup="enableButtonByCheckField()" name="applicant_name" placeholder="eg : Abc" class="form-control"/>      
                                            <div class="error-message hide" id="err-applicant_name">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label for="applicant_mobile_phone_no">{{__('form.particularofwblabelphone')}}. <span class="text-danger">*</span></label>
                                            <input type="number" id="applicant_mobile_phone_no" onkeyup="enableButtonByCheckField()" placeholder="eg : 0112345678" name="applicant_mobile_phone_no" class="form-control" required/>
                                            <div class="error-message hide" id="err-applicant_mobile_phone_no">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                        
                                        <div class="mb-3">
                                            <label for="applicant_email_address">{{__('form.particularofwblabelemail')}}<span class="text-danger">*</span></label>
                                            <input type="email"  id="applicant_email_address" onkeyup="enableButtonByCheckField()"  placeholder="eg: aa@gmail.com" name="applicant_email_address" class="form-control" required>
                                            <div class="error-message hide" id="err-applicant_email_address">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="applicant_designation">{{__('form.particularofwblabeldesignation')}}<span class="text-danger">*</span></label>
                                            <input type="text" id="applicant_designation" onkeyup="enableButtonByCheckField()" placeholder="eg : Department A" name="applicant_designation" class="form-control" required>
                                            <div class="error-message hide" id="err-applicant_designation">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="applicant_department">{{__('form.particularofwblabeldepartment')}}<span class="text-danger">*</span></label>
                                            <input type="text" id="applicant_department" onkeyup="enableButtonByCheckField()" placeholder="eg : Agency B" name="applicant_department" class="form-control" required>
                                            <div class="error-message hide" id="err-applicant_department">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <button id="btn_applicant_info" type="button" onclick="next('applicant_info','suspect_info')" class="btn btn-primary btn-hrd-theme">{{__('form.next')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="accordion-item hide" id="suspect_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true" aria-controls="panelsStayOpen-collapseThree">
                                <b>B.{{__('form.particularofsuspect')}}</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show"  id="panelsStayOpen-collapseThree" aria-labelledby="panelsStayOpen-headingThree">
                                <div class="accordion-body">
                                    <div class="form-instruction-box">
                                        <span class="form-ins-main">{{__('form.particularofsuspectinstruction')}}</span>
                                        <br>
                                        <span class="form-ins-sub">{{__('form.particularofsuspectinstruction2')}}</span>
                                    </div>

                                    <div id="suspect-container" class="suspect-container-wrapper">
                                        <div class="suspect-wrapper">
                                            <div class="suspect-title-wrapper">
                                                <span class="suspect-individual-label">{{__('form.particularofsuspectindividual')}} 1</span>
                                                <div class="suspect-append-btn"><button class="btn btn-primary btn-hrd-theme" type="button" onclick="appendSuspect()">+</button></div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="supect_name">{{__('form.particularofsuspectlabelname')}} <span class="text-danger">*</span></label>
                                                <input type="text" id="supect_name" placeholder="eg : Abc" onkeyup="enableButtonByCheckField()" name="suspect_name[]" class="form-control" required>
                                                <div class="error-message hide" id="err-suspect_name">
                                                    Please select a valid state.
                                                </div>
                                            </div>
                                    
                                            <div class="mb-3">
                                                <label for="suspect_email_address">{{__('form.particularofsuspectlabelemail')}} <span class="text-danger">*</span></label>
                                                <input type="email" placeholder="eg:aa@gmail.com" id="suspect_email_address" onkeyup="enableButtonByCheckField()" name="suspect_email_address[]" class="form-control" required>
                                                <div class="error-message hide" id="err-suspect_email_address">
                                                    Please select a valid state.
                                                </div>
                                            </div>
                                    
                                            <div class="mb-3">
                                                <label for="suspect_department">{{__('form.particularofsuspectlabeldepartment')}}<span class="text-danger">*</span></label>
                                                <input type="text" id="suspect_department" placeholder="eg:Department A" onkeyup="enableButtonByCheckField()" name="suspect_department[]" class="form-control" required>
                                                <div class="error-message hide" id="err-suspect_department">
                                                    Please select a valid state.
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="suspect_relationship">{{__('form.particularofsuspectlabelrelationship')}}<span class="text-danger">*</span></label>
                                                <input type="text" id="suspect_relationship" placeholder="eg:Colleague" onkeyup="enableButtonByCheckField()" name="suspect_relationship[]" class="form-control" required>
                                                <div class="error-message hide" id="err-suspect_relationship">
                                                    Please select a valid state.
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="suspect-append-btn"><button class="btn btn-primary" onclick="appendSuspect()">+</button></div> -->
                                    </div>
                                
                                           
                                    <div class="mb-3">
                                        <button id="btn_suspect_info" type="button" onclick="next('suspect_info','complaint_info')" class="btn btn-primary btn-hrd-theme">{{__('form.next')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div> 

                    
                    
                        <div class="accordion-item hide" id="complaint_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingFive">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseFive" aria-expanded="true" aria-controls="panelsStayOpen-collapseFive">
                                <b>C.{{__('form.particularofimproperconduct')}}</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show" id="panelsStayOpen-collapseFour" aria-labelledby="panelsStayOpen-headingFive">
                                <div class="accordion-body">
                                    <div class="form-instruction-box">
                                        <span class="form-ins-main">{{__('form.particularofimproperconductinstructionpart1')}}</span>
                                        <br>
                                        <span class="form-ins-main">{{__('form.particularofimproperconductinstructionpart2')}}</span>
                                        <br>
                                        <span class="form-ins-sub">{{__('form.particularofimproperconductinstructionpart3')}}</span>
                                        <br>
                                        <span class="form-ins-sub">{{__('form.particularofimproperconductinstructionpart4')}}</span>

                                    </div>

                                    <div>
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_date">{{__('form.particularofimproperconductdate')}}<span class="text-danger">*</span></label>
                                            <input type="date" onChange="enableButtonByCheckField()" id="complaint_improper_activity_date"   name="complaint_improper_activity_date" class="form-control" required>
                                        </div>
                                        <!-- 
                                        <div class="mb-3">
                                            <div class="input-group date" id="datepicker">
                                                <input type="text" name="complaint_improper_activity_date2" class="form-control" placeholder="dd/mm/yyyy" id="complaint_improper_activity_date2"/>
                                                <span class="input-group-append">
                                                    <span class="input-group-text bg-light d-block">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div> -->
                                        
                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_time">{{__('form.particularofimproperconducttime')}} <span class="text-danger">*</span></label>
                                            <input type="time" onChange="enableButtonByCheckField()" id="complaint_improper_activity_time" name="complaint_improper_activity_time" class="form-control" required>
                                        </div>

                                        <div class="country-info-wrapper">
                                            <div class="mb-3 country-input-info-fullwidth" id="country_input">
                                                <label for="complaint_improper_activity_country">{{__('form.country')}} <span class="text-danger">*</span></label>
                                                <select id="complaint_improper_activity_country" class="form-select" onchange="onCountryChange('complaint_improper_activity_country')"  name="complaint_improper_activity_country" aria-label="Default select example">
                                                    <option value="MAL">Malaysia</option>
                                                    <option value="other">{{__('form.other')}}</option>
                                                </select>
                                            </div>
                                            <div class="mb-3 country-input-info hide" id="other_country_input">
                                                <label for="complaint_improper_activity_other_country">{{__('form.othercountry')}} <span class="text-danger">*</span></label>
                                                <input type="text" placeholder="eg:Australia" onkeyup="enableButtonByCheckField()" onchange="enableButtonByCheckField()" id="complaint_improper_activity_other_country" name="complaint_improper_activity_other_country" class="form-control" required>
                                                <div class="error-message hide" id="err-complaint_improper_activity_other_country">
                                                    Please select a valid state.
                                                </div>
                                            </div>
                                        </div>
                                           
                                           
                                        <div class="state-info-wrapper" id="state-info">
                                            <div class="mb-3 state-info-input-wrapper">
                                                <label for="complaint_improper_activity_state">{{__('form.state')}}<span class="text-danger">*</span></label>
                                                <select class="form-select" onchange="enableButtonByCheckField()" name="complaint_improper_activity_state" aria-label="Default select example">
                                                    @foreach($stateList as $state)
                                                    <option value="{{!empty($state) ? $state['slug'] : ''}}">{{!empty($state) ? $state['name'] : ''}}</option>
                                                    @endforeach
                                                    {{--<option value="other">{{__('form.other')}}</option>--}}
                                                </select>
                                            </div>

                                            <div class="mb-3 state-info-input-wrapper">
                                                <label for="complaint_improper_activity_district">{{__('form.district')}}<span class="text-danger">*</span></label>
                                                <input type="text" placeholder="eg:Petaling Jaya" onkeyup="enableButtonByCheckField()" id="complaint_improper_activity_district" name="complaint_improper_activity_district" class="form-control" required>
                                            </div>


                                            <div class="mb-3 state-info-input-wrapper">
                                                <label for="complaint_improper_activity_postcode">{{__('form.postcode')}}<span class="text-danger">*</span></label>
                                                <input type="number" placeholder="eg:43300" onkeyup="enableButtonByCheckField()" id="complaint_improper_activity_postcode" name="complaint_improper_activity_postcode" class="form-control" required>
                                            </div>
                                        </div>


                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_place">{{__('form.particularofimproperconductplace')}}<span class="text-danger">*</span></label>
                                            <input type="text" placeholder="eg:xxmall, Petaling Jaya" onkeyup="enableButtonByCheckField()" id="complaint_improper_activity_place" name="complaint_improper_activity_place" class="form-control" required>
                                        </div>


                                        <div class="mb-3">
                                            <label for="complaint_improper_activity_detail">{{__('form.particularofimproperconductdetail')}}<span class="text-danger">*</span></label>
                                            <textarea id="complaint_improper_activity_detail" placeholder="eg:Confidential document expose" onkeyup="enableButtonByCheckField()" name="complaint_improper_activity_detail" class="form-control textarea-setting" required></textarea>
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label class="supporting-documents-label" for="complaint_supporting_docs">{{__('form.particularofimproperconductdocument')}}</label>
                                            <div id="field-container-doc">
                                                <div class="form-field">
                                                    <div class="file-upload-instruction-wrapper">
                                                        <!-- <span class="text-danger">*</span> -->
                                                        <div class="file-upload-instruction-group">
                                                            <span class="file-upload-instruction">{{__('form.particularofimproperconductdocumentinstrcution')}}</span>
                                                            <span class="file-upload-ins-sub ">/ {{__('form.particularofimproperconductdocumentinstrcution2')}}</span>
                                                            <span class="text-danger">*</span>
                                                        </div>
                                                        <div class="file-upload-instruction-group">
                                                            <span class="file-upload-instruction">{{__('form.particularofimproperconductdocumentinstrcution3')}}</span>
                                                            <span class="file-upload-ins-sub">/ {{__('form.particularofimproperconductdocumentinstrcution4')}}</span>
                                                            <span class="text-danger">*</span>
                                                        </div>
                                                       
                                                       
                                                    </div>
                                                    <div class="file-container" id="file-container" style="padding-bottom:10px">
                                                        <div class="file-wrapper">
                                                            <input type="file" class="form-control" id="supporting_doc_0" onchange="checkUpload('supporting_doc_0')" name="complaint_supporting_docs[]" >
                                                            <button type="button" class="btn btn-primary btn-hrd-theme" onClick="appendFileInput()">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="error-message hide" id="err-witness_department">
                                                Please select a valid state.
                                            </div>
                                        </div>
                                        <!-- <script type="text/javascript" src="https://form.jotform.com/jsform/232131071614038"></script> -->
                                       
                                        <!--Option using page link Check Terms-->
                                        {{--<div class="declaration-container">
                                            <div>
                                                <h4>{{__('form.declarationtitle')}}</h4>
                                                <span class="declare-lang">*{{__('form.declarationstatement')}}</sapn>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_1" name="declare_1" required>
                                                <label class="declare-label" for="declare_1">{{__('form.declaration1')}}</label><br>
                                                <span class="declare-lang">{{__('form.declaration1lang')}}</span>
                                            </div>
                                            <div class="form-check"> 
                                                <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_2" name="declare_2" required>
                                                <label class="declare-label" for="declare_2">{{__('form.declaration2')}}</label><br>
                                                <span class="declare-lang">{{__('form.declaration2lang')}}</span>
                                            </div>
                                            <div class="form-check"> 
                                                <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_3" name="declare_3" required>
                                                <label class="declare-label" for="declare_3">{{__('form.declaration3')}}</label><br>
                                                <span class="declare-lang">{{__('form.declaration3lang')}}</span>
                                            </div>
                                            <div class="form-check"> 
                                                <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_4" name="declare_4" required>
                                                <label class="declare-label" for="declare_4">{{__('form.declaration4')}}</label><br>
                                                <span class="declare-lang">{{__('form.declaration4lang')}}</span>
                                            </div>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="termsClick" onChange="enableButtonByCheckField()" id="declare_main" name="declare_main" required>
                                                <label for="declare_main">{{__('form.maindeclaration')}} <a href="{{route('termsAndConditionVerify')}}" target="_blank">To Terms Page</a></label>
                                            </div>
                                        </div>--}}
                                      

                                        <!--Option using scrollable Check Terms-->

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="termsClick" onChange="enableButtonByCheckField()" id="declare_main" name="declare_main" disabled>
                                            <label for="declare_main">{{__('form.maindeclaration')}}</label>
                                        </div>
                                    
                                        <div class="policy">
                                            @if($currentLocale == config('app.fallback_locale') || empty($currentLocale)) 
                                                @include('layouts.partials.terms._terms-policy')
                                            @else
                                                @include('layouts.partials.terms._terms-policy-bm')
                                            @endif
                                            <div class="declaration-container">
                                                <div>
                                                    <h4>{{__('form.declarationtitle')}}</h4>
                                                    <span class="declare-lang">*{{__('form.declarationstatement')}}</sapn>
                                                </div>
                                                <div class="form-check declaration-input-wrapper">
                                                    <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_1" name="declare_1" required>
                                                    <label class="declare-label" for="declare_1">{{__('form.declaration1')}}</label><br>
                                                    <span class="declare-lang">{{__('form.declaration1lang')}}</span>
                                                </div>
                                                <div class="form-check declaration-input-wrapper"> 
                                                    <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_2" name="declare_2" required>
                                                    <label class="declare-label" for="declare_2">{{__('form.declaration2')}}</label><br>
                                                    <span class="declare-lang">{{__('form.declaration2lang')}}</span>
                                                </div>
                                                <div class="form-check declaration-input-wrapper"> 
                                                    <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_3" name="declare_3" required>
                                                    <label class="declare-label" for="declare_3">{{__('form.declaration3')}}</label><br>
                                                    <span class="declare-lang">{{__('form.declaration3lang')}}</span>
                                                </div>
                                                <div class="form-check declaration-input-wrapper"> 
                                                    <input class="form-check-input" type="checkbox" onChange="enableButtonByCheckField()" id="declare_4" name="declare_4" required>
                                                    <label class="declare-label" for="declare_4">{{__('form.declaration4')}}</label><br>
                                                    <span class="declare-lang">{{__('form.declaration4lang')}}</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            
                            <div class="mb-3 btn-align-center">
                                <button id="submit" type="button" onclick="submitForm()" class="btn btn-primary btn-hrd-theme btn-submit" disabled>{{__('form.submit')}}</button>
                            </div>
                        </div> 


                        {{--<div class="mb-3 btn-align-center">
                            <button id="submit" type="button" onclick="submitForm()" class="btn btn-primary btn-hrd-theme" disabled>{{__('form.submit')}}</button>
                        </div>--}}

                    </div>

                    <button class="btn btn-primary d-none" type="button" id="loader-btn" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                    
                </form>
            </div>
        </div>
    </div>
    
    
    <script>
        let totalAccumulatedAppendCount = 0;
        let totalAccumulatedSuspectAppendCount = 0;
        let open = false;
        let exclude_required = ['applicant_name','suspect_name-2','suspect_email_address-2','suspect_department-2','suspect_relationship-2'];
        let sections_settings = {
            applicant_info : {
                validate : false,
                total_fields : 5,
                inputs : [
                    {
                        slug : 'applicant_name',
                        name : 'applicant name',
                        translate : "{{__('form.particularvalidatename')}}"
                    },{
                        slug : 'applicant_mobile_phone_no',
                        name : 'applicant mobile phone no',
                        translate : "{{__('form.particularvalidatephone')}}"
                    },
                    {
                        slug : 'applicant_email_address',
                        name : 'applicant email address',
                        translate : "{{__('form.particularvalidatemail')}}"
                    },
                    {
                        slug : 'applicant_designation',
                        name : 'applicant designation',
                        translate : "{{__('form.particularvalidatedesignation')}}"
                    },
                    {
                        slug : 'applicant_department',
                        name : 'applicant department',
                        translate : "{{__('form.particularvalidatedepartment')}}"
                    },
                ],
            },
            suspect_info : {
                validate : false,
                total_fields : 8,
                inputs : [
                    {
                        slug : 'suspect_name',
                        name : 'suspect name',
                        translate : "{{__('form.particularofsuspectvalidatename')}}"
                    },
                    // {
                    //     slug : 'suspect_mobile_phone_no',
                    //     name : 'suspect mobile phone no'
                    // },
                    {
                        slug : 'suspect_email_address',
                        name : 'suspect email address',
                        translate : "{{__('form.particularofsuspectvalidateemail')}}"
                    },
                    // {
                    //     slug : 'suspect_designation',
                    //     name : 'suspect designation'
                    // },
                    {
                        slug : 'suspect_department',
                        name : 'suspect department',
                        translate : "{{__('form.particularofsuspectvalidatedepartment')}}"
                    },
                    {
                        slug : 'suspect_relationship',
                        name : 'suspect relationship',
                        translate : "{{__('form.particularofsuspectvalidaterelationship')}}"
                    },
                    {
                        slug : 'suspect_name-2',
                        name : 'suspect name'
                    },
                    {
                        slug : 'suspect_email_address-2',
                        name : 'suspect email address'
                    },
                    {
                        slug : 'suspect_department-2',
                        name : 'suspect department'
                    },
                    {
                        slug : 'suspect_relationship-2',
                        name : 'suspect relationship'
                    },
                ],
            },
         
        }
    
        $(document).ready(function() {
            localStorage.clear();

            // $(function(){
            //     $('#datepicker').datepicker({
            //         format: 'dd/mm/yyyy',
            //     });
            // });

            // Select2 Multiple
            // $('.select2-multiple').select2({
            //     theme: "bootstrap-5",
            //     width: $( this ).data( 'width' ) ? $( this ).data( 'width' ) : $( this ).hasClass( 'w-100' ) ? '100%' : 'style',
            //     placeholder: "Select",
            //     allowClear: true,
            // });
        });
    

        function onDisclosureChange(selectorId, slug){
            enableButtonByCheckField();
            let checkbox = document.getElementById(selectorId); 
            if(checkbox.checked){
                $('#disclosure-reminder-'+slug).removeClass('hide');
            }else{
                $('#disclosure-reminder-'+slug).addClass('hide');
            } 
        }

        function onCountryChange(selectorId){
            let country = $('#'+selectorId).val();
            if(country == 'other'){
                $('#other_country_input').removeClass('hide');
                $('#country_input').removeClass('country-input-info-fullwidth');
                $('#country_input').addClass('country-input-info');
                $('#state-info').addClass('hide');
                $("#complaint_improper_activity_other_country").attr("required", "true");
                $("#complaint_improper_activity_district").val("");
                $("#complaint_improper_activity_postcode").val("");
                $("#complaint_improper_activity_district").attr("required", "false");
                $("#complaint_improper_activity_postcode").attr("required", "false");
            }else{
                $('#other_country_input').addClass('hide');
                $('#country_input').addClass('country-input-info-fullwidth');
                $('#country_input').removeClass('country-input-info');
                $('#state-info').removeClass('hide');
                $("#complaint_improper_activity_other_country").val("");
                $("#complaint_improper_activity_other_country").attr("required", "false");
                $("#complaint_improper_activity_district").attr("required", "true");
                $("#complaint_improper_activity_postcode").attr("required", "true");
            }
        }


        function declarationIsChecked(){
            let allDeclarationChecked = false;
            let declare_1 = $('#declare_1').is(':checked');
            let declare_2 = $('#declare_2').is(':checked');
            let declare_3 = $('#declare_3').is(':checked');
            let declare_4 = $('#declare_4').is(':checked');
            return (declare_1 && declare_2 && declare_3 && declare_4);
        }

        function showMustCheckAtLeastOneForDisclosure(){
            var isChecked = false;
            isChecked = $('input[name="disclosure_email[]"]:checked').length > 0 ? true : false;
            return isChecked;
        }

        /**Option 1 Checking */
        function enableButtonByCheckField(){
            let formContainer = $('#whistle-blower-form');
            let country = $('#complaint_improper_activity_country').val();
            // - 1 to exclude for other country input as required input
            // - 2 to for district & postcode input as reqired input
            let totalCount = country == 'MAL'? $('[required]').length -1 : $('[required]').length - 2;
            //let totalCount = $('[required]').length;
            let validCount = 0;
            let invalidCount = 0;
            let isAllRequiredFieldValid = false;
            let checkTerm = localStorage.getItem("termsischeck");
            
            $('[required]').each(function() {
                if ($(this).is(':invalid') || !$(this).val()){
                    valid = false;
                    invalidCount++;
                }else{
                    validCount++;      
                } 
            })
            
            if(declarationIsChecked()){
                if ($('#whistle-blower-form')[0].checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                } 
                $('#whistle-blower-form').addClass('was-validated');
            }
            // console.log('checking and debug submit')
            // console.log(totalCount)
            // console.log('valid count');
            // console.log(validCount)
            // console.log('invalid count')
            // console.log(invalidCount);
            if(validCount == totalCount){
                let ischeckedDisclosure = showMustCheckAtLeastOneForDisclosure();
                if(ischeckedDisclosure){
                    $("#termsClick").attr("checked", true);
                    $('#submit').prop('disabled', false);
                    isAllRequiredFieldValid = true;
                }else{
                    Swal.fire({
                        icon : 'warning',
                        title : "{{__('form.choosedisclosuretitle')}}",
                        text : "{{__('form.choosedisclosure')}}",
                        confirmButtonColor: '#002169',
                    })
                }
                
            }else{
                $("#termsClick").attr("checked", false);
                $('#submit').prop('disabled', true);
            }
           
        }


        /**Option 2 Checking */
        // function enableButtonByCheckField(){
        //     let totalCount = $('[required]').length;
        //     let validCount = 0;
        //     let invalidCount = 0;
        //     let isAllRequiredFieldValid = false;
        //     $('[required]').each(function() {
        //         if ($(this).is(':invalid') || !$(this).val()){
        //             valid = false;
        //             invalidCount++;
        //         }else{
        //             validCount++;      
        //         } 
        //     })
        //     console.log('total count, valid count')
        //     console.log(totalCount,  validCount)
        //     let checkTerm = localStorage.getItem("termsischeck");
        //     if(validCount == totalCount){
        //         if(checkTerm || checkTerm != null){
        //             $('#submit').prop('disabled', false);
        //             isAllRequiredFieldValid = true;
        //         }else{
        //             console.log('false')
        //             $("#termsClick").prop("checked", false);
        //             $('#submit').prop('disabled', true);
        //             Swal.fire({
        //                 title: 'Terms and condition not verfiy',
        //                 icon : 'error',
        //                 text: 'Please read through all terms and condition by click on the link and tick with the box.',
        //                 confirmButtonText: '{{__("form.confirm")}}',
        //                 denyButtonText: '{{__("form.cancel")}}',
        //                 confirmButtonColor: '#002169',
        //             })
        //         }
                
        //     }else{
        //         $("#termsClick").attr("checked", false);
        //         $('#submit').prop('disabled', true);
        //     }       
        // }

        function submitForm () {
            if ($('#whistle-blower-form')[0].checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } 
            $('#whistle-blower-form').addClass('was-validated');
            submitAjax();
        }


        function submitAjax() {
            const form = document.getElementById('whistle-blower-form');
            var formData = new FormData(form);
            Swal.fire({
                    title: '{{__("form.confirmsubmittitle")}}',
                    text: '{{$confirmSubmitText}}',
                    showCancelButton: true,
                    confirmButtonText: '{{__("form.confirm")}}',
                    denyButtonText: '{{__("form.cancel")}}',
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processCreate')}}",
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                                if(response.status == 1)
                                {
                                    // console.log('in')
                                    // console.log(response.data)
                                    localStorage.removeItem("termsischeck");
                                    Swal.fire({
                                        title: '{{__("form.submitted")}} !',
                                        text : '{{__("form.submittedtext")}}',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#002169',
                                        }).then((result) => {
                                        /* Read more about isConfirmed, isDenied below */
                                        if (result.isConfirmed) {
                                            //location.reload();
                                            var data = response.data;
                                            //console.log(data.case_id);
                                            window.location.href = data.url;
                                        }
                                    })
                                }
                            },
                            error: function(_response){
                                window.setTimeout(function () {
                                    Swal.fire('{{__("form.unsuccessful")}}!', '', 'error');

                                }, 2000);
                            }
                        });

                } else if (result.isCanceled) {
                    Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                }
            })
        }

        function checkUpload(targetElementId){
            // size: 5867185
            // type : "application/pdf"
            let availableFileTypes = ["pdf","jpg","jpeg","png","docx"];
            var file = $('#'+targetElementId).prop('files')
            // console.log('test file upload')
            // console.log(file)
            if(Object.keys(file).length != 0){
                let fileName = file[0].name;
                let fileNameSplit = fileName.split('.');
                let fileExt = fileNameSplit[1];
                // console.log('file info')
                // console.log(fileName)
                // console.log(fileExt)
                if(!availableFileTypes.includes(fileExt)){
                    Swal.fire({
                        icon: 'error',
                        title: '{{__("form.fileuploadtypeerrtitle")}}',
                        text: '{{__("form.fileuploadtypeerrtext")}}',
                        confirmButtonColor: '#002169',
                        cancelButtonColor: '#6c757d',
                        //footer: '<a href="">Why do I have this issue?</a>'
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            $('#'+targetElementId).val('')
                        } 
                    })
                   
                }
                if(file[0].size > 10485760 ){  // 10mb
                    Swal.fire({
                        icon: 'error',
                        title: '{{__("form.fileuploadsizeerrtitle")}}',
                        text: '{{__("form.fileuploadsizeerrtext")}}',
                        confirmButtonColor: '#002169',
                        cancelButtonColor: '#6c757d',
                        //footer: '<a href="">Why do I have this issue?</a>'
                    }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            $('#'+targetElementId).val('')
                        } 
                    })
                }
           }
        }

    
     
        function next(current_section,next_section){
            validateSectionInput(current_section);
            let currentSectionInfo = sections_settings[current_section];
            let checkCurrentSectionValidationStatus = currentSectionInfo.validate;
            if(checkCurrentSectionValidationStatus){
                // console.log('next success')
                // console.log('#'+next_section)
                $('#'+next_section).removeClass('hide');
                $('#btn_'+current_section).addClass('hide');
            }
           // console.log(current_section, next_section)
        }

        
        function validateSectionInput(section_key){
            let selected_section = sections_settings[section_key].inputs;
            let selected_total_fields = sections_settings[section_key].total_fields;
            let requiredValidCount = 0;
            let numberCheck = true;
            let emailCheck = true;
            let checkboxCheck = true;
            for(let i=0; i< selected_section.length; i++){
                let info = selected_section[i];
                let value = $('#'+info.slug).val(); 
                let type = $('#'+info.slug).attr('type');

                if(value == ''){
                    if(exclude_required.includes(info.slug)){
                        requiredValidCount++;
                    }else{
                        $('#err-'+info.slug).text('{{__("form.pleasefillin")}} '+info.translate);
                        $('#err-'+info.slug).removeClass('hide');
                        $('#'+info.slug).addClass('input-error');
                    }
                }else{
                    $('#err-'+info.slug).text('');
                    $('#'+info.slug).removeClass('input-error')
                    requiredValidCount++;
                }  
            
                if(type == 'number'){
                    if(!$.isNumeric(value)){
                        $('#err'+info.slug).val('{{__("form.pleasefillinonlynumber")}} '+info.translate)   
                        numberCheck = true;
                    }else{
                        numberCheck = true;
                    }
                    
                }

                if(type == 'email'){
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if(!emailReg.test(value)){
                        $('#err-'+info.slug).text('{{__("form.pleasefillincorrectemail")}} '+info.translate);
                        $('#err-'+info.slug).removeClass('hide');
                        $('#'+info.slug).addClass('input-error');
                        emailCheck = false;
                    }else{
                        emailCheck = true;
                    }
                }

                if(type == 'checkbox'){

                }
                //sections_settings[section_key].validate = true;
                if(requiredValidCount == selected_total_fields && numberCheck == true && emailCheck == true ){
                    sections_settings[section_key].validate = true;
                }       
            }   
            
        }


        function appendFileInput () {
            totalAccumulatedAppendCount++;
            $('#file-container').append(`
            <div class="file-wrapper" data-count=${totalAccumulatedAppendCount}" id="file_wrapper_${totalAccumulatedAppendCount}">
                <input type="file" class="form-control supporting_doc" id="supporting_doc_${totalAccumulatedAppendCount}"  onchange="checkUpload('supporting_doc_${totalAccumulatedAppendCount}')" name="complaint_supporting_docs[]" required>
                <button type="button" class="btn btn-primary btn-hrd-theme" onClick="removeSelectedFileInput('file_wrapper_${totalAccumulatedAppendCount}')">-</button>
            </div>
            `);
        }

        function removeSelectedFileInput(selectorIdToRemove){
            $('#'+selectorIdToRemove).remove();
        }


        function appendSuspect(){
            totalAccumulatedSuspectAppendCount++;
            let numFields = $(".suspect-wrapper").length + 1;
           
            $('#suspect-container').append(`
                <div class="suspect-wrapper" id="suspect-wrapper-${totalAccumulatedSuspectAppendCount}">   
                    <div class="suspect-title-wrapper">
                        <span class="suspect-individual-label">{{__('form.particularofsuspectindividual')}} ${numFields}</span>
                        <div class="suspect-append-btn"><button type="button" class="btn btn-primary btn-hrd-theme" onclick="removeSuspect('suspect-wrapper-${totalAccumulatedSuspectAppendCount}')">-</button></div>
                    </div>
                    <div class="mb-3">
                        <label for="supect_name">{{__('form.particularofsuspectlabelname')}} </label>
                        <input type="text" id="supect_name_${totalAccumulatedSuspectAppendCount}" name="suspect_name[]" class="form-control" >
                    </div>
            
                    <div class="mb-3">
                        <label for="suspect_email_address">{{__('form.particularofsuspectlabelemail')}}</label>
                        <input type="email" placeholder="aa@gmail.com" id="suspect_email_address_${totalAccumulatedSuspectAppendCount}" name="suspect_email_address[]" class="form-control" > 
                    </div>
            
                    <div class="mb-3">
                        <label for="suspect_department">{{__('form.particularofsuspectlabeldepartment')}}<span class="text-danger">*</span></label>
                        <input type="text" id="suspect_department_${totalAccumulatedSuspectAppendCount}" name="suspect_department[]" class="form-control">  
                    </div>

                    <div class="mb-3">
                        <label for="suspect_relationship">{{__('form.particularofsuspectlabelrelationship')}}<span class="text-danger">*</span></label>
                        <input type="text" id="suspect_relationship_${totalAccumulatedSuspectAppendCount}" name="suspect_relationship[]" class="form-control">
                    </div>
                </div>
            `);
        }


        function removeSuspect(id){
            $('#'+id).remove();
        }

    </script>
    @endsection

</html>


