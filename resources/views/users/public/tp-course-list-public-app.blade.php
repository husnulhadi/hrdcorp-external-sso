<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- Title Page-->
    <title>HRD Corp BizMatch</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <script src="https://cdn.tailwindcss.com"></script> -->
    
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->    
</head>
<style>
.th-bi-wb{
    min-width : 195px;
}

.th-bm-wb{
    min-width : 215px;
}

.status-btn-wrapper{
    display : flex;
    flex-direction : column;
    column-gap : 0.5rem;
    margin : 0.5rem 0;
}

.btn-course-upper{
    margin-bottom : 0.5rem;
}

.course-detail-wrapper{
    /* display : flex;
    column-gap : 0.5rem; */
}

.course-detail-mobile-wrapper{
    display : flex;
}

.course-mobile-label{
    font-weight :bold;

}

.course-mobile-value{

}

.cm-label{
    min-width : 200px;
    font-weight : bold;
}

.cm-comma{
    padding : 0 0.2rem;
}

.course-row-icon {
    color : #002169;
    font-size : 14px;
    margin-right : 0.5rem;
}

.first-icon-wrapper{
    display : block;
}

.second-icon-wrapper{
    display : none;
}

.course-item-wrapper{
    display : flex;
}

.course-app-container{
    padding : 1rem 0;
}

@media screen and (max-width: 980px) {
   
}

@media screen and (max-width: 750px) {
    .cm-label, .cm-comma, .course-mobile-value{
        font-size : 14px;
    }

    .search-container{
        display : block;
    }
    .first-icon-wrapper{
        display : block;
    }

    .pagination-container{
        display : block !important;
    }

    .pagination-container nav{
        overflow : auto;
    }

    .second-icon-wrapper{
        display : none;
    }
    .search-wrapper{
        display : block;
    }
    .first,.second,.fourth{
        display : none;
    }

    .search-input-wrapper{
        display : block;
    }

    .left, .right {
        width : 100%;
    }

}

</style>
    @include('layouts.partials._locale')   
    @extends('layouts.master-public')
    @section('content')

    @php
   
    $officerInfo = [];
    $statusList = config('custom.wb.officer.status');
    $currentOfficer = !empty($user) ? $user['Email'] : '';
    $currentOfficerId = !empty($user) ? $user['id'] : '1';


    $officerId = !empty($user) ? $user['id'] : '';
    $officerType = !empty($user) ? $user['type'] : '';
    $statusList = config('custom.wb.officer.status');
 

    $rowCount = 0;
    $info = [];
    $suspect = '';
    $wbTextStyle = Session::get('locale') != config('app.fallback_locale') ? 'wb-text-italic' : '';
    $thWbText = empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale') ? 'th-bi-wb' : 'th-bm-wb';
    
    //dd(url()->current());
    //dd(Request::get('status'));
    //dd(Request::get('page'));//page reset 1
    @endphp

    @if(isset($source) && $source == 'web')
    <div class="container-wrapper">
    @else
    <div class="container course-app-container">
    @endif
        <!-- <div class="container">
            <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;">
        </div> -->
       {{-- @include('layouts.partials._title',['type'=>$pageType == 'public' ? '' : 'officer']) --}}
        <div class="container">
            <div class="card">
                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                    {{__('list.course_list_title')}}
                </div>
                <div class="card-body">
                    <div class="container text-center">
                        <div class="row">
                            <div class="">
                                   <div class="search-container">
                                        <div class="search-wrapper left">
                                             <form name="course-list-search" id="course-list-search" method="get" action="{{route('coursesAppSearch')}}">
                                            {{-- csrf_field() --}}

                                                <div class="search-wrapper">
                                                    <div class="search-input-wrapper">
                                                        <div class="mb-3">
                                                            <label for="search" class="visually-hidden">{{__('list.search')}}</label>
                                                            {{--<input type="text" class="form-control" id="search" name="search" placeholder="{{__('list.search')}}" value="{{$search}}">--}}
                                                            <input type="text" class="form-control" id="search" name="search"  value="{{$search}}" placeholder="{{__('list.search')}}"/>
                                                        </div>
                                                        {{--<div class="mb-3">
                                                            <select class="form-select" name="status" aria-label="Default select example">
                                                                <option value="all" selected="true">{{__('form.all')}}</option>
                                                                @foreach($statusList as $statusL)   
                                                                
                                                                    @if($pageType == 'admin' || $pageType == 'SuperAdmin' || $pageType == 'tp' )
                                                                        @if($statusL['slug'] != 3)
                                                                        <option value="{{!empty($statusL) ? $statusL['slug'] : ''}}" <?=!empty($statusL) ? $statusL['slug'] == $status ? 'selected': '' : ''?>>{{!empty($statusL) ? __('list.'.$statusL['name']) : ''}}</option>
                                                                        @endif
                                                                    @else
                                                                        @if($statusL['slug'] == 1)
                                                                        <option value="{{!empty($statusL) ? $statusL['slug'] : ''}}" <?=!empty($statusL) ? $statusL['slug'] == $status ? 'selected': '' : ''?>>{{!empty($statusL) ?  __('list.'.$statusL['name']) : ''}}</option>
                                                                        @endif
                                                                    @endif
                                                                
                                                                @endforeach 
                                                            </select>
                                                        </div>--}}
                                                        
                                                        <div class="mb-3">
                                                            <select class="form-select" name="percentage" aria-label="Default select example">
                                                                <option value="all">{{__('form.all')}}</option>
                                                                @foreach($percentageList as $percentage)
                                                                <option value="{{!empty($percentage) ? $percentage : ''}}" <?=$percentage == $percentageVal ? 'selected' : ''?>>{{$percentage}} %</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        
                                                        
                                                    </div>
                                                    <div class="search-btn-wrapper">
                                                    <button type="submit" class="btn btn-primary btn-hrd-theme mb-3">{{__('list.search')}}</button>
                                                        <a href="{{route('coursesApp')}}"><button type="button" class="btn btn-primary btn-hrd-theme mb-3">{{__('list.reset')}}</button></a>         
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="search-wrapper right">
                                            @if($pageType != 'employer')
                                            {{--<div class="print-btn-wrapper">
                                                <form name="bizmatch-course-excel-print" id="bizmatch-course-excel-print" method="get" action="{{route('exportCourses')}}">
                                                    <input type="hidden" id="printIds" name="printIds" value=""/>
                                                    <input type="hidden" id="sortCol" name="sortCol" value="{{Request('sortCol')}}"/>
                                                    <input type="hidden" id="sortOrder" name="sortOrder" value="{{Request('sortOrder')}}"/>
                                                    <a href="#"><button type="button" id="print-pdf" onclick="onPrint()" class="btn btn-primary btn-hrd-theme">{{__('list.print_excel')}}</button></a>
                                                </form>
                                            </div>--}}
                                            @endif
                                        </div>
                                    </div>
                                
                            </div>
                            <!-- <div class="col-2">
                                <button class="btn btn-primary">Extract Data</button>
                            </div> -->
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    @if($pageType != 'employer')
                                    {{--<th><input type="checkbox" id="checkAll" onchange="checkAll()" class="form-check-input"></th>--}}
                                    @endif
                                    <th class="course-th mycoid">
                                        <div class="th-wrapper">
                                            <div class="th-text " >
                                                {{__('list.wbname')}} 
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=tp_name'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'tp_name' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=tp_name'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'tp_name' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="course-th first">
                                        <div class="th-wrapper">
                                            <div class="th-text " >
                                                 MYCOID 
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=tp_mycoid'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'tp_mycoid' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=tp_mycoid'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'tp_mycoid' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    {{-- <th class="course-th second">
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                                {{__('list.course_training_skim')}}
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=training_skim'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'training_skim' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=training_skim'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'training_skim' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    --}}
                                    <th class="course-th third">
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.course_name')}}

                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=name'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'name' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=name'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'name' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                   {{-- <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.course_price')}}
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=training_fee'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'training_fee' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=training_fee'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'training_fee' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.course_discount_rate')}}
                
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=training_discount_rate'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'training_discount_rate' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=training_discount_rate'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'training_discount_rate' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.course_discounted_price')}}
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=discounted_training_fee'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'discounted_training_fee' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=discounted_training_fee'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'discounted_training_fee' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th> --}}

                                    {{--<th class="course-th fourth">{{__('list.action')}}</th>--}}
                                </tr>
                            </thead>
                        <tbody class="course-app-webview-web" id="course-app-webview-mobile">
                            @if($list->count() == 0)
                            <tr>
                                <td colSpan="7" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                            </tr>

                            @else
                                @foreach($list as $li)   
                                    @php
                                        $li = json_decode(json_encode($li), true);
                                        $cId = $li['id'];

                                    @endphp

                                    <tr>
                                        @if($pageType != 'employer')
                                        {{--<td><input type="checkbox" id="check-{$rowCount++}" onchange="updateCheckedId()" value="{{$li['id']}}" class="form-check-input course-check"/></td> --}}
                                        @endif

                                        {{--<td><input type="checkbox" id="check-{$rowCount++}" onchange="updateCheckedId()" value="{{$li['id']}}" class="form-check-input"/></td>--}} 
                                        <td class="course-td mycoid">
                                            <div class="course-item-wrapper"> 
                                                <span class="course-row-mobile-icon first-icon-wrapper" onclick="openCouresMobileRow({{$li['id']}},'course-row-mobile-{{$li['id']}}','course-row-toggle-{{$li['id']}}')">
                                                    <i id="course-row-icon-plus2-{{$li['id']}}" class="course-row-icon fa fa-plus-circle" aria-hidden="true"></i>
                                                    <i id="course-row-icon-minus2-{{$li['id']}}" class="course-row-icon fa fa-minus-circle hide" aria-hidden="true"></i>
                                                </span> 
                                            {{$li['tp_name']}}
                                            </div>
                                        </td>
                                        <td class="course-td first">
                                            {{$li['tp_mycoid']}}
                                        </td>
                                        {{--<td class="course-td second">
                                            {{$li['training_skim']}}
                                        </td>--}}
                                        <td class="course-td third">
                                            <div class="course-item-wrapper">
                                            <span class="course-row-mobile-icon second-icon-wrapper" onclick="openCouresMobileRow({{$li['id']}},'course-row-mobile-{{$li['id']}}','course-row-toggle-{{$li['id']}}')">
                                                <i id="course-row-icon-plus-{{$li['id']}}" class="course-row-icon fa fa-plus-circle" aria-hidden="true"></i>
                                                <i id="course-row-icon-minus-{{$li['id']}}" class="course-row-icon fa fa-minus-circle hide" aria-hidden="true"></i>
                                            </span>
                                            {{$li['name']}}
                                            </div>    
                                      </td>
                                        {{--<td>{{date('d M Y h:i:s',strtotime($li['Created_date']))}}</td>--}}
                                        {{--  <td>{{$li['training_fee']}}</td>
                                        <td>{{$li['training_discount_rate']}}</td>
                                        <td>{{$li['discounted_training_fee']}}</td> --}}
                                     
                                        {{--<td class="course-td fourth">  
                                            <div class="status-btn-wrapper">
                                                <button type="button" onclick="openInterestModal('{{$cId}}',{tpcoid : '{{$li['tp_mycoid']}}' , courseId : '{{$li['id']}}', courseName : '{{$li['name']}}'})" class="btn btn-primary btn-hrd-theme action-btn-custom btn-course-upper" data-bs-toggle="modal" data-bs-target="#modalInterest">
                                                    {{__('list.interest')}}
                                                </button>
                                            </div>
                                        </td>--}}
                                    </tr>
                                    <tr id="course-row-mobile-{{$li['id']}}" class="hide" value="">
                                        <td colspan="4">
                                            <!-- <input type="hidden" id="course-row-toggle-{{$li['id']}}" value=""/> -->
                                            <div class="course-detail-mobile-container">
                                                <div class="course-detail-mobile-wrapper">
                                                    <div class="course-mobile-label">
                                                        <span class="cm-label">TP COID</span>
                                                        <span class="cm-comma">:</span>
                                                    </div>
                                                    <div class="course-mobile-value">{{$li['tp_mycoid']}}</div>
                                                </div>
                                                <div class="course-detail-mobile-wrapper">
                                                    <div class="course-mobile-label">
                                                        <span class="cm-label">{{__('list.employer_course_training_skim')}}</span>
                                                        <span class="cm-comma">:</span>
                                                    </div>
                                                    <div class="course-mobile-value">{{$li['training_skim']}}</div>
                                                </div>
                                                <div class="course-detail-mobile-wrapper">
                                                    <div class="course-mobile-label">
                                                        <span class="cm-label">{{__('list.course_training_certification')}}</span>
                                                        <span class="cm-comma">:</span>
                                                    </div>
                                                    <div class="course-mobile-value">{{$li['training_certification']}}</div>
                                                </div>
                                                <div class="course-detail-mobile-wrapper">
                                                    <div class="course-mobile-label">
                                                        <span class="cm-label">{{__('list.employer_course_name')}}</span>
                                                        <span class="cm-comma">:</span>
                                                    </div>
                                                    <div class="course-mobile-value">{{$li['name']}}</div>
                                                </div>
                                                {{--<div class="course-detail-mobile-wrapper">
                                                    <div class="course-mobile-label">
                                                        <span class="cm-label">{{__('list.employer_course_discount_rate')}}</span>
                                                        <span class="cm-comma">:</span>
                                                    </div>
                                                    <div class="course-mobile-value">{{number_format($li['training_discount_rate'],2)}}</div>
                                                </div> --}}
                                                <div class="course-detail-mobile-wrapper">
                                                    <div class="course-mobile-label">
                                                        <span class="cm-label">{{__('list.employer_course_price_1')}}</span>
                                                        <span class="cm-comma">:</span>
                                                    </div>
                                                    <div class="course-mobile-value">{{number_format($li['training_fee'],2)}}</div>
                                                </div>
                                                {{--<div class="course-detail-mobile-wrapper">
                                                    <div class="course-mobile-label">
                                                        <span class="cm-label">{{__('list.employer_course_discounted_price')}}</span>
                                                        <span class="cm-comma">:</span>
                                                    </div>
                                                    <div class="course-mobile-value">{{number_format($li['discounted_training_fee'],2)}}</div>
                                                </div> --}}
                                                <div class="status-btn-wrapper">
                                                    <button type="button" onclick="openInterestModal('{{$cId}}',{tpcoid : '{{$li['tp_mycoid']}}' , courseId : '{{$li['id']}}', courseName : '{{$li['name']}}'})" class="btn btn-primary btn-hrd-theme action-btn-custom btn-course-upper" data-bs-toggle="modal" data-bs-target="#modalInterest">
                                                        {{__('list.interest')}}
                                                    </button>
                                                </div>
                                            
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach 
                            @endif
                            </tbody>
                        
                        
                        </table>
                    </div>
            
                    <div class="d-flex pagination-container"> 
                     
                        @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        {{--<span class="">show {{ $list->perPage() }} list per page</span>--}}
                        {{--<span>{{__('list.showpagecount',['pagecount'=>$list->perPage()])}}</span>--}}
                        
                        <span>{{__('list.showpagecount2',['start'=>$list->firstItem(),'end'=>$list->lastItem(), 'total'=>$list->total()])}}</span>
                        @endif

                        @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        <span> {{$list->appends(['search' => $search, 'status'=>$status, 'sortOrder'=>Request::get('sortOrder'), 'sortCol'=>Request::get('sortCol')])->onEachSide(1)->links()}}</span>
                        {{--<span> {{$list->onEachSide(1)->links()}}</span>--}}
                        @endif
                        

                    </div>
                    <!-- <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-end">
                            <li class="page-item disabled">
                            <a class="page-link">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav> -->
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    @include('layouts.partials.courses._modal-update-course-status',['courseId'=>'','testId'=>123,'modalId'=>123,'status'=>2, 'user'=>2, 'formInfo'=>[]])
    @include('layouts.partials.courses._modal-interest-course',['courseId'=>'','testId'=>123,'modalId'=>123,'status'=>2, 'user'=>2, 'formInfo'=>[]])
    <script>

        function openCouresMobileRow(courseId, rowSelector, toggleSelector){
            console.log('open')
            console.log(rowSelector)
            //let toggleFlag = $('#'+toggleSelector).val();
           if($('#'+rowSelector).hasClass('hide')){    
                $('#'+rowSelector).removeClass('hide');  
                $('#course-row-icon-plus-'+courseId).addClass('hide');
                $('#course-row-icon-minus-'+courseId).removeClass('hide');
                $('#course-row-icon-plus2-'+courseId).addClass('hide');
                $('#course-row-icon-minus2-'+courseId).removeClass('hide');
           }else{
                $('#'+rowSelector).addClass('hide');
                $('#course-row-icon-plus-'+courseId).removeClass('hide');
                $('#course-row-icon-minus-'+courseId).addClass('hide');
                $('#course-row-icon-plus2-'+courseId).removeClass('hide');
                $('#course-row-icon-minus2-'+courseId).addClass('hide');
           }
   
        }

       function onPrint(){
            let checkedIds = [];
            let checkedLength = $('.course-check:checked').length;
            let printIds = $('#printIds').val();
            let url = '{{route('exportCourses')}}';
            if(checkedLength == 0){
                Swal.fire({
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                    title : '{{__("list.print_check_title")}}',
                    text : '{{__("list.print_check_text")}}',
                    icon : 'info'
                })
            }else{
                $('#bizmatch-course-excel-print').submit();
            }
           
       }


        function checkAll(){
            let checkedIds = [];

            let checkAllChecked = $('#checkAll').is(':checked');
            if(checkAllChecked){
                $('.course-check').prop('checked', true);
            }else{  
                $('.course-check').prop('checked', false);
            }
            
            $('.course-check:checked').each(function() {
                checkedIds.push(this.value);
                console.log(this.value);
            });
            var ids = checkedIds.join(',');
            $('#printIds').val(ids);
        }


        function updateCheckedId(){
            let checkedIds = [];
            console.log('checked id')
            $('.course-check:checked').each(function() {
                checkedIds.push(this.value);
                console.log(this.value);
            });
            var ids = checkedIds.join(',');

            console.log(ids)
            $('#printIds').val(ids);
       }


        function approve(id, approveBy, data){
            const {tpmycoid = '', course = '' } = data;
            
            //const form = document.getElementById('whistle-blower-form');
            var formData = new FormData();
            formData.append("id", id);
            formData.append("status", 1);
            formData.append("approve_by",approveBy);
            formData.append("tp_mycoid",tpmycoid);
            formData.append("course",course);
           
            Swal.fire({
                    title: '{{__("form.confirmupdate")}}?',
                    showCancelButton: true,
                    confirmButtonText: '{{__("form.confirm")}}',
                    denyButtonText: '{{__("form.cancel")}}',
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('approve')}}",
                        //contentType: 'multipart/form-data',
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: '{{__("form.statusupdated")}}!',
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#002169',
                                    cancelButtonColor: '#002169',
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            window.setTimeout(function () {
                                Swal.fire({
                                    title :'{{__("form.unsuccessful")}}!', 
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    confirmButtonColor: '#002169',
                                    icon: 'error',
                                });

                            }, 2000);
                        }
                    });

                } else if (result.isCanceled) {
                    //Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                    Swal.fire({
                        title :'{{__("form.changenotsave")}}!', 
                        confirmButtonText: '{{__("form.confirm")}}',
                        confirmButtonColor: '#002169',
                        icon: 'info',
                    });
                }
            })
        }


        function remove(id, data){
            const {tpmycoid = '', course = '' } = data;
            //const form = document.getElementById('whistle-blower-form');
            //var formData = new FormData(form);
            var formData = new FormData();
            formData.append("id", id);
            formData.append("status", 3);
            formData.append("tp_mycoid",tpmycoid);
            formData.append("course",course);

            Swal.fire({
                    title: '{{__("form.confirmupdate")}}?',
                    showCancelButton: true,
                    confirmButtonText: '{{__("form.confirm")}}',
                    denyButtonText: '{{__("form.cancel")}}',
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('remove')}}",
                        //contentType: 'multipart/form-data',
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: '{{__("form.statusupdated")}}!',
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#002169',
                                    cancelButtonColor: '#002169',
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            Swal.fire({
                                title :'{{__("form.unsuccessful")}}!', 
                                confirmButtonText: '{{__("form.confirm")}}',
                                confirmButtonColor: '#002169',
                                icon: 'error',
                            });
                        }
                    });

                } else if (result.isCanceled) {
                    Swal.fire({
                        title :'{{__("form.changenotsave")}}!', 
                        confirmButtonText: '{{__("form.confirm")}}',
                        confirmButtonColor: '#002169',
                        icon: 'info',
                    });
                }
            })
            
        }
       
    </script>

    @endsection


</html>


