<!DOCTYPE html>
<html lang="en">

<head>
    <title>HRD Corp BizMatch</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css">
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js"></script>
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->
    

</head>
    
<style>
    .wb-title-custom{
        text-align : center;
        padding : 1rem;
    }

    .custom-verification{
        display : flex;
    }
    .wb-checkbox{
        margin-right : 0.5rem;
    }

    .hide{
        display : none!important;
    }

    .btn-container{
        display: flex;
        justify-content: flex-end;
        padding : 0.5rem 0;
    }
    .input-error{
        border : 1px solid #FF0000 !important;
    }

    .error-message{
        color : #FF0000;
    }

    .input-error:focus{
        box-shadow : 0 0 0 0.25rem rgba(255, 0, 0,.25) !important;
    }

    .file-wrapper{
        display:flex;
        column-gap : 0.5rem;
        padding : 0.5rem 0;
    }

    .supporting-documents-label{
        margin : 0.2rem 0;
    }

    .btn-align-center{
        display : flex;
        justify-content : center;
        padding : 0.5rem 0;
    }


    /**Custom dropdown */

    .custom-multiple-select{

    }

    .custom-input{

    }

    .custom-dropdown{
        border :1px solid #ced4da;
        border-radius : .25rem;
        height : 200px;
        overflow : auto;
    }

    .custom-dropdown-ul{
        list-style-type : none;
        margin : 0;
        padding : 0;
        /* padding : 0.5rem 0; */
    }

    .single:hover{
        background-color : grey; 
    }

    .single {
        padding : 0.2rem 0;
    }

    .custom-dropdown-ul-span{
        padding : 0 1rem;
    }

    .bold {
        font-weight : bold;
    }

    .custom-dropdown-li:hover{
        background-color : grey;
    }

    .custom-dropdown-li-wrapper{
        display : flex;
        align-items : center;
        padding : 0.2rem 1rem;
        column-gap : 0.5rem;        
    }

    .scroll {
        height : 150px;
        overflow : auto;
    }

    .declaration-wrapper{
        display:flex;
    }

    .btn-wrapper{
        display: flex;
        justify-content: center;
        column-gap: 0.5rem;
    }

    .form-container{
        margin : 1rem auto;
    } 
    
    .form-instruction-box{
        padding: 0.5rem 0;
        /* font-weight : bold; */
        font-style : italic;
    }

    .form-ins-main{

    }

    .form-ins-sub{
        font-size : 14px;
    }

    .suspect-individual-label{
        font-style : italic;
    }

    .suspect-title-wrapper{
        display : flex;
        align-items: center;
        column-gap: 1rem;
    }

    .policy{
        height: 200px;
        overflow: auto;
        padding: 2rem;
        border: 1px solid #ced4da;
        border-radius: 4px;
        /* border: 1px solid #002169;
        border-radius: 0!important; */
    }


    .file-upload-instruction-wrapper{
        /* display : flex; */
    }

    .file-upload-instruction-group{
        display : flex;
    }

    .file-upload-instruction{
        /* font-style : italic; */
        font-size : 12px; 
        font-weight : bold;
    }

    .file-upload-ins-sub{
        font-size : 12px; 
    }

    .form-check-input:checked {
        background-color: #002169;
        border-color: #002169;
    }

    .country-info-wrapper, .state-info-wrapper{
        display:flex;
        column-gap:1rem;
    }

    .country-input-info-fullwidth{
        width : 100%;
    }

    .country-input-info{
        width : 50%;
    }

    .state-info-input-wrapper{
        width : 33%;
    }

    .custom-container-setting{
        margin-top : 8rem;
    }

    .form-title{
        display : flex;
        justify-content : space-between;
        align-items : center;
    }

    .disclosure-reminder{
        font-style : italic;
        font-size : 14px;
    }

    .declaration-input-wrapper{
        padding-bottom : 0.5rem;
    }

    .wb-text{
        font-weight : 500;
    }
    .wb-text-italic{
        font-style : italic;
    }

    
    .btn-submit:disabled{
        background-color: #6c757d!important;
        border : 1px solid #6c757d;
    }

    .course-wrapper{
        padding: .375rem .75rem;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        margin-bottom : 0.5rem;
    }

    .course-price-container{
        display : flex;
        column-gap : 1rem;
    }

    .price-input-wrapper{
        width : 100%;
    }

    .btn-link{
        text-decoration : none;
    }

    @media screen and (max-width: 750px) {
        .form-title{
            display : block;
        }

        .user-guide{
            width : 100%;
        }

        .state-info-wrapper{
            display : block;
        }

        .state-info-input-wrapper {
            width : 100%;
        }

        .file-upload-instruction-wrapper, .file-upload-instruction-group{
            display : block;
        }

        .course-price-container{
            display : block;
        }
    }


</style>
    @include('layouts.partials._locale')
    @extends('layouts.master')
    @section('content')
    
    @php 
    
    $userType = !empty($user) ? $user['type'] : '';
    $userId = !empty($user) && !empty($user['id']) ? $user['id'] : '';
    $trainingSchemeList = config('custom.wb.officer.statustrainingscheme');
    $typeoftrainingcertificate = config('custom.wb.officer.typeoftrainingcertificate');
    if(!empty($tpInfo)){
        $tpId = !empty($tpInfo) && !empty($tpInfo['id'])? $tpInfo['id'] : '';
        $tpMycoid = !empty($tpInfo) && !empty($tpInfo['tp_mycoid'])? $tpInfo['tp_mycoid'] : '';
        $tpName = !empty($tpInfo) && !empty($tpInfo['poc_name'])? $tpInfo['poc_name'] : '';
        $tpEmail = !empty($tpInfo) && !empty($tpInfo['poc_email'])? $tpInfo['poc_email'] : '';
        $tpPhone = !empty($tpInfo) && !empty($tpInfo['poc_phone'])? $tpInfo['poc_phone'] : '';
    }else{
        $tpId = !empty($tp) && !empty($tp['id'])? $tp['id'] : '';
        $tpMycoid = !empty($tp) && !empty($tp['tp_mycoid'])? $tp['tp_mycoid'] : '';
        $tpName = !empty($tp) && !empty($tp['poc_name'])? $tp['poc_name'] : '';
        $tpEmail = !empty($tp) && !empty($tp['poc_email'])? $tp['poc_email'] : '';
        $tpPhone = !empty($tp) && !empty($tp['poc_phone'])? $tp['poc_phone'] : '';
    }
     
    $currentLocale = !empty(App::currentLocale()) ? App::currentLocale() : config('app.fallback_locale');
    $wbTextStyle = Session::get('locale') != config('app.fallback_locale') ? 'wb-text-italic' : '';

    $confirmSubmitText = Session::get('locale') != config('app.fallback_locale') ? __('form.confirmsubmittext',['wbtext'=>'Whistleblower','send'=>'Hantar']) : __('form.confirmsubmittext');
    @endphp
    
    <!-- <div class="container">
        <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
    </div> -->

    <div class="container-wrapper custom-container-setting">
        <div class="container form-title">
            @include('layouts.partials._title',['type'=>'officer'])
            <div class="container" style="padding: 2rem 0;text-align:right;">
                <a class="btn-link" href="{{route('tpCourseList',['devOfficerId'=>$userId])}}" >
                    <button type="button" class="btn btn-primary btn-hrd-theme user-guide" data-bs-dismiss="modal">{{__('form.back')}}</button>
                </a>
                <a class="btn-link" href="https://hrdcorp-my.sharepoint.com/:w:/g/personal/wongleeping_hrdcorp_gov_my/EYJYbdaQB6tIpReiE5cwFtoBOajylY8Tof9hi4YD1zKmhQ?e=HeqZKT" target="_blank">
                    <button type="button" class="btn btn-primary btn-hrd-theme user-guide" data-bs-dismiss="modal">{{__('form.userguide')}}</button>
                </a>

              

            </div>
        </div>
        <div class="container">
            <div class="card">
                {{--<div class="card-header text-center font-weight-bold hrd-card-header-text">
                    {{ __('form.wbform') }}
                </div>--}}
               
                <!-- <form name="whistle-blower-form" id="whistle-blower-form" action="{{route('processCreate')}}" method="post" enctype="multipart/form-data" novalidate> -->
                <form name="tp-form" id="tp-form"  method="post" enctype="multipart/form-data" novalidate>
                <input type="hidden" name="user_id" value="{{$userId}}"/>    
                <input type="hidden" name="tp_id" value="{{$tpId}}"/>
                    {{ csrf_field() }}
                    <div class="accordion" id="accordionPanelsStayOpenExample">
                        <div class="accordion-item " id="tp_info">
                            <h2 class="accordion-header hrd-acc-btn-header" id="panelsStayOpen-headingTwo">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
                                @if(empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale') )
                                <b>A. {{__('form.courseform_tpheader')}} / {{__('form.courseform_tpheader2')}} </b> 
                                @else
                                <b>A. {{__('form.courseform_tpheader')}}/ {{__('form.courseform_tpheader2')}}</b>
                                @endif
                            </button>
                            </h2>
                            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
                                <div class="accordion-body">
                                    <div class="form-instruction-box">
                                        <span class="form-ins-main">{{__('form.particularofinstruction')}}</span><br>
                                        <span class="form-ins-sub">{{__('form.particularofinstruction2')}}</span>
                                    </div>
                                    <div>
                                        <div class="mb-3">
                                            <label for="tp_mycoid">{{__('form.tp_mycoid')}}<span class="text-danger">*</span></label>
                                            @if(!empty($tpInfo) || !empty($tp))
                                            <input type="text" id="tp_mycoid2" name="tp_mycoid2" placeholder="eg : XY321" value="{{$tpMycoid}}" class="form-control" disabled/>      
                                            <input type="hidden" id="tp_mycoid" onkeyup="enableButtonByCheckField('tp_mycoid')" name="tp_mycoid" placeholder="eg : XY321" value="{{$tpMycoid}}" class="form-control" required/>
                                            @else
                                            <input type="text" id="tp_mycoid" onkeyup="enableButtonByCheckField('tp_mycoid')" name="tp_mycoid" placeholder="eg : XY321" value="{{$tpMycoid}}" class="form-control" required/>      
                                            @endif
                                            <div class="error-message hide" id="err-tp_mycoid">
                                                <!-- Please select a valid state. -->
                                            </div>
                                        </div>
                                    
                                        <div class="mb-3">
                                            <label for="tp_name">{{__('form.tp_name')}}</label>
                                            @if(!empty($tpInfo)  || !empty($tp))
                                            <input type="text" id="tp_name2" placeholder="eg : Best TP Ever" name="tp_name2" value="{{$tpName}}" class="form-control" disabled/>
                                            <input type="hidden" id="tp_name" onkeyup="enableButtonByCheckField('tp_name')" placeholder="eg : Best TP Ever" name="tp_name" value="{{$tpName}}" class="form-control" />
                                            @else
                                            <input type="text" id="tp_name" onkeyup="enableButtonByCheckField('tp_name')" placeholder="eg : Best TP Ever" name="tp_name" value="{{$tpName}}" class="form-control" />
                                            @endif
                                            <div class="error-message hide" id="err-tp_name">
                                                <!-- Please select a valid state. -->
                                            </div>
                                        </div>
                                        
                                        <div class="mb-3">
                                            <label for="tp_email">{{__('form.tp_email')}}</label>
                                            @if(!empty($tpInfo) || !empty($tp))
                                            <input type="text"  id="tp_email2"  placeholder="eg: aa@gmail.com" name="tp_email2" value="{{$tpEmail}}" class="form-control" disabled>
                                            <input type="hidden"  id="tp_email" onkeyup="enableButtonByCheckField('tp_email')"  placeholder="eg: aa@gmail.com" name="tp_email" value="{{$tpEmail}}" class="form-control">
                                            @else
                                            <input type="email"  id="tp_email" onkeyup="enableButtonByCheckField('tp_email')"  placeholder="eg: aa@gmail.com" name="tp_email" value="{{$tpEmail}}" class="form-control">
                                            @endif                                            
                                            <div class="error-message hide" id="err-tp_email">
                                                <!-- Please select a valid state. -->
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="tp_contact_no">{{__('form.tp_contact_no')}}</label>
                                            @if(!empty($tpInfo) || !empty($tp))
                                            <input type="number" id="tp_contact_no2"  placeholder="eg : 0112345678" name="tp_contact_no" value="{{$tpPhone}}" class="form-control" disabled/> 
                                            <input type="hidden" id="tp_contact_no" onkeyup="enableButtonByCheckField('tp_contact_no')" placeholder="eg : 0112345678" name="tp_contact_no" value="{{$tpPhone}}" class="form-control"/> 
                                            @else
                                            <input type="number" id="tp_contact_no" onkeyup="enableButtonByCheckField('tp_contact_no')" placeholder="eg : 0112345678" name="tp_contact_no" value="{{$tpPhone}}" class="form-control"/> 
                                            @endif
                                            <div class="error-message hide" id="err-tp_contact_no">
                                                <!-- Please select a valid state. -->
                                            </div>
                                        </div>
                                       
                                       
                                       {{--<div class="mb-3">
                                            <button id="btn_tp_info" type="button" onclick="next('tp_info','course_info')" class="btn btn-primary btn-hrd-theme">{{__('form.next')}}</button>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="accordion-item " id="course_info">
                            <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                            <button class="accordion-button hrd-acc-btn-header" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true" aria-controls="panelsStayOpen-collapseThree">
                                <b>B. {{__('form.courseform_courseheader')}}/  {{__('form.courseform_courseheader2')}}</b>
                            </button>
                            </h2>
                            <div class="accordion-collapse collapse show"  id="panelsStayOpen-collapseThree" aria-labelledby="panelsStayOpen-headingThree">
                                <div class="accordion-body">
                                    <div class="form-instruction-box">
                                       {{-- <span class="form-ins-main">{{__('form.particularofsuspectinstruction')}}</span>
                                        <br>
                                        <span class="form-ins-sub">{{__('form.particularofsuspectinstruction2')}}</span>--}}

                                    </div>

                                    <div id="suspect-container" class="suspect-container-wrapper">
                                        <div class="course-wrapper">
                                            
                                            <div class="suspect-title-wrapper">
                                                <span class="suspect-individual-label">{{__('form.course_title')}}</span>
                                                <div class="suspect-append-btn"><button class="btn btn-primary btn-hrd-theme" type="button" onclick="appendCourse()">+</button></div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="tp_training_scheme">{{__('form.tp_training_scheme')}} <span class="text-danger">*</span></label>
                                               {{-- <input type="text" id="tp_training_scheme" onkeyup="enableButtonByCheckField('tp_training_scheme', {toCalculate : false, selectorId : ''})" placeholder="eg :  HRDCorp Claimaible Courses" name="tp_skill_area[]" class="form-control" required> --}}
                                                
                                                <select class="form-select" id="tp_training_scheme" name="tp_skill_area[]" onchange="enableButtonByCheckField('tp_training_scheme', {toCalculate : false, selectorId : ''})" aria-label="Default select example">
                                                    @foreach($trainingSchemeList as $status)   
                                                    <option value="{{!empty($status) ? $status['value'] : ''}}">{{!empty($status) ? $status['name'] : ''}}</option>
                                                    @endforeach 
                                                </select>
                                                <div class="error-message hide" id="err-tp_training_scheme">
                                                    <!-- Please select a valid state. -->
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="tp_training_certificate">{{__('form.tp_training_certificate')}} <span class="text-danger">*</span></label>
                                               {{-- <input type="text" id="tp_training_certificate" onkeyup="enableButtonByCheckField('tp_training_certificate', {toCalculate : false, selectorId : ''})" placeholder="eg :  HRDCorp Claimaible Courses" name="tp_skill_area[]" class="form-control" required> --}}
                                                
                                                <select class="form-select" id="tp_training_certificate" name="tp_training_certificate[]" onchange="enableButtonByCheckField('tp_training_scheme', {toCalculate : false, selectorId : ''})" aria-label="Default select example">
                                                    @foreach($typeoftrainingcertificate as $status)   
                                                    <option value="{{!empty($status) ? $status['value'] : ''}}">{{!empty($status) ? $status['name'] : ''}}</option>
                                                    @endforeach 
                                                </select>
                                                <div class="error-message hide" id="err-tp_training_scheme">
                                                    <!-- Please select a valid state. -->
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="tp_course_name">{{__('form.course_name')}}<span class="text-danger">*</span></label>
                                                <input type="text" id="tp_course_name" placeholder="eg : Abc" onkeyup="enableButtonByCheckField('tp_course_name',{toCalculate : false, selectorId : ''})" name="tp_course_name[]" class="form-control" required>
                                                <div class="error-message hide" id="err-tp_course_name">
                                                    Please select a valid state.
                                                </div>
                                            </div>

                                            <div class="course-price-container">
                                                <div class="mb-3 price-input-wrapper">
                                                    <label for="tp_course_price">{{__('form.course_price')}}<span class="text-danger">*</span></label>
                                                    <input type="number" placeholder="eg:100" id="tp_course_price" onkeyup="enableButtonByCheckField('tp_course_price',{toCalculate : true, selectorId : 'tp_course_discounted_price', priceSelectorId : 'tp_course_price', discountSelectorId : 'tp_course_discount_rate', discountedValueSelectorId : 'tp_course_discounted_price_value'})" name="tp_course_price[]" class="form-control" required>
                                                    <div class="error-message hide" id="err-tp_course_price">
                                                        Please select a valid state.
                                                    </div>
                                                </div>
                                        
                                                <div class="mb-3 price-input-wrapper">
                                                    <label for="tp_course_discount_rate">{{__('form.course_discount_rate')}}<span class="text-danger">*</span></label>
                                                    <input type="number" id="tp_course_discount_rate" placeholder="eg: 20" onkeyup="enableButtonByCheckField('tp_course_discount_rate',{toCalculate : true, selectorId : 'tp_course_discounted_price', priceSelectorId : 'tp_course_price', discountSelectorId : 'tp_course_discount_rate',  discountedValueSelectorId : 'tp_course_discounted_price_value'})" name="tp_course_discount_rate[]" class="form-control" required>
                                                    <div class="error-message hide" id="err-tp_course_discount_rate">
                                                        Please select a valid state.
                                                    </div>
                                                </div>

                                                <div class="mb-3 price-input-wrapper">
                                                    <label for="tp_course_discounted_price">{{__('form.course_price_after_discount')}}</label>
                                                    <input type="number" id="tp_course_discounted_price" name="tp_course_discounted_price[]" class="form-control" disabled>
                                                    <input type="hidden" id="tp_course_discounted_price_value" name="tp_course_discounted_price_value[]" class="form-control">
                                                </div>
                                               
                                            </div>

                                            
                                        </div>
                                        <!-- <div class="suspect-append-btn"><button class="btn btn-primary" onclick="appendCourse()">+</button></div> -->
                                    </div>
                                </div>
                            </div>
                        </div> 
             
                        <div class="mb-3 btn-align-center">
                            <button id="add-course-submit" type="button" onclick="submitForm()" class="btn btn-primary btn-hrd-theme btn-submit" disabled>{{__('form.submit')}}</button>
                            <button id="add-course-loading" type="button" onclick="interest()" class="btn btn-primary btn-hrd-theme hide"> 
                                <div class="spinner-border text-primary" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </button>
                        </div>

                    </div>

                   
                    
                </form>
            </div>
        </div>
    </div>
    
    
    <script>
        let totalAccumulatedAppendCount = 0;
        let totalAccumulatedSuspectAppendCount = 0;
        let open = false;
      
    
        $(document).ready(function() {
            localStorage.clear();

            // $(function(){
            //     $('#datepicker').datepicker({
            //         format: 'dd/mm/yyyy',
            //     });
            // });

            // Select2 Multiple
            // $('.select2-multiple').select2({
            //     theme: "bootstrap-5",
            //     width: $( this ).data( 'width' ) ? $( this ).data( 'width' ) : $( this ).hasClass( 'w-100' ) ? '100%' : 'style',
            //     placeholder: "Select",
            //     allowClear: true,
            // });
        });
    

        function loading(){
            $('#add-course-submit').addClass('hide');
            $('#add-course-loading').removeClass('hide');
        }

        function afterLoad(){
            $('#add-course-submit').removeClass('hide');
            $('#add-course-loading').addClass('hide');
        }

        function calculateAfterDiscount(coursePriceSelectorId , courseDiscountRateSelectorId, showAfterDiscountSelecrtorId, showAfterDiscountValueSelecrtorId){
            let oriPrice = $('#'+coursePriceSelectorId).val();
            let discountRate = $('#'+courseDiscountRateSelectorId).val();
            let remainDisocuntRate = (100 - discountRate)/100;
            let afterDiscount = 0;
            afterDiscount = oriPrice*remainDisocuntRate;
            $('#'+showAfterDiscountSelecrtorId).val(afterDiscount.toFixed(2));
            $('#'+showAfterDiscountValueSelecrtorId).val(afterDiscount.toFixed(2))
    
        }
      
        function enableButtonByCheckField(selectorId, calculateInfo = {}){
            let formContainer = $('#tp-form');
            let totalCount = $('[required]').length;
            let validCount = 0;
            let invalidCount = 0;
            let isAllRequiredFieldValid = false;
           
            const{toCalculate = false, selectorId : calSelectorId = '', priceSelectorId = '', discountSelectorId = '', discountedValueSelectorId = ''} = calculateInfo;
            // console.log('test id')
            // console.log(calculateInfo)

            if(toCalculate){
                calculateAfterDiscount(priceSelectorId , discountSelectorId, calSelectorId, discountedValueSelectorId);
            }
            
            $('[required]').each(function() {
                if ($(this).is(':invalid') || !$(this).val()){
                    valid = false;
                    invalidCount++;

                    console.log($(this).attr('id'))
                }else{
                    validCount++;      
                } 
            })

            console.log('total count')
            console.log(totalCount)
            console.log('valid count');
            console.log(validCount)
            console.log('invalid count')
            console.log(invalidCount);
            if(validCount == totalCount){
                $('#add-course-submit').prop('disabled', false);
                isAllRequiredFieldValid = true;
                // Swal.fire({
                //     icon : 'warning',
                //     title : "{{__('form.choosedisclosuretitle')}}",
                //     text : "{{__('form.choosedisclosure')}}",
                //     confirmButtonColor: '#002169',
                // })
            }else{
                $('#add-course-submit').prop('disabled', true);
            }
           
        }

        function submitForm () {
            if ($('#tp-form')[0].checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } 
            $('#tp-form').addClass('was-validated');
            submitAjax();
        }


        function submitAjax() {
            const form = document.getElementById('tp-form');
            var formData = new FormData(form);
            Swal.fire({
                    title: '{{__("form.confirmsubmittitle")}}',
                    text: '{{$confirmSubmitText}}',
                    showCancelButton: true,
                    confirmButtonText: '{{__("form.confirm")}}',
                    denyButtonText: '{{__("form.cancel")}}',
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    loading();
                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('processCreate')}}",
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                                afterLoad();
                                if(response.status == 1)
                                {

                                    const {data = []} = response;
                                    const {url = ''} = data != null ? data[0] : [];
                                   
                                    // console.log(data)
                                    // console.log(data[0])
                                    // console.log(url)
                                    Swal.fire({
                                        title: '{{__("form.submitted")}} !',
                                        text : '{{__("form.submittedtext")}}',
                                        confirmButtonText: 'OK',
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#002169',
                                        }).then((result) => {
                                        /* Read more about isConfirmed, isDenied below */
                                        if (result.isConfirmed) {
                                            //location.reload();
                                            
                                            window.location.href = url;
                                        }
                                    })
                                }
                            },
                            error: function(_response){
                                afterLoad();
                                window.setTimeout(function () {
                                    //Swal.fire('{{__("form.unsuccessful")}}!', '', 'error');
                                    Swal.fire({
                                        title :'{{__("form.unsuccessful")}}!', 
                                        confirmButtonText: '{{__("form.confirm")}}',
                                        confirmButtonColor: '#002169',
                                        icon: 'error',
                                    });

                                }, 2000);
                            }
                        });

                } else if (result.isCanceled) {
                    //Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                    Swal.fire({
                        title :'{{__("form.changenotsave")}}!', 
                        confirmButtonText: '{{__("form.confirm")}}',
                        confirmButtonColor: '#002169',
                        icon: 'info',
                    });
                }
            })
        }

        // function checkUpload(targetElementId){
        //     // size: 5867185
        //     // type : "application/pdf"
        //     let availableFileTypes = ["pdf","jpg","jpeg","png","docx"];
        //     var file = $('#'+targetElementId).prop('files')
        //     // console.log('test file upload')
        //     // console.log(file)
        //     if(Object.keys(file).length != 0){
        //         let fileName = file[0].name;
        //         let fileNameSplit = fileName.split('.');
        //         let fileExt = fileNameSplit[1];
        //         // console.log('file info')
        //         // console.log(fileName)
        //         // console.log(fileExt)
        //         if(!availableFileTypes.includes(fileExt)){
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: '{{__("form.fileuploadtypeerrtitle")}}',
        //                 text: '{{__("form.fileuploadtypeerrtext")}}',
        //                 confirmButtonColor: '#002169',
        //                 cancelButtonColor: '#6c757d',
        //                 //footer: '<a href="">Why do I have this issue?</a>'
        //             }).then((result) => {
        //                 /* Read more about isConfirmed, isDenied below */
        //                 if (result.isConfirmed) {
        //                     $('#'+targetElementId).val('')
        //                 } 
        //             })
                   
        //         }
        //         if(file[0].size > 10485760 ){  // 10mb
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: '{{__("form.fileuploadsizeerrtitle")}}',
        //                 text: '{{__("form.fileuploadsizeerrtext")}}',
        //                 confirmButtonColor: '#002169',
        //                 cancelButtonColor: '#6c757d',
        //                 //footer: '<a href="">Why do I have this issue?</a>'
        //             }).then((result) => {
        //                 /* Read more about isConfirmed, isDenied below */
        //                 if (result.isConfirmed) {
        //                     $('#'+targetElementId).val('')
        //                 } 
        //             })
        //         }
        //    }
        // }
      
        // function appendFileInput () {
        //     totalAccumulatedAppendCount++;
        //     $('#file-container').append(`
        //     <div class="file-wrapper" data-count=${totalAccumulatedAppendCount}" id="file_wrapper_${totalAccumulatedAppendCount}">
        //         <input type="file" class="form-control supporting_doc" id="supporting_doc_${totalAccumulatedAppendCount}"  onchange="checkUpload('supporting_doc_${totalAccumulatedAppendCount}')" name="complaint_supporting_docs[]" required>
        //         <button type="button" class="btn btn-primary btn-hrd-theme" onClick="removeSelectedFileInput('file_wrapper_${totalAccumulatedAppendCount}')">-</button>
        //     </div>
        //     `);
        // }

        function removeSelectedFileInput(selectorIdToRemove){
            $('#'+selectorIdToRemove).remove();
        }


        function appendCourse(){
            $('#submit').prop('disabled', true);
            totalAccumulatedSuspectAppendCount++;
            let numFields = $(".course-wrapper").length + 1;
           
            $('#suspect-container').append(`
                <div class="course-wrapper" id="course-wrapper-${totalAccumulatedSuspectAppendCount}">
                                            
                    <div class="suspect-title-wrapper">
                        <span class="suspect-individual-label">Course ${totalAccumulatedSuspectAppendCount}</span>
                        <div class="suspect-append-btn"><button class="btn btn-primary btn-hrd-theme" type="button" onclick="removeCourse('course-wrapper-${totalAccumulatedSuspectAppendCount}')">-</button></div>
                    </div>

                    <div class="mb-3">
                        <label for="tp_training_scheme">{{__('form.tp_training_scheme')}}</label>
                        {{--<input type="text" id="tp_training_scheme_${totalAccumulatedSuspectAppendCount}" onkeyup="enableButtonByCheckField('tp_training_scheme_${totalAccumulatedSuspectAppendCount}',{toCalculate : ${false}, selectorId : ''})" placeholder="eg :  HRDCorp Claimaible Courses" name="tp_skill_area[]" class="form-control" required>
                        <div class="error-message hide" id="err-tp_training_scheme">
                            <!-- Please select a valid state. -->
                        </div> --}}

                        <select class="form-select" id="tp_training_scheme_${totalAccumulatedSuspectAppendCount}" name="tp_skill_area[]" onchange="enableButtonByCheckField('tp_training_scheme_${totalAccumulatedSuspectAppendCount}', {toCalculate :${false}, selectorId : ''})" aria-label="Default select example">
                            @foreach($trainingSchemeList as $status)   
                            <option value="{{!empty($status) ? $status['value'] : ''}}">{{!empty($status) ? $status['name'] : ''}}</option>
                            @endforeach 
                        </select>
                    </div>

                    <div class="mb-3">
                        <label for="tp_training_certificate_${totalAccumulatedSuspectAppendCount}">{{__('form.tp_training_certificate')}} <span class="text-danger">*</span></label>
                        {{-- <input type="text" id="tp_training_certificate_${totalAccumulatedSuspectAppendCount}" onkeyup="enableButtonByCheckField('tp_training_certificate', {toCalculate : false, selectorId : ''})" placeholder="eg :  HRDCorp Claimaible Courses" name="tp_skill_area[]" class="form-control" required> --}}
                        
                        <select class="form-select" id="tp_training_certificate_${totalAccumulatedSuspectAppendCount}" name="tp_training_certificate[]" onchange="enableButtonByCheckField('tp_training_scheme', {toCalculate : false, selectorId : ''})" aria-label="Default select example">
                            @foreach($typeoftrainingcertificate as $status)   
                            <option value="{{!empty($status) ? $status['value'] : ''}}">{{!empty($status) ? $status['name'] : ''}}</option>
                            @endforeach 
                        </select>
                        <div class="error-message hide" id="err-tp_training_scheme">
                            <!-- Please select a valid state. -->
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="tp_course_name_${totalAccumulatedSuspectAppendCount}">{{__('form.course_name')}}<span class="text-danger">*</span></label>
                        <input type="text" id="tp_course_name_${totalAccumulatedSuspectAppendCount}" placeholder="eg : Abc" onkeyup="enableButtonByCheckField('tp_course_name_${totalAccumulatedSuspectAppendCount}',{toCalculate : ${false}, selectorId : ''})" name="tp_course_name[]" class="form-control" required>
                        <div class="error-message hide" id="err-suspect_name">
                            Please select a valid state.
                        </div>
                    </div>

                    <div class="course-price-container">
                        <div class="mb-3 price-input-wrapper">
                            <label for="tp_course_price_${totalAccumulatedSuspectAppendCount}">{{__('form.course_price')}}<span class="text-danger">*</span></label>
                            <input type="number" placeholder="eg:100" id="tp_course_price_${totalAccumulatedSuspectAppendCount}" onkeyup="enableButtonByCheckField('tp_course_price_${totalAccumulatedSuspectAppendCount}',{toCalculate : true, selectorId : 'tp_course_discounted_price_${totalAccumulatedSuspectAppendCount}', priceSelectorId : 'tp_course_price_${totalAccumulatedSuspectAppendCount}', discountSelectorId : 'tp_course_discount_rate_${totalAccumulatedSuspectAppendCount}',  discountedValueSelectorId : 'tp_course_discounted_price_value_${totalAccumulatedSuspectAppendCount}'})" name="tp_course_price[]" class="form-control" required>
                            <div class="error-message hide" id="err-suspect_email_address">
                                Please select a valid state.
                            </div>
                        </div>
                
                        <div class="mb-3 price-input-wrapper">
                            <label for="tp_course_discount_rate_${totalAccumulatedSuspectAppendCount}">{{__('form.course_discount_rate')}}<span class="text-danger">*</span></label>
                            <input type="number" id="tp_course_discount_rate_${totalAccumulatedSuspectAppendCount}" placeholder="eg:20" onkeyup="enableButtonByCheckField('tp_course_discount_rate_${totalAccumulatedSuspectAppendCount}',{toCalculate : true, selectorId : 'tp_course_discounted_price_${totalAccumulatedSuspectAppendCount}', priceSelectorId : 'tp_course_price_${totalAccumulatedSuspectAppendCount}', discountSelectorId : 'tp_course_discount_rate_${totalAccumulatedSuspectAppendCount}',discountedValueSelectorId : 'tp_course_discounted_price_value_${totalAccumulatedSuspectAppendCount}'})" name="tp_course_discount_rate[]" class="form-control" required>
                            <div class="error-message hide" id="err-suspect_department">
                                Please select a valid state.
                            </div>
                        </div>

                        <div class="mb-3 price-input-wrapper">
                            <label for="tp_course_discounted_price_${totalAccumulatedSuspectAppendCount}">{{__('form.course_price_after_discount')}}<span class="text-danger">*</span></label>
                            <input type="text" id="tp_course_discounted_price_${totalAccumulatedSuspectAppendCount}" name="tp_course_price_after_discount[]" class="form-control" disabled>
                            <input type="hidden" id="tp_course_discounted_price_value_${totalAccumulatedSuspectAppendCount}" name="tp_course_discounted_price_value[]" class="form-control">
                        </div>
                       <!--<div class="mb-3 price-input-wrapper">
                            <button id="btn_suspect_info" type="button" onclick="" class="btn btn-primary btn-hrd-theme">Calculate</button>
                        </div>-->
                    </div>

                  
                </div>
            `);
        }


        function removeCourse(id){
            $('#add-course-submit').prop('disabled', false);
            $('#'+id).remove();
        }

    </script>
    @endsection

</html>


