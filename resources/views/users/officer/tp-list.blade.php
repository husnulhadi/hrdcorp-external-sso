<!DOCTYPE html>
<html lang="en">
<head>
    <title>HRD Corp BizMatch</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <script src="https://cdn.tailwindcss.com"></script> -->
    
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->    
</head>
<style>
.th-bi-wb{
    min-width : 195px;
}

.th-bm-wb{
    min-width : 215px;
}

.status-btn-wrapper{
    display : flex;
    flex-direction : column;
    column-gap : 0.5rem;
}

.btn-course-upper{
    margin-bottom : 0.5rem;
}

.course-detail-wrapper{
    /* display : flex;
    column-gap : 0.5rem; */
}
</style>
    @include('layouts.partials._locale')
    @extends('layouts.master')
    @section('content')

    @php
   
    $officerInfo = [];
    $statusList = config('custom.wb.officer.status');
    $currentOfficer = !empty($user) ? $user['Email'] : '';
    $currentOfficerId = !empty($user) ? $user['id'] : '1';


    $officerId = !empty($user) ? $user['id'] : '';
    $officerType = !empty($user) ? $user['type'] : '';
    $statusList = config('custom.wb.officer.status');
 

    $rowCount = 0;
    $info = [];
    $suspect = '';
     $wbTextStyle = Session::get('locale') != config('app.fallback_locale') ? 'wb-text-italic' : '';
    $thWbText = empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale') ? 'th-bi-wb' : 'th-bm-wb';
    
    
    //dd(url()->current());
    //dd(Request::get('status'));
    //dd(Request::get('page'));//page reset 1
    @endphp
    <div class="container-wrapper">
        <!-- <div class="container">
            <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;">
        </div> -->
        @include('layouts.partials._title',['type'=>'officer'])
        <div class="container">
            <div class="card">
                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                    {{__('list.tp_title')}}
                   
                </div>
                <div class="card-body">
                    <div class="container text-center">
                        <div class="row">
                            <div class="">
                                {{--<form name="whistle-blower-search" id="whistle-blower-search" method="get" action="{{route('tpCourseListSearch',['devOfficerId'=>$currentOfficerId])}}">--}}
                                    {{-- csrf_field() --}}
                                    
                                    <div class="search-container">
                                        <div class="search-wrapper left">
                                            <form name="whistle-blower-search" id="whistle-blower-search" method="get" action="{{route('tpListSearch',['devOfficerId'=>$currentOfficerId])}}">
                                                <div class="search-wrapper">
                                                    <div class="search-input-wrapper">
                                                        <div class="mb-3">
                                                            <label for="search" class="visually-hidden">{{__('list.search')}}</label>
                                                            {{--<input type="text" class="form-control" id="search" name="search" placeholder="{{__('list.search')}}" value="{{$search}}">--}}
                                                            <input type="text" class="form-control" id="search" name="search"  value="{{$search}}" placeholder="{{__('list.search')}}"/>
                                                        </div>
                                                        {{--<div class="mb-3">
                                                            <select class="form-select" name="status" aria-label="Default select example">
                                                                <option value="all" selected="true">{{__('form.all')}}</option>
                                                                @foreach($statusList as $statusL)   
                                                                    @if($pageType == 'admin' || $pageType == 'SuperAdmin' || $pageType == 'tp' )
                                                                        @if($statusL['slug'] != 3)
                                                                        <option value="{{!empty($statusL) ? $statusL['slug'] : ''}}" <?=!empty($statusL) ? $statusL['slug'] == $status ? 'selected': '' : ''?>>{{!empty($statusL) ?  __('list.'.$statusL['name']) : ''}}</option>
                                                                        @endif
                                                                    @else
                                                                        @if($statusL['slug'] == 1)
                                                                        <option value="{{!empty($statusL) ? $statusL['slug'] : ''}}" <?=!empty($statusL) ? $statusL['slug'] == $status ? 'selected': '' : ''?>>{{!empty($statusL) ?  __('list.'.$statusL['name']) : ''}}</option>
                                                                        @endif
                                                                    @endif
                                                                
                                                                
                                                                @endforeach 
                                                            </select>
                                                        </div>--}}
                                                    </div>
                                                    <div class="search-btn-wrapper">
                                                        <button type="submit" class="btn btn-primary btn-hrd-theme mb-3">{{__('list.search')}}</button>
                                                        <a href="{{route('tpList',['devOfficerId'=>$currentOfficerId])}}"><button type="button" class="btn btn-primary btn-hrd-theme mb-3">{{__('list.reset')}}</button></a>
                                                        {{--@if($pageType == 'admin')
                                                        <a href="{{route('newcourse')}}"><button type="button" onclick="" class="btn btn-primary btn-hrd-theme mb-3">{{__('list.course_list_add_btn_text')}}</button></a>
                                                        @endif--}}
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="search-wrapper right">
                                        <div class="print-btn-wrapper">
                                                <form name="bizmatch-tp-excel-print" id="bizmatch-tp-excel-print" method="get" action="{{route('exportTP')}}">
                                                    <input type="hidden" id="printIds" name="printIds" value=""/>
                                                    <input type="hidden" id="sortCol" name="sortCol" value="{{Request('sortCol')}}"/>
                                                    <input type="hidden" id="sortOrder" name="sortOrder" value="{{Request('sortOrder')}}"/>
                                                    <a href="#"><button type="button" id="print-pdf" onclick="onPrint()" class="btn btn-primary btn-hrd-theme">{{__('list.print_excel')}}</button></a>
                                                    <a href="#"><button type="button" id="print-pdf-all" onclick="onPrintAll()" class="btn btn-primary btn-hrd-theme">{{__('list.print_excel_all')}}</button></a>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                {{--</form>--}}
                            </div>
                            <!-- <div class="col-2">
                                <button class="btn btn-primary">Extract Data</button>
                            </div> -->
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr> 
                                    <th><input type="checkbox" id="checkAll" onchange="checkAll()" class="form-check-input "></th>
                                    
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text " >
                                            MYCOID
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=tp_mycoid'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'tp_mycoid' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=tp_mycoid'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'tp_mycoid' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                                {{__('list.tp_name')}} 
                                              
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=poc_name'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'poc_name' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=poc_name'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'poc_name' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.tp_email')}}
                                            
                                            </div>

                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=poc_email'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'poc_email' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=poc_email'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'poc_email' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                    <th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                            {{__('list.tp_contact')}}
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=poc_phone'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'poc_phone' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=poc_phone'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'poc_phone' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        
                                    </th>
                                
                                    {{--<th>
                                        <div class="th-wrapper">
                                            <div class="th-text">
                                           {{__('list.status')}}
                                           
                                            </div>
                                            <div class="th-icon-wrapper">
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=asc&sortCol=status'?>"><i class="fa fa-long-arrow-up sort-icon {{Request('sortOrder') == 'asc' && Request('sortCol') == 'status' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                                <a href="<?=url()->current().'?status='.Request::get('status').'&page=1&sortOrder=desc&sortCol=status'?>"><i class="fa fa-long-arrow-down sort-icon {{Request('sortOrder') == 'desc' && Request('sortCol') == 'status' ? 'active-icon' : ''}}" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </th>--}}
                                     
                                    {{-- <th>{{__('list.action')}}</th> --}}
                                </tr>
                            </thead>
                        <tbody>
                            @if($list->count() == 0)
                            <tr>
                                <td colSpan="7" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                            </tr>

                            @else
                                @foreach($list as $li)   
                                    @php
                                        $li = json_decode(json_encode($li), true);
                                        $cId = $li['id'];
                                    @endphp
                                    <tr>
                                        <td><input type="checkbox" id="check-{$rowCount++}" onchange="updateCheckedId()" value="{{$li['id']}}" class="form-check-input interest-check"/></td> 
                                
                                        <td>{{$li['tp_mycoid']}}</td>
                                        <td>{{$li['poc_name']}}</td>
                                        <td>{{$li['poc_email']}}</td>
                                        {{--<td>{{date('d M Y h:i:s',strtotime($li['Created_date']))}}</td>--}}
                                        <td>{{$li['poc_phone']}}</td>
                                        {{--<td>{{$li['status']}}</td>--}}
                                       {{-- <td> 
                                            @if($pageType == 'admin')
                                                @if($officerType == 'admin' || $officerType == 'SuperAdmin')
                                                    @if($li['current_status'] == 0)
                                                    <div class="status-btn-wrapper">
                                                        <button type="button" onclick="approve('{{$cId}}',{{$officerId}},{empmycoid : '{{$li['mycoid']}}', tpmycoid : '{{$li['tpMycoid']}}', course : '{{$li['name']}}', email : '{{$li['email']}}'})" class="btn btn-primary btn-hrd-theme action-btn-custom btn-course-upper" >
                                                            {{__('list.approve')}}
                                                        </button>
                                                        <button type="button" onclick="reject('{{$cId}}',{{$officerId}}, {empmycoid : '{{$li['mycoid']}}', tpmycoid : '{{$li['tpMycoid']}}', course : '{{$li['name']}}', email : '{{$li['email']}}'})" class="btn btn-primary btn-hrd-theme action-btn-custom btn-course-upper" >
                                                            {{__('list.reject')}}
                                                        </button>
                                                    </div>
                                                    @endif
                                                @endif
                                            @endif

                                        </td>--}}
                                    </tr>
                                @endforeach 
                            @endif
                            </tbody>
                        </table>
                        <!-- <div class="row">
                            <div class="col" id="buttonSection" style="display:none;">
                                <button class="btn btn-primary" id="rejectButton">Reject</button>
                                <button class="btn btn-primary" id="completeButton">Complete</button>
                            </div>
                        </div> -->
                    </div>
            
                    <div class="d-flex pagination-container"> 
                     
                       @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        <span>{{__('list.showpagecount2',['start'=>$list->firstItem(),'end'=>$list->lastItem(), 'total'=>$list->total()])}}</span>
                        @endif

                        @if($list instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        <span> {{ $list->appends(['search' => $search, 'status'=>$status, 'sortOrder'=>Request::get('sortOrder'), 'sortCol'=>Request::get('sortCol')])->links() }}</span>
                        @endif
                        

                    </div>
                    <!-- <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-end">
                            <li class="page-item disabled">
                            <a class="page-link">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav> -->
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    @include('layouts.partials.courses._modal-update-course-status',['courseId'=>'' ,'testId'=>123,'modalId'=>123,'status'=>2, 'user'=>2, 'formInfo'=>[]])
    @include('layouts.partials.courses._modal-interest-course',['courseId'=>'','testId'=>123,'modalId'=>123,'status'=>2, 'user'=>2, 'formInfo'=>[]])
    <script>
       
        
       function onPrint(){
            let checkedIds = [];
            let checkedLength = $('.interest-check:checked').length;
            let printIds = $('#printIds').val();
            let url = '{{route('exportCourses')}}';
            if(checkedLength == 0){
                Swal.fire({
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                    title : '{{__("list.print_check_title")}}',
                    text : '{{__("list.print_check_text")}}',
                    icon : 'info'
                })
            }else{
                $('#bizmatch-tp-excel-print').submit();
            }
           
       }


       function onPrintAll(){
        $('.interest-check').prop('checked',false);
        $('#printIds').val('');
            Swal.fire({
                confirmButtonColor: '#002169',
                cancelButtonColor: '#6c757d',
                title : '{{__("list.print_check_all_title")}}',
                text : '{{__("list.print_check_all_text")}}',
                icon : 'info',
                showCancelButton: true,
            }).then((result) => {
                if(result.isConfirmed){
                    $('#bizmatch-tp-excel-print').submit();
                }
                
                if(result.isCanceled){

                }
            })
    }



        function checkAll(){
            let checkedIds = [];
            let checkAllChecked = $('#checkAll').is(':checked');
            if(checkAllChecked){
                $('.interest-check').prop('checked', true);
            }else{  
                $('.interest-check').prop('checked', false);
            }
            
            $('.interest-check:checked').each(function() {
                checkedIds.push(this.value);
                console.log(this.value);
            });
            var ids = checkedIds.join(',');
            $('#printIds').val(ids);
        }


        function updateCheckedId(){
            let checkedIds = [];
            console.log('checked id')
            $('.interest-check:checked').each(function() {
                checkedIds.push(this.value);
                console.log(this.value);
            });
            var ids = checkedIds.join(',');

            console.log(ids)
            $('#printIds').val(ids);
       }


        function approve(id, approveBy, data){
            //const form = document.getElementById('whistle-blower-form');
            const {empmycoid = '', tpmycoid = '', course = '', email = ''} = data;
            // console.log('approve')
            // console.log(data)
            var formData = new FormData();
            formData.append("id", id);
            formData.append("current_status", 1);
            formData.append("created_by",approveBy);
            formData.append("emp_mycoid",empmycoid);
            formData.append("tp_mycoid",tpmycoid);
            formData.append("course",course);
            formData.append("applicant_email",email);
            
            Swal.fire({
                    title: '{{__("form.confirmupdate")}}?',
                    showCancelButton: true,
                    confirmButtonText: '{{__("form.confirm")}}',
                    denyButtonText: '{{__("form.cancel")}}',
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('approveInterest')}}",
                        //contentType: 'multipart/form-data',
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: '{{__("form.statusupdated")}}!',
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#002169',
                                    cancelButtonColor: '#002169',
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            window.setTimeout(function () {
                                Swal.fire({
                                    title :'{{__("form.unsuccessful")}}!', 
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    confirmButtonColor: '#002169',
                                    icon: 'error',
                                });

                            }, 2000);
                        }
                    });

                } else if (result.isCanceled) {
                    //Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                    Swal.fire({
                        title :'{{__("form.changenotsave")}}!', 
                        confirmButtonText: '{{__("form.confirm")}}',
                        confirmButtonColor: '#002169',
                        icon: 'info',
                    });
                }
            })
        }

        function reject(id, rejectBy, data){
            const {empmycoid = '', tpmycoid = '', course = '', email = ''} = data;
            //const form = document.getElementById('whistle-blower-form');
            var formData = new FormData();
            formData.append("id", id);
            formData.append("current_status", 2);
            formData.append("created_by",rejectBy);
            formData.append("emp_mycoid",empmycoid);
            formData.append("tp_mycoid",tpmycoid);
            formData.append("course",course);
            formData.append("applicant_email",email);
            Swal.fire({
                    title: '{{__("form.confirmupdate")}}?',
                    showCancelButton: true,
                    confirmButtonText: '{{__("form.confirm")}}',
                    denyButtonText: '{{__("form.cancel")}}',
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('rejectInterest')}}",
                        //contentType: 'multipart/form-data',
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: '{{__("form.statusupdated")}}!',
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#002169',
                                    cancelButtonColor: '#002169',
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            window.setTimeout(function () {
                                Swal.fire({
                                    title :'{{__("form.unsuccessful")}}!', 
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    confirmButtonColor: '#002169',
                                    icon: 'error',
                                });

                            }, 2000);
                        }
                    });

                } else if (result.isCanceled) {
                    //Swal.fire('{{__("form.changenotsave")}}', '', 'info')
                    Swal.fire({
                        title :'{{__("form.changenotsave")}}!', 
                        confirmButtonText: '{{__("form.confirm")}}',
                        confirmButtonColor: '#002169',
                        icon: 'info',
                    });
                }
            })
        }


        function remove(id){
            //const form = document.getElementById('whistle-blower-form');
            //var formData = new FormData(form);
            var formData = new FormData();
            formData.append("id", id);
            formData.append("status", 3);
           
            Swal.fire({
                    title: '{{__("form.confirmupdate")}}?',
                    showCancelButton: true,
                    confirmButtonText: '{{__("form.confirm")}}',
                    denyButtonText: '{{__("form.cancel")}}',
                    confirmButtonColor: '#002169',
                    cancelButtonColor: '#6c757d',
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    $.ajax({
                        type:'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('remove')}}",
                        //contentType: 'multipart/form-data',
                        processData: false, // Prevent jQuery from converting the data to a string
                        contentType: false, 
                            data : formData,
                            success: function(response){
                            if(response.data == 1)
                            {
                                Swal.fire({
                                    title: '{{__("form.statusupdated")}}!',
                                    confirmButtonText: '{{__("form.confirm")}}',
                                    allowOutsideClick: false,
                                    confirmButtonColor: '#002169',
                                    cancelButtonColor: '#002169',
                                    }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        location.reload();
                                    }
                                })
                            }
                        },
                        error: function(_response){
                            Swal.fire({
                                title :'{{__("form.unsuccessful")}}!', 
                                confirmButtonText: '{{__("form.confirm")}}',
                                confirmButtonColor: '#002169',
                                icon: 'error',
                            });
                        }
                    });

                } else if (result.isCanceled) {
                    Swal.fire({
                        title :'{{__("form.changenotsave")}}!', 
                        confirmButtonText: '{{__("form.confirm")}}',
                        confirmButtonColor: '#002169',
                        icon: 'info',
                    });
                }
            })
            
        }
       
    </script>

    @endsection


</html>


