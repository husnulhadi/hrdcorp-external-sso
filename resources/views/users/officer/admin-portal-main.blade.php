<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title Page-->
    <title>HRD Corp BizMatch</title>

    <!-- Main CSS-->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> -->
</head>
    <style>
        .wb-title-custom{
            text-align : center;
            padding : 1rem;
        }

        .custom-verification{
            display : flex;
        }
        .wb-checkbox{
            margin-right : 0.5rem;
        }

        .hide{
            display : none!important;
        }

        .btn-container{
            display: flex;
            justify-content: flex-end;
            padding : 0.5rem 0;
        }
        .input-error{
            border : 1px solid #FF0000 !important;
        }

        .error-message{
            color : #FF0000;
        }

        .input-error:focus{
           box-shadow : 0 0 0 0.25rem rgba(255, 0, 0,.25) !important;
        }

        .file-wrapper{
            display:flex;
            column-gap : 0.5rem;
            padding : 0.5rem 0;
        }

        .supporting-documents-label{
            margin : 0.2rem 0;
        }

        .btn-align-center{
            display : flex;
            justify-content : center;
            padding : 0.5rem 0;
        }

        .container-custom{
            padding : 1rem;
        }

    </style>

    
    @extends('layouts.master-officer')
    
    @section('content')
    <body class="bg">
        <div class="container">
            <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" height="150px" style="padding-top:30px; padding-bottom:30px">
        </div>
        <div class="container" style="padding-bottom:10px; text-align:right;">
            <a href="https://hrdcorp-my.sharepoint.com/:b:/g/personal/wanmohdfatihin_hrdcorp_gov_my/EZyhkC2BkQNDp4QSPn9EJPcBDmzSXPDUvZDjNjNmP9HKoA?e=RCo0p2" target="_blank"><button type="button" class="btn btn-primary" data-bs-dismiss="modal">User Guide</button></a>
        </div>
        <div class="container container-custom">
            <div class="row">
                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                           <div class="btn-align-center"><a href="{{route('inbox')}}"><button class="btn btn-primary">Inbox (2)</button></a></div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <div class="btn-align-center"><button class="btn btn-primary">Master Case</button></div>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <div class="btn-align-center"><button class="btn btn-primary">Report</button></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </body>
    
    <script>
        let totalAccumulatedAppendCount = 0;
        let sections_settings = {
            applicant_info : {
                validate : false,
                inputs : [
                    {
                        slug : 'applicant_name',
                        name : 'applicant name'
                    },{
                        slug : 'applicant_mobile_phone_no',
                        name : 'applicant mobile phone no'
                    },
                    {
                        slug : 'applicant_email_address',
                        name : 'applicant email address'
                    },
                    {
                        slug : 'applicant_designation',
                        name : 'applicant designation'
                    },
                    {
                        slug : 'applicant_department',
                        name : 'applicant department'
                    },
                ],
            },
            suspect_info : {
                validate : false,
                inputs : [
                    {
                        slug : 'suspect_name',
                        name : 'suspect name'
                    },{
                        slug : 'suspect_mobile_phone_no',
                        name : 'suspect mobile phone no'
                    },
                    {
                        slug : 'suspect_email_address',
                        name : 'suspect email address'
                    },
                    {
                        slug : 'suspect_designation',
                        name : 'suspect designation'
                    },
                    {
                        slug : 'suspect_department',
                        name : 'suspect department'
                    },
                ],
            },
            witness_info : {
                validate : false,
                inputs : [
                    {
                        slug : 'witness_name',
                        name : 'witness name'
                    },{
                        slug : 'witness_mobile_phone_no',
                        name : 'witness mobile phone no'
                    },
                    {
                        slug : 'witness_email_address',
                        name : 'witness email address'
                    },
                    {
                        slug : 'witness_designation',
                        name : 'witness designation'
                    },
                    {
                        slug : 'witness_department',
                        name : 'witness department'
                    },
                ],
            },
            complaint_info : {}
        }

        // add form validation using Bootstrap
        $('#whistle-blower-form').on('submit', function(event) {
            if ($('#whistle-blower-form')[0].checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
            } else {
            // show the loader button
            //$("#book-flight-btn").addClass("d-none"); // hide the book flight button
            //$("#loader-btn").removeClass("d-none"); // show the loader button
            }
            $('#whistle-blower-form').addClass('was-validated');
        });

        function next(current_section,next_section){
            validateSectionInput(current_section);
            let currentSectionInfo = sections_settings[current_section];
            let checkCurrentSectionValidationStatus = currentSectionInfo.validate;
            if(checkCurrentSectionValidationStatus){
                // console.log('next success')
                // console.log('#'+next_section)
                $('#'+next_section).removeClass('hide');
                $('#btn_'+current_section).addClass('hide');
            }
           // console.log(current_section, next_section)
        }

        
        function validateSectionInput(section_key){
            let selected_section = sections_settings[section_key].inputs;
            let requiredValidCount = 0;
            let numberCheck = false;
            let emailCheck = false;
            let checkboxCheck = false;
            for(let i=0; i< selected_section.length; i++){
                let info = selected_section[i];
                let value = $('#'+info.slug).val(); 
                let type = $('#'+info.slug).attr('type');
                if(value == ''){
                    $('#err-'+info.slug).text('Please fill in '+info.name);
                    $('#err-'+info.slug).removeClass('hide');
                    $('#'+info.slug).addClass('input-error');
                }else{
                    $('#err-'+info.slug).text('');
                    $('#'+info.slug).removeClass('input-error')
                    requiredValidCount++;
                }  
            
                if(type == 'number'){
                    if(!$.isNumeric(value)){
                        $('#err'+info.slug).val('Please fill only number for '+info.name)   
                    }else{
                        numberCheck = true;
                    }
                    
                }

                if(type == 'email'){
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if(!emailReg.test(value)){
                        console.log('Please fill in correct email format')
                        $('#err-'+info.slug).text('Please fill in correct email format for'+info.name);
                        $('#err-'+info.slug).removeClass('hide');
                        $('#'+info.slug).addClass('input-error');
                    }else{
                        emailCheck = true;
                    }
                }

                if(type == 'checkbox'){

                }

                if(requiredValidCount == 5 && numberCheck == true && emailCheck == true ){
                    sections_settings[section_key].validate = true;
                }       
            }   
        }


        function appendFileInput () {
            totalAccumulatedAppendCount++;
            $('#file-container').append(`
            <div class="file-wrapper" data-count=${totalAccumulatedAppendCount} id="supporting_doc_${totalAccumulatedAppendCount}">
                <input type="file" class="form-control supporting_doc" name="complaint_supporting_docs[]" required>
                <button type="button" class="btn btn-primary" onClick="removeSelectedFileInput('supporting_doc_${totalAccumulatedAppendCount}')">-</button>
            </div>
            `);
        }

        function removeSelectedFileInput(selectorIdToRemove){
            $('#'+selectorIdToRemove).remove();
        }

    </script>
    @endsection

</html>


