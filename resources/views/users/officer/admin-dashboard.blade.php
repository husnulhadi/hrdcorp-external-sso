<!DOCTYPE html>
<html lang="en">
<head>
    
    <title>HRD Corp BizMatch</title>
    <!-- Main CSS-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- <link href="/assets/css/main.css" rel="stylesheet" media="all">  -->
    <!-- <link rel="stylesheet" href="/assets/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="/assets/js/bootstrap.bundle.min.js"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{!! asset('assets/css/custom.css') !!}" rel="stylesheet" type="text/css">

    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> 
    <!-- <script src="https://cdn.tailwindcss.com"></script> -->
    
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" /> -->
    <!-- Styles -->    
</head>
<style>
.th-bi-wb{
    min-width : 195px;
}

.th-bm-wb{
    min-width : 215px;
}

.status-btn-wrapper{
    display : flex;
    flex-direction : column;
    column-gap : 0.5rem;
}

.btn-course-upper{
    margin-bottom : 0.5rem;
}

.course-detail-wrapper{
    /* display : flex;
    column-gap : 0.5rem; */
}

.count-box-wrapper{
    /* display : flex; */
    display: grid;
    grid-template-columns: auto auto auto;
    column-gap : 0.5rem;
    /* justify-content: center; */
    align-items: center;
    /* padding: 1rem; */
}

.count-box{
    /* border : 1px solid purple; */
    margin :5px 5px;
    padding : 2rem 4rem;
    border-radius : 4px;
    background: #002169;
    color: #F04E23;
    min-width : 262px;
}


.cb-today {
    background: #F04E23;
    color: #002169;
}

.cb-value{
    font-size : 35px;
    font-weight : bold;
}
.cb-title, .cb-value{
    text-align : center;
}

.daily-log-container{
    max-height : 450px;
    overflow : auto;
}
/* @property --num {
  syntax: "<integer>";
  initial-value: 0;
  inherits: false;
}

.cb-value {
  transition: --num 5s;
  /* counter-set: num var(--num);
  counter-set: none;
  font: 800 40px system-ui;
}

.cb-value::after {
  content: counter(num);
}

/* .cb-value:hover {
  --num: 100;
} 

body {
  margin: 2rem;
} */


@media screen and (max-width: 1200px) {
    .count-box-wrapper{
        display: grid;
        grid-template-columns: auto auto auto;
        /* display : block; */
        row-gap : 0.5rem;
    }

}


@media screen and (max-width: 1000px) {
    .count-box-wrapper{
        display: grid;
        grid-template-columns: auto auto;
        /* display : block; */
        row-gap : 0.5rem;
    }

   

}





@media screen and (max-width: 750px) {
    .count-box-wrapper{
        display: block;
     
    }

    .count-box{
        margin-bottom : 0.5rem;
    }
}

</style>

    @include('layouts.partials._locale')
    @extends('layouts.master')
    @section('content')

    @php
   
    $officerInfo = [];
    $statusList = config('custom.wb.officer.status');
    $currentOfficer = !empty($user) ? $user['Email'] : '';
    $currentOfficerId = !empty($user) ? $user['id'] : '1';


    $officerId = !empty($user) ? $user['id'] : '';
    $officerType = !empty($user) ? $user['type'] : '';
    $statusList = config('custom.wb.officer.status');
 

    $rowCount = 0;
    $info = [];
    $suspect = '';
     $wbTextStyle = Session::get('locale') != config('app.fallback_locale') ? 'wb-text-italic' : '';
    $thWbText = empty(Session::get('locale')) || Session::get('locale') == config('app.fallback_locale') ? 'th-bi-wb' : 'th-bm-wb';
    

    @endphp
    <div class="container-wrapper">
        <!-- <div class="container">
            <img src="{!! asset('assets/img/logo-removebg.png') !!}" alt="logo" class="centerz" style="padding-top:30px !important; padding-bottom:30px !important; height:150px !important;">
        </div> -->
        @include('layouts.partials._title',['type'=>'officer'])
        <div class="container">
            <div class="card">
                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                    {{__('list.dashboard_title')}}
                </div>
                <div class="card-body">
                    <div class="count-box-container">
                        <div class="count-box-wrapper">
                            <div class="count-box">
                                <div class="cb-title">{{__('list.dashboard_total_course_title')}}</div>
                                <div id="total-courses" class="cb-value">{{$totalCourses}}</div>
                            </div>
                            <div class="count-box">
                                <div class="cb-title">{{__('list.dashboard_total_tp_title')}}</div>
                                <div class="cb-value">
                                    <span id="total-tp-logged"  class="cb-value" >{{$totalTpLogged}}</span><span class="cb-value">/</span><span id="total-tp"  class="cb-value">{{$totalTp}}</span>
                                
                                </div>
                            </div>
                            <div class="count-box">
                                <div class="cb-title">{{__('list.dashboard_total_employer_title')}}</div>
                                <div class="cb-value">
                                    <span id="total-employer-logged" class="cb-value" >{{$totalEmployersLogged}}</span><span class="cb-value">/</span><span id="total-employer" class="cb-value" >{{$totalEmployers}}</span>
                                    </div>
                            </div>
                            <div class="count-box">
                                <div class="cb-title">{{__('list.dashboard_total_interested_title')}}</div>
                                <div id="total-applicant" class="cb-value">{{$totalInterested }}</div>
                            </div>
                                <div class="count-box">
                                    <div class="cb-title">{{__('list.dashboard_total_enrollment_title')}}</div>
                                    <div id="total-enrollement" class="cb-value">{{$enrollment[0]->Total_Enrollment}}</div>
                                </div>
                                <div class="count-box">
                                    <div class="cb-title">{{__('list.dashboard_total_enrollment_value_title')}} (RM)</div>
                                    @php
                                   // dd($enrollmentValue[0]->Total_EnrollmentValue);

                                    @endphp
                            
                                    <div id="total-enrollmentvalue" class="cb-value">{{!empty($enrollmentValue[0]->Total_EnrollmentValue) ? $enrollmentValue[0]->Total_EnrollmentValue : 0}}</div>
                                </div>
                        </div>

                    </div>
                    <br>
                    <div class="card">
                        <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                        {{__('list.dashboard_activity_title')}}
                        </div>
                        <div class="count-box-container">
                            <div class="count-box-wrapper">
                                <div class="count-box cb-today">
                                    <div class="cb-title">{{__('list.dashboard_total_course_title')}}</div>
                                    <div id="today-courses" class="cb-value">{{$todayCourses}}</div>
                                </div> 
                                <div class="count-box cb-today">
                                    <div class="cb-title">{{__('list.dashboard_total_tp_title')}}</div>
                                    <div id="today-tp" class="cb-value">{{$todayTp}}</div>
                                </div>
                                <div class="count-box cb-today">
                                    <div class="cb-title">{{__('list.dashboard_total_employer_title')}}</div>
                                    <div id="today-employer" class="cb-value">{{$todayEmployers}}</div>
                                </div>
                                <div class="count-box cb-today">
                                    <div class="cb-title">{{__('list.dashboard_total_interested_title')}}</div>
                                    <div id="today-applicant" class="cb-value">{{$todayInterested }}</div>
                                </div>
                                <div class="count-box cb-today">
                                    <div class="cb-title">{{__('list.dashboard_total_enrollment_title')}}</div>
                                    <div id="today-enrollement" class="cb-value">{{!empty($enrollmentToday[0]->Total_Enrollment) ? $enrollmentToday[0]->Total_Enrollment : 0}}</div>
                                </div>
                                <div class="count-box cb-today">
                                    <div class="cb-title">{{__('list.dashboard_total_enrollment_value_title')}} (RM)</div>
                                    <div id="today-enrollmentvalue" class="cb-value">{{!empty($enrollmentValueToday[0]->Total_EnrollmentValue) ? $enrollmentValueToday[0]->Total_EnrollmentValue : 0}}</div>
                                </div>
                            </div>

                        </div>
                        <div class="card-body">
                            <div class="card">
                                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                                {{__('list.dashboard_employer_enrollment_list_title')}}
                                </div>
                                <div class="card-body">
                                    <!-- start course table -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text ">
                                                                MYCOID
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.applicant_name')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.applicant_course')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            TP MYCOID
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.applicant_email')}}
                                                            </div>
                                                        </div>
                                                        
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.applicant_contact')}}
                                                            </div>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.applicant_enrollment')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.employer_course_discounted_price')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.dashboard_total_enrollment_value_title')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            @if(count($enrollmentValueListToday) == 0)
                                            <tr>
                                                <td colSpan="9" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                                            </tr>

                                            @else
                                                @foreach($enrollmentValueListToday as $li6)   
                                                    @php
                                                        $li6 = json_decode(json_encode($li6), true);
                                                    @endphp
                                                    <tr>
                                                        <td>{{$li6['mycoid']}}</td>
                                                        <td>{{$li6['name']}}</td>
                                                        <td>{{$li6['courseName']}}</td>
                                                        <td>{{$li6['tpMycoid']}}</td>
                                                        <td>{{$li6['email']}}</td>
                                                        <td>{{$li6['contact']}}</td>
                                                        <td>{{$li6['no_of_enrollment']}}</td>
                                                        <td>{{number_format($li6['discountedFee'],2)}}</td>
                                                        <td>{{number_format($li6['Total_EnrollmentValue'],2)}}</td>
                                                    </tr>
                                                @endforeach 
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end interest table -->

                                </div>
                            </div>

                            <br>
                            <div class="card">
                                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                                {{__('list.dashboard_course_registered_list_title')}}
                                </div>
                                <div class="card-body">
                                    <!-- start course table -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text ">
                                                                MYCOID
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.course_training_skim')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.course_training_certification')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.course_name')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.course_price')}}
                                                            </div>
                                                        </div>
                                                        
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.course_discount_rate')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.course_discounted_price')}}
                                                            </div>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.status')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            @if(count($coursesList) == 0)
                                            <tr>
                                                <td colSpan="8" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                                            </tr>

                                            @else
                                                @foreach($coursesList as $li)   
                                                    @php
                                                        $li = json_decode(json_encode($li), true);
                                                        $cId = $li['id'];
                                                
                                                    @endphp
                                                    <tr>
                                                        <td>{{$li['tp_mycoid']}}</td>
                                                        <td>{{$li['training_skim']}}</td>
                                                        <td>{{$li['training_certification']}}</td>
                                                        <td>{{$li['name']}}</td>
                                                        <td>{{number_format($li['training_fee'],2)}}</td>
                                                        <td>{{$li['training_discount_rate']}}</td>
                                                        <td>{{number_format($li['discounted_training_fee'],2)}}</td> 
                                                        <td>{{$li['status'] == 0  ? 'Pending' : ($li['status'] == 1 ? 'Approved':'Rejected') }}</td> 

                                                     
                                                    </tr>
                                                @endforeach 
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end course table -->

                                </div>
                            </div>

                            <br>
                            <div class="card">
                                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                               {{__('list.dashboard_tp_registered_list_title')}}
                                </div>
                                <div class="card-body">
                                    <!-- start course table -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text ">
                                                                MYCOID
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.tp_name')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.tp_email')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.tp_contact')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.status')}}
                                                            </div>
                                                        </div>
                                                        
                                                    </th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            @if(count($tpList) == 0)
                                            <tr>
                                                <td colSpan="5" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                                            </tr>

                                            @else
                                                @foreach($tpList as $li2)   
                                                    @php
                                                        $li2 = json_decode(json_encode($li2), true);
                                                        $cId2 = $li2['id'];
                                                
                                                    @endphp
                                                    <tr>
                                                        <td>{{$li2['tp_mycoid']}}</td>
                                                        <td>{{$li2['poc_name']}}</td>
                                                        <td>{{$li2['poc_email']}}</td>
                                                        <td>{{$li2['poc_phone']}}</td>
                                                        <td>{{$li2['tp_type']}}</td>
                                                    </tr>
                                                @endforeach 
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end tp table -->

                                </div>
                            </div>

                            <br>
                            <div class="card">
                                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                                {{__('list.dashboard_interest_declaration_list_title')}}
                                </div>
                                <div class="card-body">
                                    <!-- start course table -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text ">
                                                                MYCOID
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.applicant_name')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.applicant_course')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            TP MYCOID
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.applicant_email')}}
                                                            </div>
                                                        </div>
                                                        
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.applicant_contact')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.applicant_business_forte')}}
                                                            </div>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.applicant_enrollment')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.employer_course_price')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.employer_course_price_discount_rate')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.employer_course_discounted_price')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.status')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            @if(count($interestList) == 0)
                                            <tr>
                                                <td colSpan="12" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                                            </tr>

                                            @else
                                                @foreach($interestList as $li2)   
                                                    @php
                                                        $li2 = json_decode(json_encode($li2), true);
                                                        $cId = $li2['id'];
                                                    @endphp
                                                    <tr>
                                                        <td>{{$li2['mycoid']}}</td>
                                                        <td>{{$li2['name']}}</td>
                                                        <td>{{$li2['courseName']}}</td>
                                                        <td>{{$li2['tpMycoid']}}</td>
                                                        <td>{{$li2['email']}}</td>
                                                        <td>{{$li2['contact']}}</td>
                                                        <td>{{$li2['business_forte']}}</td>
                                                        <td>{{$li2['no_of_enrollment']}}</td>
                                                        <td>{{number_format($li2['fee'],2)}}</td>
                                                        <td>{{$li2['discountRate']}}</td>
                                                        <td>{{number_format($li2['discountedFee'],2)}}</td>
                                                        <td>{{$li2['status'] == 0 ? 'Pending' : ($li2['status'] == 1 ? 'Approved':'Rejected') }}</td>
                                                    </tr>
                                                @endforeach 
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end interest table -->

                                </div>
                            </div>

                            <br>
                            <div class="card">
                                <div class="card-header text-center font-weight-bold hrd-card-header-text">   
                                Employer List Registered for Today
                                </div>
                                <div class="card-body">
                                    <!-- start course table -->
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text ">
                                                                MYCOID
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.employer_name')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                                {{__('list.employer_email')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                            {{__('list.employer_contact')}}
                                                            </div>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div class="th-wrapper">
                                                            <div class="th-text">
                                                           {{__('list.status')}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                        <tbody>
                                            @if(count($employerList) == 0)
                                            <tr>
                                                <td colSpan="5" style="text-align:center;margin:0.5rem 0">{{__('list.nodatafound')}}</td>
                                            </tr>

                                            @else
                                                @foreach($employerList as $li4)   
                                                    @php
                                                        $li4 = json_decode(json_encode($li4), true);
                                                        $cId = $li4['id'];
                                                    @endphp
                                                    <tr>
                                                        <td>{{$li4['mycoid']}}</td>
                                                        <td>{{$li4['poc_name']}}</td>
                                                        <td>{{$li4['poc_email']}}</td>
                                                        <td>{{$li4['poc_phone']}}</td>
                                                        <td>{{$li4['tp_type']}}</td>
                                                    </tr>
                                                @endforeach 
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- end employer table -->

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                  
            </div>
        </div>
    </div>

    <script>
       
    $(document).ready(function(){
     
        let selectorList = ['#total-courses','#total-tp','#total-tp-logged','#total-applicant','#total-employer','#total-employer-logged','#total-enrollement','#total-enrollmentvalue','#today-courses','#today-tp','#today-applicant','#today-employer','#today-enrollment','#today-enrollmentvalue'];
        let priceSelector = ['#total-enrollmentvalue','#today-enrollmentvalue'];

        for(let i=0; i<selectorList.length; i++){
            let currentSelector = selectorList[i];
            $(currentSelector).prop('Counter',0).animate({
                Counter: $(currentSelector).text()
            }, {
                duration: 4000,
                easing: 'swing',
                step: function (now) {

                    if(priceSelector.includes(currentSelector)){
                        // $(currentSelector).text(now.toFixed(2));
                        const n = now;
                        const numberFormatter = Intl.NumberFormat('en-US');
                        const formatted = numberFormatter.format(n);
                        $(currentSelector).text(numberFormatter.format(Math.ceil(now)));
                    }else{
                        $(currentSelector).text(Math.ceil(now));
                    }
                    
                }
            });
       }
        
    })
     
       
    </script>

    @endsection


</html>


