<?php

return [
    'logout'=>'Log Keluar',
    'form'=>'Borang',
    'checkmysubmission'=>'Semak Aduan Saya',
    'termsandcondition'=>'Terma dan Syarat',
    'hrdportal'=>'Kembali ke Portal HRD Corp',
    'systemname'=>'HRD Corp BizMatch',
    'newcourse'=>'Kursus Latihan Baru',
    'courselist'=>'Senarai Kursus Latihan',
    'interestlist'=>'Senarai Pengesahan Majikan',
    'employerlist'=>'Senarai Majikan',
    'tplist'=>'Senarai Penyedia Latihan'
];

?>