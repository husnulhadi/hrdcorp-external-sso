<?php

return [
    'footerdescription1'=>'Penafian: HRD Corp tidak akan bertanggungjawab ke atas sebarang kehilangan atau kerosakan yang disebabkan oleh penggunaan maklumat yang diperoleh dari laman web ini.',
    'footerdescription2'=>'Hak Cipta © :year HRD Corp Hak cipta terpelihara | Dasar Keselamatan dan Privasi',
    'offer_text'=>'Tawaran Kursus Menarik  Daripada',
];

?>