<?php

return [
    'caseid'=>'ID Laporan',
    'wbname'=>'Penyedia Kursus',
    'casedetail'=>'Perincian Laporan',
    'submitdate'=>'Tarikh Hantar',
    'picofficer'=>'Pegawai Bertanggungjawab',
    'status'=>'Status',
    'nodatafound'=>'Tiada data dijumpai',
    'search'=>'Cari',
    'reset'=>'Tetapan Semula',
    'action'=>'Tindakan',
    'update'=>'Kemaskini',
    'pickup'=>'Kemaskini',
    'showpagecount'=>'tunjukkan :pagecount bilangan halaman setiap halaman',
    'showpagecount2'=>'paparkan dari :start hingga :end daripada :total penghantaran',
    'listtitle'=>'Senarai Pemberi Maklumat',
    'checkmysubmissiontitle'=>'Semak Serahan Laporan',

    'print_excel'=>'Eksport Excel',
    'print_excel_all'=>'Eksport Keseluruhan Excel',
    'course_list_title'=>'Senarai Kursus',
    'course_list_add_btn_text'=>'Tambah',
    'course_training_certification' => 'Sijil Latihan Kursus',
    'course_training_skim' => 'Skim Latihan Kursus',
    'course_name' => 'Nama Kursus',
    'course_price'=>'Yuran Kursus (RM)',
    'course_discount_rate'=> 'Kadar Diskaun (%)',
    'course_discounted_price'=>'Yuran Selepas Diskaun (%)',
    'detail'=>'Butiran',
    'status'=>'Status',
    'updated_by'=>'Dikemaskini Oleh',
    'reject_reason'=>'Sebab Tolak',
    'approve'=>'Lulus',
    'approved'=>'Lulus',
    'pending'=>'Belum Selesai',
    'edit'=>'Sunting',
    'reject'=>'Tolak',
    'remove'=>'Padam',
    'delete'=>'Padam',
    'interest'=>'Berminat',
    'interested'=>'Telah berminat',
    'interest_title'=>'Senarai Pengesahan Majikan',

    'applicant_name'=>'Nama Majikan',
    'applicant_email'=>'E-mel Majikan',
    'applicant_contact'=>'Maklumat Hubungi Pemohon',
    'applicant_business_forte'=>'Jenis Perniagaan',
    'applicant_course'=>'Kursus',
    'applicant_enrollment'=>'Jumlah pelatih yang disarankan',

    
    'employer_title'=>'Senarai Majikan',
    'employer_name'=>'Nama Majikan',
    'employer_email'=>'E-mel Majikan',
    'employer_contact'=>'Maklumat Hubungi',
    'employer_business_forte'=>'No. Pendaftaran Syarikat',
    'employer_course_price'=>'Yuran Kursus (RM)',
    'employer_course_price_discount_rate'=>'Kadar Diskaun (%)',
    'employer_course_discounted_price'=>'Yuran Selepas Diskaun (%)',

    'tp_title'=>'Senarai Penyedia Latihan',
    'tp_name'=>'Nama Penyedia Latihan',
    'tp_email'=>'E-mel Penyedia Latihan',
    'tp_contact'=>'Maklumat Hubungi Penyedia Latihan',

    'print_check_title'=>'Masalah pencetakan',
    'print_check_text'=>'Sila tandakan sekurang-kurangnya satu untuk meneruskan pencetakan.',
    'print_check_all_title'=>'Pengesahan Cetak',
    'print_check_all_text'=>'Adakah anda mengesahkan untuk mencetak semua?',


    'dashboard_title'=>'Papan Pemuka',
    'dashboard_total_course_title'=>'Kursus',
    'dashboard_total_tp_title'=>'Penyedia Latihan',
    'dashboard_total_interested_title'=>'Pelatih yang disarankan',
    'dashboard_total_employer_title'=>'Majikan',
    'dashboard_total_enrollment_title'=>'Penyertaan',
    'dashboard_total_enrollment_value_title'=>'Yuran Penyertaan', 
    'dashboard_activity_title'=>'Aktiviti Harian',

    'dashboard_employer_enrollment_list_title'=>'Senarai Pendaftaran Majikan & Yuran Hari Ini',
    'dashboard_course_registered_list_title'=>'Senarai Kursus Yang Didaftar Hari Ini',
    'dashboard_tp_registered_list_title'=>'Senarai Penyedia Latihan Yang Didaftar Hari Ini',
    'dashboard_interest_declaration_list_title'=>'Senarai Penyata Minat untuk Hari Ini',



    'daily_course_register'=>'Kursus \':coursename\' - :tpmycoid telah ditambah.',
    'daily_course_update' => 'Status kursus \':coursename\' - :tpmycoid telah dikemas kini kepada :status.',
    'daily_tp_register'=>'Penyedia latihan :tpemail telah daftar.',
    'daily_interest_register'=>'Pemohon :interestemail telah mengisytihar kursus berminat.',
    'daily_employer_register'=>'Majikan :empemail telah daftar.',
    'default_register'=> 'Pemohon telah mengisytihar kursus berminat',
];


?>