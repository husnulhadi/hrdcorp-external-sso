<?php
 
// resources/lang/en/messages.php
return [
    'welcome' => 'Selamat datang ',
    'welcome2'=> 'ke HRD Corp Whistleblower!',
    'test'=>'Bahasa',
    'thankyou' => 'Terima kasih di atas laporan anda. Pihak kami akan meneliti dengan kadar segera.',
    'thankyoureferencecaseidtext1'=>'Berikut adalah Id Kes',
    'thankyoureferencecaseidtext2'=>'sebagai rujukan.',
    'thankyouchecksubmission'=>'Anda boleh menyemak serahan laporan melalui butang di bawah',
    'checkmysubssionbtn'=>'Semak Serahan',
    'failtoregister'=>'Pendaftaran pengguna baru tidak berjaya.',
    'invalidpassword'=>'Kata Laluan Tidak Sah',
    'usernotexist'=>'Pengguna tidak wujud.',
    'usernotexistTP'=>'We cant locate your account. Please contact ithelpdesk@hrdcorp.gov.my for more details.',
    'resetsuccess'=>'Pautan reset kata laluan telah dihantar ke emel!',
    'passwordnotmatch'=>'Kata laluan tidak sepadan!',
    'passwordnotreset'=>'Kata laluan tidak berjaya direset.',
    'registersuccess'=>'Daftar pengguna baru berjaya!',
    'resetpasswordsuccess'=>'Penetapan kata lalauan berjaya!',
    'resetpasswordsuccessmsg'=>'Sila klik di sini untuk meneruskan log masuk',
    'regtpexistinguser'=>'Akaun anda sudah wujud. Sila log masuk dengan menggunakan emel atau MYCOID anda. Kata laluan asal adalah 123456.',
];


?>