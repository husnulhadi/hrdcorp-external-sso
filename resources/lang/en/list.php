<?php

return [
    'caseid'=>'Case Id',
    'wbname'=>'Training Provider ',
    'casedetail'=>'Case Detail',
    'submitdate'=>'Submitted Date',
    'picofficer'=>'Officer',
    'status'=>'Status',
    'nodatafound'=>'No Data Found',
    'search'=>'Search',
    'reset'=>'Reset',
    'update'=>'Update',
    'action'=>'Action',
    'pickup'=>'Pick up',
    'showpagecount'=>'show :pagecount list per page',
    'showpagecount2'=>'show from :start to :end of :total entries',
    'listtitle'=>'List of Report Submission',
    'checkmysubmissiontitle'=>'Check My Submission',

    'print_excel'=>'Export Excel',
    'print_excel_all'=>'Export Excel All',
    'course_list_title'=>'Course List',
    'course_list_add_btn_text'=>'Add',
    'course_training_skim' => 'Training Scheme',
    'course_training_certification' => 'Training Certification',
    'course_name' => 'Course Name',
    'course_price'=>'Course Fee (RM)',
    'course_discount_rate'=> 'Course Discount Rate (%)',
    'course_discounted_price'=>'Fee After Discount (RM)',
    'detail'=>'Detail',
    'status'=>'Status',
    'updated_by'=>'Updated By',
    'reject_reason'=>'Reason Reject',
    'approve'=>'Approve',
    'approved'=>'Approved',
    'reject'=>'Reject',
    'remove'=>'Remove',
    'pending'=>'Pending',
    'edit'=>'Edit',
    'interest'=>'Interest',
    'interested'=>'Interested',
    'delete'=>'Deleted',


    'interest_title'=>'List of Employers Declaration',
    'applicant_name'=>'Employer Name',
    'applicant_email'=>'Email',
    'applicant_contact'=>'Contact',
    'applicant_business_forte'=>'Nature of Business',
    'applicant_course'=>'Course',
    'applicant_enrollment'=>'No of Enrollment',


    'employer_title'=>'List of Employers',
    'employer_name'=>'Name',
    'employer_email'=>'Email',
    'employer_contact'=>'Contact',
    'employer_business_forte'=>'Nature of Business',
    'employer_course_training_skim'=>'Training Scheme',
    'employer_course_name'=>'Course Name',
    'employer_course_price_1'=>'Course fee (RM)',
    'employer_course_discount_rate'=>'Discount Rate (%)',
    'employer_course_discounted_price'=>'Price After Discount (RM)',
    

    'employer_course_price'=>'Course Fee (RM)',
    'employer_course_price_discount_rate'=>'Discount Rate (%)',
    'employer_course_discounted_price'=>'Fee After Discount (RM)',

    
    'tp_title'=>'List of Training Providers',
    'tp_name'=>'Training Provider Name',
    'tp_email'=>'Training Provider Email',
    'tp_contact'=>'Training Provider Contact',

    'print_check_title'=>'Invalid Print',
    'print_check_text'=>'Please tick at least one to proceed print',
    'print_check_all_title'=>'Print Confirmation',
    'print_check_all_text'=>'Are you confirm to print all?',

    'dashboard_title'=>'Dashboard',
    'dashboard_total_course_title'=>'Courses',
    'dashboard_total_tp_title'=>'Training Provider',
    'dashboard_total_interested_title'=>'Interested',
    'dashboard_total_employer_title'=>'Employers',
    'dashboard_total_enrollment_title'=>'Enrollment',
    'dashboard_total_enrollment_value_title'=>'Enrollment Fee', 
    'dashboard_activity_title'=>'Daily Activity',
    
    'dashboard_employer_enrollment_list_title'=>' Employer Enrollment & Fee List for Today',
    'dashboard_course_registered_list_title'=>'Course List Registered for Today',
    'dashboard_tp_registered_list_title'=>'Training Provider List Registered for Today',
    'dashboard_interest_declaration_list_title'=>' Interest Declaration List for Today',


    'daily_course_register'=>'A course \':coursename\' - :tpmycoid has been added.',
    'daily_course_update' => 'Course \':coursename\' - :tpmycoid status has been update to :status.',
    'daily_tp_register'=>'A trainig provider :tpemail has been registered.',
    'daily_interest_register'=>'An applicant :interestemail has been registered.',
    'daily_employer_register'=>'An employer :empemail has been registered.',
    'default_register'=> 'An interest submitted by :applicantemail.',

    





];


?>