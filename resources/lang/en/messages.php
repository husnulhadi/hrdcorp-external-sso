<?php
 
// resources/lang/en/messages.php

return [
    'welcome' => 'Welcome ',
    'welcome2'=> 'to HRD Corp Whistleblower!',
    'test' => 'English',
    'thankyou' => 'Thank you for your report. We will look into it promptly.',
    'thankyoureferencecaseidtext1'=>'Here is your Case ID :',
    'thankyoureferencecaseidtext2'=>'for your reference.',
    'thankyouchecksubmission'=>'To review your submission, click the button below :',
    'checkmysubssionbtn'=>'Check Submission',
    'failtoregister'=>'Fail To Register',
    'invalidpassword'=>'Invalid Password',
    'usernotexist'=>'User not exist',
    'usernotexistTP'=>'We cant locate your account. Please contact ithelpdesk@hrdcorp.gov.my for more details.',
    'resetsuccess'=>'Password reset link has send to email!',
    'passwordnotmatch'=>'Password does not match!',
    'passwordnotreset'=>'Password do not reset successfully!',
    'registersuccess'=>'Register Successfully!',
    'resetpasswordsuccess'=>'Password Reset Successfully!',
    'resetpasswordsuccessmsg'=>'Please click here to proceed login',
    'regtpexistinguser'=>'Your account already exists. Please log in using your email or MYCOID. The default password is 123456.',
];  


?>