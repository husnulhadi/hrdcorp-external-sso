<?php

return [
    'logout'=>'Log Out',
    'form'=>'Form',
    'checkmysubmission'=>'Check My Submission',
    'termsandcondition'=>'Terms and Condition',
    'hrdportal'=>'Back to HRD Corp Portal',
    'systemname'=>'HRD Corp BizMatch',
    'newcourse'=>'Submit New Course',
    'courselist'=>'Course List',
    'interestlist'=>'List of Employers Declaration',
    'employerlist'=>'Employer List',
    'tplist'=> 'Training Provider',
];

?>