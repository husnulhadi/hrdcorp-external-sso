<?php

return [
    'footerdescription1'=>'Disclaimer: HRD Corp shall not be liable for any loss or damage caused by the usage of any information obtained from this website.',
    'footerdescription2'=>'Copyright © :year HRD Corp All rights reserved | Safety and Privacy Policy',
    'offer_text'=>'Great Deals Offered By',
];

?>