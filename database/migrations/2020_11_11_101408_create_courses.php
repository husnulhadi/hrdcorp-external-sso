<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('tp_mycoid', 255)->nullable();
            $table->string('training_provider', 255)->nullable();
            $table->string('name', 255);
            $table->string('training_skim', 255);
            $table->date('training_start_date')->nullable();
            $table->date('training_end_date')->nullable();
            $table->float('training_hours', 8, 2)->nullable();
            $table->tinyInteger('training_certification')->default(1);
            $table->string('training_mode', 255)->nullable();
            $table->string('training_type', 255)->nullable();
            $table->string('skill_areas', 255)->nullable();
            $table->string('training_state', 255)->nullable();
            $table->float('training_fee', 8, 2);
            $table->float('training_discount_rate', 8, 2);
            $table->float('discounted_training_fee', 8, 2);
            $table->float('training_allowance', 8, 2)->nullable();
            $table->Integer('participant')->unsigned()->nullable();
            $table->bigInteger('key_skill_id')->unsigned()->nullable();
            $table->text('summary')->nullable();
            $table->text('content')->nullable();
            $table->string('target_group', 255)->nullable();
            $table->float('training_starter_kit_cost', 8, 2)->default(0.00);
            $table->tinyInteger('course_approval_status')->default(1);
            $table->tinyInteger('query_status')->default(0);
            $table->text('query_description')->nullable();
            $table->text('reject_reason')->nullable();
            $table->string('pic_name',255)->nullable();
            $table->string('pic_email',255)->nullable();
            $table->string('pic_phone',255)->nullable();
            $table->tinyInteger('status')->default(1);
             $table->integer('course_approved_by')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('modified_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
