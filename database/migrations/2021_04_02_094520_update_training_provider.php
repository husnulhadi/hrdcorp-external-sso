<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTrainingProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_provider', function (Blueprint $table) {
            $table->id();
            $table->text('tp_mycoid')->nullable();
            $table->string('username')->nullable();
            $table->string('confirm_password')->nullable();
            $table->enum('tp_type', ['registered', 'non-registered'])->index();
            $table->string('poc_name',255)->nullable();
            $table->string('poc_email',255)->nullable();
            $table->string('poc_phone',255)->nullable();
            $table->string('location',255)->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('modified_by')->default(0);
            $table->tinyInteger('status')->default(1)->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
