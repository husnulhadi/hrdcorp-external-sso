<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ApiLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('api_logs')){
			Schema::create('api_logs', function (Blueprint $table) {
				$table->increments('id');
				$table->string('url')->nullable();
				$table->integer('parameter')->nullable();
				$table->time('start_time')->nullable();
				$table->time('end_time')->nullable();
				$table->string('ip_address')->nullable();
				$table->integer('user_id')->nullable();
				$table->timestamps();
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
