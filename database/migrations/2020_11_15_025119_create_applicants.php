<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses');
            $table->bigInteger('applicant_type')->unsigned()->default(1);
            $table->string('mycoid', 255);
            $table->string('name', 255);
            $table->string('email', 255)->nullable()->index();
            $table->string('contact', 255);
            $table->string('business_forte', 255);
            $table->integer('no_of_enrollment')->default(0);
            $table->string('profile', 255)->nullable();
            $table->string('current_status', 255)->nullable();
            $table->tinyInteger('approved_flag')->default(0)->index();
            $table->date('approved_date')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('modified_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
