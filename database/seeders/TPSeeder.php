<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class TPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultPassword = 123456;
        $data = [
            [
                "id"=>1,
                "username" => "\N",
                "confirm_password" => Hash::make($defaultPassword),
                "poc_name" => "chong cak ment",
                "poc_email" =>"handiwork2u@gmail.com",
                "poc_phone" => "102877893",
                "location" => "Kuala Lumpur",
                "tp_mycoid" => "202303052341(LA0055055-P)",
                "tp_type" => "non-registered", 
            ],
            [
                "id"=>2,
                "username" => "491535v",
                "confirm_password" => Hash::make($defaultPassword),
                "poc_name" => "KKYS SDN BHD",
                "poc_email" =>"heryani@ktys.edu.my",
                "poc_phone" => "102877893",
                "location" => "Sabah",
                "tp_mycoid" => "491535V",
                "tp_type" => "registered", 
            ],
            [
                "id"=>3,
                "username" => "821498P",
                "confirm_password" => Hash::make($defaultPassword),
                "poc_name" => "CF LEARNING SERVICES SDN BHD",
                "poc_email" =>"Fanny@cfrontier.com",
                "poc_phone" => "10198700898",
                "location" => "Sabah",
                "tp_mycoid" => "821498P",
                "tp_type" => "registered", 
            ],
            [
                "id"=>4,
                "username" => "002739261-K",
                "confirm_password" => Hash::make($defaultPassword),
                "poc_name" => "Active Mind training services",
                "poc_email" =>"izder2@yahoo.com",
                "poc_phone" => "013-2085215",
                "location" => "Selangor",
                "tp_mycoid" => "002739261-K",
                "tp_type" => "registered", 
            ],
            [
                "id"=>5,
                "username" => "\N",
                "confirm_password" => Hash::make($defaultPassword),
                "poc_name" => "Peopleplus Asia Consultancy",
                "poc_email" =>"peopleplus.asia@gmail.com",
                "poc_phone" => "182236200",
                "location" => "Selangor",
                "tp_mycoid" => "202003100218-1",
                "tp_type" => "non-registered", 
            ],
        ];
        

        DB::table('training_provider')->insert($data);

    }
}
