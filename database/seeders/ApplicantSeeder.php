<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ApplicantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'course_id'=>1,
                'applicant_type'=>1,
                'mycoid'=>"emp1.bizmatch@yopmail.com",
                'name'=>"emp1.bizmatch@yopmail.com",
                'email'=>"emp1",
                'contact'=>12121,
                'business_forte'=>'B1',
                'current_status'=>0
            ],
            [
                'course_id'=>2,
                'applicant_type'=>1,
                'mycoid'=>"emp1.bizmatch@yopmail.com",
                'name'=>"emp1.bizmatch@yopmail.com",
                'email'=>"emp1",
                'contact'=>12121,
                'business_forte'=>'B1',
                'current_status'=>0
            ],
            [
                'course_id'=>3,
                'applicant_type'=>1,
                'mycoid'=>"emp1.bizmatch@yopmail.com",
                'name'=>"emp1.bizmatch@yopmail.com",
                'email'=>"emp6",
                'contact'=>12121,
                'business_forte'=>'B2',
                'current_status'=>0
            ],
            [
                'course_id'=>4,
                'applicant_type'=>1,
                'mycoid'=>"emp2.bizmatch@yopmail.com",
                'name'=>"emp2.bizmatch@yopmail.com",
                'email'=>"emp2",
                'contact'=>12121,
                'business_forte'=>'B2',
                'current_status'=>0
            ],
            [
                'course_id'=>5,
                'applicant_type'=>1,
                'mycoid'=>"emp3.bizmatch@yopmail.com",
                'name'=>"emp3.bizmatch@yopmail.com",
                'email'=>"emp3",
                'contact'=>12121,
                'business_forte'=>'B3',
                'current_status'=>0
            ],
            [
                'course_id'=>6,
                'applicant_type'=>1,
                'mycoid'=>"emp3.bizmatch@yopmail.com",
                'name'=>"emp3.bizmatch@yopmail.com",
                'email'=>"emp3",
                'contact'=>12121,
                'business_forte'=>'B3',
                'current_status'=>0
            ],

        ];
        DB::table('applicants')->insert($data);

    }
}
