<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultPassword = 123456;
        $data = [
            [
                'id'=>1,
                'type'=>"SuperAdmin",
                'name'=>"Super Admin 1",
                'username'=>"superadmin1.bizmatch@yopmail.com",
                'email'=>"superadmin1.bizmatch@yopmail.com",
                'password'=>Hash::make($defaultPassword),
                'ref_id'=>0,
            ],
            [
                'id'=>2,
                'type'=>"admin",
                'name'=>"Admin 1",
                'username'=>"admin1.bizmatch@yopmail.com",
                'email'=>"admin1.bizmatch@yopmail.com",
                'password'=>Hash::make($defaultPassword),
                'ref_id'=>0,
            ],
            [
                'id'=>3,
                "type"=>"tp",
                "name" => "chong cak ment",
                "username" => "handiwork2u.bizmatch@yopmail.com",
                "email" =>"handiwork2u.bizmatch@yopmail.com",
                "password" => Hash::make($defaultPassword),
                "ref_id"=>1,
            ],
            [
                'id'=>4,
                "type"=>"tp",
                "name" => "KKYS SDN BHD",
                "username" => "heryani.bizmatch@yopmail.com",
                "email" =>"heryani.bizmatch@yopmail.com",
                "password" => Hash::make($defaultPassword), 
                "ref_id"=>2, 
            ],
            [
                'id'=>5,
                "type"=>"tp",
                "name" => "CF LEARNING SERVICES SDN BHD",
                "username" => "Fanny.bizmatch@yopmail.com",
                "email" =>"Fanny.bizmatch@yopmail.com",
                "password" => Hash::make($defaultPassword),
                "ref_id"=>3, 
            ],
            [
                'id'=>6,
                "type"=>"tp",
                "name" => "Active Mind training services",
                "username" =>"izder2.bizmatch@yopmail.com",
                "email" =>"izder2.bizmatch@yopmail.com",
                "password" => Hash::make($defaultPassword),
                "ref_id"=>4, 
            ],
            [
                'id'=>7,
                "type"=>"tp",
                "name" => "Peopleplus Asia Consultancy",
                "username" => "peopleplus.asia.bizmatch@yopmail.com",
                "email" =>"peopleplus.asia.bizmatch@yopmail.com",
                "password" => Hash::make($defaultPassword),
                "ref_id"=>5,
            ],
            [
                'id'=>8,
                "type"=>"employer",
                "name" => "employer1.bizmatch@yopmail.com",
                "username" => "employer1.bizmatch@yopmail.com",
                "email" =>"employer1@yopmail.com",
                "password" => Hash::make($defaultPassword),
                "ref_id"=>0,
            ],
            [
                'id'=>9,
                "type"=>"employer",
                "name" => "employer2.bizmatch@yopmail.com",
                "username" => "employer2.bizmatch@yopmail.com",
                "email" =>"employer2.bizmatch@yopmail.com",
                "password" => Hash::make($defaultPassword),
                "ref_id"=>0,
            ],
            [
                'id'=>10,
                "type"=>"employer",
                "name" => "employer3.bizmatch@yopmail.com",
                "username" => "employer3.bizmatch@yopmail.com",
                "email" =>"employer3.bizmatch@yopmail.com",
                "password" => Hash::make($defaultPassword),
                "ref_id"=>0,
            ],



        ];

        
        DB::table('users')->insert($data);

    }
}
