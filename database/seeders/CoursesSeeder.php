<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class CoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id'=>1,
                'tp_mycoid'=>'202303052341(LA0055055-P)',
                'training_provider'=>'1',
                'name'=>'TP 1 Course 1',
                'skill_areas'=>'Skim TP 1',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'chong cak ment',
                'pic_email'=>'handiwork2u@gmail.com',
                'pic_phone'=>'1212',
                'status'=>0,
                'created_by'=>1

            ],
            [
                'id'=>2,
                'tp_mycoid'=>'202303052341(LA0055055-P)',
                'training_provider'=>'1',
                'name'=>'TP 1 Course 2',
                'skill_areas'=>'Skim TP 1',
                'training_fee'=>345,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>265.65,
                'pic_name'=>'chong cak ment',
                'pic_email'=>'handiwork2u@gmail.com',
                'pic_phone'=>'1212',
                'status'=>0,
                'created_by'=>1

            ],
            [
                'id'=>3,
                'tp_mycoid'=>'491535V',
                'training_provider'=>'2',
                'name'=>'TP 2 Course 1',
                'skill_areas'=>'Skim TP 2',
                'training_fee'=>345,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>265.65,
                'pic_name'=>'KKYS SDN BHD',
                'pic_email'=>'heryani@ktys.edu.my',
                'pic_phone'=>'3434',
                'status'=>0,
                'created_by'=>1

            ],
            [
                'id'=>4,
                'tp_mycoid'=>'491535V',
                'training_provider'=>'2',
                'name'=>'TP 2 Course 2',
                'skill_areas'=>'Skim TP 2',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'KKYS SDN BHD',
                'pic_email'=>'heryani@ktys.edu.my',
                'pic_phone'=>'3434',
                'status'=>0,
                'created_by'=>1

            ],
            [
                'id'=>5,
                'tp_mycoid'=>'821498P',
                'training_provider'=>'3',
                'name'=>'TP 3 Course 1',
                'skill_areas'=>'Skim TP 3',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'CF LEARNING SERVICES SDN BHD',
                'pic_email'=>'Fanny@cfrontier.com"',
                'pic_phone'=>'5656',
                'status'=>0,
                'created_by'=>1
            ],
            [
                'id'=>6,
                'tp_mycoid'=>'821498P',
                'training_provider'=>'3',
                'name'=>'TP 3 Course 2',
                'skill_areas'=>'Skim TP 3',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'CF LEARNING SERVICES SDN BHD',
                'pic_email'=>'Fanny@cfrontier.com"',
                'pic_phone'=>'5656',
                'status'=>0,
                'created_by'=>1
            ],
            [
                'id'=>7,
                'tp_mycoid'=>'002739261-K',
                'training_provider'=>'4',
                'name'=>'TP 4 Course 1',
                'skill_areas'=>'Skim TP 4',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'Active Mind training services',
                'pic_email'=>'izder2@yahoo.com',
                'pic_phone'=>'7878',
                'status'=>0,
                'created_by'=>1
            ],
            [
                'id'=>8,
                'tp_mycoid'=>'002739261-K',
                'training_provider'=>'4',
                'name'=>'TP 4 Course 2',
                'skill_areas'=>'Skim TP 4',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'Active Mind training services',
                'pic_email'=>'izder2@yahoo.com',
                'pic_phone'=>'7878',
                'status'=>0,
                'created_by'=>1
            ],
            [
                'id'=>9,
                'tp_mycoid'=>'202003100218-1',
                'training_provider'=>'5',
                'name'=>'TP 5 Course 1',
                'skill_areas'=>'Skim TP 5',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'Peopleplus Asia Consultancy',
                'pic_email'=>'peopleplus.asia@gmail.com',
                'pic_phone'=>'7878',
                'status'=>0,
                'created_by'=>1
            ],
            [
                'id'=>10,
                'tp_mycoid'=>'202003100218-1',
                'training_provider'=>'5',
                'name'=>'TP 5 Course 2',
                'skill_areas'=>'Skim TP 5',
                'training_fee'=>300,
                'training_discount_rate'=>23,
                'discounted_training_fee'=>231,
                'pic_name'=>'Peopleplus Asia Consultancy',
                'pic_email'=>'peopleplus.asia@gmail.com',
                'pic_phone'=>'7878',
                'status'=>0,
                'created_by'=>1
            ],
        ];

        DB::table('courses')->insert($data);
    }
}
