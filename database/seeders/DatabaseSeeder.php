<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        // $this->call(RefDepartmentTableSeeder::class);
        // $this->command->info('Department table seeded!');
        // $this->call(RefAirlineTableSeeder::class);
        // $this->command->info('Airline table seeded!');
        // $this->call(RefBranchTableSeeder::class);
        // $this->command->info('Branch table seeded!');
        // $this->call(RefFlightClassTableSeeder::class);
        // $this->command->info('Flight Class table seeded!');
        // $this->call(RefFlightTypeTableSeeder::class);
        // $this->command->info('Flight Type table seeded!');
        // $this->call(RefCostCenterTableSeeder::class);
        // $this->command->info('Cost Center table seeded!');
        // $this->call(RefGlCodeTableSeeder::class);
        // $this->command->info('GL Code table seeded!');
        //$this->call(RefRequestStatusTableSeeder::class);
        $this->call([
            TPSeeder::class,
            UsersSeeder::class,
            CoursesSeeder::class,
            ApplicantSeeder::class,
        ]);

        $this->command->info('Bizmatch table seeded!');
    }
}
