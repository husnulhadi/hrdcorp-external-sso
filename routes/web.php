<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\LoginController@login')->name('login');
Route::get('/login', 'App\Http\Controllers\LoginController@login')->name('login');
Route::get('/sso', 'App\Http\Controllers\LoginController@sso')->name('sso');
Route::get('/loginldap', 'App\Http\Controllers\LoginController@loginldap')->name('loginldap');
Route::get('/register', 'App\Http\Controllers\LoginController@register')->name('register');
Route::get('/registerTP', 'App\Http\Controllers\LoginController@registerTP')->name('registerTP');
Route::get('/forgetPassword', 'App\Http\Controllers\LoginController@forgotPassword')->name('forgotPassword');
Route::get('/resetPassword', 'App\Http\Controllers\LoginController@resetPassword')->name('resetPassword');
Route::post('/processLogin', 'App\Http\Controllers\LoginController@processLogin')->name('processLogin');
Route::post('/processLoginAD', 'App\Http\Controllers\LoginController@processLoginAD')->name('processLoginAD');
Route::post('/processLoginLdap', 'App\Http\Controllers\LoginController@processLoginLdap')->name('processLoginLdap');
Route::post('/processRegister', 'App\Http\Controllers\LoginController@processRegister')->name('processRegister');
Route::post('/processRegisterTP', 'App\Http\Controllers\LoginController@processRegisterTP')->name('processRegisterTP');

Route::post('/processForgotPassword', 'App\Http\Controllers\LoginController@processForgotPassword')->name('processForgotPassword');
Route::post('/processResetPassword', 'App\Http\Controllers\LoginController@processResetPassword')->name('processResetPassword');
Route::get('/logout', 'App\Http\Controllers\LoginController@logout')->name('logout');

//Route::get('/thankYou', 'App\Http\Controllers\BizMatchController@thankYou')->name('thankYou');
//Route::get('/create', 'App\Http\Controllers\BizMatchController@createForm')->name('create');
// Route::get('/checkMySubmission', 'App\Http\Controllers\BizMatchController@trackList')->name('trackList');
// Route::get('/searchMySubmission', 'App\Http\Controllers\BizMatchController@searchTrackList')->name('searchTrackList');

/**
 * public interest
 */
Route::get('/courses', 'App\Http\Controllers\BizMatchController@publicCourseList')->name('courses');
Route::get('/coursesSearch', 'App\Http\Controllers\BizMatchController@publicCourseListSearch')->name('coursesSearch');
Route::get('/courses/app', 'App\Http\Controllers\BizMatchController@publicCourseListApp')->name('coursesApp');
Route::get('/courses/app/search', 'App\Http\Controllers\BizMatchController@publicCourseListAppSearch')->name('coursesAppSearch');
Route::post('/processInterest', 'App\Http\Controllers\BizMatchController@processInterest')->name('processInterest');

//Route::post('/processCreate', 'App\Http\Controllers\BizMatchController@create')->name('processCreate');

Route::get('/change-language/{locale}', function ($locale) {
    if (in_array($locale, ['en', 'bm'])) {
        Session::put('locale', $locale);
        App::setLocale($locale);
    }
    return redirect()->back();
})->name('changeLanguage');

Route::middleware('checkLogin')->prefix('officer')->group(function () {
    Route::get('/dashboard/{devOfficerId}', 'App\Http\Controllers\BizMatchController@adminDashboard')->name('adminDashboard');

    Route::get('/list/{devOfficerId}', 'App\Http\Controllers\BizMatchController@tpCourseList')->name('tpCourseList');
    Route::get('/list/{devOfficerId}/search', 'App\Http\Controllers\BizMatchController@tpCourseListSearch')->name('tpCourseListSearch');
    
    Route::get('/list/interest/{devOfficerId}', 'App\Http\Controllers\BizMatchController@interestList')->name('interestList');
    Route::get('/list/interest/search/{devOfficerId}', 'App\Http\Controllers\BizMatchController@interestListSearch')->name('interestListSearch');
    Route::get('/newcourse', 'App\Http\Controllers\BizMatchController@createForm')->name('newcourse');

    Route::get('/list/tp/{devOfficerId}', 'App\Http\Controllers\BizMatchController@tpList')->name('tpList');
    Route::get('/list/tp/search/{devOfficerId}', 'App\Http\Controllers\BizMatchController@tpListSearch')->name('tpListSearch');
    Route::get('/list/employer/{devOfficerId}', 'App\Http\Controllers\BizMatchController@employerList')->name('employerList');
    Route::get('/list/employer/search/{devOfficerId}', 'App\Http\Controllers\BizMatchController@employerListSearch')->name('employerListSearch');
    //Route::get('/list/detail/{profileId}', 'App\Http\Controllers\BizMatchController@getComplaintUpdateView')->name('complaintUpdateView');
    //Route::post('/processUpdate', 'App\Http\Controllers\BizMatchController@processUpdate')->name('processUpdate');
    //Route::post('/processAssign', 'App\Http\Controllers\BizMatchController@processAssign')->name('processAssign');

    Route::post('/processCreate', 'App\Http\Controllers\BizMatchController@create')->name('processCreate');
    Route::post('/updateCourse', 'App\Http\Controllers\BizMatchController@updateCourse')->name('updateCourse');
    Route::post('/processApprove', 'App\Http\Controllers\BizMatchController@approve')->name('approve');
    Route::post('/processReject', 'App\Http\Controllers\BizMatchController@reject')->name('reject');
    Route::post('/processRemove', 'App\Http\Controllers\BizMatchController@remove')->name('remove');
    Route::post('/processApproveInterest', 'App\Http\Controllers\BizMatchController@approveInterest')->name('approveInterest');
    Route::post('/processRejectInterest', 'App\Http\Controllers\BizMatchController@rejectInterest')->name('rejectInterest');

});

/**Test */
Route::get('officer/list/detail/test/{profileId}', 'App\Http\Controllers\BizMatchController@getComplaintUpdateView2')->name('complaintUpdateView2');

/**
 * Test print view
 */

/**Pdf */
// Route::get('/pdf','App\Http\Controllers\WhistleBlowerPdfController@createPDF')->name('createPDF');
// Route::get('/pdf/personal','App\Http\Controllers\WhistleBlowerPdfController@createPDFPersonal')->name('createPDFPersonal');

// Route::get('/printComplaintView','App\Http\Controllers\WhistleBlowerPdfController@printComplaintView')->name('printComplaintView');
// Route::get('/printComplaintPersonalView','App\Http\Controllers\WhistleBlowerPdfController@printComplaintPersonalView')->name('printComplaintPersonalView');

/**Excel */
Route::get('/exportExcel','App\Http\Controllers\WhistleBlowerExcelController@exportExcel')->name('exportExcel');
Route::get('/exportCourses','App\Http\Controllers\BizMatchExcelController@exportCourses')->name('exportCourses');
Route::get('/exportInterest','App\Http\Controllers\BizMatchExcelController@exportInterest')->name('exportInterest');
Route::get('/exportTP','App\Http\Controllers\BizMatchExcelController@exportTP')->name('exportTP');
Route::get('/exportEmployer','App\Http\Controllers\BizMatchExcelController@exportEmployer')->name('exportEmployer');

/**Test email Template */
// Route::get('/testSendEmail','App\Http\Controllers\BizMatchController@testSendEmail')->name('testSendEmail');
// Route::get('/officerEmailContent','App\Http\Controllers\WhistleBlowerEmailController@officerEmailContent')->name('officerEmailContent');
// Route::get('/applicantEmailContent','App\Http\Controllers\WhistleBlowerEmailController@applicantEmailContent')->name('applicantEmailContent');

// Route::get('/updateWBstatusToApplicant','App\Http\Controllers\WhistleBlowerEmailController@updateWBstatusToApplicant')->name('updateWBstatusToApplicant');
// Route::get('/updateWBstatusToOfficer','App\Http\Controllers\WhistleBlowerEmailController@updateWBstatusToOfficer')->name('updateWBstatusToOfficer');
// Route::get('/forgetPasswordEmail','App\Http\Controllers\WhistleBlowerEmailController@forgetPasswordEmail')->name('forgetPasswordEmail');
// Route::get('/reminderEmailForPendingCase','App\Http\Controllers\WhistleBlowerEmailController@reminderEmailForPendingCase')->name('reminderEmailForPendingCase');

/**
 * For Email Template Demo
 */
Route::get('/toApplicantAfterSubmitInterest','App\Http\Controllers\BizMatchEmailController@toApplicantAfterSubmitInterest')->name('toApplicantAfterSubmitInterest');
Route::get('/toOfficerAfterSubmitInterest','App\Http\Controllers\BizMatchEmailController@toOfficerAfterSubmitInterest')->name('toOfficerAfterSubmitInterest');
Route::get('/toApplicantAfterUpdateStatusInterest','App\Http\Controllers\BizMatchEmailController@toApplicantAfterUpdateStatusInterest')->name('toApplicantAfterUpdateStatusInterest');
Route::get('/toOfficerAfterUpdateStatusInterest','App\Http\Controllers\BizMatchEmailController@toOfficerAfterUpdateStatusInterest')->name('toOfficerAfterUpdateStatusInterest');
Route::get('/toOfficerAfterAddCourse','App\Http\Controllers\BizMatchEmailController@toOfficerAfterAddCourse')->name('toOfficerAfterAddCourse');
Route::get('/toOfficerAfterUpdateStatusCourse','App\Http\Controllers\BizMatchEmailController@toOfficerAfterUpdateStatusCourse')->name('toOfficerAfterUpdateStatusCourse');
Route::get('/toOfficerAfterUpdateDetailCourse','App\Http\Controllers\BizMatchEmailController@toOfficerAfterUpdateDetailCourse')->name('toOfficerAfterUpdateDetailCourse');
Route::get('/toOfficerAfterRemoveCourse','App\Http\Controllers\BizMatchEmailController@toOfficerAfterRemoveCourse')->name('toOfficerAfterRemoveCourse');

Route::get('/testingEmail','App\Http\Controllers\BizMatchEmailController@toForgetPassword')->name('toForgetPassword');
Route::get('/toForgetPassword','App\Http\Controllers\BizMatchEmailController@toForgetPassword')->name('toForgetPassword');



/**
 * Terms and condition 
 */
Route::get('/termsAndCondition','App\Http\Controllers\BizMatchController@termsConditionPage')->name('termsAndConditionPage');
Route::get('/termsAndConditionVerify','App\Http\Controllers\BizMatchController@termsConditionVerify')->name('termsAndConditionVerify');

/**
 * test Send LP
 */
Route::get('/testSendEmailLP','App\Http\Controllers\BizMatchController@testSendEmailLP')->name('testSendEmailLP');
?>
