<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/**Testing email in api*/

Route::post('/testSendAfterSubmit', 'App\Http\Controllers\WhistleBlowerController@testSendEmail')->name('testSendAfterSubmit');
Route::post('/testSendMailAfterUpdateStatus', 'App\Http\Controllers\WhistleBlowerController@testUpdate')->name('testSendMailAfterUpdateStatus');
Route::post('/testCheckDisclosureEmail', 'App\Http\Controllers\WhistleBlowerController@testCheckDisclosureEmail')->name('testCheckDisclosureEmail');
Route::post('/processForgotPassword', 'App\Http\Controllers\LoginController@processForgotPassword')->name('processForgotPassword');
Route::post('/testCronJob', 'App\Http\Controllers\WhistleBlowerController@testCronJob')->name('testCronJob');



