<?php

namespace App\Exports;

use App\Models\Book;

use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Session;

//, WithHeadings
class EmployerExport implements FromCollection, WithHeadings
{

    function __construct($data) {
        $this->printIds = Arr::get($data,'printIds','');
    }

    public function collection()
    {
        $user = Auth::user();
        $userId = $user->id;
        $userType = $user->type; 
        $printIds = $this->printIds;
        $expPrintIds = explode(',',$printIds);


        if($printIds == null){
            $app = DB::table('applicants as app')
            ->select("email","id")
            ->groupBy('email');

            $result = DB::table('applicants as app1')
                ->select(['app1.id','app1.email','app1.name',"app1.mycoid","app1.contact","app1.business_forte", 
                DB::raw('(CASE  WHEN current_status = 1 THEN "Approved"
                WHEN current_status = 2 THEN "Rejected"
                ELSE "Pending"
                END ) AS status_name')  ])
                ->joinSub($app, 'app', function ($join)
                {
                    $join->on("app.id", '=', 'app1.id');
                })
                ->orderBy('app1.id')
                ->paginate(10);

        }else{
            $result = DB::table('applicants')
                ->when($printIds != null, function($query) use($expPrintIds) {
                    return $query->whereIn('applicants.id',$expPrintIds);
                })
                ->select([
                    'applicants.id','applicants.mycoid','applicants.name','applicants.email','applicants.contact','applicants.business_forte',
                    DB::raw('(CASE  WHEN current_status = 1 THEN "Approved"
                                    WHEN current_status = 2 THEN "Rejected"
                                    ELSE "Pending"
                                    END ) AS status_name')                      
                ])
                ->orderBy('applicants.id','desc')
                ->get();
        }
        
    
        return $result;
    }


    public function headings(): array
    {

        if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale')){
            return ["ID", "Employer MYCOID", "Name","Email","Contact","Nature of Business","Status"];
        }else{
            return ["ID", "MYCOID Majikan", "Nama","E-mel","Maklumat Hubungi","Jenis Perniagaan","Status"];
        }
        
    }
   
}


?>