<?php

namespace App\Exports;

use App\Http\Resources\ProfileMstResource;
use Maatwebsite\Excel\Concerns\FromCollection;
/**
 * Testing customize column
 */
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
// use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

use App\Models\WBLoginUser;
use App\Models\WBProfileMst;
use App\Models\WBSuspectMst;
use App\Models\WBComplaintMst;
use App\Models\WBActivityMst;
use App\Models\WBUploadActivity;
use App\Models\WBUploadFile;


class UsersExport implements FromCollection, WithHeadings
{
    public function collection()
    {
        // return WBLoginUser::all();
        //$result = WBLoginUser::select(['username','Email','Status'])->get();
          //->select(['CaseID','ComName'])
                // ->where(function ($query){
                //     return $query->orWhereHas('complaint', function ($query) {
                //                     //$query->where('ImproperActivityDetail','like','%'.$search.'%');    
                //                     $query->select(['ImproperActivityDetail']);
                //                 });
                // })


        $currentOfficer = 'testofficer1@gmail.com';
        $profileIDs = [118,119];
        $listToPrint = [];
        $list = WBProfileMst::with(['complaint' => function($query){
            $query->select('ComplaintID', 'ProfileID', 'ImproperActivityDetail');
        }])
        ->whereIn('ProfileID',$profileIDs)
                ->orderBy('created_at','desc')  //paginate
                ->get();    
        dd($list);
        $list->each(function($item) {
            dd($item['caseID']);
            $listToPrint[]['CaseID'] = $item['CaseID'];
            $listToPrint[]['ComName'] = $item['ComName'];
            $complaint = $item->complaint;
            $listToPrint[]['case_detail'] = $complaint['ImproperActivityDetail'];
            $listToPrint[]['submit_date'] = $item['Created_date'];
            $listToPrint[]['status'] = $item['Status'];
            return $listToPrint;
        });

        dd($listToPrint);
        // $result = new ProfileMstResource($list);    
        // dd($result);
        //return $result;

    }


    public function headings(): array
    {
        return ["Case Id", "Whistle Blower Name", "Case Detail", "Submit Date","PIC Officer","Status"];
    }

}


?>