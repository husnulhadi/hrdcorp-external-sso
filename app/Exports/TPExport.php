<?php

namespace App\Exports;

use App\Models\Book;

use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Session;

//, WithHeadings
class TPExport implements FromCollection, WithHeadings
{

    function __construct($data) {
        $this->printIds = Arr::get($data,'printIds','');
    }

    public function collection()
    {
        $user = Auth::user();
        $userId = $user->id;
        $userType = $user->type; 
        $printIds = $this->printIds;
        $expPrintIds = explode(',',$printIds);
      
        $result = DB::table('training_provider')
                ->when($printIds != null, function($query) use($expPrintIds) {
                    return $query->whereIn('id',$expPrintIds);
                 })
                 ->select([
                    'id','tp_mycoid','poc_name','poc_email','poc_phone',
                    DB::raw('(CASE  WHEN status = 1 THEN "Active"
                                    ELSE "Inactive"
                                    END ) AS status_name')                      
                ])
                ->orderBy('id','desc')
                ->get();
    
        return $result;
    }


    public function headings(): array
    {
        if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale')){
            return ["ID", "Training Provider MYCOID", "Training Provider Name","Training Provider Email","Training Provider Contact","Status"];
        }else{
            return ["ID", "Training Provider MYCOID", "Nama Penyedia Latihan","E-mel Penyedia Latihan","Maklumat Hubungi Penyedia Latihan","Status"];
        }
        
    }
   
}


?>