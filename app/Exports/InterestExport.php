<?php

namespace App\Exports;

use App\Models\Book;

use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Session;

//, WithHeadings
class InterestExport implements FromCollection, WithHeadings
{

    function __construct($data) {
        $this->printIds = Arr::get($data,'printIds','');
    }

    public function collection()
    {
        $user = Auth::user();
        $userId = $user->id;
        $userType = $user->type; 
        $printIds = $this->printIds;
        $userRefId = $user->ref_id;
        $expPrintIds = explode(',',$printIds);
      
        $result = DB::table('applicants')
                ->leftJoin('courses as cs','cs.id','=','applicants.course_id')     
                ->when($printIds != null, function($query) use($expPrintIds) {
                    return $query->whereIn('applicants.id',$expPrintIds);
                })
                ->when($userType == 'tp', function ($query) use($userRefId) {
                    return $query->where('cs.training_provider','=',$userRefId);
                })
                ->select([
                    'applicants.id','applicants.mycoid','applicants.name','cs.name as cname','cs.tp_mycoid','applicants.email','applicants.contact','applicants.business_forte','applicants.no_of_enrollment',
                    // DB::raw('(CASE  WHEN current_status = 1 THEN "Approved"
                    //                 WHEN current_status = 2 THEN "Rejected"
                    //                 ELSE "Pending"
                    //                 END ) AS status_name')                      
                ])
                ->orderBy('applicants.id','desc')
                ->get();
    
        return $result;
    }


    public function headings(): array
    {
      
        if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale')){
            return ["ID", "MYCOID","Employer Name","Email","Course Name","Training Provider MYCOID","Contact","Nature of Business","No of Enrollment"];
        }else{
            return ["ID", "MYCOID","Nama Majikan","E-mel","Nama Kursus","MYCOID Penyedia Latihan","Maklumat Hubungi","Jenis Perniagaan","Jumlah pelatih yang disarankan"];
        }
        
    }
   
}


?>