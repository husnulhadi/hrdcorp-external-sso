<?php

namespace App\Exports;

use App\Models\Book;

use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Session;

//, WithHeadings
class CoursesExport implements FromCollection, WithHeadings
{

    function __construct($data) {
        //$this->id = Arr::get($data,'id','');
        $this->printIds = Arr::get($data,'printIds','');
        // $this->search = Arr::get($data,'search','');
        // $this->status = Arr::get($data,'status','');
        // $this->sortCol = Arr::get($data,'sortCol','');
        // $this->sortOrder = Arr::get($data,'sortOrder','');
    }

  

    public function collection()
    {
      
        $user = Auth::user();
        $userId = $user->id;
        $userType = $user->type; 
        $printIds = $this->printIds;
        $userRefId = $user->ref_id;
        $expPrintIds = explode(',',$printIds);
      
       
        $result = DB::table('courses')
                ->leftJoin('users as usr','usr.id','=','courses.created_by')
                ->leftJoin('training_provider as tp','tp.id','=','courses.training_provider')
                //->where('status','!=',3)
                ->when($userType == 'tp', function ($query) use($userRefId) {
                    return $query->where('training_provider','=',$userRefId);
                })
                ->when($printIds != null, function($query) use($expPrintIds) {
                    return $query->whereIn('courses.id',$expPrintIds);
                })
                // ->select([
                //     'courses.id','courses.tp_mycoid','tp.poc_name','courses.training_skim','courses.name','courses.training_fee','courses.training_discount_rate','courses.discounted_training_fee','usr.email as usremail',
                //     DB::raw('(CASE  WHEN courses.status = 1 THEN "Approved"
                //                     WHEN courses.status = 2 THEN "Rejected"
                //                     ELSE "Pending"
                //                     END ) AS status_name')                      
                // ])
                ->select([
                    'courses.id','courses.tp_mycoid','tp.poc_name','courses.training_skim','courses.name','courses.training_fee','usr.email as usremail',
                    DB::raw('(CASE  WHEN courses.status = 1 THEN "Approved"
                                    WHEN courses.status = 2 THEN "Rejected"
                                    ELSE "Pending"
                                    END ) AS status_name')                      
                ])
                ->orderBy('courses.id','desc')
                ->get();
        
        return $result;
    }


    public function headings(): array
    {
        // if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale')){
        //     return ["ID", "TP MYCOID", "Training Provider","Training Scheme","Course Name","Course Price (RM)","Course Discount Rate (%)","After Discount Price (RM)","Updated By","Status"];
        // }else{
        //     return ["ID", "TP MYCOID", "Penyedia Latihan","Skim Latihan Kursus","Nama Kursus","Yuran Kursus (RM)","Kadar Diskaun (%)","Yuran Selepas Diskaun (RM)","Dikemaskini oleh","Status"];
        // }

        if(Session::get('locale') == null || Session::get('locale') == config('app.fallback_locale')){
            return ["ID", "TP MYCOID", "Training Provider","Training Scheme","Course Name","Course Price (RM)","Updated By","Status"];
        }else{
            return ["ID", "TP MYCOID", "Penyedia Latihan","Skim Latihan Kursus","Nama Kursus","Yuran Kursus (RM)","Dikemaskini oleh","Status"];
        }
    }
   
}


?>