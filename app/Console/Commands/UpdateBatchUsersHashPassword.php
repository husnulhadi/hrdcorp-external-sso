<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SendReminderForPendingCase;
use App\Services\WhistleBlowerEmailService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
/**Bizmatch */
use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;

class UpdateBatchUsersHashPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:usershashpassword';
    //{startId : The start ID of to update} {endId : The end ID to update}

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all csv imported users password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $startId = $this->ask('Please provide start ID?');
        $endId = $this->ask('Please provide end ID?');

        $updateUsers = [];
        $defaultPassword = 123456;
        $users = Users::where(function($query) use($startId, $endId){
           return $query->where('id','>=',$startId)
                        ->where('id','<=',$endId);
        })->get();

        foreach($users as $user){
            $updateUsers[] = [
                'id'=>$user->id,
                'password'=>Hash::make($defaultPassword)
            ];
        }

        $bulkUpdate = Users::upsert($updateUsers,['id','password']);
        if($bulkUpdate){
            $this->info('The command was successful testing!');
        }else{
            $this->error('The command was fail testing!');
        }
    }
}
