<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SendReminderForPendingCase;
use App\Services\WhistleBlowerEmailService;

class SendEmailReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:weeklyreminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly reminder pending cases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sendWBEmail = new WhistleBlowerEmailService();
        $sendWBEmail->sendReminder(); 
        //return 0;
    }
}
