<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\SendReminderForPendingCase;
use App\Console\Commands\SendEmailReminder;
use App\Console\Commands\UpdateBatchUsersHashPassword;


class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('mail:weeklyreminder')->weekly();
        /**Testing Purpose */
        //$schedule->command('mail:weeklyreminder')->everyMinute();
       // $schedule->job(new SendReminderForPendingCase)->everyTwoMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');

    }

    protected $commands = [
        SendEmailReminder::class,
        UpdateBatchUsersHashPassword::class,
    ];
}
