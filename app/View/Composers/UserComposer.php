<?php

namespace App\View\Composers;

use Illuminate\View\View;
use Session;
use App\Models\Users;
use App\Models\TrainingProvider;
use Illuminate\Support\Arr;

class UserComposer{


/**
     * The user repository implementation.
     *
     * @var \App\Repositories\UserRepository
     */
    protected $users;
 
    /**
     * Create a new profile composer.
     *
     * @param  \App\Repositories\UserRepository  $users
     * @return void
     */
    // public function __construct(UserRepository $users)
    // {
    //     // Dependencies are automatically resolved by the service container...
    //     //$this->users = $users;
    // }
 

    public function compose(View $view)
    {

        $username = Session::get('username');
        $user = Users::where('username','=',$username)->first();
        $user = !empty($user) ? $user : [];
        //dd($user);
        
        $view->with('user',$user);
    }
}



?>