<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\View\Composers\UserComposer;
use App\View\Composers\TPComposer;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        // Register the view composer for a specific view
        //View::composer('users.public.create-form', UserComposer::class);
        //View::composer('*', UserComposer::class);
         
        // Alternatively, you can register the view composer for multiple views using an array:
        View::composer(
            ['users.officer.new-course','users.officer.tp-course-list','users.officer.interest-list','users.officer.tp-list','users.officer.employer-list','layouts.partials.courses._modal-update-course-status','layouts.partials.courses._modal-update-course','users.officer.admin-dashboard','users.public.create-form', 'login','users.officer.complaint-list','layouts.partials._modal','layouts.partials._modal-adjusted','layouts.partials._navbar','layouts.partials._navbar-public','layouts.partials._title'],
            UserComposer::class
        );

        View::composer(
            ['users.officer.new-course','users.officer.tp-course-list','users.officer.interest-list','users.officer.tp-list','users.officer.employer-list','layouts.partials.courses._modal-update-course-status','layouts.partials.courses._modal-update-course','users.officer.admin-dashboard','users.public.create-form', 'login','users.officer.complaint-list','layouts.partials._modal','layouts.partials._modal-adjusted','layouts.partials._navbar','layouts.partials._navbar-public','layouts.partials._title'],
            TPComposer::class
        );

    }
}
