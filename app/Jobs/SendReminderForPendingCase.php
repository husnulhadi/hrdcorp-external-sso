<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Pdf;
use Session;
use App\Models\WBProfileMst;
use App\Models\WBSuspectMst;
use App\Models\WBComplaintMst;
use App\Models\WBActivityMst;
use App\Models\WBLoginUser;
use App\Models\WBUploadActivity;
use App\Models\WBUploadFile;
use App\Services\WhistleBlowerEmailService;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\Http\Resources\ProfileMstResource;
use App\Http\Resources\ComplaintMstResource;
use App\Http\Resources\ProfileMstCollection;

class SendReminderForPendingCase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $sendWBMail = WhistleBlowerEmailService();
        $sendWBEmail->sendReminder(); 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
