<?php

namespace App\Services;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Models\WBLoginUser;
use App\Models\WBProfileMst;
use App\Models\WBSuspectMst;
use App\Models\WBComplaintMst;
use App\Models\WBActivityMst;
use App\Models\WBUploadActivity;
use App\Models\WBUploadFile;
use App\Models\WBDisclosureEmail;
use Carbon\Carbon;

class WhistleBlowerEmailService{

    // public function processOfficerEmailListToSendForSubmit1($data){
    //     $disclosureTypes = Arr::get($data, 'disclosure_email',[]); /**put type currently */
    //     $listOfDisclosure = config('custom.wb.officer.disclosure');
    //     $emailToSendList = []; $uniqedEmailToSendList = []; 
    //     $emailUsers = []; $emailToDsiclosure = [];
    //     $wbLoginUser = WBLoginUser::get();
    //     if(count($disclosureTypes) > 0){
    //         for($i=0; $i<count($disclosureTypes); $i++){
    //             $disclosureType = $disclosureTypes[$i];
    //             $toSendEmailByType = Arr::get($listOfDisclosure,$disclosureType.'.email_group_to_send');
    //             for($b=0; $b<count($toSendEmailByType); $b++){
    //                 array_push($emailToSendList,$toSendEmailByType[$b]);
    //             }
    //         }
    //         $uniqedEmailToSendList = array_unique($emailToSendList); 
           
    //         //$wbLoginUser = WBLoginUser::where('Email','not like','%'.$uniquedEmailString.'%')->get();
    //         //$emailToDsiclosure = $wbLoginUser->pluck('Email')->toArray();
    //         foreach($wbLoginUser as $user){
    //             $comEmail = Arr::get($user,'Email','');
    //             if(in_array($comEmail,$uniqedEmailToSendList)){
    //                 array_push($emailUsers, $user);
    //             }else{
    //                 array_push($emailToDsiclosure, $comEmail);
    //             }
    //         }
    //         // dump('Test email to send');
    //         // dump($emailUsers, $emailToDsiclosure);
    //     }else{
    //         foreach($wbLoginUser as $user){
    //             array_push($emailUsers, $user);
    //         }
    //     }
    //     return ['email_list_to_send'=>$emailUsers, 'disclosure_email_list'=>$emailToDsiclosure];
        
    // }

    // public function processOfficerEmailListToSendAfterUpdateStatus1($data){
    //     $caseId = Arr::get($data,'caseId','');
    //     $usersList = [];
    //     $profile = WBProfileMst::where('CaseID','=',$caseId)->first();
    //     if(!empty($profile)){
    //         $disclosureEmail = $profile['DisclosureEmail'];
    //         $disclosureEmailDecode = json_decode(json_decode($disclosureEmail));
    //         $usersList = WBLoginUser::whereNotIn('Email', $disclosureEmailDecode)->get();
    //         //$emailToSend = count($usersList) > 0 ? $usersList->pluck('Email')->toArray() : [];
    //     }  
    //     return $usersList;
    // }

    // public function processOfficerEmailListToSendForSubmit_1($data){
    //     //email to disclosure : The person wont receive
    //     //email to send : The person who will receive email
    //     $disclosureTypes = Arr::get($data, 'disclosure_email',[]);
    //     $emailToSend = [];
    //     $emailToDsiclosure = [];
    //     $emailToSendUserInfo = [];
    //     $includeInternalOfficer = array_merge($disclosureTypes,[0]);
    //     if(count($disclosureTypes) > 0){
    //         $emailToSendList = WBDisclosureEmail::whereIn('id',$includeInternalOfficer)->get();
    //         $emailToSend = $emailToSendList->pluck('email')->toArray();
    //         $usersList = WBLoginUser::get();
    //         foreach($usersList as $user){
    //             $userDisGroup = Arr::get($user,'disclosure_group','');
    //             $userEmail = Arr::get($user,'Email','');
    //             $explodeUserDisGroup = explode(",",$userDisGroup);
    //             $includeDisclosure = false;
    //             foreach($explodeUserDisGroup as $expDisGroup){
    //                 if(in_array($expDisGroup,$disclosureTypes)){
    //                     $includeDisclosure = true;
    //                 }
    //             }
    //             if($includeDisclosure == false && $userDisGroup != '0'){
    //                 array_push($emailToDsiclosure, $userEmail);
    //             }else{
    //                 // if($userDisGroup == '0'){
    //                 //     array_push($emailToSend, $userEmail);
    //                 // }
    //             }
    //             // dump($user['fullname']);
    //             // dump($includeDisclosure);   
    //         }
    //     }
    //     // dump('check dis funct');
    //     // dump($emailToSend, $emailToDsiclosure);
    //     return ['email_list_to_send'=>$emailToSend, 'disclosure_email_list'=>$emailToDsiclosure];
    // }



    // public function processOfficerEmailListToSendAfterUpdateStatus_1($data){
    //     $caseId = Arr::get($data,'caseId','');
    //     $usersList = []; $emailToSend = [];
    //     $profile = WBProfileMst::where('CaseID','=',$caseId)->first();
    //     dump('test get officer after update');
    //     $disclosureTypes = explode(',',Arr::get($profile,'DisclosureType',''));
    //     $disclosureTypesIncldueInternal = array_merge($disclosureTypes,["0"]); // merge the internal officer code
    //     if(count($disclosureTypesIncldueInternal) > 0){
    //         $emailToSendList = WBDisclosureEmail::whereIn('id',$disclosureTypesIncldueInternal)->get();
    //         $emailToSendDis = $emailToSendList->pluck('email')->toArray();
    //         //$emailToSendNeutralOfficerResult = WBLoginUser::where('disclosure_group','like','%0%')->get();
    //         // $emailToSendNeutralOfficer =  $emailToSendNeutralOfficerResult->pluck('Email')->toArray();
    //         // $emailToSend = array_merge($emailToSendDis, $emailToSendNeutralOfficer);
    //         //$emailToSend = $emailToSendDis;
    //         // dd($emailToSend);
    //         //dump('test process func of officer after update');
    //     }
    //     return $emailToSend;
    // }

    public function processEmailToSendOfficer($data){

        $emailToSend = [];
        $emailToDsiclosure = []; 
        $eachUserReceiveGroup = [];
        $suspectEmails = [];
        $foundSuspect = false;

        $suspect_emails = Arr::get($data,'suspect_email_address',[]);
        $disclosureTypes = Arr::get($data, 'disclosure_email',[]);
       
        $includeInternalOfficer = array_merge($disclosureTypes,[0]);

        $emailToSendList = WBDisclosureEmail::whereIn('id',$includeInternalOfficer)->get();
        $emailToSendByDisclosure = $emailToSendList->pluck('email')->toArray();
        
        $usersList = WBLoginUser::get();
        foreach($usersList as $user){    
            $userDisGroup = Arr::get($user,'disclosure_group','');
            $userEmail = Arr::get($user,'Email','');
            $explodeUserDisGroup = explode(",",$userDisGroup);
            $includeDisclosure = false; //include disclosure types
            /**check each user's disclosure group whether include in case's disclosure group */
            foreach($explodeUserDisGroup as $expDisGroup){
                if(in_array($expDisGroup,$disclosureTypes)){
                    $includeDisclosure = true;    
                }
            }
            if($includeDisclosure == false && $userDisGroup != '0'){
                /**officer who does not receive email */
                array_push($emailToDsiclosure, $userEmail);
            }else{
                /**officer who will be receive */
                if(!in_array($userEmail, $suspect_emails)){
                    array_push($eachUserReceiveGroup, $userEmail);
                }else{
                    /**suspect officer */
                    $foundSuspect = true;
                    array_push($suspectEmails, $userEmail);
                }
               
            }
        }
        // dump('found suspect');
        // dump($foundSuspect);
        if($foundSuspect){
            $emailToSend = $eachUserReceiveGroup;
        }else{
            $emailToSend = $emailToSendByDisclosure;
        }

        $data = [
           'email_to_send'=>$emailToSend,
           'email_to_disclosure'=>$emailToDsiclosure,
           'suspect_email'=>$suspectEmails
        ];

        return $data;
    }



    public function processOfficerEmailListToSendForSubmit($data){
        //email to disclosure : The person wont receive
        //email to send : The person who will receive email
        //dump('test dev after submit');
        $disclosureTypes = Arr::get($data, 'disclosure_email',[]);
        if(count($disclosureTypes) > 0){
            $result = $this->processEmailToSendOfficer($data);       
        }
        return ['email_list_to_send'=>Arr::get($result,'email_to_send',[]), 'disclosure_email_list'=>Arr::get($result,'email_to_disclosure',[])];
    }


    public function processOfficerEmailListToSendAfterUpdateStatus($data){
        $caseId = Arr::get($data,'caseId','');
        $usersList = []; $emailToSend = [];
        $profile = WBProfileMst::with(['suspect'])->where('CaseID','=',$caseId)->first();
        $suspect = !empty($profile) ? $profile->suspect : [];
        $suspectEmails = $suspect->pluck('suspect_email')->toArray();    
        $disclosureTypes = explode(',',Arr::get($profile,'DisclosureType',''));
        $data['disclosure_email'] = $disclosureTypes;
        $data['suspect_email_address'] = $suspectEmails;
        $result = $this->processEmailToSendOfficer($data);
        $emailToSend = Arr::get($result,'email_to_send',[]);
        return $emailToSend;
    }

    public function processPublicUserEncryptUrl($caseId){       
        $searchUrl = route('searchTrackList');
        $searchUrlWithParam = $searchUrl.'?status=all&searchfromemail=';
        $param = urlencode(base64_encode($caseId));
        //$param = urlencode(base64_encode('WB169043133890'));
        $encodeUrl = $searchUrlWithParam.$param;
        return $encodeUrl;
    }

    public function processOfficerTraceUrl(){
        //$url = env('APP_URL').'/'.env('APP_NAME').'/public/login.php'; 
        $url = route('login');
        return $url;
    }

    /**after submit */
    public function sendEmailAfterSubmitWB($data){
        $this->toOfficerAfterSubmitWB($data);
        $this->toPublicAfterSubmitWB($data);
    }

    public function toOfficerAfterSubmitWB($data){
        $emailToSend = Arr::get($data,'email_to_send',[]);
        $data['trace_url'] = $this->processOfficerTraceUrl();
        //dump('after submit officer');
        /**
         * WB after submit send email to officer
         */
        //dump($data);
        foreach($emailToSend as $email){
            //dump($email);
            $data['receiver_name'] = 'all';
            $receiver = $email;
            $send = Mail::to($receiver)->send(new SendMail($data, 'Whistleblower Submission', 'email.emailNotiContentOfficer'));
        }

        /**Testing data */
        // $receiver = [
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'wongleeping@hrdcorp.gov.my'],
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'lptesting1416@gmail.com'],
        //     ['receiver_name'=>'Hadi',
        //     'receiver_email'=>'hadi123test@yopmail.com'],  
        // ];
        // foreach($receiver as $r){
        //     $data['receiver_name'] = Arr::get($r,'receiver_name','');
        //     $to = Arr::get($r,'receiver_email','');
        //     $send = Mail::to($to)->send(new SendMail($data, 'Whistle Blower Submission', 'email.emailNotiContentOfficer'));
        // }
        
    }

    public function toPublicAfterSubmitWB($data){
        $caseId = Arr::get($data,'case_id','');
        $data['receiver_name'] = Arr::get($data,'applicant_name','LP');
        $getEncryptLink = $this->processPublicUserEncryptUrl($caseId);
        $receiver = Arr::get($data,'applicant_email_address','lptesting1416@gmail.com');
        $data['trace_url'] = $getEncryptLink;
        $send = Mail::to($receiver)->send(new SendMail($data, 'Whistleblower Submission', 'email.emailNotiContentApplicant'));   
        // dump('public after submit');
        // dump($receiver);
        // die();
        /**Testing */
        // $receiver = [
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'wongleeping@hrdcorp.gov.my'],
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'lptesting1416@gmail.com'],
        //     ['receiver_name'=>'Hadi',
        //     'receiver_email'=>'hadi123test@yopmail.com'],  
        // ];
        // foreach($receiver as $r){
        //     $data['receiver_name'] = Arr::get($r,'receiver_name','');
        //     $to = Arr::get($r,'receiver_email','');
        //     $send = Mail::to($to)->send(new SendMail($data, 'Whistle Blower Submission', 'email.emailNotiContentApplicant'));
        // }
    }

    /**Update status*/

    public function sendEmailNotiAfterUpdateStatus($data){
        $caseDt = [];
        $this->toOfficerWhenUpdateCaseStatus($data, $caseDt);
        $this->toPublicWhenUpdateCaseStatus($data, $caseDt);
    }

    public function toOfficerWhenUpdateCaseStatus($data, $caseDt){
        $data['trace_url'] = $this->processOfficerTraceUrl();
        $officerEmailToSend = $this->processOfficerEmailListToSendAfterUpdateStatus($data);
        //dump('test officer update status');
        /**
         * Send email to officer after update status
         */
         if(count($officerEmailToSend) > 0){
            foreach($officerEmailToSend as $officerEmail){
                $receiver = $officerEmail;
                $data['receiver_name'] = 'all';
                //dump($receiver);
                $send = Mail::to($receiver)->send(new SendMail($data, 'Whistleblower Case Update', 'email.emailUpdateStatusNotiContentOfficer'));
            }
         }
        /**
         * Testing send officer email
         */
        // $receiver = [
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'wongleeping@hrdcorp.gov.my'],
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'lptesting1416@gmail.com'],
        //     ['receiver_name'=>'Hadi',
        //     'receiver_email'=>'hadi123test@yopmail.com'],  
        // ];
        // foreach($receiver as $r){
        //     $data['receiver_name'] = Arr::get($r,'receiver_name','');
        //     $to = Arr::get($r,'receiver_email','');
        //     $send = Mail::to($to)->send(new SendMail($data, 'Whistle Blower Case Update', 'email.emailUpdateStatusNotiContentOfficer'));
        // }
        
    }

    public function toPublicWhenUpdateCaseStatus($data, $caseDt){
        //send to user only
        //dump('test pulic update status');
        $data['trace_url'] = $this->processPublicUserEncryptUrl(Arr::get($data,'caseId',''));
        $receiver = Arr::get($data, 'applicantEmail','lptesting1416@gmail.com');
        $data['receiver_name'] = Arr::get($data, 'applicantName','Testing LP');
        $send = Mail::to($receiver)->send(new SendMail($data, 'Whistleblower Case Update', 'email.emailUpdateStatusNotiContentApplicant'));
        // dump('public');
        // dump($data, $receiver);
        // die();
        
        /**Testing Email */
        // $receiver = [
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'wongleeping@hrdcorp.gov.my'],
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'lptesting1416@gmail.com'],
        //     ['receiver_name'=>'Hadi',
        //     'receiver_email'=>'hadi123test@yopmail.com'],  
        // ];
        // foreach($receiver as $r){
        //     $data['receiver_name'] = Arr::get($r,'receiver_name','');
        //     $to = Arr::get($r,'receiver_email','');
        //     $send = Mail::to($to)->send(new SendMail($data, 'Whistle Blower Case Update', 'email.emailUpdateStatusNotiContentApplicant'));
        // }
        
    }

    public function sendResetPasswordEmail($data){
        $userInfo = Arr::get($data,'user_info');
        $data['receiver_name'] = Arr::get($userInfo, 'fullname');
        $receiver = Arr::get($userInfo,'Email','lptesting1416@gmail.com');
        $data['trace_url'] = Arr::get($data,'reset_password_url','');
        $send = Mail::to($receiver)->send(new SendMail($data, 'Whistleblower Password Reset', 'email.emailForgotPassword'));
        // dump('test send forgot password');
        // dump($data, $receiver);
        // die();
        /**Testing Email */
        // $receiver = [
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'wongleeping@hrdcorp.gov.my'],
        //     ['receiver_name'=>'Lee Ping',
        //     'receiver_email'=>'lptesting1416@gmail.com'],
        //     ['receiver_name'=>'Hadi',
        //     'receiver_email'=>'hadi123test@yopmail.com'],  
        // ];
        // foreach($receiver as $r){
        //     $data['receiver_name'] = Arr::get($r,'receiver_name','');
        //     $to = Arr::get($r,'receiver_email','');
        //     //$send = Mail::to($to)->send(new SendMail($data, 'Whistle Blower Case Update', 'email.emailForgotPassword'));
        // }
    }

    public function sendReminder(){
        //dump('test send reminder process');
        //check the complaint list one week ago by current date or time
        $pendingList = [];
        $suspectEmails = [];
        $pendingResult = WBProfileMst::with(['suspect'])->where('Status','=',1)->where('created_at','<=',DB::raw('DATE_SUB(NOW(), INTERVAL 5 DAY)'))->get();
        foreach($pendingResult as $pList){
            array_push($pendingList, Arr::get($pList, 'CaseID'));
            $suspect = $pList->suspect;
            foreach($suspect as $sus){
                array_push($suspectEmails, Arr::get($sus,'suspect_email',''));
            }
        }

        $userList = WBLoginUser::where('disclosure_group','not like','%0%')->get();
        if(count($pendingList) > 0){
            $receiver = 'lptesting1416@gmail.com';
            $data = [];
            $data['pending_case'] = $pendingList;
            //dump('reminder email receiver');
            foreach($userList as $user){
                if(!in_array($user, $suspectEmails)){
                    $data['receiver_name'] = 'all';
                    $to = Arr::get($user,'Email','');
                    //dump($to);
                    $send = Mail::to($to)->send(new SendMail($data, 'Reminder of Whistleblower Pending Cases', 'email.emailReminderPendingCase'));
                }
            }
            
            /**Testing Email */
            // // dump($pendingCases);
            // $receiver = [
            //     ['receiver_name'=>'Lee Ping',
            //     'receiver_email'=>'wongleeping@hrdcorp.gov.my'],
            //     ['receiver_name'=>'Lee Ping',
            //     'receiver_email'=>'lptesting1416@gmail.com'],
            //     ['receiver_name'=>'Hadi',
            //     'receiver_email'=>'hadi123test@yopmail.com'],  
            // ];
            // foreach($receiver as $r){
            //     $data['receiver_name'] = Arr::get($r,'receiver_name','');
            //     $to = Arr::get($r,'receiver_email','');
            //     //$send = Mail::to($to)->send(new SendMail($data, 'Reminder of Whistleblower Pending Cases', 'email.emailReminderPendingCase'));
            // }
        }   
    }

}





?>
