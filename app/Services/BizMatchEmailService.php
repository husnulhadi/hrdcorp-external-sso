<?php

namespace App\Services;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Models\WBLoginUser;
use App\Models\WBProfileMst;
use App\Models\WBSuspectMst;
use App\Models\WBComplaintMst;
use App\Models\WBActivityMst;
use App\Models\WBUploadActivity;
use App\Models\WBUploadFile;
use App\Models\WBDisclosureEmail;

use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;
use Carbon\Carbon;

class BizMatchEmailService{
    /**TODO
     * 1. After add course (admin & TP)
     * 2. Admin only approve course - admin & tp receive 
     * 3. Employer and tp, admin receive email noti after submit interest 
     * 4. Remove course (admin & tp) 
     * 
     */
   
    // function __construct(){
       
    // }

    public function getAdminAndTPList($data,$tpMycoid){
        $users = Users::whereIn('type',['admin','SuperAdmin'])->get();
        $tp = TrainingProvider::where('tp_mycoid','=', $tpMycoid)->first();
        $tpEmail = Arr::get($tp,'poc_email', '');
        $adminEmails = $users->pluck('email')->toArray();
        $mergedEmails = array_merge($adminEmails,[$tpEmail]); 
        return $mergedEmails;
    }

    public function processOfficerTraceUrl(){
        $url = route('login');
        return $url;
    }

    public function sendInterestAfterSubmit($data){
        $this->toApplicantAfterSubmitInterest($data);
        $this->toOfficerAfterSubmitInterest($data);
    }

    public function sendInterestAfterUpdateStatus($data){
        $this->toApplicantAfterUpdateStatusInterest($data);
        $this->toOfficerAfterUpdateStatusInterest($data);
    }

    public function toApplicantAfterSubmitInterest($data){
        // array:13 [
        //     "_token" => "20H4GxQ5YOPVY6yF7T0xs3srmXBD2UdnrqFQR5Lr"
        //     "course_id" => null
        //     "course_tp_mycoid" => "dimassimatupang27@gmail.com"
        //     "course_name" => "Course1"
        //     "employer_mycoid" => "emp4@yopmail.com"
        //     "employer_name" => "Emp4"
        //     "employer_contact" => "12323"
        //     "employer_email" => "emp4@yopmail.com"
        //     "employer_business_forte" => "B4"
        //     "id" => "4"
        //     "applicant_type" => "1"
        //     "current_status" => "1"
        //     "trace_url" => "http://localhost/bizmatch-hrdcorp/public/login"
        //   ]
        $data['default_password'] = 123456;
        $data['trace_url'] = $this->processOfficerTraceUrl();
        $receiver = Arr::get($data,'employer_email','');
        if(!empty($receiver)){
            // dump('test after submit interest applicant');
            // dump($receiver, $data);
            $send = Mail::to($receiver)->send(new SendMail($data, 'ACKNOWLEDGEMENT OF COURSE INTEREST SUBMISSION','email.interest.emailInterestApplicantAfterSubmit'));
        }
    }

    public function toOfficerAfterSubmitInterest($data){
        
        //  array:12 [
        //     "_token" => "sKUC5tDYa5uUx3bVymhXUzVAsXyIxYaPihubMkBF"
        //     "course_id" => null
        //     "course_tp_mycoid" => "PPM0011415071967"
        //     "course_name" => "C2"
        //     "employer_mycoid" => "adasd"
        //     "employer_name" => "werwer"
        //     "employer_contact" => "234234"
        //     "employer_email" => "sdf@df.dfdf"
        //     "employer_business_forte" => "efefef"
        //     "id" => "2"
        //     "applicant_type" => "1"
        //     "current_status" => "1"
        //   ]
        $data['trace_url'] = $this->processOfficerTraceUrl();
        $tpMycoid = Arr::get($data, "course_tp_mycoid", "");
        //$mergedEmails = $this->getAdminAndTPList($data, $tpMycoid);

        $tp = TrainingProvider::where('tp_mycoid','=', $tpMycoid)->first();
        $receiver = Arr::get($tp,'poc_email', '');
        
        // dump($receiver, $data); 
        //die();  
        // foreach($mergedEmails as $email){      
        //     $receiver = $email;
        //     if(!empty($receiver)){
        //         // dump('test after submit interest officer');
        //         // dump($receiver, $data);   
        //         $send = Mail::to($receiver)->send(new SendMail($data, 'NOTIFICATION OF COURSE INTEREST DECLARATION SUBMISSION ','email.interest.emailInterestOfficerAfterSubmit'));
        //     }
        // }

        if(!empty($receiver)){
            $send = Mail::to($receiver)->send(new SendMail($data, 'NOTIFICATION OF COURSE INTEREST DECLARATION SUBMISSION ','email.interest.emailInterestOfficerAfterSubmit'));
        }   
    }

    public function toApplicantAfterUpdateStatusInterest($data){
        // array:11 [
        //     "id" => "3"
        //     "current_status" => "2"
        //     "created_by" => "1"
        //     "emp_mycoid" => "emp3@yopmail.com"
        //     "tp_mycoid" => "PPM0011415071967"
        //     "course" => "EMP3"
        //     "applicant_email" => "emp3@yopmail.com"
        //     "status_prevoius" => "Pending"
        //     "status" => "Reject"
        //     "updated_by" => "wongleeping@yopmail.com"
        //     "trace_url" => "http://localhost/bizmatch-hrdcorp/public/login"
        //   ]

        $data['trace_url'] = $this->processOfficerTraceUrl();
        $receiver = Arr::get($data, 'applicant_email', '');
        if(!empty($receiver)){
            // dump('test applicant after update interest');
            // dump($receiver, $data);
            $send = Mail::to($receiver)->send(new SendMail($data, 'ACKNOWLEDGEMENT OF COURSE INTEREST DECLARATION SUBMISSION','email.interest.emailInterestApplicantUpdateStatus'));
        }
    }

    public function toOfficerAfterUpdateStatusInterest($data){
        // array:11 [
        //     "id" => "3"
        //     "current_status" => "2"
        //     "created_by" => "1"
        //     "emp_mycoid" => "emp3@yopmail.com"
        //     "tp_mycoid" => "PPM0011415071967"
        //     "course" => "EMP3"
        //     "applicant_email" => "emp3@yopmail.com"
        //     "status_prevoius" => "Pending"
        //     "status" => "Reject"
        //     "updated_by" => "wongleeping@yopmail.com"
        //     "trace_url" => "http://localhost/bizmatch-hrdcorp/public/login"
        //   ]
        $data['trace_url'] = $this->processOfficerTraceUrl();
        $tpMycoid = Arr::get($data, "tp_mycoid", "");
        $mergedEmails = $this->getAdminAndTPList($data, $tpMycoid);
    
        foreach($mergedEmails as $email){
            $receiver = $email;
            if(!empty($receiver)){
                // dump('test officr after update interest');
                // dump($receiver, $data);  
                $send = Mail::to($receiver)->send(new SendMail($data, 'NOTIFICATION OF STATUS UPDATE FOR APPLICANT COURSE INTEREST','email.interest.emailInterestOfficerUpdateStatus'));
            }
        }
        //die();
    }


    public function toOfficerAfterAddCourse($data){
        //dd($data);
        // array:14 [
        //     "user_id" => "1"
        //     "tp_id" => null
        //     "_token" => "3YFB10DrZStuPiL0jcSwHfHAXRSgcPbBS4lXpTtk"
        //     "tp_mycoid" => "MOA"
        //     "tp_name" => null
        //     "tp_email" => null
        //     "tp_contact_no" => null
        //     "tp_skill_area" => array:2 [
        //       0 => "SKim MOA 1"
        //       1 => "Skim MOA 2"
        //     ]
        //     "tp_course_name" => array:2 [
        //       0 => "Course MOA 1"
        //       1 => "Course MOA 2"
        //     ]
        //     "tp_course_price" => array:2 [
        //       0 => "230"
        //       1 => "400"
        //     ]
        //     "tp_course_discount_rate" => array:2 [
        //       0 => "10"
        //       1 => "30"
        //     ]
        //     "tp_course_discounted_price_value" => array:2 [
        //       0 => "207.00"
        //       1 => "280.00"
        //     ]
        //     "updated_by" => "wongleeping@yopmail.com"
        //     "tp_email_db" => "hrdfadmin@hrdf.com.my"
        //   ]

        $courses = implode(',',Arr::get($data, 'tp_course_name', []));
        $data['courses'] = $courses;
        $data['trace_url'] = $this->processOfficerTraceUrl();
        
        $tpEmail = Arr::get($data,'tp_email_db','');
        $users = Users::where('type','=','admin')->get();
        $adminEmails = $users->pluck('email')->toArray();
        $mergedEmails = array_merge($adminEmails,[$tpEmail]);
        
        // dump('to officer after add course');
        // dump($data);
        foreach($mergedEmails as $email){
            $receiver = $email;
            //dump($receiver);
            if(!empty($receiver)){
                $send = Mail::to($receiver)->send(new SendMail($data, 'NOTIFICATION ON REGISTRATION NEW COURSE(S)','email.courses.emailAddCourseToOfficer'));
            }
        }
        //die();
    }


    public function toOfficerAfterUpdateDetailCourse($data){
         
        // array:11 [
        //     "_token" => "w3zGIA6dEbcuhwXp37IbHuKb3ExCWOWOrcG2i4iO"
        //     "course_id" => "15"
        //     "course_tp_mycoid" => "PPM0011415071967"
        //     "course_name" => null
        //     "tp_skill_area" => "Skim PPM0011415071967 -103- 1-teesting88"
        //     "tp_course_name" => "PPM0011415071967 -103-Course1"
        //     "tp_course_price" => "130"
        //     "tp_course_discount_rate" => "23"
        //     "tp_course_discounted_price_value" => "28.98"
        //     "current_status" => "1"
        //     "updated_by" => "wongleeping@yopmail.com"
        //   ]


        $data['trace_url'] = $this->processOfficerTraceUrl();
        $tpMycoid = Arr::get($data, "course_tp_mycoid");
        $mergedEmails = $this->getAdminAndTPList($data, $tpMycoid);

        // dump('after update status');
        // dump($data);
        foreach($mergedEmails as $email){
            $receiver = $email;
            if(!empty($receiver)){
                //dump($receiver);
                $send = Mail::to($receiver)->send(new SendMail($data, 'NOTIFICATION ON COURSE DETAIL UPDATE', 'email.courses.emailUpdateDetailCourseToOfficer'));
            }
            
        }
        //die();
    }

    public function toOfficerAfterUpdateStatusCourse($data){
        /**
         * Approve post data
         */
        // array:7 [
        //     "id" => "7"
        //     "status" => "Approved"
        //     "approve_by" => "1"
        //     "tp_mycoid" => "MOA"
        //     "course" => "Course MOA 1"
        //     "status_previous" => "Pending"
        //     "updated_by" => "wongleeping@yopmail.com"
        //     "trace_url" => "http://localhost/bizmatch-hrdcorp/public/login"
        // ]


        /**
         * Reject post data
         */
        // array:10 [
        //     "_token" => "3YFB10DrZStuPiL0jcSwHfHAXRSgcPbBS4lXpTtk"
        //     "course_id" => "7"
        //     "course_tp_mycoid" => "MOA"
        //     "course_name" => "Course MOA 1"
        //     "reason" => "Test reason"
        //     "id" => "7"
        //     "status" => "2"
        //     "tp_mycoid" => "Course MOA 1"
        //     "course" => "MOA"
        //     "trace_url" => "http://localhost/bizmatch-hrdcorp/public/login"
        //   ]
        
        $data['trace_url'] = $this->processOfficerTraceUrl();
        $tpMycoid = Arr::get($data, "tp_mycoid");
        $mergedEmails = $this->getAdminAndTPList($data, $tpMycoid);

        // dump('after update status');
        // dump($data);
        foreach($mergedEmails as $email){
            $receiver = $email;
            if(!empty($receiver)){
                //dump($receiver);
                $send = Mail::to($receiver)->send(new SendMail($data, 'NOTIFICATION ON COURSE UPDATE', 'email.courses.emailUpdateStatusCourseToOfficer'));
            }
            
        }
        //die();
    
    }


    public function toOfficerAfterDeleteCourse($data){

        // array:6 [
        //     "id" => "8"
        //     "status" => "3"
        //     "tp_mycoid" => "MOA"
        //     "course" => "Course MOA 2"
        //     "updated_by" => "wongleeping@yopmail.com"
        //     "trace_url" => "http://localhost/bizmatch-hrdcorp/public/login"
        //   ]
        //only admin receive email
        $data['trace_url'] = $this->processOfficerTraceUrl();
        $tpMycoid = Arr::get($data, "tp_mycoid");
        $mergedEmails = $this->getAdminAndTPList($data, $tpMycoid);
        // dump('delete course');
        // dump($data);
        foreach($mergedEmails as $email){
            $receiver = $email;
            if(!empty($receiver)){
                //dump($receiver);
                $send = Mail::to($receiver)->send(new SendMail($data, 'NOTIFICATION OF COURSE REMOVAL', 'email.courses.emailRemoveCourseToOfficer'));
                //return view('email.courses.emailUpdateStatusCourseToOfficer');
            }
            
        }
    }

    // public function processPublicUserEncryptUrl($caseId){       
    //     $searchUrl = route('searchTrackList');
    //     $searchUrlWithParam = $searchUrl.'?status=all&searchfromemail=';
    //     $param = urlencode(base64_encode($caseId));
    //     //$param = urlencode(base64_encode('WB169043133890'));
    //     $encodeUrl = $searchUrlWithParam.$param;
    //     return $encodeUrl;
    // }

    public function sendResetPasswordEmail($data){
        $userInfo = Arr::get($data,'user_info');
        $data['receiver_name'] = Arr::get($userInfo, 'fullname');
        $receiver = Arr::get($userInfo,'email','lptesting1416@gmail.com');
        $data['trace_url'] = Arr::get($data,'reset_password_url','');
        $send = Mail::to($receiver)->send(new SendMail($data, 'Password Reset for Business Matching', 'email.emailForgotPassword'));
     
    }

}


?>
