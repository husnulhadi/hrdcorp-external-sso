<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Support\Arr;
use App\Models\WBProfileMst;
use App\Models\WBSuspectMst;
use App\Models\WBComplaintMst;
use App\Models\WBActivityMst;
use App\Models\WBLoginUser;
use App\Models\WBUploadActivity;
use App\Models\WBUploadFile;
use App\Http\Resources\ProfileMstResource;
use App\Http\Resources\ComplaintMstResource;

class WhistleBlowerPdfController extends Controller{

    
    // Ref : https://stackoverflow.com/questions/53144534/fopenc-xampp-htdocs-prolearning-storage-fonts
    // Generate PDF
    // Ref for issue unable load image in pdf print : https://stackoverflow.com/questions/37079529/laravel-dompdf-how-to-load-image-from-storage-folder
    public function printComplaintView(){
        $list = [];
        return view('pdf.printpdf', compact('list'));
    }

    public function printComplaintPersonalView(){
        //$profileId = Arr::get($all_req, 'printProfileId');
        $profileId = 108;
        if(!empty($profileId)){

            $info = WBProfileMst::with(['complaint','suspect','uploadFile','activity'])->where('profileID','=',$profileId)->first();
            // $list = WBProfileMst::with(['complaint' => function ($query) {
            //     $query->select(['ProfileID','ImproperActivityPlace']);
            // },'suspect','uploadFile'])->get();
            $allDt = new ProfileMstResource($info);
            $complaint = !empty($info->complaint) ? $info->complaint : [];
            $suspect = !empty($info->suspect) ? $info->suspect : [];
            $activity = !empty($info->activity) ? $info->activity : [];

            $allDt->setAttribute('complaint',$complaint);
            $allDt->setAttribute('suspect',$suspect);
            $allDt->setAttribute('activity',$activity);
            $statusList = config('custom.wb.officer.status');
            return view('pdf.printpdfpersonal', compact('info','statusList'));
        }
    }

    public function createPDF(Request $request) {
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="whistle_blower_list_file.pdf"');
        header('Content-Transfer-Encoding: binary');
        $all_req = $request->all();
        $printIds = Arr::get($all_req, 'printIds');
        if(!empty($printIds)){
            $printIdsArr = explode(',',$printIds);
            $statusList = config('custom.wb.officer.status');
            // share data to view
            $list = WBProfileMst::with(['complaint','user'])->whereIn('ProfileID',$printIdsArr)->get();
            $list->each(function($item){
                $item->setAttribute('complaint',$item->complaint);
                $item->setAttribute('user',$item->user);
                return $item;
            });
            
            //view()->share('list',$list);
            $pdf = PDF::loadView('pdf/printpdf', ['list'=>$list, 'statusList' => $statusList])->setOptions(['defaultFont' => 'sans-serif']);
            //return $pdf::stream('whistle_blower_list_file.pdf', array("Attachment" => false));
            // download PDF file with download method
            return $pdf->download('whistle_blower_list_file.pdf');
        }else{
            return redirect()->back(); 
        }
    }



    public function createPDFPersonal(Request $request) {
        $all_req = $request->all();
        //dd($all_req);
        $profileId = Arr::get($all_req, 'printProfileId');
        if(!empty($profileId)){
            $info = WBProfileMst::with(['complaint','suspect','uploadFile','activity'])->where('profileID','=',$profileId)->first();
            // $list = WBProfileMst::with(['complaint' => function ($query) {
            //     $query->select(['ProfileID','ImproperActivityPlace']);
            // },'suspect','uploadFile'])->get();
            $allDt = new ProfileMstResource($info);
            $complaint = !empty($info->complaint) ? $info->complaint : [];
            $suspect = !empty($info->suspect) ? $info->suspect : [];
            $activity = !empty($info->activity) ? $info->activity : [];
            $allDt->setAttribute('complaint',$complaint);
            $allDt->setAttribute('suspect',$suspect);
            $allDt->setAttribute('activity',$activity);
            $statusList = config('custom.wb.officer.status');
            //view()->share('list',$list);
            $pdf = PDF::loadView('pdf/printpdfpersonal', ['info'=>$allDt, 'statusList' => $statusList])->setOptions(['defaultFont' => 'sans-serif']);
            return $pdf->stream('whistle_blower_personal_list_file.pdf',array("Attachment" => false));
            // download PDF file with download method
            
            //return $pdf->download('whistle_blower_personal_list_file.pdf');
        }else{
            return redirect()->back(); 
        }
       
    }

}

  

?>