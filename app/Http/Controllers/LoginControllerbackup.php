<?php

namespace App\Http\Controllers;

use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Arr;
use App\Http\Controllers\Controller;
use App\Services;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
// use App\Services\BizMatchEmailService;
use App\Services\BizMatchEmailService;

use App\Models\Users;
use App\Models\User;
use App\Models\TrainingProvider;
use App\Models\Employer;
// use App\Models\WBDisclosureEmail;
// use App\Models\Emp_ProfileModel;
// use App\Models\Master_RuleModel;
// use App\Models\Emp_RegistrationModel;

// use App\Models\WBLoginUser;
use Session;
use Cache;
use DB;
use Cookie;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $cook=request()->cookie();
        
        
        //filtering attributes from cookies aft AD auth
         if(isset($cook['remember_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'])){
            //dd("failed-AD"); 
            $ad_filter=explode("|", $cook['remember_web_59ba36addc2b2f9401580f014c7f58ea4e30989d']);
            $users = Users::where('remember_token',$ad_filter[1])->first();
            $token = $request->_token;
            
            if(!empty($users)){
            
                //dd($cook);
                $Users = new Users();
                $user = Users::where('username','=',$users->email)->first();
                    if(!empty($user)){
                        // dd($cook);
                            // Session::put('username',$user->email);
                            // Session::put('token', $cook['XSRF-TOKEN']);
                            session(['username' => $users->email]);
                            session(['token' => $cook['XSRF-TOKEN']]);
                            if ($request->session()->has('username')) {
                                //return redirect()->route('listById',['officerId'=>$user->id]);
                                //return redirect()->route('tpCourseList',['devOfficerId'=>$user->id]);
                               
                                if($user->type == 'employer'){
                                    return redirect()->route('interestList',['devOfficerId'=>$user->id]);
                                }else{
                                    return redirect()->route('tpCourseList',['devOfficerId'=>$user->id]);
                                } 
                                
                            }else{

                                echo "failed";
                            }
                            
                    }else{
                        return view('login')->withErrors(['email' => __('messages.usernotexist')]);
                        // return redirect()->back()->withErrors(['email' => 'User not exist']);
                    }
            }
            // dd($users->email);

        }
        
        if ($request->session()->has('username')) {
            $user = Users::where('username','=',Session::get('username'))->first();
                    if(!empty($user)){
                        //return redirect()->route('tpCourseList',['devOfficerId'=>$user->id]);
                     
                        if($user->type == 'employer'){
                            return redirect()->route('interestList',['devOfficerId'=>$user->id]);
                       }else{
                            return redirect()->route('tpCourseList',['devOfficerId'=>$user->id]);
                       }    
                    }
        }
        $source = 'web';
        return view('auth.login',compact('source'));
    }

    public function loginldap(Request $request)
    {
        return view('auth.loginldap');
    }

    public function register(Request $request){
        //$disclosure_email = WBDisclosureEmail::where('id','!=','0')->get();
        $disclosure_email = [];
        return view('wb-auth.register', compact('disclosure_email'));
    }

    public function registerTP(Request $request){
        //$disclosure_email = WBDisclosureEmail::where('id','!=','0')->get();
        $disclosure_email = [];
        return view('wb-auth.register-tp', compact('disclosure_email'));
    }

    public function forgotPassword(Request $request){
        return view('wb-auth.forgot-password');
    }

    public function resetPassword(Request $request){
        $encodedId = $request['id'];
        $id = urldecode(base64_decode($encodedId));
        return view('wb-auth.reset-password', compact('id'));
    }

    public function processRegister(Request $request){
      
        $allReq = $request->all();
        $users = new Users();

        $register = $users->create($allReq);
        
        if($register){
            return redirect()->back()->with('registersuccess', __('messages.registersuccess'));
        }else{
            return redirect()->back()->withErrors(['msg' => __('messages.failtoregister')]);    
        } 
    }


    public function processRegisterTP(Request $request){
      
        $allReq = $request->all();
        $tp = new TrainingProvider();
        $registerTP = $tp->create($allReq);
        $allReq['ref_id'] = $registerTP; 
        $users = new Users();
        $register = $users->create($allReq);

        
        if($register){
            return redirect()->back()->with('registersuccess', __('messages.registersuccess'));
        }else{
            return redirect()->back()->withErrors(['msg' => __('messages.failtoregister')]);    
        } 
    }


    public function processLogin(Request $request)
    {
        Session::flush();
        $email = $request->email;
        $password = $request->password;
        $token = $request->_token;
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $Users = new Users();
        $user = Users::where('email','=',$email)->first();
        if(!empty($user)){
            // if (Hash::check($password, $user->password)) {
            //     // Session::put('username',$request->email);
            //     // Session::put('token', $token);
            //     session(['username' => $user->username]);
            //     session(['token' => $token]);
            //     //return redirect()->route('listById',['officerId'=>$user->id]);
            //     return redirect()->route('listById2',['devOfficerId'=>$user->id]);
            // }else{
            //     return redirect()->back()->withErrors(['password' => __('messages.invalidpassword')]);
            // }
            $userdata = array(
              'email' => $email ,
              'password' => $password
            );
            // attempt to do the login
            if (Auth::attempt($userdata)){
                // Session::put('username',$request->email);
                // Session::put('token', $token);
                session(['username' => $user->username]);
                session(['token' => $token]);
                //return redirect()->route('listById',['officerId'=>$user->id]);
                
                if($user->type == 'employer'){
                    return redirect()->route('interestList',['devOfficerId'=>$user->id]);
                }else{
                    return redirect()->route('tpCourseList',['devOfficerId'=>$user->id]);
                }
                
            }else{
                return redirect()->back()->withErrors(['password' => __('messages.invalidpassword')]);
            }
        }else{
            return redirect()->back()->withErrors(['email' => __('messages.usernotexist')]);
        }  
    }
    public function processLoginAD(Request $request)
    {
        dd($request);
    }
    public static function LdapLogin(string $userName,string $password)
    {
        try {
            $ldap_host = env("ldap_host","");
            $ldap = ldap_connect($ldap_host) or die("Could not connect to LDAP server.");
            ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
            $dn = "uid=" . $userName . ",cn=external,cn=users,o=psmb,C=MY";
            $dnn = "uid=" . $userName . ",cn=users,o=psmb,C=MY";
            if ($bind = @ldap_bind($ldap, $dn, $password)) {
                Log::info("LDAP Success");
                return "Success";
            } else {
                Log::info("LDAP ".ldap_error($ldap));
                return ldap_error($ldap); //Invalid credentials
            }
        }catch (Exception $e) {
            Log::error("Exception in " .$e);
            return "Invalid credentials";
        }
    }
    public function getRegisteredEmployers($username)
    {
        try {
            return DB::connection('ldapmysql')->select("SELECT
                pm.MYCO_ID AS company_register_number,
                CASE WHEN pd.company_name IS NULL THEN pd.ENTERPRISE_NAME ELSE pd.company_name END AS company_name,
                cmn_contact_mst.MOBILE AS phone_number,
                cmn_contact_mst.EMAIL AS email,
                CONCAT_WS(',',addM.ADDRESS_LINE_1 ,
                addM.ADDRESS_LINE_2 ,
                addM.ADDRESS_LINE_3 ,
                addM.CITY_NAME ,
                addM.PINCODE,
                stateM.STATE_NAME ) AS address,
                pd.EMPR_REGISTRATION_DT AS EMPR_REGISTRATION_DT,
                pd.NO_EMPLOYEE,
                CASE WHEN pd.STATUS_ID = 1 THEN 'Active' ELSE 'Deactive' END AS status,
                ind.INDUSTRY_TYPE_NAME AS industry,
                sector.SECTOR_CODE,
                sector.SECTOR_DESC
            FROM PRM_EMPLOYER_MST pm
            JOIN PRM_EMPLOYER_DTLS pd ON pm.EMPLOYER_ID = pd.EMPLOYER_ID
            LEFT JOIN CMN_IND_SECTOR_MST sector ON pd.IND_SECTOR_ID = sector.IND_SECTOR_ID
            LEFT JOIN CMN_INDUSTRY_MST ind ON sector.INDUSTRY_ID = ind.INDUSTRY_ID
            LEFT JOIN CMN_ADDRESS_MST addM ON pd.ENTERPRISE_ADDRESS_ID = addM.ADDRESS_ID
            LEFT JOIN CMN_STATE_MST stateM ON addM.STATE_ID = stateM.STATE_ID
            INNER JOIN cmn_contact_mst ON cmn_contact_mst.SR_NO = pd.COMPANY_CONTACT_ID WHERE pm.MYCO_ID = '".$username."'");
                    } catch (Exception $e) {
                        Log::error("Error in getRegisteredEmployers" . $e);
                        return "";
                    }
    }

    public function getRegisteredTP($username)
    {
        // try {
        //     return DB::connection('ldapmysql')->select("SELECT PRM_TP_MST.myco_id, PRM_TP_DTLS.TP_NAME, CMN_LOOKUP_MST.lookup_desc, prm_tp_dtls.expiry_date, PRM_TP_DTLS.no_of_trainers, PRM_TP_DTLS.NO_OF_EMPLOYEES, CMN_ADDRESS_MST.address_line_1 || CASE WHEN CMN_ADDRESS_MST.address_line_2 IS NULL THEN '' ELSE ', ' || CMN_ADDRESS_MST.address_line_2 END || CASE WHEN CMN_ADDRESS_MST.address_line_3 IS NULL THEN '' ELSE ', ' || CMN_ADDRESS_MST.address_line_3 END || CASE WHEN CMN_ADDRESS_MST.address_line_4 IS NULL THEN '' ELSE ', ' || CMN_ADDRESS_MST.address_line_4 END AS Address, CMN_ADDRESS_MST.pincode AS postcode, CMN_STATE_MST.state_name, CMN_CONTACT_MST.mobile, CMN_CONTACT_MST.fax,CMN_CONTACT_MST.email,  PRM_TP_DTLS.status_id,PRM_TP_DTLS.created_date FROM PRM_TP_MST JOIN PRM_TP_DTLS ON PRM_TP_MST.TP_ID = PRM_TP_DTLS.TP_ID LEFT JOIN CMN_LOOKUP_MST ON prm_tp_dtls.company_status = CMN_LOOKUP_MST.lookup_id LEFT JOIN CMN_ADDRESS_MST ON PRM_TP_DTLS.tp_address_id = CMN_ADDRESS_MST.address_id LEFT JOIN CMN_STATE_MST ON CMN_ADDRESS_MST.state_id = CMN_STATE_MST.state_id LEFT JOIN CMN_CONTACT_MST ON PRM_TP_DTLS.tp_contact_id = CMN_CONTACT_MST.sr_no WHERE PRM_TP_MST.myco_id = '".$username."'");
        // } catch (Exception $e) {
        //     Log::error("Error in getRegisteredTP" . $e);
        //     return "";
        // }
        $result2 = ['status' => 200,'type' => 'tp'];
        $result2['data'] = DB::connection('ldapmysql')->select("SELECT PRM_TP_MST.myco_id, PRM_TP_DTLS.TP_NAME, CMN_LOOKUP_MST.lookup_desc, prm_tp_dtls.expiry_date, PRM_TP_DTLS.no_of_trainers, PRM_TP_DTLS.NO_OF_EMPLOYEES, CMN_ADDRESS_MST.address_line_1 || CASE WHEN CMN_ADDRESS_MST.address_line_2 IS NULL THEN '' ELSE ', ' || CMN_ADDRESS_MST.address_line_2 END || CASE WHEN CMN_ADDRESS_MST.address_line_3 IS NULL THEN '' ELSE ', ' || CMN_ADDRESS_MST.address_line_3 END || CASE WHEN CMN_ADDRESS_MST.address_line_4 IS NULL THEN '' ELSE ', ' || CMN_ADDRESS_MST.address_line_4 END AS Address, CMN_ADDRESS_MST.pincode AS postcode, CMN_STATE_MST.state_name, CMN_CONTACT_MST.mobile, CMN_CONTACT_MST.fax,CMN_CONTACT_MST.email,  PRM_TP_DTLS.status_id,PRM_TP_DTLS.created_date FROM PRM_TP_MST JOIN PRM_TP_DTLS ON PRM_TP_MST.TP_ID = PRM_TP_DTLS.TP_ID LEFT JOIN CMN_LOOKUP_MST ON prm_tp_dtls.company_status = CMN_LOOKUP_MST.lookup_id LEFT JOIN CMN_ADDRESS_MST ON PRM_TP_DTLS.tp_address_id = CMN_ADDRESS_MST.address_id LEFT JOIN CMN_STATE_MST ON CMN_ADDRESS_MST.state_id = CMN_STATE_MST.state_id LEFT JOIN CMN_CONTACT_MST ON PRM_TP_DTLS.tp_contact_id = CMN_CONTACT_MST.sr_no WHERE PRM_TP_MST.myco_id ='".$username."'"
            );
                 
        $response=$result2;
        if(count($response['data'])>0){
            
        }else{

            $result = ['status' => 200,'type' => 'emp'];
            $result['data'] = DB::connection('ldapmysql')->select("SELECT
                pm.MYCO_ID AS company_register_number,
                CASE WHEN pd.company_name IS NULL THEN pd.ENTERPRISE_NAME ELSE pd.company_name END AS company_name,
                cmn_contact_mst.MOBILE AS phone_number,
                cmn_contact_mst.EMAIL AS email,
                CONCAT_WS(',',addM.ADDRESS_LINE_1 ,
                addM.ADDRESS_LINE_2 ,
                addM.ADDRESS_LINE_3 ,
                addM.CITY_NAME ,
                addM.PINCODE,
                stateM.STATE_NAME ) AS address,
                pd.EMPR_REGISTRATION_DT AS EMPR_REGISTRATION_DT,
                pd.NO_EMPLOYEE,
                CASE WHEN pd.STATUS_ID = 1 THEN 'Active' ELSE 'Deactive' END AS status,
                ind.INDUSTRY_TYPE_NAME AS industry,
                sector.SECTOR_CODE,
                sector.SECTOR_DESC
            FROM PRM_EMPLOYER_MST pm
            JOIN PRM_EMPLOYER_DTLS pd ON pm.EMPLOYER_ID = pd.EMPLOYER_ID
            LEFT JOIN CMN_IND_SECTOR_MST sector ON pd.IND_SECTOR_ID = sector.IND_SECTOR_ID
            LEFT JOIN CMN_INDUSTRY_MST ind ON sector.INDUSTRY_ID = ind.INDUSTRY_ID
            LEFT JOIN CMN_ADDRESS_MST addM ON pd.ENTERPRISE_ADDRESS_ID = addM.ADDRESS_ID
            LEFT JOIN CMN_STATE_MST stateM ON addM.STATE_ID = stateM.STATE_ID
            INNER JOIN cmn_contact_mst ON cmn_contact_mst.SR_NO = pd.COMPANY_CONTACT_ID WHERE pm.MYCO_ID = '".$username."'");
            $response=$result;
        }
        return $response;

    }

    public function decryptValue($data){
        return openssl_decrypt($data, env('SECURITY_CIPHERING','AES-128-CTR'), env('SECURITY_KEY','135792468011112'), env('SECURITY_OPTIONS','0'), env('SECURITY_IV','HPC-UPSKILLS-DAT'));
    }

    public function processLoginLdap(Request $request)
    {
        $username = $request->mycoid;
        $password = $request->password;
        $password = $password;
        $def = '123456';
        if($this::LdapLogin($username, $password) == "Success"){
                $regTP = $this->getRegisteredTP($username);
                
                if (!empty($regTP['data'])) {

                    // recheck tp exist in db
                    $userCount=Users::where('email',$regTP['data'][0]->email)->count();

                    if($regTP['type'] == "emp"){

                        $tp_data=Employer::updateOrInsert(
                            [
                                'tp_mycoid' => $username
                            ],[
                                'username'=> $regTP['data'][0]->email,
                                'confirm_password'=> Hash::make($def),
                                'poc_name'=> $regTP['data'][0]->company_name ,
                                'poc_email'=> $regTP['data'][0]->email ,
                                'poc_phone'=> $regTP['data'][0]->phone_number ,
                                'location'=> $regTP['data'][0]->address ,
                                'tp_mycoid'=> $regTP['data'][0]->company_register_number ,
                                'tp_type'=> 'registered' ,
                            ]
                        );

                        $data_fetch = Employer::where('tp_mycoid','=',$username)->first();

                        $data1['username'] = $data_fetch['email'];
                        $data1['password'] = $data_fetch['confirm_password'];
                        $data1['ref_id'] = $data_fetch['id'] ;
                        $data1['role'] = 1 ;
                        $data1['email'] = $data_fetch['email'] ;
                        $data1['name'] = $data_fetch['poc_name'] ;
                        $data1['type'] = 'tp' ;

                    }else{

                        $emp_data=TrainingProvider::updateOrInsert(
                            [
                                'tp_mycoid' => $username
                            ],[
                                'username'=> $regTP['data'][0]->email,
                                'confirm_password'=> Hash::make($def),
                                'poc_name'=> $regTP['data'][0]->TP_NAME ,
                                'poc_email'=> $regTP['data'][0]->email ,
                                'poc_phone'=> $regTP['data'][0]->mobile ,
                                'location'=> $regTP['data'][0]->state_name ,
                                'tp_mycoid'=> $regTP['data'][0]->myco_id ,
                                'tp_type'=> 'registered' ,
                            ]
                        );

                        $data_fetch = TrainingProvider::where('tp_mycoid','=',$username)->first();

                        $data3['username'] = $data_fetch['email'];
                        $data3['password'] = $data_fetch['confirm_password'];
                        $data3['ref_id'] = $data_fetch['id'] ;
                        $data3['role'] = 1 ;
                        $data3['email'] = $data_fetch['email'] ;
                        $data3['name'] = $data_fetch['poc_name'] ;
                        $data3['type'] = 'employer' ;

                    }

                    $user_data=Users::updateOrInsert(
                        [
                            'email' => $data_fetch['username']
                        ],[
                            'username'=> $data_fetch['username'],
                            'password'=> $data_fetch['confirm_password'],
                            'ref_id'=> $data_fetch['id'] ,
                            'role'=> 1 ,
                            'email'=> $data_fetch['username'] ,
                            'name'=> $data_fetch['poc_name'] ,
                            'type'=> 'registered' ,
                        ]
                    );
                    $user = Users::where('username','=',$data_fetch['username'])->first();
                    $userdata = array(
                      'email' => $data_fetch['username'],
                      'password' => '123456'
                    );

                    if (Auth::attempt($userdata)){
                        session(['username' => $data_fetch['username']]);
                        session(['token' => $data_fetch['confirm_password']]);
                        
                        if($user['type'] == 'employer'){
                            return redirect()->route('interestList',['devOfficerId'=>$user->id]);
                        }else{
                            return redirect()->route('tpCourseList',['devOfficerId'=>$user->id]);
                        }
                        
                    }else{
                        return redirect()->back()->withErrors(['mycoid' => __('messages.invalidpassword')]);
                    } 

                    


                }else{

                    
                    return redirect()->back()->withErrors(['mycoid' => __('messages.usernotexist')]);

                }
        }else{
               return redirect()->back()->withErrors(['mycoid' => __('messages.usernotexistTP')]);
        }
            
    }


    public function processForgotPassword(Request $request){
        //$token = $request->_token;
        $email = $request->email;
        $request->validate([
            'email' => 'required',
        ]);
        $Users = new Users();
        $user = Users::where('email','=',$email)->first();
        if(!empty($user)){
            //$resetUserInfo = ['user'=>$user,'token'=>$token];
            //send reset password lin to user email
            $resetPasswordUrl = route('resetPassword');
            $query_string = urlencode(base64_encode($user->id));
            $en_url = $resetPasswordUrl.'?id='.$query_string;
           // send email 
           $emailDt = [
            'email'=>$email,
            'user_info'=>$user,
            'reset_password_url'=>$en_url
           ];
            $bizMatchSendEmail = new BizMatchEmailService();
            $bizMatchSendEmail->sendResetPasswordEmail($emailDt);
            return redirect()->back()->with('resetsuccess', __('messages.resetsuccess'));

        }else{
            return redirect()->back()->withErrors(['email' => __('messages.usernotexist')]);
        }
    }


    public function processResetPassword(Request $request){  
        $email = $request['email'];  
        $userId = $request['id'];
        $newPassword = $request['new_password'];
        $confirmPassword = $request['confirm_password'];
        if($newPassword != $confirmPassword){
            return redirect()->back()->withErrors(['msg' => __('messages.passwordnotmatch')]);
        }
        $user = Users::find($userId);
       
        if(!empty($user) && $user->email == $email){
            $update = $user->resetPassword($newPassword);
            if($update){
                return redirect()->back()->with(['resetpasswordsuccess'=> __('messages.resetpasswordsuccess')]);
            }else{
                return redirect()->back()->withErrors(['msg' => __('messages.passwordnotreset')]);
            }
        }else{
            return redirect()->back()->withErrors(['msg' => __('messages.usernotexist')]);
        }

    }
    
    // public function processLogin(Request $request)
    // {
    //     $username = $request->email;
    //     $password = $request->password;
    //     $token = $request->_token;
    //     $request->validate([
    //         'email' => 'required',
    //         'password' => 'required',
    //     ]);
    //     $hashed = Hash::make($password); // save $hashed value
    //     // dd($request->all());
    //     $credentials = $request->only('email', 'password');
    //     // dd(Auth::attempt(['email' => $username, 'password' => $password, 'role' => 'tp']));
    //     if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    //         Session::put('emp_validation', 1);
    //         Session::put('emp_type', 'tp');
    //         Session::put('emp_details', 'test');
    //         Session::put('', $username);
    //         Session::put('config', 1);
    //         Session::put('token', $token);

    //         return redirect()->route('list');
    //     }

    //     $password = [
    //         'message' => 'Invalid Credentials!',
    //     ];

    //     return redirect("/")->withErrors(['password' => $password]);

    // }

    public function logout(Request $request)
    {
        // Auth::logout();
        // $request->session()->invalidate();
        // $request->session()->regenerateToken();
        $request->session()->forget('username');
        $request->session()->forget('token');
 
        $request->session()->flush();
        // Session::destroy();
        Auth::logout();
        //return redirect('/');
        return redirect()->route('login');
    }
}
