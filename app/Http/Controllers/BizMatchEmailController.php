<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
// use Pdf;
// use Session;
use Illuminate\Support\Arr;

/**Bizmatch */
use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;

class BizMatchEmailController extends Controller {

    public function testSendEmail(){ 
        //dd('test send mail');
        $test = Mail::to('leeping950116@gmail.com')->send(new SendMail(array(), 'Test Email', 'email.emailNotiContentApplicant'));
        dump($test);
    }


    public function toApplicantAfterSubmitInterest(){
        return view('email.interest.emailInterestApplicantAfterSubmit');
    }

    public function toOfficerAfterSubmitInterest(){
        return view('email.interest.emailInterestOfficerAfterSubmit');
    }

    public function toApplicantAfterUpdateStatusInterest(){
        return view('email.interest.emailInterestApplicantUpdateStatus');
    }

    public function toOfficerAfterUpdateStatusInterest(){
        return view('email.interest.emailInterestOfficerUpdateStatus');
    }

    public function toOfficerAfterAddCourse(){
        return view('email.courses.emailAddCourseToOfficer');
    }

    public function toOfficerAfterUpdateDetailCourse(){
        return view('email.courses.emailUpdateDetailCourseToOfficer');
    }

    public function toOfficerAfterUpdateStatusCourse(){
        return view('email.courses.emailUpdateStatusCourseToOfficer');
    }

    public function toOfficerAfterRemoveCourse(){
        return view('email.courses.emailRemoveCourseToOfficer');
    }

    public function toForgetPassword(){
        $data = [];
        $data['receiver_name'] = 'Lee Ping';
        $data['trace_url'] = 'http://localhost/WhistleBlower/public/resetPassword?id=MTU%3D';
        return view('email.emailForgotPassword',compact('data'));
    }


    public function toPendingCaseReminder(){
        $data['receiver_name'] = 'all';
        $data['pending_case'] = [
        'WB169279824513','WB169279807582','WB169279774660',
        'WB169269600649','WB169269561274','WB169269517910',
    ];
        return view('email.emailReminderPendingCase',compact('data'));
    }

}
?>



