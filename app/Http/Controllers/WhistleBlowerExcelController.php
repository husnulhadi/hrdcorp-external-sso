<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class WhistleBlowerExcelController extends Controller 
{
    public function exportExcel() 
    {
        
        return Excel::download(new UsersExport, 'users.xlsx');
    }



}