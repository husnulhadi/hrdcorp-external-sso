<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Pdf;
use Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Services\WhistleBlowerEmailService;
use App\Services\BizMatchEmailService;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\Http\Resources\ProfileMstResource;
use App\Http\Resources\ComplaintMstResource;
use App\Http\Resources\ProfileMstCollection;


use App\Models\WBProfileMst;
use App\Models\WBSuspectMst;
use App\Models\WBComplaintMst;
use App\Models\WBActivityMst;
use App\Models\WBUploadActivity;
use App\Models\WBUploadFile;
use App\Models\WBDisclosureEmail;

/**Bizmatch */
use App\Models\Users;
use App\Models\Applicant;
use App\Models\CourseAttachment;
use App\Models\Courses;
use App\Models\TrainingProvider;
use App\Models\User;
use App\Models\Employer;

class BizMatchController extends Controller {
    
    public function testSendEmailLP(){
        // $data = [];
        // /**Testing */
        //$receiver = 'leeping950116@gmail.com';
        $data['receiver_name'] = 'Lee Ping';
       
        $receiver = ["hadi123test@yopmail.com","lptesting1416@gmail.com",'wongleeping@hrdcorp.gov.my'];
        
        foreach($receiver as $r){
            $send = Mail::to($r)->send(new SendMail($data, 'Whistle Blower Submission', 'email.emailSample'));
        } 
        echo 'Test Sending Email';
        //return $send;
        // $data = new Carbon(strtotime('22/09/2023'));
        // dd($data->format('Y-m-d'));
    }


    public function adminDashboard(Request $request){
        $list = [];
        date_default_timezone_set('Asia/Kuala_Lumpur');
        //$courses = Courses::get();

        $currentDate = Carbon::now()->format('Y-m-d');
        $coursesList = Courses::select([
            'courses.*'
            ,
            DB::raw('(CASE  WHEN status = 1 THEN "Approved"
            WHEN status = 2 THEN "Rejected"
            ELSE "Pending"
            END ) AS status_name')                      
        ])
                        ->whereDate('created_at','=',$currentDate)->orWhereDate('updated_at','=',$currentDate)->get();
        $tpList =  TrainingProvider::whereDate('created_at','=',$currentDate)->get();
        $interestList = Applicant::leftJoin('courses', 'courses.id', '=', 'applicants.course_id')->whereDate('applicants.created_at','=',$currentDate)->select('applicants.*','courses.name as courseName','courses.tp_mycoid as tpMycoid','courses.training_fee as fee','courses.training_discount_rate as discountRate','courses.discounted_training_fee as discountedFee','courses.status')->get();  


        $employerList = Employer::whereDate('created_at','=',$currentDate)->get();

        $totalCourses = Courses::count();
        $totalTp = TrainingProvider::count();
        $totalTpLogged = Users::where('type','tp')->count();
        $totalInterested = Applicant::count();
        $totalEmployers2 = Employer::count();
        $totalEmployersLogged = Users::where('type','employer')->count();
        $app = DB::table('applicants as app')
                ->select("mycoid","id")
                ->groupBy('mycoid');

        $totalEmployers = DB::table('applicants as app1')
            ->select(['app1.id','app1.email','app1.name',"app1.mycoid","app1.contact","app1.business_forte"])
            ->joinSub($app, 'app', function ($join)
            {
                $join->on("app.id", '=', 'app1.id');
            })->count();

        $todayCourses = Courses::whereDate('created_at','=',$currentDate)->count();
        $todayTp = TrainingProvider::whereDate('created_at','=',$currentDate)->count();
        $todayInterested = Applicant::whereDate('created_at','=',$currentDate)->count();
        $todayEmployers = Employer::whereDate('created_at','=',$currentDate)->count();

        $enrollment = Applicant::leftJoin('courses', 'courses.id', '=', 'applicants.course_id')->select(DB::raw("SUM(applicants.no_of_enrollment) as Total_Enrollment"))->get(); 
        $enrollmentToday = Applicant::leftJoin('courses', 'courses.id', '=', 'applicants.course_id')->whereDate('applicants.created_at','=',$currentDate)->select(DB::raw("SUM(applicants.no_of_enrollment) as Total_Enrollment"))->get(); 
        // $enrollmentListToday = Applicant::leftJoin('courses', 'courses.id', '=', 'applicants.course_id')->whereDate('courses.created_at','=',$currentDate)->select(DB::raw("applicants.no_of_enrollment as Total_Enrollment"))->get(); 

        $enrollmentValue = Applicant::leftJoin('courses', 'courses.id', '=', 'applicants.course_id')->select(DB::raw("SUM(applicants.no_of_enrollment*courses.discounted_training_fee) as Total_EnrollmentValue"))->get();
        $enrollmentValueToday = Applicant::leftJoin('courses', 'courses.id', '=', 'applicants.course_id')->whereDate('applicants.created_at','=',$currentDate)->select(DB::raw("SUM(applicants.no_of_enrollment*courses.discounted_training_fee) as Total_EnrollmentValue"))->get(); 


        $enrollmentValueListToday = Applicant::leftJoin('courses', 'courses.id', '=', 'applicants.course_id')->whereDate('applicants.created_at','=',$currentDate)->select('applicants.*','courses.name as courseName','courses.tp_mycoid as tpMycoid','courses.training_fee as fee','courses.training_discount_rate as discountRate','courses.discounted_training_fee as discountedFee','courses.status',DB::raw("applicants.no_of_enrollment*courses.discounted_training_fee as Total_EnrollmentValue"))->get();

        return view('users.officer.admin-dashboard', compact('list','totalCourses','totalTp','totalInterested','totalEmployers','todayCourses','todayTp','todayInterested','todayEmployers','coursesList','tpList','interestList','employerList','enrollment','enrollmentToday','enrollmentValue','enrollmentValueToday','enrollmentValueListToday','totalEmployersLogged','totalTpLogged'));    
        
    }


    public function createForm(Request $request) {
        $tpmycoid = $request->get('tpmycoid'); 
        //dd($tpmycoid);
        $tpInfo = [];
        if(!empty($tpmycoid)){
            $tpInfo = TrainingProvider::where('tp_mycoid','=',$tpmycoid)->first();
        }
        return view('users.officer.new-course', compact('tpInfo'));
    }   


    public function create(Request $request){
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $postedDt = $request->all();
        $userId = $request->get('user_id');
        $tpMycoid = $request->get('tp_mycoid');
        $tpId = $request->get('tp_id');
        $tpName = $request->get('tp_name');
        $tpReqEmail = $request->get('tp_email');
        $tpPhone = $request->get('tp_contact_no');
        $courseSkillArea = $request->get('tp_skill_area');
        $courseName = $request->get('tp_course_name');
        $coursePrice = $request->get('tp_course_price');
        $courseCertificate = $request->get('tp_training_certificate');
        $courseDiscountRate = $request->get('tp_course_discount_rate');    
        $courseAfterDiscountPrice = $request->get('tp_course_discounted_price_value');

        $user = Auth::user();
        $userType = $user->type;
        $postedDt['updated_by'] = $user->email;
        $postedDt['user_type'] = $user->type; 

        $tp = TrainingProvider::where('tp_mycoid',$tpMycoid)->first();
        $tpEmail = Arr::get($tp,'poc_email','');
        $postedDt['tp_email_db'] = $tpEmail; 
        $postedDt['password'] = 123456;
        $adminList = ['admin','SuperAdmin'];

        if(empty($tp)){
            //add new provider
            $addtp = new TrainingProvider();
            $addedTp = $addtp->create($postedDt);
        
            $userDt = [
                'username'=>$tpReqEmail,
                'password'=>123456,
                'refid'=>$addedTp,
                'email'=>$tpReqEmail,
                'fullname'=>$tpName,
                'type'=>'tp'
            ];

            $users = new Users();
            $users->create($userDt);
            if(!$users){
                return response()->json(['status'=>0,'data'=>[]]);
            }
            
        }

        $courses = [];
        for($i =0; $i<count($courseSkillArea); $i++){
            $obj = [
                'tp_mycoid'=>$tpMycoid,
                'training_provider'=>$tpId,
                'name'=>$courseName[$i],
                'training_skim'=>$courseSkillArea[$i],
                'training_certification'=>$courseCertificate[$i],
                'training_fee'=>$coursePrice[$i],
                'training_discount_rate'=>$courseDiscountRate[$i],
                'discounted_training_fee'=>$courseAfterDiscountPrice[$i],
                'pic_name'=>$tpName,
                'pic_email'=>$tpEmail,
                'pic_phone'=>$tpPhone,
                'status'=>in_array($userType,$adminList) ? 1 : 0,
                'created_by'=>$userId,
                'created_at'=>Carbon::now()->format('Y-m-d H:i:s')
            ];
            array_push($courses, $obj);
        }

        $data = [
            'url'=>route('tpCourseList',['devOfficerId'=>$userId])
        ];

        try{
            $bizMatchCourse = new Courses();
            $saveBizMatchCourse = $bizMatchCourse->bulkCreate($courses);
            
            if($saveBizMatchCourse){

                $bizMatchMailService = new BizMatchEmailService();
                $bizMatchMailService->toOfficerAfterAddCourse($postedDt);

                return response()->json(['status'=>1,'data'=>[$data]]);
            }else{
                return response()->json(['status'=>0,'data'=>[]]);
            }



        }catch(Exception $e){
            //throw $e;
            return response()->json(['status'=>0,'data'=>[]]);
        }
    
    }


    public function updateCourse(Request $request){
          // array:10 [
        //     "_token" => "w3zGIA6dEbcuhwXp37IbHuKb3ExCWOWOrcG2i4iO"
        //     "course_id" => "15"
        //     "course_tp_mycoid" => "PPM0011415071967"
        //     "course_name" => null
        //     "tp_skill_area" => "Skim PPM0011415071967 -103- 1"
        //     "tp_course_name" => "PPM0011415071967 -103-Course1"
        //     "tp_course_price" => "126"
        //     "tp_course_discount_rate" => "23"
        //     "tp_course_discounted_price_value" => "28.98"
        //     "current_status" => "1"
        //   ]
        $allReq = $request->all();
        $courses = new Courses();
        $user = Auth::user();
        $updatedBy = $user->email;
        $allReq['updated_by'] = $updatedBy;
        $allReq['user_id'] = $user->id;
        $allReq['user_type'] = $user->type;
        $allReq['status'] =  $user->type == 'tp' ? 0 : 1; 
       
        $updateCourse = $courses->updateCourse($allReq);

        if($updateCourse){
            $bizMatchMailService = new BizMatchEmailService();
            $bizMatchMailService->toOfficerAfterUpdateDetailCourse($allReq);

            return response()->json(['status'=>1,'data'=>[]]);
        }else{
            return response()->json(['status'=>0,'data'=>[]]);
        }
    }

   
    // public function trackList(Request $request) {
    //     $list = [];
    //     $search = '';
    //     $status = 'all';
    //     $id = $request->get('id');
    //     //$decode_id = urlencode(base64_encode($id));
    //     $decode_id = urldecode(base64_decode($id));
    //     return view('users.public.track-list', compact('list','search','status'));
    // }

   
    public function tpCourseList(Request $request, $devOfficerId){
        $allReq = $request->all();
        $sortCol = Arr::get($allReq ,'sortCol', 'courses.created_at');
        $sortOrder = Arr::get($allReq, 'sortOrder','desc');
        $user = Auth::user();
        $userId = $user->id;
        $userType = $user->type; 
        $search = ''; $status = 'all'; $percentageVal = 'all';
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $currentOfficer = Session::get('username');
    
        if($userType == 'tp'){
            $tp = DB::table('users')->leftjoin('training_provider as tp', 'tp.id', '=', 'users.ref_id')->where('users.id','=',$userId)->first();
            $tpmycoid = $tp->tp_mycoid;
        }else{
            $tpmycoid = '';
        }
        // dd($tpmycoid);
        if($userType == 'employer'){
            $userRefId = $user->ref_id;
            $empInterestCourse = DB::table('applicants')->where('id','=',$userRefId)->get();
            $courseIds = $empInterestCourse->pluck('course_id')->toArray();           
        }else{
            $courseIds = [];
        }

        $list = DB::table('courses')
                ->leftJoin('users as usr','usr.id','=','courses.created_by')
                //->where('status','!=',3)
                ->when( $userType == 'employer', function($query) use($tpmycoid){
                    return $query->where('status','=',1);           
                 })
                ->when( $tpmycoid != '', function($query) use($tpmycoid){
                   return $query->where('tp_mycoid','=',$tpmycoid);
                })
                ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
                    return $query->orderBy($sortCol, $sortOrder);
                })
                ->select(['courses.*','usr.email as usremail'])
                ->orderBy('courses.created_at','desc')
                ->paginate(10);

        $percentages = $list->pluck('training_discount_rate')->toArray();
        $percentageList = array_unique($percentages);
    
        //dd(DB::getQueryLog(), $list);
        $officers = config('custom.wb.officer.users');
        $statusList = config('custom.wb.officer.status');
        $search = NULL;
        $pageType = $userType;
        $source = 'web';
        return view('users.officer.tp-course-list', compact('list','statusList','search','status','pageType','courseIds','source','percentageList','percentageVal'));
    }

    public function tpCourseListSearch(Request $request){
        $allReq = $request->all();
        $user = Auth::user();
        $usersId = $user->id;
        $userType = $user->type; 
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $sortCol = Arr::get($allReq ,'sortCol', 'courses.created_at');
        $sortOrder = Arr::get($allReq, 'sortOrder','desc');
        $percentageVal = $request->get('percentage');
        //dd($percentage);
        $searchVal = $request->get('search');
        $status = $request->get('status');
        $userId = $request->get('devOfficerId'); 
        $currentOfficer = Session::get('username');

        $search = str_replace(',', '', $searchVal);
        DB::enableQueryLog();

  
        if($userType == 'tp'){
            $tp = DB::table('users')->leftjoin('training_provider as tp', 'tp.id', '=', 'users.ref_id')->where('users.id','=',$usersId)->first();
            $tpmycoid = $tp->tp_mycoid;
        }else{
            $tpmycoid = '';
        }

        if($userType == 'employer'){
            $userRefId = $user->ref_id;
            $empInterestCourse = DB::table('applicants')->where('id','=',$userRefId)->get();
            $courseIds = $empInterestCourse->pluck('course_id')->toArray();           
        }else{
            $courseIds = [];
        }

        $list = DB::table('courses')
        ->leftJoin('users as usr','usr.id','=','courses.course_approved_by')
        ->when( $tpmycoid != '', function($query) use($tpmycoid){
           return $query->where('tp_mycoid','=',$tpmycoid);
        })
        //->where('status','!=',3)
        ->when( $userType == 'employer', function($query) use($tpmycoid){
            return $query->where('status','=',1);
         })
        ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
            return $query->orderBy($sortCol, $sortOrder);
        })
        ->when( $status != 'all', function($query) use($status){
                return $query->where('status','=', $status);
        })
        ->when( $percentageVal != 'all', function($query) use($percentageVal){
            return $query->where('training_discount_rate','=',$percentageVal);
         })
        ->where(function ($query) use($search) {
            return $query->where('tp_mycoid','like','%'.$search.'%')
                         ->orWhere('courses.name','like','%'.$search.'%')
                         ->orWhere('usr.email','like','%'.$search.'%')
                         ->orWhere('courses.reject_reason','like','%'.$search.'%')
                         ->orWhere('courses.training_fee','like','%'.$search.'%')
                         ->orWhere('courses.training_discount_rate','like','%'.$search.'%')
                         ->orWhere('courses.discounted_training_fee','like','%'.$search.'%')
                         ->orWhere('courses.training_skim','like','%'.$search.'%');
        })
        ->orderBy('created_at','desc')
        ->select(['courses.*','usr.email as usremail'])
        ->paginate(10);
 
        $percentages = $list->pluck('training_discount_rate')->toArray();
        $percentageList = array_unique($percentages);
    
        $currentOfficer = Session::get('username');
        $officers = config('custom.wb.officer.users');
        $statusList = config('custom.wb.officer.status');
        $pageType = $userType;
        $source = 'web';

        return view('users.officer.tp-course-list', compact('list','statusList','search','status','pageType','courseIds','source','percentageList','percentageVal'));
    }


    public function approve(Request $request){
        $allReq = $request->all();
        $user = Auth::user();
        $updatedBy = $user->email;
       
        $courses = new Courses();
        $courses->approve($allReq);

        $allReq['status_previous'] = 'Pending';
        $allReq['status'] = 'Approved';
        $allReq['user_id'] = $user->id;
        $allReq['updated_by'] = $updatedBy;
        $allReq['user_type'] = $user->type;

        if($courses){
             $bizMatchMailService = new BizMatchEmailService();
             $bizMatchMailService->toOfficerAfterUpdateStatusCourse($allReq);
            return response()->json(['data' => 1]);
        }else{
            return response()->json(['data' => 0]);
        }
    }


    public function reject(Request $request){
        $allReq = $request->all();
        $user = Auth::user();
        $updatedBy = $user->email;
        $courses = new Courses();
        $courses->reject($allReq);

        $allReq['status_previous'] = 'Pending';
        $allReq['status'] = 'Reject';
        $allReq['user_id'] = $user->id;
        $allReq['updated_by'] = $updatedBy;
        $allReq['user_type'] = $user->type;

        if($courses){
             $bizMatchMailService = new BizMatchEmailService();
             $bizMatchMailService->toOfficerAfterUpdateStatusCourse($allReq);
            return response()->json(['data' => 1]);
        }else{
            return response()->json(['data' => 0]);
        }
    }

    public function remove(Request $request){
        $allReq = $request->all();
        $user = Auth::user();
        $updatedBy = $user->email;
        $courses = new Courses();
        $courses->remove($allReq);
        $allReq['updated_by'] = $updatedBy;
        $allReq['user_id'] = $user->id;
        $allReq['user_type'] = $user->type;
        if($courses){
            $bizMatchMailService = new BizMatchEmailService();
            $bizMatchMailService->toOfficerAfterDeleteCourse($allReq);
            return response()->json(['data' => 1]);
        }else{
            return response()->json(['data' => 0]);
        }   
    }


    public function publicCourseListApp(Request $request){
        $allReq = $request->all();
        $sortCol = Arr::get($allReq ,'sortCol');
        $sortOrder = Arr::get($allReq, 'sortOrder');

        // $user = Auth::user();
        // $userId = $user->id;
        // $userType = $user->type; 
        $search = Arr::get($allReq, 'search', ''); 
        $status = 'all'; $percentageVal = 'all';
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $currentOfficer = Session::get('username');

 

        $list = DB::table('courses')
                ->leftJoin('users as usr','usr.id','=','courses.created_by')
                ->leftJoin('training_provider as tp','tp.id','=','courses.training_provider')
                ->where(function ($query) use($search) {
                    return $query->where('courses.status','!=','3')
                                ->where('courses.status','=','1');
                        
                })
                ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
                    return $query->orderBy($sortCol, $sortOrder);
                })
                ->select(['courses.*','usr.email as usremail','tp.poc_name as tp_name'])
                ->orderBy('courses.created_at','desc')
                ->paginate(10);

        $percentages = $list->pluck('training_discount_rate')->toArray();
        $percentageList = array_unique($percentages);
        
        $statusList = config('custom.wb.officer.status');
        $pageType = 'public';
        $source = 'app';
        return view('users.public.tp-course-list-public-app', compact('list','statusList','search','status','pageType','source','percentageList','percentageVal'));
    }


    public function publicCourseListAppSearch(Request $request){
        $allReq = $request->all();
        $sortCol = Arr::get($allReq ,'sortCol');
        $sortOrder = Arr::get($allReq, 'sortOrder');
        // $user = Auth::user();
        // $userId = $user->id;
        // $userType = $user->type; 
        $searchVal = Arr::get($allReq, 'search', ''); 
        $status = 'all';
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $currentOfficer = Session::get('username');
        $percentageVal = $request->get('percentage');
        $search = str_replace(',', '', $searchVal);

        $list = DB::table('courses')
                ->leftJoin('users as usr','usr.id','=','courses.created_by')
                ->leftJoin('training_provider as tp','tp.id','=','courses.training_provider')
                ->where(function ($query) use($search) {
                    return $query->where('courses.status','!=','3')
                                ->where('courses.status','=','1');
                            
                })
                ->where(function ($query) use($search) {
                    return $query->where('courses.tp_mycoid','like','%'.$search.'%')
                                 ->orWhere('courses.name','like','%'.$search.'%')
                                 ->orWhere('usr.email','like','%'.$search.'%')
                                 ->orWhere('courses.reject_reason','like','%'.$search.'%')
                                 ->orWhere('courses.training_fee','like','%'.$search.'%')
                                 ->orWhere('courses.training_discount_rate','like','%'.$search.'%')
                                 ->orWhere('courses.discounted_training_fee','like','%'.$search.'%')
                                 ->orWhere('courses.training_skim','like','%'.$search.'%')
                                 ->orWhere('tp.poc_name','like','%'.$search.'%');
                })
                ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
                    return $query->orderBy($sortCol, $sortOrder);
                })
                ->when( $percentageVal != 'all', function($query) use($percentageVal){
                    return $query->where('training_discount_rate','=',$percentageVal);
                 })
                ->select(['courses.*','usr.email as usremail','tp.poc_name as tp_name'])
                ->orderBy('courses.created_at','desc')
                ->paginate(10);

        $percentages = $list->pluck('training_discount_rate')->toArray();
        $percentageList = array_unique($percentages);

        $statusList = config('custom.wb.officer.status');
        $pageType = 'public';
        $source = 'app';
        return view('users.public.tp-course-list-public-app', compact('list','statusList','search','status','pageType','source','percentageList','percentageVal'));
    }

    public function publicCourseList(Request $request){
        $allReq = $request->all();
        $sortCol = Arr::get($allReq ,'sortCol');
        $sortOrder = Arr::get($allReq, 'sortOrder');
        // $user = Auth::user();
        // $userId = $user->id;
        // $userType = $user->type; 
        $search = ''; $status = 'all'; $percentageVal = 'all';
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $currentOfficer = Session::get('username');

        $list = DB::table('courses')
                ->leftJoin('users as usr','usr.id','=','courses.created_by')
                ->leftJoin('training_provider as tp','tp.id','=','courses.training_provider')
                ->where(function ($query) use($search) {
                    return $query->where('courses.status','!=','3')
                                ->where('courses.status','=','1');
                            
                })
                ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
                    return $query->orderBy($sortCol, $sortOrder);
                })
                ->select(['courses.*','usr.email as usremail','tp.poc_name as tp_name'])
                ->orderBy('courses.created_at','desc')
                ->paginate(10);

        $percentages = $list->pluck('training_discount_rate')->toArray();
        $percentageList = array_unique($percentages);
        
        $statusList = config('custom.wb.officer.status');
        $pageType = 'public';
        $source = 'web';
        return view('users.public.tp-course-list-public', compact('list','statusList','search','status','pageType','source','percentageList','percentageVal'));
    }


    public function publicCourseListSearch(Request $request){
        $allReq = $request->all();
        $sortCol = Arr::get($allReq ,'sortCol');
        $sortOrder = Arr::get($allReq, 'sortOrder');
        // $user = Auth::user();
        // $userId = $user->id;
        // $userType = $user->type; 
        $searchVal = $request->get('search');
        $status = $request->get('status');
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $currentOfficer = Session::get('username');
        $percentageVal = $request->get('percentage');

        $search = str_replace(',', '', $searchVal);

        DB::enableQueryLog();
        $list = DB::table('courses')
                ->leftJoin('users as usr','usr.id','=','courses.created_by')
                ->leftJoin('training_provider as tp','tp.id','=','courses.training_provider')
                ->where(function ($query) use($search) {
                    return $query->where('courses.status','!=','3')
                                ->where('courses.status','=','1');
                            
                })
                ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
                    return $query->orderBy($sortCol, $sortOrder);
                })
                ->where(function ($query) use($search) {
                    return $query->where('courses.tp_mycoid','like','%'.$search.'%')
                                 ->orWhere('courses.name','like','%'.$search.'%')
                                 ->orWhere('courses.training_fee','like','%'.$search.'%')
                                 ->orWhere('courses.training_discount_rate','like','%'.$search.'%')
                                 ->orWhere('courses.discounted_training_fee','like','%'.$search.'%')
                                 ->orWhere('courses.training_skim','like','%'.$search.'%')
                                 ->orWhere('tp.poc_name','like','%'.$search.'%');
                })
                ->when( $percentageVal != 'all', function($query) use($percentageVal){
                    return $query->where('training_discount_rate','=',$percentageVal);
                 })
                ->select(['courses.*','usr.email as usremail','tp.poc_name as tp_name'])
                ->orderBy('courses.created_at','desc')
                ->paginate(10);

        $percentages = $list->pluck('training_discount_rate')->toArray();
        $percentageList = array_unique($percentages);
        //dd(DB::getQueryLog(), $list);
        
        $statusList = config('custom.wb.officer.status');
        $pageType = 'public';
        $source = 'web';
        return view('users.public.tp-course-list-public', compact('list','statusList','search','status','pageType','source','percentageList','percentageVal'));
    }


    public function processInterest(Request $request){ 
        $allReq = $request->all();
        $applicant = new Applicant();
        $applicant->create($allReq);
        

        if($applicant){
            $checkIfExist = Users::where('email','=',Arr::get($allReq, 'employer_email',''))->first();
            if(empty($checkIfExist)){
              
                $registerEmployer = [
                    'emp_mycoid' => Arr::get($allReq,'employer_mycoid',''),
                    'emp_name' => Arr::get($allReq,'employer_name',''),
                    'emp_email' => Arr::get($allReq,'employer_email',''),
                    'emp_contact' => Arr::get($allReq,'employer_contact',''),
                    'emp_status' => 1,
                ];

                $employer = new Employer();
                $refId = $employer->create($registerEmployer);

                $registerApplicantAsUser = [
                    'type'=>'employer',
                    'refid'=>$refId,
                    'fullname'=>Arr::get($allReq,'employer_name',''),
                    'username'=>Arr::get($allReq,'employer_email',''),
                    'email'=>Arr::get($allReq,'employer_email',''),
                    'password'=>123456,
                ];
                $users = new Users();
                $users->create($registerApplicantAsUser);

                if($users){
                    $bizMatchMailService = new BizMatchEmailService();
                    $bizMatchMailService->sendInterestAfterSubmit($allReq);
                    return response()->json(['data' => 1]);  
                }else{
                    return response()->json(['data' => 0]);
                }
            }else{
                $bizMatchMailService = new BizMatchEmailService();
                $bizMatchMailService->sendInterestAfterSubmit($allReq);
                return response()->json(['data' => 1]);
            }

          
        }else{
            return response()->json(['data' => 0]);
        }
    }


    public function interestList(Request $request, $devOfficerId){
        $allReq = $request->all();
        $sortCol = Arr::get($allReq ,'sortCol');
        $sortOrder = Arr::get($allReq, 'sortOrder');
        $user = Auth::user();
        $userId = $user->id;
        $userType = $user->type; 
        $userEmail = $user->email;
        $userRefId = $user->ref_id;
        $search = ''; $status = 'all';
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $currentOfficer = Session::get('username');
        
        if($userType == 'tp'){
            $tp = DB::table('users')->leftjoin('training_provider as tp', 'tp.id', '=', 'users.ref_id')->where('users.id','=',$userId)->first();
            $tpmycoid = $tp->tp_mycoid;
        }else{
            $tpmycoid = '';
        }

        $list = DB::table('applicants')
                ->leftJoin('courses as cs','cs.id','=','applicants.course_id')
                ->when( $tpmycoid != '', function($query) use($tpmycoid){
                    return $query->where('cs.tp_mycoid','=',$tpmycoid);
                })
                ->when( $userType == 'employer', function($query) use($userEmail){
                    return $query->where('applicants.email','=',$userEmail);
                 })
                ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
                    return $query->orderBy($sortCol, $sortOrder);
                })
                ->select(['applicants.*','cs.name as courseName','cs.tp_mycoid as tpMycoid', 'cs.training_fee as fee','cs.training_discount_rate as discountRate','cs.discounted_training_fee as discountedFee'])
                ->orderBy('applicants.created_at','desc')
                ->paginate(10);
        
        $statusList = config('custom.wb.officer.statusinterest');
        $pageType = $userType;
       
        return view('users.officer.interest-list', compact('list','statusList','search','status','pageType'));
    }


    public function interestListSearch(Request $request, $devOfficerId){
        $allReq = $request->all();
        $sortCol = Arr::get($allReq ,'sortCol');
        $sortOrder = Arr::get($allReq, 'sortOrder');
        $user = Auth::user();
        $userId = $user->id;
        $userType = $user->type; 
        $userEmail = $user->email;
        $search = Arr::get($allReq, 'search');
        $status = Arr::get($allReq, 'status');
        $pagePerCount = config('custom.wb.officer.setting.page.count');
        $currentOfficer = Session::get('username');
        
        if($userType == 'tp'){
            $tp = DB::table('users')->leftjoin('training_provider as tp', 'tp.id', '=', 'users.ref_id')->where('users.id','=',$userId)->first();
            $tpmycoid = $tp->tp_mycoid;
        }else{
            $tpmycoid = '';
        }

        $list = DB::table('applicants')
                ->leftJoin('courses as cs','cs.id','=','applicants.course_id')
                ->when( $tpmycoid != '', function($query) use($tpmycoid){
                    return $query->where('cs.tp_mycoid','=',$tpmycoid);
                 })
                 ->when( $userType == 'employer', function($query) use($userEmail){
                    return $query->where('applicants.email','=',$userEmail);
                 })
                ->where(function ($query) use($search) {
                    return $query->where('mycoid','like','%'.$search.'%')
                                 ->orWhere('applicants.name','like','%'.$search.'%')
                                 ->orWhere('email','like','%'.$search.'%')
                                 ->orWhere('business_forte','like','%'.$search.'%')
                                 ->orWhere('contact','like','%'.$search.'%')
                                 ->orWhere('cs.name','like','%'.$search.'%')
                                 ->orWhere('cs.tp_mycoid','like','%'.$search.'%');
                })
                ->when( $status != 'all', function($query) use($status){
                    return $query->where('applicants.current_status','=', $status);
                })
                ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
                    return $query->orderBy($sortCol, $sortOrder);
                })
                //->select(['applicants.*','cs.name as courseName','cs.tp_mycoid as tpMycoid'])
                ->select(['applicants.*','cs.name as courseName','cs.tp_mycoid as tpMycoid', 'cs.training_fee as fee','cs.training_discount_rate as discountRate','cs.discounted_training_fee as discountedFee'])
                ->orderBy('applicants.created_at','desc')
                ->paginate(10);
        
        $statusList = config('custom.wb.officer.statusinterest');
        $pageType = $userType;
        return view('users.officer.interest-list', compact('list','statusList','search','status','pageType'));
    }

    public function approveInterest(Request $request){
        $user = Auth::user();
        $allReq = $request->all();
        $applicant = new Applicant();
        $applicant->approve($allReq);
       
        $allReq['status_prevoius'] = 'Pending';
        $allReq['status'] = 'Approved';
        $allReq['updated_by'] = $user->email;

        if($applicant){
            $appId = Arr::get($allReq,'id','');
            $applicantInfo = $applicant->find($appId);
            $registerApplicantAsUser = [
                'type'=>'employer',
                'refid'=>$appId,
                'fullname'=>Arr::get($applicantInfo,'name',''),
                'username'=>Arr::get($applicantInfo,'email',''),
                'email'=>Arr::get($applicantInfo,'email',''),
                'password'=>123456,
            ];

            $checkIfExist = Users::where('ref_id','=',$appId)->first();

            $bizMatchMailService = new BizMatchEmailService();
            $bizMatchMailService->sendInterestAfterUpdateStatus($allReq);

            if(empty($checkIfExist)){
                $users = new Users();
                $users->create($registerApplicantAsUser);

                if($users){
                    return response()->json(['data' => 1]);
                }else{
                    return response()->json(['data' => 0]);
                }
            }else{
                return response()->json(['data' => 1]);
            }
           
        }else{
            return response()->json(['data' => 0]);
        }
    }


    public function rejectInterest(Request $request){
        $allReq = $request->all();
        $user = Auth::user();
        $allReq = $request->all();
        $applicant = new Applicant();
        $applicant->reject($allReq);

        $allReq['status_prevoius'] = 'Pending';
        $allReq['status'] = 'Reject';
        $allReq['updated_by'] = $user->email;
        $bizMatchMailService = new BizMatchEmailService();
        $bizMatchMailService->sendInterestAfterUpdateStatus($allReq);

        if($applicant){
            return response()->json(['data' => 1]);
        }else{
            return response()->json(['data' => 0]);
        }
        
    }


    public function tpList(Request $request){
        $allReq = $request->all();
        $search = Arr::get($allReq,'search', '');
        $status = Arr::get($allReq,'status', '');
        $user = Auth::user();
        $userType = $user->type;
        $list = TrainingProvider::orderBy('created_at','desc')->paginate(10);
    
        $statusList = config('custom.wb.officer.statusinterest');
        $pageType = $userType;
        return view('users.officer.tp-list', compact('list','statusList','search','status','pageType'));
    }



    public function tpListSearch(Request $request){
        
        $allReq = $request->all();
        $search = Arr::get($allReq,'search', '');
        $status = Arr::get($allReq,'status', '');
        $sortCol = Arr::get($allReq,'sortCol', '');
        $sortOrder = Arr::get($allReq,'sortOrder', '');
        
        $user = Auth::user();
        $userType = $user->type;
        $list =  TrainingProvider::where(function ($query) use($search) {
            return $query->where('tp_mycoid','like','%'.$search.'%')
                         ->orWhere('poc_name','like','%'.$search.'%')
                         ->orWhere('poc_email','like','%'.$search.'%')
                         ->orWhere('poc_phone','like','%'.$search.'%');
        })
        // ->when( $status != 'all', function($query) use($status){
        //     return $query->where('status','=', $status);
        // })
        ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
            return $query->orderBy($sortCol, $sortOrder);
        })
        //->select(['applicants.*','cs.name as courseName','cs.tp_mycoid as tpMycoid'])
        // ->select(['applicants.*','cs.name as courseName','cs.tp_mycoid as tpMycoid', 'cs.training_fee as fee','cs.training_discount_rate as discountRate','cs.discounted_training_fee as discountedFee'])
        ->orderBy('created_at','desc')
        ->paginate(10);

        $statusList = config('custom.wb.officer.statusinterest');
        $pageType = $userType;
        return view('users.officer.tp-list', compact('list','statusList','search','status','pageType'));

    }



    public function employerList(Request $request){
        
        $allReq = $request->all();
        $search = Arr::get($allReq,'search', '');
        $status = Arr::get($allReq,'status', '');
        $sortCol = Arr::get($allReq,'sortCol', '');
        $sortOrder = Arr::get($allReq,'sortOrder', '');
        $user = Auth::user();
        $userType = $user->type;
        /**
         * 1st version
         */
        // $uniqueList = Applicant::all()->unique('email');
        // $ids = $uniqueList->pluck('id')->toArray();

        // $list = DB::table('applicants')
        // ->when( $status != 'all', function($query) use($status){
        //     return $query->where('current_status','=', $status);
        // })
        //     ->whereIn('id', function($query) {
        //     $query->unique('email')->pluck('id')->toArray();
        // })
        // ->whereIn('id',$ids)
        // ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
        //     return $query->orderBy($sortCol, $sortOrder);
        // })
        // ->paginate(10);
          
          
        /**workable but need disabled strict mode */
        // $list = Applicant::select('*')
        //     ->groupBy('email')
        //     ->when( $sortCol != '', function($query) use($status, $sortOrder, $sortCol){
        //         return $query->orderBy($sortCol, $sortOrder);
        //     })
        //     ->paginate(10);

        $app = DB::table('applicants as app')
                ->select("email","id")
                ->groupBy('email');

        $list = DB::table('applicants as app1')
            ->select(['app1.id','app1.email','app1.name',"app1.mycoid","app1.contact","app1.business_forte"])
            ->joinSub($app, 'app', function ($join)
            {
                $join->on("app.id", '=', 'app1.id');
            })
            //->orderBy('app.email')
            ->paginate(10);

        // $data = $ad->getcodes()->get()->unique('email');
        // $list = [];
        $statusList = config('custom.wb.officer.statusinterest');
        $pageType = $userType;
        return view('users.officer.employer-list', compact('list','statusList','search','status','pageType'));

    }


    public function employerListSearch(Request $request){
        $allReq = $request->all();
        $search = Arr::get($allReq,'search', '');
        $status = Arr::get($allReq,'status', '');
        $sortCol = Arr::get($allReq,'sortCol', '');
        $sortOrder = Arr::get($allReq,'sortOrder', '');
        $allReq = $request->all();
        $search = Arr::get($allReq,'search', '');
        $status = Arr::get($allReq,'status', '');
        $sortCol = Arr::get($allReq,'sortCol', '');
        $sortOrder = Arr::get($allReq,'sortOrder', '');
        $user = Auth::user();
        $userType = $user->type;

        $app = DB::table('applicants as app')
                ->select("email","id")
                ->groupBy('email');

        $list = DB::table('applicants as app1')
            ->select(['app1.id','app1.email','app1.name',"app1.mycoid","app1.contact","app1.business_forte"])
            ->joinSub($app, 'app', function ($join)
            {
                $join->on("app.id", '=', 'app1.id');
            })
            ->where(function ($query) use($search) {
                    return $query->where('app1.mycoid','like','%'.$search.'%')
                                ->orWhere('app1.name','like','%'.$search.'%')
                                ->orWhere('app1.email','like','%'.$search.'%')
                                ->orWhere('app1.contact','like','%'.$search.'%');
            })
            //->orderBy('app1.email')
            ->paginate(10);
        // $data = $ad->getcodes()->get()->unique('email');
        // $list = [];
        $statusList = config('custom.wb.officer.statusinterest');
        $pageType = $userType;
        return view('users.officer.employer-list', compact('list','statusList','search','status','pageType'));

    }
    /**
     * Assign
     */
    // public function processAssign(Request $request){
    //     date_default_timezone_set('Asia/Kuala_Lumpur');
    //     $current_datetime = Carbon::now()->format('Y-m-d H:i:s'); 
    //     $req_dt = $request->all();
    //     $profileId = Arr::get($req_dt, 'profileId');
    //     $officerId = Arr::get($req_dt, 'officerId');
    //     $complaintId = Arr::get($req_dt, 'complaintId');
    //     $suspectId = Arr::get($req_dt, 'suspectId');
    //     $suspctIds = explode(",",$suspectId);
    //     $remark = Arr::get($req_dt, 'remark');
    //     $supporting_docs = Arr::get($req_dt, 'supporting_docs');
    //     $assign_status = Arr::get($req_dt, 'assignStatus',2);
    //     $current_status = Arr::get($req_dt,'currentStatus','');
    //     $statusList = config('custom.wb.officer.status'); 

    //     $req_dt['remark'] = 'pick up case';
    //     $req_dt['previous_status_name_bi'] = $statusList[intval($current_status)]['name_email_bi']; 
    //     $req_dt['previous_status_name_bm'] = $statusList[intval($current_status)]['name_email_bm'];
    //     $req_dt['status_name_bi'] = $statusList[intval($assign_status)]['name_email_bi'];
    //     $req_dt['status_name_bm'] = $statusList[intval($assign_status)]['name_email_bm'];
        
    //     $format_suspect = [];
    //     foreach($suspctIds as $sus){
    //         array_push($format_suspect, intval($sus));
    //     }
    //     $encodeSuspectIds = json_encode($format_suspect);
       
    //     $WBProfileMst = WBProfileMst::where('ProfileID','=',$profileId)->update([
    //         'Status' => $assign_status,
    //         'UpdateBy'=> Arr::get($req_dt, 'officerId', ''),
    //         'UpdateDate' => $current_datetime,
    //         'PICOfficer' => $officerId
    //     ]);

    //     $WBActivityMst = new WBActivityMst();
    //     $activity_dt = [
    //         'remark'=>'pick up case',
    //         'activity_status'=>$assign_status,
    //         'activity_assignto'=>$officerId,
    //         'activity_upd_by'=>$officerId,
    //     ];
    //     $save_activity_id = $WBActivityMst->create($activity_dt, $complaintId, $profileId, $encodeSuspectIds);
        
    //     $WBSendEmail = new WhistleBlowerEmailService();
    //     $WBSendEmail->sendEmailNotiAfterUpdateStatus($req_dt);

    //     if($WBProfileMst){
    //         return response()->json(['data' => 1]);
    //     }else{
    //         return response()->json(['data' => 0]);
    //     }
        
    // }

    // /**
    //  * update status
    //  */
    // public function processUpdate(Request $request){
    //     date_default_timezone_set('Asia/Kuala_Lumpur');
    //     $current_datetime = Carbon::now()->format('Y-m-d H:i:s'); 
    //     $req_dt = $request->all();
    //     $applicant_name = Arr::get($req_dt,'applicantName');
    //     $profileId = Arr::get($req_dt, 'profileId');
    //     $officerId = Arr::get($req_dt, 'officerId');
    //     $complaintId = Arr::get($req_dt, 'complaintId');
    //     $suspectId = Arr::get($req_dt, 'suspectId');
    //     $status = Arr::get($req_dt, 'status');
    //     $current_status = Arr::get($req_dt,'currentStatus','');
    //     $remark = Arr::get($req_dt, 'remark');
    //     $statusList = config('custom.wb.officer.status'); 
    //     $req_dt['previous_status_name_bi'] = $statusList[intval($current_status)]['name_email_bi']; 
    //     $req_dt['previous_status_name_bm'] = $statusList[intval($current_status)]['name_email_bm'];
    //     $req_dt['status_name_bi'] = $statusList[intval($status)]['name_email_bi'];
    //     $req_dt['status_name_bm'] = $statusList[intval($status)]['name_email_bm'];

    //    // $supporting_docs = Arr::get($req_dt, 'supporting_docs');
    //     $suspctIds = explode(",",$suspectId);
    //     $format_suspect = [];
    //     $verify = false;
    //     foreach($suspctIds as $sus){
    //         array_push($format_suspect, intval($sus));
    //     }
    //     $encodeSuspectIds = json_encode($format_suspect);
    //     $WBProfileMst = WBProfileMst::where('ProfileID','=',$profileId)->update([
    //             'Status' => Arr::get($req_dt, 'status', 0),
    //             'UpdateBy'=> Arr::get($req_dt, 'officerId', ''),
    //             'UpdateDate' => $current_datetime
    //     ]);
        
    //     $WBActivityMst = new WBActivityMst();
    //     $activity_dt = [
    //         'remark'=>$remark,
    //         'activity_status'=>$status,
    //         'activity_assignto'=>$officerId,
    //         'activity_upd_by'=>$officerId,
    //     ];
    //     $save_activity_id = $WBActivityMst->create($activity_dt, $complaintId, $profileId, $encodeSuspectIds);
    //     $test = [];
    //     if($request->hasfile('supporting_docs')){
    //         foreach($request->file('supporting_docs') as $file){
    //             $filename = time().rand(1,100).'.'.$file->extension();
    //             $path = 'wbattachments/'.$save_activity_id;
    //             $file->move(public_path($path), $filename);
    //             $attachmentData = [
    //                 'type'   => 2,
    //                 'path'   => $save_activity_id.'/'.$filename,
    //                 'name'   => $filename,
    //                 'applicant_name'=>$applicant_name
    //             ];
    //             array_push($test, $attachmentData);
    //             $WBUploadFile = new WBUploadFile();
    //             $save_upload_file_id = $WBUploadFile->create($attachmentData, $profileId);
    //             $WBUploadActivity = new WBUploadActivity();
    //             $save_upload_activity = $WBUploadActivity->create($attachmentData, $save_upload_file_id ,$save_activity_id);
    //         }
    //     }
    //     $verify = true;
    //     $WBSendEmail = new WhistleBlowerEmailService();
    //     $sendEmail = $WBSendEmail->sendEmailNotiAfterUpdateStatus($req_dt);
     
    //     if($verify){
    //         return response()->json(['data' => 1]);
    //     }else{
    //         return response()->json(['data' => 0]);
    //     }
    // }
    
    // public function thankYou(Request $request){

    //     $encodedCaseId = $request->get('caseId');
    //     $searchUrl = route('searchTrackList');
    //     $searchUrlWithParam = $searchUrl.'?status=all&searchfromemail='.$encodedCaseId;
    //     $caseId = !empty($encodedCaseId) ? urldecode(base64_decode($encodedCaseId)) : '';

    //     return view('thankyou',compact('caseId','searchUrlWithParam'));
    // }

    // public function termsConditionVerify(){
    //     $currentLocale = Session::get('locale'); 
    //     $isVerify = true;
    //     if($currentLocale == config('app.fallback_locale')){
    //         return view('terms.terms-condition',compact('isVerify'));    
    //     }else{
    //         return view('terms.terms-condition-bm',compact('isVerify'));
    //     }
    // }

    // public function termsConditionPage(Request $request){
    //     //dd($request->getLocale());
    //     $currentLocale = Session::get('locale'); 
    //     $isVerify = false;
    //     if($currentLocale == config('app.fallback_locale')){
    //         return view('terms.terms-condition',compact('isVerify'));    
    //     }else{
    //         return view('terms.terms-condition-bm',compact('isVerify'));
    //     }
        
    // }


    // /**
    //  * Testing send email API
    //  */

    //  public function testCheckDisclosureEmail(Request $request){
    //     //dd('check disclosure email');
    //     //processOfficerEmailListToSend($data);
    //     dump('check disclosure');
    //     $allDt = $request->all();
    //     $WBSendEmail = new WhistleBlowerEmailService();
    //     $WBSendEmail->processOfficerEmailListToSendForSubmit($allDt);
    //     //$WBSendEmail->processOfficerEmailListToSendAfterUpdateStatus($allDt);
    //  }

    //  public function getComplaintUpdateView2($profileId){
    //     $info = WBProfileMst::with(['complaint','suspect','uploadFile','activity.uploadActivity'])->where('profileID','=',$profileId)->first();
    //     // $list = WBProfileMst::with(['complaint' => function ($query) {
    //     //     $query->select(['ProfileID','ImproperActivityPlace']);
    //     // },'suspect','uploadFile'])->get();
    //     //dd($info->activity);
    //     $activity = $info->activity;
    //     $activity->load(['user']);
    //     $allDt = new ProfileMstResource($info);
    //     //dd($allDt);
    //     $statusList = config('custom.wb.officer.status');
    //     $countyList = config('location.country'); 
    //     $stateList = config('location.state');
    //     return response()->json(['data' =>$allDt, 'status_list'=>$statusList, 'country_list'=> $countyList, 'state_list'=>$stateList]);
    // }

    // public function testSendEmail(Request $request) {
    //     $data = $request->all();
    //     $WBSendEmail = new WhistleBlowerEmailService();
    //     $getDisclosureCheckInfo = $WBSendEmail->processOfficerEmailListToSendForSubmit($data); 
    //     $email_to_send = Arr::get($getDisclosureCheckInfo,'email_list_to_send',[]);
    //     $disclosure_email =  Arr::get($getDisclosureCheckInfo,'disclosure_email_list',[]);
    //     //$disclosure_email =  Arr::get($getDisclosureCheckInfo,'email_to_send_users',[]);
    //     dump('email to send after submit');
    //     //dump($getDisclosureCheckInfo);
    //     $data['email_to_send'] = $email_to_send;
    //     $WBSendEmail = new WhistleBlowerEmailService();
    //     $WBSendEmail->sendEmailAfterSubmitWB($data);
 
    // }


    // public function testUpdate(Request $request){
     
    //     $req_dt = $request->all();
    //     // $applicant_name = Arr::get($req_dt,'applicantName','Test Applicant');
    //     // $applicant_name = Arr::get($req_dt,'applicantEmail','testapplicant@gmail.com');
    //     // $caseId = Arr::get($req_dt, 'caseId',108);
    //     // $profileId = Arr::get($req_dt, 'profileId',108);
    //     // $officerId = Arr::get($req_dt, 'officerId',8);
    //     // $complaintId = Arr::get($req_dt, 'complaintId',52);
    //     // $suspectId = Arr::get($req_dt, 'suspectId', '');
    //     // $status = Arr::get($req_dt, 'status', 2);
    //     // $remark = Arr::get($req_dt, 'remark', 'Test Update');
    //     $status = Arr::get($req_dt,'status');
    //     $current_status = Arr::get($req_dt,'current_status');
    //     $statusList = config('custom.wb.officer.status'); 
    //     $req_dt['previous_status_name_bi'] = $statusList[intval($current_status)]['name_email_bi']; 
    //     $req_dt['previous_status_name_bm'] = $statusList[intval($current_status)]['name_email_bm'];
    //     $req_dt['status_name_bi'] = $statusList[intval($status)]['name_email_bi'];
    //     $req_dt['status_name_bm'] = $statusList[intval($status)]['name_email_bm'];
    //     dump('test update');
    //     $WBSendEmail = new WhistleBlowerEmailService();
    //     $WBSendEmail->sendEmailNotiAfterUpdateStatus($req_dt);
    // }


    // public function testCronJob(Request $request){
    //     dump('test cron job');
    //     $WBSendEmail = new WhistleBlowerEmailService();
    //     $WBSendEmail->sendReminder();
    // }


   
}
