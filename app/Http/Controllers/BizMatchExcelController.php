<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Exports\CoursesExport;
use App\Exports\InterestExport;
use App\Exports\TPExport;
use App\Exports\EmployerExport;
use Maatwebsite\Excel\Facades\Excel;

class BizMatchExcelController extends Controller 
{
    public function exportExcel() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }


    public function exportCourses(Request $request) 
    {
        $data = $request->all();
        return Excel::download(new CoursesExport($data), 'courses.xlsx',\Maatwebsite\Excel\Excel::XLSX);
        ob_end_clean();
    
    }

    public function exportInterest(Request $request) 
    {
        $data = $request->all();
        return Excel::download(new InterestExport($data), 'interest.xlsx',\Maatwebsite\Excel\Excel::XLSX);
        ob_end_clean();
    }

    public function exportTP(Request $request) 
    {
        $data = $request->all();
        return Excel::download(new TPExport($data), 'tp.xlsx',\Maatwebsite\Excel\Excel::XLSX);
        ob_end_clean();
    }

    public function exportEmployer(Request $request) 
    {
        $data = $request->all();
        return Excel::download(new EmployerExport($data), 'employer.xlsx',\Maatwebsite\Excel\Excel::XLSX);
        ob_end_clean();
    }





}