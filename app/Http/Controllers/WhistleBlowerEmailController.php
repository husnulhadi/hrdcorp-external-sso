<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
// use Pdf;
// use Session;
use Illuminate\Support\Arr;
use App\Models\WBProfileMst;
use App\Models\WBSuspectMst;
use App\Models\WBComplaintMst;
use App\Models\WBActivityMst;
use App\Models\WBUploadActivity;
use App\Models\WBUploadFile;

class WhistleBlowerEmailController extends Controller {

    public function applicantEmailContent(){
        return view('email.emailNotiContentApplicant');
    }


    public function officerEmailContent(){
        return view('email.emailNotiContentOfficer');
    }


    public function updateWBstatusToApplicant(){
        return view('email.emailUpdateStatusNotiContentApplicant');
    }


    public function updateWBStatusToOfficer(){
        return view('email.emailUpdateStatusNotiContentOfficer');
    }


    public function forgetPasswordEmail(){
        return view('email.emailForgotPassword');
    }


    public function reminderEmailForPendingCase(){
        return view('email.emailReminderPendingCase');
    }

    public function testSendEmail(){ 
        //dd('test send mail');
        $test = Mail::to('leeping950116@gmail.com')->send(new SendMail(array(), 'Test Email', 'email.emailNotiContentApplicant'));
        dump($test);
    }


    /**
     * Test send email after submit for public
     */
    public function toApplicantEmailAfterSubmit(Request $request){
        $getLatestCase = WBProfileMst::orderBy('created_at','desc')->first();
        $caseId = Arr::get($getLatestCase,'CaseID');
        $param = urlencode(base64_encode($caseId));
        $complaint = !empty($getLatestCase) ? $getLatestCase->complaint : [];
        $data = [];
        $data['case_id'] = $caseId;
        $data['trace_url'] = env('APP_URL').'/WhistleBlower/public/searchMySubmission?status=all&searchfromemail='.$param;
        $data['receiver_name'] = Arr::get($getLatestCase,'ComName', '');
        $data['complaint_improper_activity_detail'] = Arr::get($complaint,'ImproperActivityDetail', '');
        return view('email.emailNotiContentApplicant',compact('data'));
    }


    public function toOfficerEmailAfterSubmit(){
        $getLatestCase = WBProfileMst::orderBy('created_at','desc')->first();
        $caseId = Arr::get($getLatestCase,'CaseID');
        //$param = urlencode(base64_encode($caseId));
        $complaint = !empty($getLatestCase) ? $getLatestCase->complaint : [];
        $data = [];
        $data['case_id'] = $caseId;
        $data['trace_url'] = env('APP_URL').'/WhistleBlower/public';
        $data['receiver_name'] = config('custom.wb.officer.fallback_email_detail.receiver_name');
        $data['complaint_improper_activity_detail'] = Arr::get($complaint,'ImproperActivityDetail', '');
        return view('email.emailNotiContentOfficer',compact('data'));
    }

    public function toApplicantEmailAfterUpdateStatus(Request $request){
        $getLatestCase = WBProfileMst::orderBy('created_at','desc')->first();
        $caseId = Arr::get($getLatestCase,'CaseID');
        $param = urlencode(base64_encode($caseId));
        $complaint = !empty($getLatestCase) ? $getLatestCase->complaint : [];
        $status = Arr::get($getLatestCase, 'Status','');
        $data['applicantName'] = Arr::get($getLatestCase,'ComName','');
        $data['remark'] = 'Update Status';
        $data['officerEmail'] = config('custom.wb.officer.fallback_email_detail.receiver_email');;
        $data['officerName'] = config('custom.wb.officer.fallback_email_detail.receiver_name');
        //$emaildata = [];
        $data['caseId'] = $caseId;
        $data['trace_url'] = env('APP_URL').'/WhistleBlower/public/searchMySubmission?status=all&searchfromemail='.$param;
        $data['receiver_name'] = Arr::get($getLatestCase,'ComName', '');
        $data['complaint_improper_activity_detail'] = Arr::get($complaint,'ImproperActivityDetail', '');
        $statusList = config('custom.wb.officer.status'); 
        $prev_status = intval($status) > 1 ? intval($status) - 1 : 1;
        $data['previous_status_name_bi'] = $statusList[$prev_status]['name_email_bi'];
        $data['previous_status_name_bm'] = $statusList[$prev_status]['name_email_bm'];
        $data['status_name_bi'] = $statusList[intval($status)]['name_email_bi'];
        $data['status_name_bm'] = $statusList[intval($status)]['name_email_bm'];
        return view('email.emailUpdateStatusNotiContentApplicant',compact('data'));
    }

    public function toOfficerEmailAfterUpdateStatus(Request $request){
        $getLatestCase = WBProfileMst::orderBy('created_at','desc')->first();
        $caseId = Arr::get($getLatestCase,'CaseID');
        //$param = urlencode(base64_encode($caseId));
        $status = Arr::get($getLatestCase, 'Status','');
        $complaint = !empty($getLatestCase) ? $getLatestCase->complaint : [];
        $data = [];
        $data['caseId'] = $caseId;
        $data['applicantName'] = Arr::get($getLatestCase,'ComName','');
        $data['remark'] = 'Update Status';
        $data['officerEmail'] = 'abc@gmail.com';
        $data['officerName'] = config('custom.wb.officer.fallback_email_detail.receiver_name');
        $data['trace_url'] = env('APP_URL').'/WhistleBlower/public';
        $data['receiver_name'] = config('custom.wb.officer.fallback_email_detail.receiver_name');
        $data['complaint_improper_activity_detail'] = Arr::get($complaint,'ImproperActivityDetail', '');
        $statusList = config('custom.wb.officer.status'); 
        $prev_status = intval($status) > 1 ? intval($status) - 1 : 1;
        $data['previous_status_name_bi'] = $statusList[$prev_status]['name_email_bi'];
        $data['previous_status_name_bm'] = $statusList[$prev_status]['name_email_bm'];
        $data['status_name_bi'] = $statusList[intval($status)]['name_email_bi'];
        $data['status_name_bm'] = $statusList[intval($status)]['name_email_bm'];
        return view('email.emailUpdateStatusNotiContentOfficer',compact('data'));
    }


    /**
     * Show the view of email after update status
     */

    public function toEmailViewAfterUpdateStatus(){
        $getLatestCase = WBProfileMst::orderBy('created_at','desc')->first();
        $caseId = Arr::get($getLatestCase,'CaseID');
        $data = [];

    }



    public function toForgetPassword(){
        $data = [];
        $data['receiver_name'] = 'Lee Ping';
        $data['trace_url'] = 'http://localhost/WhistleBlower/public/resetPassword?id=MTU%3D';
        return view('email.emailForgotPassword',compact('data'));
    }


    public function toPendingCaseReminder(){
        $data['receiver_name'] = 'all';
        $data['pending_case'] = [
        'WB169279824513','WB169279807582','WB169279774660',
        'WB169269600649','WB169269561274','WB169269517910',
    ];
        return view('email.emailReminderPendingCase',compact('data'));
    }

}
?>



