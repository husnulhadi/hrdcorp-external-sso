<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Http\Request;
use App\Models\Users;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {   
        if(Session::get('username')){

            $validateUser = Users::where('username','=',Session::get('username'))->first();
            if(!empty($validateUser)){
                return $next($request);
            }else{
                return redirect('/');
            }

            //  return $next($request);
        }else{
            return redirect('/');
        }
        


    }
}
