<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActivityMstResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'activity_id' =>$this->activity_id,
            'remark' => $this->remark,
            'activity_status'=>$this->activity_status,
            'activity_datetime'=>$this->activity_datetime,
            'upload_activity'=>$this->uploadActivity,
            'user'=>!empty($this->user) ? new UserMstResource($this->user) : []
       
        ];
    }
}
