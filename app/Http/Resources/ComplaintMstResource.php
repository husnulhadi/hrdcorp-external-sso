<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ComplaintMstResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'ComplaintID' => $this->ComplaintID,
            'ImproperActivityDate' => $this->ImproperActivityDate,
            'ImproperActivityTime' => $this->ImproperActivityTime,
            'ImproperActivityPlace' => $this->ImproperActivityPlace,
            'ImproperActivityDetail' => $this->ImproperActivityDetail,
            'ImproperActivityCountry' => $this->ImproperActivityCountry,
            'ImproperActivityOtherCountry' => $this->ImproperActivityOtherCountry,
            'ImproperActivityState' => $this->ImproperActivityState,
            'ImproperActivityDistrict' => $this->ImproperActivityDistrict,
            'ImproperActivityPostcode' => $this->ImproperActivityPostcode,
            'q1'=>$this->Q1,
            'q2'=>$this->Q2,
            'q3'=>$this->Q3,
            'q4'=>$this->Q4,
            'q5'=>$this->Q5,
            'q6'=>$this->Q6,
            'q7'=>$this->Q7,
            'q8'=>$this->Q8,
        ];
    }
}
