<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\WBDisclosureEmail;

class ProfileMstResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $emailGroup = [];
        if(!empty($this->DisclosureType)){
            $disclosureTypes = explode(',',$this->DisclosureType);
            if(count($disclosureTypes) > 0){
                $emailGroup = WBDisclosureEmail::whereIn('id',$disclosureTypes)->get();
            }
        }
        
        return [
            'profile_id' => $this->ProfileID,
            'case_id'=> $this->CaseID,
            'pic_officer'=> $this->PICOfficer,
            'status'=>$this->Status,
            'applicant_info'=>$this->applicant_info,
            'disclosure_emails'=>$this->DisclosureEmails,
            'disclosure_types'=>$this->DisclosureType,
            'disclosure_email_group'=>$emailGroup,
            'suspect_info'=> $this->suspect_info,
            'file_info'=>$this->uploadFile,
            'complaint_info'=>!empty($this->complaint) ? new ComplaintMstResource($this->complaint) : [],
            //'activity_info'=>$this->activity,
            'activity_info'=>!empty($this->activity) ? new ActivityMstCollection($this->activity) : [],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
