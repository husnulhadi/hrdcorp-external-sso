<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Employer extends Model
{
    use HasFactory;
    protected $table = 'employer';

    public function create($data) {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        // $this->emp_mycoid = Arr::get($data,'emp_mycoid','');
        // $this->emp_name = Arr::get($data,'emp_name','');
        // $this->emp_email = Arr::get($data,'emp_email','');
        // $this->emp_contact = Arr::get($data,'emp_email','');
        // $this->emp_status = Arr::get($data,'emp_status',0);

        $this->mycoid = Arr::get($data,'emp_mycoid','');
        $this->username = Arr::get($data,'emp_email','');
        $this->poc_name = Arr::get($data,'emp_name','');
        $this->poc_email = Arr::get($data,'emp_email','');
        $this->poc_phone = Arr::get($data,'emp_contact','');
        $this->status = Arr::get($data,'emp_status',0);
        $this->save();

        return $this->id;
    }
}
