<?php

namespace App\Models;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Arr;

class TrainingProvider extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'training_provider';
    protected $fillable = [
        'tp_mycoid',
        'username',
        'confirm_password',
        'tp_type',
        'poc_name',
        'poc_email',
        'poc_phone',
        'location',
        'status',
        'global_api_token'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    // protected $hidden = [
    //     'password',
    //     'remember_token',
    // ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];


    public function create($data) {

        date_default_timezone_set('Asia/Kuala_Lumpur');
        $this->username = Arr::get($data,'tp_email','');
        $this->confirm_password = empty(Arr::get($data,'password','')) ? '' : Hash::make(Arr::get($data,'password',''));
        $this->poc_name = Arr::get($data,'tp_name');
        $this->poc_email = Arr::get($data,'tp_email');
        $this->poc_phone = Arr::get($data,'tp_contact_no');
        $this->location = Arr::get($data,'location','');
        $this->tp_mycoid = Arr::get($data,'tp_mycoid','');
        $this->tp_type = Arr::get($data,'tp_type','non-registered');
        $this->status = 1;
        $this->save();
        return $this->id;
    }
}
