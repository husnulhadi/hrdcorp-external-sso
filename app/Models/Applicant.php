<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Arr;
use Carbon\Carbon;


class Applicant extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'applicants';
    // course_id,applicant_type,mycoid,name,email,profile,company_name
    protected $fillable = [
        'course_id',
        'applicant_type',
        'mycoid',
        'name',
        'email',
        'profile',
        'company_name',
        'no_of_enrollment'
    ];

   /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    // protected $hidden = [
    //     'password',
    //     'remember_token',
    // ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function create($data){
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $this->course_id = Arr::get($data,'id','');
        $this->applicant_type = Arr::get($data,'applicant_type','');
        $this->mycoid = Arr::get($data,'employer_mycoid','');
        $this->name = Arr::get($data,'employer_name','');
        $this->email = Arr::get($data,'employer_email','');
        $this->contact = Arr::get($data,'employer_contact','');
        $this->business_forte = Arr::get($data,'employer_business_forte','');
        $this->no_of_enrollment = Arr::get($data,'employer_no_of_enrollment','');
        $this->current_status = Arr::get($data, 'current_status');
        $save = $this->save();
        return $save;
    }

    public function approve($data){
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $applicant = Applicant::find(Arr::get($data,'id',''));
        $applicant->current_status = Arr::get($data,'current_status','');
        $applicant->created_by = Arr::get($data,'created_by','');
        $applicant->approved_date = Carbon::now()->format('Y-m-d');
        $applicant->approved_flag = 1;
        return $applicant->save();
    }


    public function reject($data){
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $applicant = Applicant::find(Arr::get($data,'id',''));
        $applicant->current_status = Arr::get($data,'current_status','');
        $applicant->created_by = Arr::get($data,'created_by','');
        $applicant->approved_flag = 0;
        return $applicant->save();
    }

}
