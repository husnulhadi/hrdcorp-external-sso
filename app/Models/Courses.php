<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Arr;
use Carbon\Carbon;


class Courses extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'courses';
    // tp_mycoid,training_provider,name,training_start_date,training_end_date,training_hours,training_certification,training_mode,training_type,skill_areas,training_state,training_fee,discounted_training_fee,training_allowance,participant,key_skill_id,summary,content,target_group,training_starter_kit_cost,course_approval_status,query_status,query_description,reject_reason,pic_name,pic_email,pic_phone,status,course_approved_by,
    protected $fillable = [
        'tp_mycoid',
        'training_provider',
        'name',
        'training_start_date',
        'training_end_date',
        'training_hours',
        'training_certification',
        'training_mode','training_type',
        'skill_areas','training_state',
        'training_fee',
        'discounted_training_fee',
        'training_allowance',
        'participant',
        'key_skill_id',
        'summary',
        'content',
        'target_group',
        'training_starter_kit_cost',
        'course_approval_status',
        'query_status',
        'query_description',
        'reject_reason',
        'pic_name',
        'pic_email',
        'pic_phone',
        'status',
        'course_approved_by'
    ];

   /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    // protected $hidden = [
    //     'password',
    //     'remember_token',
    // ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function bulkCreate($data){
        $saveCourse = Courses::insert($data);
        return $saveCourse;
    }


    public function updateCourse($data){
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $courseId = Arr::get($data, 'course_id', '');
        $updateCourse = Courses::find($courseId);
        $updateCourse->name = Arr::get($data,'tp_course_name','');
        $updateCourse->tp_mycoid = Arr::get($data,'course_tp_mycoid','');
        $updateCourse->training_skim = Arr::get($data,'tp_skill_area','');
        $updateCourse->training_certification = Arr::get($data,'tp_course_certification','');
        $updateCourse->training_fee = Arr::get($data,'tp_course_price',0);
        $updateCourse->training_discount_rate = Arr::get($data,'tp_course_discount_rate',0);
        $updateCourse->discounted_training_fee = Arr::get($data,'tp_course_discounted_price_value',0);
        $updateCourse->modified_by = Arr::get($data, 'user_id', '');
        $updateCourse->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $updateCourse->status = Arr::get($data,'status','');
        return $updateCourse->save();
    }


    public function approve($data){
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $courseId = Arr::get($data,'id');
        $status = Arr::get($data, 'status');
        $approvedBy = Arr::get($data, 'approve_by');  
        $courses = Courses::find($courseId);
        $courses->status = $status;
        $courses->course_approved_by = $approvedBy;
        $courses->modified_by = Arr::get($data, 'user_id', '');
        $courses->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $update = $courses->save();
        return $update;
    }


    public function reject($data){
        $courseId = Arr::get($data,'id');
        $status = Arr::get($data, 'status');
        $courses = Courses::find($courseId);
        $courses->status = $status;
        $courses->reject_reason = Arr::get($data,'reason');
        $courses->modified_by = Arr::get($data, 'user_id', '');
        $courses->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $update = $courses->save();
        return $update;
    }


    public function remove($data){
        $courseId = Arr::get($data,'id');
        $status = Arr::get($data, 'status');
        $courses = Courses::find($courseId);
        $courses->status = $status;
        $courses->modified_by = Arr::get($data, 'user_id', '');
        $courses->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $update = $courses->save();
        return $update;
    }



}

