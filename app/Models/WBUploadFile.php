<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class WBUploadFile extends Model
{
    use HasFactory;
    protected $table = 'upload_file';

    public function profile(){
        return $this->belongsTo(WBProfileMst::class, 'ProfileID', 'UploadID');
    }

    public function create($data, $profileID) {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $this->ProfileID = $profileID;
        $this->ComName = Arr::get($data,'applicant_name','');
        $this->FileName = Arr::get($data,'name','');
        $this->FilePath = Arr::get($data,'path','');
        //$this->Content = '';
        $this->Type = Arr::get($data,'type','');
        $this->Date = Carbon::now()->format('Y-m-d');
        $this->save();
        return $this->id;
    }
    
}
