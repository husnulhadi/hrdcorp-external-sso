<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class WBUploadActivity extends Model
{
    use HasFactory;
    protected $table = 'upload_activityfile';

    public function activity(){
        return $this->belongsTo(WBActivityMst::class, 'activity_id', 'activity_id');
    }

    public function create($data, $uploadId, $activityID){
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $this->UploadID = $uploadId;
        $this->activity_id = $activityID;
        $this->FileName = Arr::get($data,'name','');
        $this->Type = Arr::get($data,'type','');
        $this->FilePath = Arr::get($data,'path','');
        //$this->Content = $data['content'];
        $this->Date = Carbon::now()->format('Y-m-d');
        $this->save();
    }

}
