<?php

namespace App\Models;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Users extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function create($data) {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $this->username = Arr::get($data,'username','');
        $this->password = empty(Arr::get($data,'password','')) ? '' : Hash::make(Arr::get($data,'password',''));
        $this->ref_id = Arr::get($data,'refid','');
        $this->role = 1;
        $this->email = Arr::get($data,'email');
        $this->name = Arr::get($data,'fullname');
        $this->type = Arr::get($data,'type',0);
        return $this->save();
    }

    public function resetPassword($data){
        $this->password = empty($data) ? '' : Hash::make($data);
        return $this->save();
    }

}
